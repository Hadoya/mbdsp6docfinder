import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header'
import Banner from './components/Banner'
import {BrowserRouter} from 'react-router-dom';
import Content from './components/Content';
import GrandTitre from './components/GrandTitre';

function App() {
  return (
    <div className="App">
       <BrowserRouter>
         <Header/>
         <Content/>
         <br/>
      </BrowserRouter>
       
    </div>
  );
}

export default App;
