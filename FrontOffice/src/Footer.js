import React,{Component} from 'react'
import Grid from '@material-ui/core/Grid';
import {Link} from 'react-router-dom'
export default class Footer extends Component {
    render(){
    return(
        <div>
            <br/>
        <footer class="footer-box">
          <div class="container">
              <div class="row">
                  <div class="col-md-12 margin-bottom_30">
             <img src="assets/img/DocFinder3.png" alt="#" width="150px"/>
          </div>
                 <div class="col-xl-6 white_fonts">
                      <div class="row">
            <div class="col-md-12 white_fonts margin-bottom_30">
               <h3>Nous contacter</h3>
            </div>
                          <div class="col-md-4">
                              <div class="full icon">
                                  <img src="assets/images/social1.png"/>
                              </div>
                              <div class="full white_fonts">
                                  <p>Antananarivo
                                      <br/>Madagascar</p>
                              </div>
                          </div>
                          <div class="col-md-5">
                              <div class="full icon">
                                  <img src="assets/images/social2.png"/>
                              </div>
                              <div class="full white_fonts">
                                  <p>hadoya@gmail.com
                                      <br/>docFinder@gmail.com</p>
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="full icon">
                                  <img src="assets/images/social3.png"/>
                              </div>
                              <div class="full white_fonts">
                                  <p>+2616632147
                                      <br/>+2616632148</p>
                              </div>
                          </div>
              <div class="col-md-12">
                 <ul class="full social_icon margin-top_20">
                                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                              </ul>
              </div>
                      </div>
                  </div>
          
           <div class="col-xl-6 white_fonts">
              <div class="row">
               <div class="col-md-6">
                 <div class="footer_blog footer_menu">
                  <h3>Menus</h3>
                  <ul> 
                    <li><Link className="nav-link active" to="/accueil"  >Accueil</Link></li>
                    <li><Link className="nav-link" to="/saviezVous">Saviez-vous</Link></li>
                    <li><Link className="nav-link" to="/diagnostic2">Diagnostic</Link></li>
                    <li><Link className="nav-link" to="/corpsMedicaux">Médecins</Link></li>
                    <li><Link className="nav-link active" style={{"background":"#f2184f","color":"#fff"}} to="/inscription">Inscription</Link></li>
                    <li><Link className="nav-link" to="/contact">Contact </Link></li>
                </ul>
               </div>
               <div class="footer_blog recent_post_footer">
                 <h3>DocFinder</h3>
                 <p>Une version mobile de cette application est aussi disponible</p>
               </div>
               </div>
               <div class="col-md-6">
                 <div class="footer_blog full">
                   <h3>Appréciation</h3>
                 <div class="newsletter_form">
                   
                     <input type="text" placeholder="Vous avez aimez ce site?" name="#" required />
                     <button>Valider</button>
                 
                 </div>
               </div>
               </div>
            </div>
           </div>
    
              </div>
              
          </div>
      </footer>
      <div class="footer_bottom">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <p class="crp">© 2020 HaDoYa . All Rights Reserved.</p>
                  <ul class="bottom_menu">
                      <li><a href="#">Equipe DocFinder</a></li>
                      <li><a href="#">T&eacute;l&eacute;charger apk</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
   </div>
    );      
}
}
