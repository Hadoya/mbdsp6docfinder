import React,{Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import GrandTitre from './GrandTitre'
import { Alert, AlertTitle } from '@material-ui/lab';
import AndroidIcon from '@material-ui/icons/Android';
import Galery from './Galery';
import axios from 'axios';
import {Link, Redirect} from 'react-router-dom'
import FaceIcon from '@material-ui/icons/Face';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export default class  DetailMedecin extends Component {
  constructor(props){
    super(props)
     this.state={
       firstname:'',
       lastname:'',
       telephone:'',
       address:'',
       town:'',
       town:'',
       city:'',
       description:'',
       tarif:0,
      
    }
}
componentDidMount(){
  axios.get('http://localhost:8080/doctor/'+this.props.match.params.id)
  .then(response=>{
    console.log('ezzzbgrfg',response)
    this.setState({
      firstname:response.data.firstname,
      telephone:response.data.telephone,
      address:response.data.address,
      town:response.data.town,
      city:response.data.city,
      description:response.data.description,
      tarif:response.data.tarif,
      lastname:response.data.lastname,
    
    })
   
  })
  .catch(error=>{
    console.log('ezzzbgrfg',error)
  })
}

render(){
  const {firstname,lastname,telephone,address,town,city,description,tarif}=this.state
  return (
    <div>  <br/>
      <GrandTitre name="Corps Medicaux"/>
      <div className="section layout_padding">
         <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="full">
                        <div className="heading_main text_align_left">
                          <div className="left">
                            <p className="section_count"></p>
                          </div>
                          <div className="right">
                              <h2><span className="theme_color">{`Dr ${firstname} ${lastname} `}</span> </h2>
                           </div>	
                        </div>
                    </div>
                </div>
            </div>
         </div>
        </div>
    <Grid container spacing={2}>
        
        <Grid item xs={2}>
         
        </Grid>
        <Grid item xs={4}>
         <Paper >
         <Typography  variant="h5">
            Fiche de détail:
         </Typography>
              <table>
                <tr>
                   <td> &nbsp;&nbsp;</td>
                   <td><Avatar style={{"width":"100px","height":"100px",textAlign:'center',backgroundColor:'#f5b24e'}}alt="Remy Sharp"><FaceIcon fontSize="large"></FaceIcon></Avatar></td>
                   <td>
                   <div >
                      <Typography  style={{textAlign:'left',color:'#363434'}} variant="subtitle1">
                         <label>&nbsp;&nbsp;Nom:</label> <span style={{color:'#4d4a4a'}}>{firstname}</span><br/>
                         <label>&nbsp;&nbsp;Prénom(s):</label><span style={{color:'#4d4a4a'}}>{lastname}</span><br/>
                         <label>&nbsp;&nbsp;Description:</label> <span style={{color:'#4d4a4a'}}>{description}</span><br/>
                         <label>&nbsp;&nbsp;Lieu:</label> <span style={{color:'#4d4a4a'}}>{city}&nbsp;{town}</span><br/>
                         <label>&nbsp;&nbsp;Tél:</label> <span style={{color:'#4d4a4a'}}>{telephone}</span><br/>
                         <label>&nbsp;&nbsp;Adresse:</label> <span style={{color:'#4d4a4a'}}>{address}</span><br/>
                        </Typography>
                        
                        </div>
                   </td>
                </tr>
              </table>
              
      
       
  
        </Paper>
        </Grid>
        
        <Grid item xs={3}></Grid>
    </Grid>

    </div>
  );
}
}