import React,{Component,useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import GrandTitre from './GrandTitre';
import Axios from 'axios'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';



export default class Inscription extends Component{
  constructor(props) {
    super(props)
    this.onChangeSexe=this.onChangeSexe.bind(this)
    this.onChangebirthdate=this.onChangebirthdate.bind(this)
    this.onChangeLastname=this.onChangeLastname.bind(this)
    this.onChangeFirstname=this.onChangeFirstname.bind(this)
    this.onChangeEmail=this.onChangeEmail.bind(this)
    this.onChangePassword=this.onChangePassword.bind(this)
    this.onSubmit=this.onSubmit.bind(this)

		this.state={
        lastname:'',
        firstname:'',
        email:'',
        password:'',
        birthdate:'',
        sexe:''
      
      };
    
    }
    onChangebirthdate(e){
      this.setState({
        birthdate:e.target.value
      });
    }
    onChangeSexe(e){
      this.setState({
        sexe:e.target.value
      });
    }
    onChangeLastname(e){
      this.setState({
        lastname:e.target.value
      });
    }
    onChangeFirstname(e){
      this.setState({
        firstname:e.target.value
      });
      window.sessionStorage.setItem("sessionfirstname",e.target.value );
    }
    

    onChangeEmail(e){
      this.setState({
        email:e.target.value
      });
    }
    onChangePassword(e){
      this.setState({
        password:e.target.value
      });
    }
    onSubmit(e){
      e.preventDefault();
      const patient={
        lastname:this.state.lastname,
        firstname:this.state.firstname,
        email:this.state.email,
        password:this.state.password,
        gender:this.state.sexe,
        telephone:this.state.telephone,
        birthdate:this.state.birthdate,
        estPatient:1
        
      }
      console.log(patient)
      Axios.post('http://localhost:8080/patient/add',patient)
      .then(res=>console.log(res.data));
      alert('Votre compte a été crée avec succès!')
       window.location='/';
    }
   
  render(){
    
  return (
    <div >
       <br/>
	   <GrandTitre name="inscription"/>
    <Container component="main" maxWidth="xs">
    
    
      <CssBaseline />
      <br/>
      <br/>
      <div >
        
        <Avatar >
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Inscription en tant que patient
        </Typography>
        <form onSubmit={this.onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstname"
                variant="outlined"
                required
                fullWidth
                id="firstname"
                label="Nom "
                value={this.state.firstname}
                onChange={this.onChangeFirstname}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastname"
                label="Pr&eacute;nom(s)"
                name="lastname"
                value={this.state.lastname}
                onChange={this.onChangeLastname}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Adresse Email"
                name="email"
                autoComplete="email"
                value={this.state.email}
                onChange={this.onChangeEmail}
              />
            </Grid>
            <Grid item xs={12}>
              <label>Date de naissance</label>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="date"
                type="date"
                name="date"
                autoComplete="birthdate"
                value={this.state.birthdate}
                onChange={this.onChangebirthdate}
              />
            </Grid>
            <Grid item xs={12}>
            <RadioGroup required row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeSexe}>
          <FormControlLabel
            value="m"
            control={<Radio color="primary" />}
            label="Masculin"
            labelPlacement="start"
            
          />
          <FormControlLabel 
            value="w"
            control={<Radio color="primary" />}
            label="Féminin"
            labelPlacement="start"
          />
         </RadioGroup>
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={this.state.password}
                onChange={this.onChangePassword}
              />
            </Grid>
         
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
           
          >
            S'inscrire
          </Button>
         
        </form>
      </div>
      <Box mt={5}>
        
      </Box>
    </Container>
    </div>
  );
}
}