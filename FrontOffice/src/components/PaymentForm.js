import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export default function PaymentForm() {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Précisez la région où se trouve la douleur
      </Typography>
      
        <Grid item xs={12} md={6}>
        <img src="assets/img/corps.jpg" width="250px"/>
        </Grid>
        <Grid container spacing={3}>
      
        <Grid item xs={12} >
          <TextField
            required
            id="douleur"
            label="Précisez ici partie du corps où se trouve la douleur..."
            fullWidth
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}