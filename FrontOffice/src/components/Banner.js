import React,{Component} from 'react'
import AndroidIcon from '@material-ui/icons/Android';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Link, Redirect} from 'react-router-dom' 
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import Axios from 'axios';

class Banner extends Component {
  constructor(props) {
    super(props);
    this.onChangeEmail=this.onChangeEmail.bind(this)
    this.onChangePassword=this.onChangePassword.bind(this)
    this.onSubmit=this.onSubmit.bind(this)
    this.handleClickOpen=this.handleClickOpen.bind(this)
    this.state={
     open:false,
    email:'',
     password:'',
     onePatient:[],
     notconnected:false,
    }

        
 }
 handleClickOpen = () => {
   if(window.sessionStorage.getItem("sessionfirstname")!=null && window.sessionStorage.getItem("estPatient")==0  && window.sessionStorage.getItem("sessionfirstname")!='undefined'){
     alert('télechargement de apk de doctor')
   }
   else if(window.sessionStorage.getItem("sessionfirstname")!=null && window.sessionStorage.getItem("estPatient")==1  && window.sessionStorage.getItem("sessionfirstname")!='undefined'){
    alert('télechargement de apk de patient')
  }
   else{
    this.setState({
      open:true
  });
   }
  
};

 handleClose = () => {
  this.setState({
      open:false
  });
};
onChangeEmail(e){
  this.setState({
    email:e.target.value
  });
}
onChangePassword(e){
  this.setState({
    password:e.target.value
  });
}
onSubmit(e){
  e.preventDefault();
        const patient={
          email:this.state.email,
          password:this.state.password,
        }
        Axios.post('https://docfinder-backend.herokuapp.com/patient/login',patient)
        .then(response=>{
            if(response.data.estPatient==1){
                this.setState({onePatient:response.data,open:false})
                window.sessionStorage.setItem("sessionfirstname",this.state.onePatient.firstname)
                window.sessionStorage.setItem("estPatient","1")
                window.sessionStorage.setItem("idPatient",response.data._id)
               console.log(this.state.onePatient.firstname)
               window.location="/"
            }
             if(response.data.estPatient==0){
              //Find from table doctor where idDoctor=_id
                 Axios.post('https://docfinder-backend.herokuapp.com/doctor/findByIdpatient',{idPatient:response.data._id})
                 .then(res=>{
                   //alert("etatvalidation="+res.data[0].etatValidation)
                     //If valider ->effectuer l'operation
                     if(res.data[0].etatValidation==1){
                      this.setState({onePatient:res.data,open:false})
                      window.sessionStorage.setItem("sessionfirstname",res.data[0].firstname)
                      window.sessionStorage.setItem("estPatient","0")
                      window.sessionStorage.setItem("idPatient",res.data._id)
                     // console.log(this.state.onePatient.firstname)
                      window.location="/"
                     }
                        //sinon ->message d'erreur (en attente de validation)
                     else{
                      this.setState({open:false})
                        alert('Votre compte n\'est pas encore validé par l\'admin. Veuillez attrendre la validation!')
                     }
                 })
            }
           else{this.setState({notconnected:true,open:true,error:'Email inexistant ou mot de passe invalide. Veuillez réessayer!'}) }
            })
          .catch()
}
componentDidMount(){
  console.log("estPatient="+window.sessionStorage.getItem("estPatient"))
}
handleClick = () => {
  window.sessionStorage.removeItem("sessionfirstname")
  window.location="/"
 
};
   
    render() {
      let estPatient;
    
      if(window.sessionStorage.getItem("estPatient")=='1'){
        estPatient=  <Button variant="contained" color="secondary" onClick={this.handleClickOpen}><AndroidIcon></AndroidIcon>&nbsp;T&eacute;l&eacute;charger Appli patient</Button>
      }
       else if(window.sessionStorage.getItem("estPatient")=='0'){
        estPatient=  <Button variant="contained" color="secondary" onClick={this.handleClickOpen}><AndroidIcon></AndroidIcon>&nbsp;T&eacute;l&eacute;charger Appli doctor</Button>
      }
      else{
        estPatient=  <Button variant="contained" color="secondary" onClick={this.handleClickOpen}><AndroidIcon></AndroidIcon>&nbsp;T&eacute;l&eacute;charger</Button>
      }
    return(
        <div className="ulockd-home-slider">
        <div className="container-fluid">
            <div className="row">
                <div className="pogoSlider" id="js-main-slider">
                    <div className="pogoSlider-slide" style={{"backgroundImage":"url(assets/img/banner-img.png)"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="slide_text">
                                        <h3>Bienvenue dans DocFinder</h3>
                                        <br/>
                                        <h4><span className="theme_color">Votre santé, c'est notre priorité !</span></h4>
                                        <p>La santé est une richesse inconnue pour celui qui la possède</p>
                                        <br/>
                                     {estPatient}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pogoSlider-slide" style={{"backgroundImage":"url(assets/img/fond2.jpg)"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="slide_text">
                                        <h3>DocFinder<br/></h3>
                                        <br/>
                                        <h4><span className="theme_color"></span>Votre santé, c'est notre souci !</h4>
                                        <p>Qui n'a santé n'a rien, mais par elle on a tout.</p>
                                        <br/>
                                        {estPatient}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Connectez</DialogTitle>
        <form onSubmit={this.onSubmit}>
        <DialogContent>
          <DialogContentText>
            Pour t&eacute;l&eacute;charger cette application mobile, veuillez remplir les champs suivants avant de continuez
          </DialogContentText>
          
          <TextField
             margin="dense"
            id="name"
            label="Votre adresse Email"
            type="email"
            fullWidth
            value={this.state.email}
            onChange={this.onChangeEmail}
          />
           <TextField
            margin="dense"
            id="password"
            label="Votre mot de passe"
            type="password"
            fullWidth
            value={this.state.password}
            onChange={this.onChangePassword}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            Annuler
          </Button>
          <Button  type="submit" color="primary">
            Connectez
          </Button>
        
        </DialogActions>
        </form>
        <Grid container >
            <Grid item>
              <Link to="/inscription" className="nav-link">
               Vous n'avez pas un compte? Inscrire
              </Link>
            </Grid>
          </Grid>
          <br/>
      </Dialog>
    
    </div>
    )      
}
}
export default Banner