import React ,{Component}from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import GrandTitre from './GrandTitre';
import Galery from './Galery';
import emailjs from 'emailjs-com';
 class Contact extends Component{

render(){
  return (
    <div> <br/>
   <GrandTitre name="CONTACT"/>
    <div className="section layout_padding">
         
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="full">
                        <div className="heading_main text_align_left">
						   <div className="left">
						     <p className="section_count">01</p>
						   </div>
						   <div className="right">
						    <p className="small_tag">Pour plus d'informations</p>
                            <h2><span className="theme_color">Nous Contacter</span> </h2>
                          </div>	
                        </div>
                    </div>
                </div>
            </div>	
			<div className="row margin-top_30">
				
				<div className="col-lg-7 col-sm-7 col-xs-12 margin-top_30">
				  <div className="contact-block">
				  
					<form id="contactForm">
					  <div className="row">
						<div className="col-md-6">
							<div className="form-group">
								<input type="text" className="form-control" style={{"backgroundColor":"white","border":"1px solid gray"}}id="name" name="name" placeholder="Votre nom" required data-error="Please enter your name"/>
								<div className="help-block with-errors"></div>
							</div>                                 
						</div>
						<div className="col-md-6">
							<div className="form-group">
								<input type="text" placeholder="Votre adresse Email" id="email" className="form-control" style={{"backgroundColor":"white","border":"1px solid gray"}} name="name" required data-error="Please enter your email"/>
								<div className="help-block with-errors"></div>
							</div> 
						</div>
						<div className="col-md-12">
							<div className="form-group">
								<input type="text" placeholder="Votre numéro téléphonique" id="number" className="form-control" style={{"backgroundColor":"white","border":"1px solid gray"}} name="number" required data-error="Please enter your number"/>
								<div className="help-block with-errors"></div>
							</div> 
						</div>
						<div className="col-md-12">
							<div className="form-group"> 
								<textarea className="form-control" style={{"backgroundColor":"white","border":"1px solid gray"}} id="message" placeholder="Votre message" rows="8" data-error="Write your message" required></textarea>
								<div className="help-block with-errors"></div>
							</div>
							<div className="submit-button text-center">
								<button className="btn btn-common" id="submit" type="submit">Envoyer </button>
								<div id="msgSubmit" className="h3 text-center hidden"></div> 
								<div className="clearfix"></div> 
							</div>
						</div>
					  </div>            
					</form>
				  </div>
				</div>


				<div className="col-lg-5 col-sm-5 col-xs-12 margin-top_30">
					<div className="left-contact">
						<div className="media cont-line">
							<div className="media-left icon-b">
								<i className="fa fa-location-arrow" aria-hidden="true"></i>
							</div>
							<div className="media-body dit-right">
								<h4>Adresse</h4>
								<p>Antananarivo Madagascar</p>
							</div>
						</div>
						<div className="media cont-line">
							<div className="media-left icon-b">
								<i className="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div className="media-body dit-right">
								<h4>Email</h4>
								<a href="#">Hadoya@gmail.com</a>
								
							</div>
						</div>
						<div className="media cont-line">
							<div className="media-left icon-b">
								<i className="fa fa-volume-control-phone" aria-hidden="true"></i>
							</div>
							<div className="media-body dit-right">
								<h4>Tél</h4>
								<a href="#">+261 55 478 99</a><br/>
								<a href="#">+261 75 895 23</a>
							</div>
						</div>
					</div>
				</div>


			</div>
        </div>
    </div>
	<Galery/>
    </div>
  );
}
}
export default  Contact