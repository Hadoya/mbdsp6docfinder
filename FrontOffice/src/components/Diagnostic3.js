import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import TitleParagraph from './TitleParagraph'
import { render } from '@testing-library/react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import GrandTitre from './GrandTitre';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Slider from '@material-ui/core/Slider';



export default class Diagnostic3 extends Component {
    
  

  constructor(props) {
    super(props)
  
    this.onChangeBrulure=this.onChangeBrulure.bind(this)
    this.onChangeIngestion=this.onChangeIngestion.bind(this)
    this.onChangePerteVaginale=this.onChangePerteVaginale.bind(this)
    this.onChangeTraumatisme=this.onChangeTraumatisme.bind(this)
    this.onChangeUrinerouge=this.onChangeUrinerouge.bind(this)
    this.onChangeVoyage=this.onChangeVoyage.bind(this)
    this.onChangeDiabete=this.onChangeDiabete.bind(this)
    this.onChangeAccouchementrecent=this.onChangeAccouchementrecent.bind(this)
    this.onChangeCalculvesicule=this.onChangeCalculvesicule.bind(this)
    this.onChangeSliderventre=this.onChangeSliderventre.bind(this)
   
    this.onSubmit=this.onSubmit.bind(this)

		this.state={
        brulure:'',
        ingestion:'',
        pertevaginale:'',
        traumatisme:'',
        urinerouge:'',
        voyage:'',
        diabete:'',
        accouchementrecent:'',
        calculvesicule:'', 
        sliderventre:'',
      
      };
    
    }
    
    onChangeSliderventre= (event, newValue) => {
        this.setState({
            sliderventre:newValue
        });
      }
    onChangeBrulure(e){
      this.setState({
        brulure:e.target.value
      });
    }
    onChangeIngestion(e){
      this.setState({
        ingestion:e.target.value
      });
    }
    onChangePerteVaginale(e){
      this.setState({
        pertevaginale:e.target.value
      });
    }
    onChangeTraumatisme(e){
      this.setState({
        traumatisme:e.target.value
      });
    }
    onChangeUrinerouge(e){
        this.setState({
            urinerouge:e.target.value
          });
    }
    onChangeVoyage(e){
      this.setState({
        voyage:e.target.value
      });
    }
    onChangeDiabete(e){
        this.setState({
          diabete:e.target.value
        });
      }
      onChangeAccouchementrecent(e){
        this.setState({
          accouchementrecent:e.target.value
        });
    }
    onChangeCalculvesicule(e){
        this.setState({
          calculvesicule:e.target.value
        });
    }
    onSubmit(e){
      e.preventDefault();
      window.localStorage.setItem("brulure",this.state.brulure)
      window.localStorage.setItem("ingestion",this.state.ingestion)
      window.localStorage.setItem("pertevaginale",this.state.pertevaginale)
      window.localStorage.setItem("traumatisme",this.state.traumatisme)
      window.localStorage.setItem("urinerouge",this.state.urinerouge)
      window.localStorage.setItem("voyage",this.state.voyage)
      window.localStorage.setItem("diabete",this.state.diabete)
      window.localStorage.setItem("accouchementrecent",this.state.accouchementrecent)
      window.localStorage.setItem("calculvesicule",this.state.calculvesicule)
      window.localStorage.setItem("sliderventre",this.state.sliderventre)

      console.log('brulure=',window.localStorage.getItem("brulure"))
      console.log('ingestion=',window.localStorage.getItem("ingestion"))
      console.log('pertevaginale=',window.localStorage.getItem("pertevaginale"))
      console.log('traumatisme=',window.localStorage.getItem("traumatisme"))
      console.log('urinerouge=',window.localStorage.getItem("urinerouge"))
      console.log('voyage=',window.localStorage.getItem("voyage"))
      console.log('diabete=',window.localStorage.getItem("diabete"))
      console.log('accouchementrecent=',window.localStorage.getItem("accouchementrecent"))
      console.log('calculvesicule=',window.localStorage.getItem("calculvesicule"))
      console.log('sliderventre=',window.localStorage.getItem("sliderventre"))
      this.props.history.push('/findiagnostic')
    }
    
render(){
   

    
  return (
    <div>
      <br/> 
      <GrandTitre name="Diagnostic"/>
      <br/>
      <Grid container spacing={2}>
        
        <Grid item xs={3}>
          
        </Grid>
        <Grid item xs={6}>
          
          
          <TitleParagraph
                sectionCount="02" 
                smallTag=""
                    mainTitle="AUTRE SYMPTOMES" 
                    addtitle=""
                    subtitle="La santé et la raison sont les vrais trésors de l'homme "
            />
          <br/>  <br/>  <br/>    
          <Paper className={this.useStyles}>
          <Typography variant="subtitle1" ><strong style={{"color":"#9c0546"}}>Remplir les champs suivants pour bien preéciser votre douleur</strong></Typography><hr/>
      <form style={{"paddingLeft":"50px","paddingRight":"50px"}}onSubmit={this.onSubmit}>
      <Grid container xs={12}>
      <List  component="nav"  aria-label="contacts">
      <ListItem >
            
            <ListItemText style={{"fontFamily":"Arial","fontSize":"13px"}}> 
                 Brûlure en urinant
                <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeBrulure}>
                  <FormControlLabel style={{"fontSize":"13px"}}
                    value="oui"
                    control={<Radio color="primary" />}
                    label="oui"
                    labelPlacement="start"
                  />
                  <FormControlLabel style={{"fontSize":"13px"}}
                    value="non"
                    control={<Radio color="primary" />}
                    label="non"
                    labelPlacement="start"
                  />
                  <FormControlLabel style={{"fontSize":"13px"}}
                    value="nesaitpas"
                    control={<Radio color="primary" />}
                    label="ne sait pas"
                    labelPlacement="start"
                  />
                </RadioGroup>
          </ListItemText>
      </ListItem>
      <ListItem >
        <ListItemText> 
                    Ingestion accidentelle de produit ménager
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeIngestion}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
       
        <ListItem>
        
              <ListItemText>
                  <Typography id="discrete-slider" gutterBottom>
                  Mal au ventre
                  </Typography>
                  <Typography variant="subtitle2" >
                   Echelle visuelle de la douleur(0=absence de la douleur/ 10=douleur insupportable)
                  </Typography>
                  <Slider onChange={this.onChangeSliderventre}
                    defaultValue={0}
                    value={this.state.sliderventre}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={1}
                    marks
                    min={0}
                    max={10}
                  />
                
              </ListItemText>
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Perte vaginales anormales
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangePerteVaginale}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Traumatisme ou plale récente
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeTraumatisme}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Urine rouge
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeUrinerouge}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <Typography variant="subtitle2" style={{"color":"crimson"}} >
          ANTECEDENTS, HABITUDES, MODE DE VIE,...
          <hr/>
        </Typography>

        <ListItem >
        <ListItemText> 
                    Voyage récent à l'étranger
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeVoyage}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                   Antécedent de diabète insuline-dépendant
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeDiabete}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Accouchement récent
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeAccouchementrecent}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Antécedent de calculs de la vésicule
                    <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeCalculvesicule}>
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <br/> 
           <Button type="submit" variant="contained" color="secondary">
        Continuer
      </Button>
    </List>
      </Grid>
   
       </form>
        <br/> 
    
        </Paper >
  
        
        </Grid>
        <Grid item xs={3}>
         
        </Grid>
      </Grid>
      <br/> 
      <br/> 
      <br/> 
      <br/> 
      
    </div>
  );
}
}