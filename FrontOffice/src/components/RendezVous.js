import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TitleParagraph from './TitleParagraph'
import { render } from '@testing-library/react';
import GrandTitre from './GrandTitre';
import {Link, Redirect} from 'react-router-dom' 
import axios from 'axios';
import Moment from 'moment';
import 'moment/locale/fr';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import DateRangeIcon from '@material-ui/icons/DateRange';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import CancelIcon from '@material-ui/icons/Cancel';

export default class RendezVous extends Component {
  

  constructor(props) {
    super(props)
    this.estValide=this.estValide.bind(this)
   
    this.state={
        idpatient:window.sessionStorage.getItem("idPatient"),
        appointmentDate:'',
        lastNamePatient: '',
        firstNamePatient: '',
        createdAt: '',
        validation:'',
        idDoctor:'',
        appointments:[],
        docName:'',
        docPrenom:'',
        descriptionDoc:'',
        message:''
      };
    
    }
    estValide(nombre){
        if(nombre=="1"){
            this.setState({validation:'Votre demande a été validée'})
          }
         if(nombre=="0"){
            this.setState({validation:'En attente de validation'})
          }
         if(nombre=="-1"){
            this.setState({validation:'Votre demande a été réfusée'})
          }
     }
     componentDidMount(){
      
        axios.get('http://localhost:8080/appointment/searchByIdpatient/'+this.state.idpatient)
        .then(response=>{
          if(response.data.length>0){
            console.log('ezzzbgrfg',response)
            this.setState({
              appointmentDate: response.data.appointmentDate,
              lastNamePatient: response.data.lastNamePatient,
              firstNamePatient:response.data.firstNamePatient,
              createdAt: response.data.createdAt,
              idDoctor: response.data.idDoctor,
              appointments:response.data,
              validation:response.data.validation,
              message:''
            
            })
          }
          else{
            this.setState({
        
              message:'Pas de demande de rendez-vous'
            
            })
          }
        
       
         
         
        })
        .catch(error=>{
            this.setState({validation:'Pas de demande'})
          console.log('ezzzbgrfg',error)
        })
     }
    detailRDV(id){
        axios.get('http://localhost:8080/doctor/'+id)
        .then(res=>{
            console.log(res.data)
            this.setState({
                docName: res.data.firstname,
                docPrenom: res.data.lastname,
                descriptionDoc: res.data.description
            })
        })
    }
  
render(){
  
  return (
    <div>
      <br/> 
      <GrandTitre name="Ma fiche de rendez-vous"/>
      <br/>
      <div className="row">
        
        <div className="col-md-2">
          
        </div>
        <div className="col-md-6">
           
          <Paper >
          <React.Fragment >
          <Typography variant="h5">Résultat(s) de mes/ma demande(s) de Rendez-vous:</Typography>
                <hr/>
                <h4>{this.state.message}</h4>
                <table>
                   
                { this.state.appointments.map(app=>
              
                 <div>
                    <tr>
                        <td>   <Typography variant="subtitle1" style={{"color":"#2b2a2a","textAlign":"justify","paddingLeft":"20px","paddingRight":"20px"}}>
                    <DateRangeIcon/>&nbsp;Le&nbsp;
                    {Moment(app.appointmentDate).locale("de").format('dddd Do MMMM YYYY')}&nbsp;
                    à&nbsp;
                    {Moment(app.appointmentDate).format('hh:mm')}<br/>
                    
                 </Typography></td>
                 <td>&nbsp;&nbsp;</td>
                        <td>
                    {(() => {
                         
                        if (app.validation===parseInt("0")) {
                        return (
                        <Chip
                            icon={<HourglassEmptyIcon   style={{color:'white'}} />}
                            label="En attente de validation..."
                            clickable
                            style={{backgroundColor:'#b54414',color:'white'}}
                            deleteIcon={<DoneIcon />}
                          />
                        )
                        } else if (app.validation===parseInt("1")) {
                        return (
                            <Chip
                            icon={<DoneOutlineIcon   style={{color:'white'}} />}
                            label="Demande validée"
                            clickable
                            style={{backgroundColor:'#6cab24',color:'white'}}
                            deleteIcon={<DoneIcon />}
                          />
                        )
                        } else if (app.validation===parseInt("-1")) {
                            return (
                  
                                <Chip
                            icon={<CancelIcon   style={{color:'white'}} />}
                            label="Demande réfusée"
                            clickable
                            style={{backgroundColor:'#bd2a2a',color:'white'}}
                            deleteIcon={<DoneIcon />}
                          />
                            )
                        } else {
                        return (
                            <div>Pas de demande</div>
                        )
                        }
                    })()}</td>
                    <td>&nbsp;<Link style={{color:'#1449db',fontSize:13,fontFamily:'Arial'}} onClick={()=>this.detailRDV(app.idDoctor)}>Plus de détail</Link></td>
                    </tr>
                    <hr/>
                    </div>
              
                )}
                    </table>
                <br/>    
                <a href="/" button>
                <Button variant="contained" color="default">
                     Revenir vers la page d'accueil
                </Button>
                </a>
                <br/>    
              </React.Fragment>
        <br/> 
    
        </Paper >
  
        
        </div>
        <div className="col-md-3">
            <Paper style={{backgroundColor:'#cfd2d4'}}>
                <br/>
            <Typography variant="h6" >
                Plus de détail:
                <Avatar alt="Remy Sharp"  style={{width:100,height:100,backgroundColor:'#6d6f70',marginLeft:'35%'}}><PermIdentityIcon style={{width:80,height:80}} fontSize="large" /></Avatar>
            </Typography>
            <table>
                <tr>
                    <td>&nbsp;</td>
                    <td>  <Typography variant="subtitle2" style={{"color":"#2b2a2a","textAlign":"left","paddingLeft":"20px","paddingRight":"20px"}}>
                        <h4>Dr. {this.state.docName}</h4>
                        Le médecin pour cette demande de Rendez-Vous est {this.state.docName}.<br/>
                        <hr/>
                        <b>Nom:</b>&nbsp;{this.state.docName}&nbsp;{this.state.docPrenom}<br/>
                        <b>Description:</b>&nbsp;{this.state.descriptionDoc}
                        </Typography>
                        <br/><br/>
                    </td>
                </tr>

            </table>
          
          
          </Paper>
        </div>
      </div>
      <br/> 
      <br/> 
      <br/> 
      <br/> 
      
    </div>
  );
}
}