import React from 'react'
import AndroidIcon from '@material-ui/icons/Android';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Link} from 'react-router-dom' 
import Grid from '@material-ui/core/Grid';
function Galery(){
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
  
    return(
        <div className="section layout_padding">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="full">
                        <div className="heading_main text_align_left">
						   <div className="left">
						     <p className="section_count">02</p>
						   </div>
						   <div className="right">
						   
                            <h2><span className="theme_color">A PROPOS de DOCFINDER</span> </h2>
							<p className="large"></p>
                          </div>	
                        </div>
                    </div>
                </div>
            </div>
			<div className="row margin-top_30">
                <div className="col-lg-12">
                    <p style={{"textAlign":"justify"}}>Sur ce site DocFinder, vous pouvez désormais connâitre facilement la cause de votre douleur et aussi ce qu'il faut faire pour le premier soin
                         avant d'aller chez le médecin, en saisisant juste vos symptômes.
                       
                        Après, nous suggestions les médecins autour de vous selon votre lieu, qui est le responsable de ce maladie.
                        Pour connaitre la cause de votre douleur, cliquez le bouton Diagnostiquer. Vous pouvez aussi télécharger la version mobile de l'application sur l'autre bouton.</p>
                </div>

                <div className="col-lg-12">
                    <div className="full center">
                        <Link className="hvr-radial-out button-theme" to="/diagnostic2" >Diagnostiquer </Link>&nbsp;
                      
                    </div>
                </div>
                <div className="col-lg-12 margin-top_30">
                    <div id="demo" className="carousel slide" data-ride="carousel">

                       
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img  style={{height:200}}className="img-responsive"  src="assets/images/img1.png" alt="#" />
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img style={{height:200}} className="img-responsive"  src="assets/images/img2.png" alt="#" />
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img style={{height:200}} className="img-responsive"  src="assets/images/img3.png" alt="#" />
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img style={{height:200}} className="img-responsive"  src="assets/images/img4.png" alt="#" />
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img style={{height:200}} className="img-responsive"  src="assets/images/img1.png" alt="#" />
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img style={{height:200}} className="img-responsive"  src="assets/images/img2.png" alt="#" />
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img style={{height:200}} className="img-responsive"  src="assets/images/img3.png" alt="#" />
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                        <img style={{height:200}} className="img-responsive"  src="assets/images/img4.png" alt="#" />
                                    </div>
                                </div>
                            </div>
                        </div>

                       
                        <a className="carousel-control-prev" href="#demo" data-slide="prev">
                            <span className="carousel-control-prev-icon"></span>
                        </a>
                        <a className="carousel-control-next" href="#demo" data-slide="next">
                            <span className="carousel-control-next-icon"></span>
                        </a>

                    </div>
                </div>

               

            </div>
        </div>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Connectez</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Pour t&eacute;l&eacute;charger cette application mobile, veuillez remplir les champs suivants avant de continuez
          </DialogContentText>
          <TextField
             margin="dense"
            id="name"
            label="Votre adresse Email"
            type="email"
            fullWidth
          />
           <TextField
            margin="dense"
            id="password"
            label="Votre mot de passe"
            type="password"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Annuler
          </Button>
          <Button onClick={handleClose} color="primary">
            Connectez
          </Button>
          
        </DialogActions>
        
        <Grid container >
            <Grid item>
              <Link to="/inscription" className="nav-link">
               Vous n'avez pas un compte? Inscrire
              </Link>
            </Grid>
          </Grid>
          <br/>
      </Dialog>
    </div> 
    )      
}
export default Galery