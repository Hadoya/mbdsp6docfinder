import React,{Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import GrandTitre from './GrandTitre'
import TextField from '@material-ui/core/TextField';
import Galery from './Galery'
import {Link,useHistory} from 'react-router-dom' 
import Pagination from '@material-ui/lab/Pagination';
import axios from 'axios';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import FaceIcon from '@material-ui/icons/Face';
import ReactPaginate from 'react-paginate';
import  './style.css';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}))


export default class  CorpsMedicaux extends Component {
  
  constructor(props) {
    super(props)
    this.onChangeEmail=this.onChangeEmail.bind(this)
    this.demandeRDV=this.demandeRDV.bind(this)
    this.onChangePassword=this.onChangePassword.bind(this)
    this.onSubmit=this.onSubmit.bind(this)
    this.handleClickOpen=this.handleClickOpen.bind(this)
    this.handlePageClick = this.handlePageClick.bind(this);
    this.detailDoctor=this.detailDoctor.bind(this)
    this.state={
      doctors:[],
      oneDoctor:[],
      open:false,
      notconnected:false,
      id:1,
      search:'',
      firstname:'...',
      lastname:'...',
      telephone:'...',
      address:'...',
      town:'...',
      town:'...',
      city:'...',
      description:'...',
      tarif:0,
      nombre:'',
      offset: 0,
		  perPage: 3,
      currentPage: 0,
      idDoctor:''

    };
  }
  demandeRDV(id){
    
    if(window.sessionStorage.getItem("idPatient")=='undefined'){
    alert('Veuillez-vous connecter d\'abord avant de demander un Rendez-vous')
  }
  else if(window.sessionStorage.getItem("idPatient")==null){
    alert('Veuillez-vous connecter d\'abord avant de demander un Rendez-vous')
  }
  else{
    //window.location.href="demandeRDV/"+id;
   // <Redirect to={`/demandeRDV/${id}`} />
   this.props.history.push("demandeRDV/"+id);

  }
}
  onChangeEmail(e){
    this.setState({
      email:e.target.value
    });
  }
  onChangePassword(e){
    this.setState({
      password:e.target.value
    });
  }
  onSubmit(e){
    e.preventDefault();
          const patient={
            email:this.state.email,
            password:this.state.password,
          }
          axios.post('http://localhost:8080/patient/login',patient)
          .then(response=>{
              if(response.data.estPatient==1){
                  this.setState({onePatient:response.data,open:false})
                  window.sessionStorage.setItem("sessionfirstname",this.state.onePatient.firstname)
                  window.sessionStorage.setItem("estPatient","1")
                 console.log(this.state.onePatient.firstname)
                 window.location="/"
              }
               if(response.data.estPatient==0){
                //Find from table doctor where idDoctor=_id
                   axios.post('http://localhost:8080/doctor/findByIdpatient',{idPatient:response.data.id})
                   .then(res=>{
                     //alert("etatvalidation="+res.data[0].etatValidation)
                       //If valider ->effectuer l'operation
                       if(res.data[0].etatValidation==1){
                        this.setState({onePatient:res.data,open:false})
                        window.sessionStorage.setItem("sessionfirstname",res.data[0].firstname)
                        window.sessionStorage.setItem("estPatient","0")
                       // console.log(this.state.onePatient.firstname)
                        window.location="/"
                       }
                          //sinon ->message d'erreur (en attente de validation)
                       else{
                        this.setState({open:false})
                          alert('Votre compte n\'est pas encore validé par l\'admin. Veuillez attrendre la validation!')
                       }
                   })
              }
             else{this.setState({notconnected:true,open:true,error:'Email inexistant ou mot de passe invalide. Veuillez réessayer!'}) }
              })
            .catch()
  }
  handleClose = () => {
    this.setState({
        open:false
    });
  };
  handleClickOpen = () => {
    window.sessionStorage.setItem("idDoctor",this.state.onePatient.firstname)
    this.setState({ open:true });
    
 };

  detailDoctor(id){
    axios.get('http://localhost:8080/doctor/'+id)
  .then(response=>{
    console.log('ezzzbgrfg',response)
    this.setState({
      firstname:response.data.firstname,
      telephone:response.data.telephone,
      address:response.data.address,
      town:response.data.town,
      city:response.data.city,
      description:response.data.description,
      tarif:response.data.tarif,
      lastname:response.data.lastname,
    
    })
  })
  .catch(error=>{
    console.log('ezzzbgrfg',error)
  })
}
 componentDidMount(){
    axios.get('http://localhost:8080/doctor/okdoctors/')
    .then(response=>{
      const data = response.data;
			const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
			const postdata = slice.map(doctor => <React.Fragment>
				 <ListItem alignItems="flex-start" key={doctor.id}>
              <ListItemAvatar>
                <Avatar>Dr. </Avatar>
              </ListItemAvatar>
            
            <ListItemText>
                  <strong onClick={()=>this.detailDoctor(doctor.id)} style={{"color":"#686869",cursor:'pointer'}}>Dr {doctor.firstname}&nbsp; {doctor.lastname} </strong><br/> 
                  <Typography  variant='subtitle1'>{doctor.description}  </Typography>
                  <Typography variant='subtitle2'>{doctor.city}|| {doctor.town}  </Typography>
             </ListItemText>
            
            
             <Tooltip onClick={()=>this.demandeRDV(doctor.id)}  title="Demande de rendez-vous" aria-label="add">
            <Fab color="primary" >
              <EventAvailableIcon />
            </Fab>
          </Tooltip>
             </ListItem>
			</React.Fragment>)
		  this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage)})
    })
    .catch(error=>{
      console.log(error)
    })
 }
 searchHandler=e=>{
 if(e.target.value!=null){
  const doctorEvent={
    wordToSearch:e.target.value
   }
  axios.post('http://localhost:8080/doctor/okdoctorssearch/',doctorEvent)
  .then(response=>{
    const data = response.data;
			const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
			const postdata = slice.map(doctor => <React.Fragment>
				 <ListItem alignItems="flex-start" key={doctor.id}>
              <ListItemAvatar>
                <Avatar>Dr. </Avatar>
              </ListItemAvatar>
            
            <ListItemText>
                  <strong onClick={()=>this.detailDoctor(doctor.id)} style={{"color":"#686869",cursor:'pointer'}}>Dr {doctor.firstname}&nbsp; {doctor.lastname} </strong><br/> 
                  <Typography  variant='subtitle1'>{doctor.description}  </Typography>
                  <Typography variant='subtitle2'>{doctor.city}|| {doctor.town}  </Typography>
             </ListItemText>
            
            
             <Tooltip onClick={()=>this.demandeRDV(doctor.id)}  title="Demande de rendez-vous" aria-label="add">
            <Fab color="primary" >
              <EventAvailableIcon />
            </Fab>
          </Tooltip>
             </ListItem>
			</React.Fragment>)
    this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),nombre:response.data.length+' résultat(s) trouvé(s)'})

  })
  .catch(error=>{
    console.log(error)
  })
 }
 else{
  this.setState({nombre:''})
 }
  
 }
 handlePageClick = (e) => {
  const selectedPage = e.selected;
  const offset = selectedPage * this.state.perPage;

  this.setState({
      currentPage: selectedPage,
      offset: offset
  }, () => {
      this.componentDidMount()
  });

};
render(){
  const {doctors,errorMsg}=this.state
  const {firstname,lastname,telephone,address,town,city,description,tarif}=this.state

  

  return (
    <div> <br/>
      
      <GrandTitre name="Corps medicaux"/>
        <div className="section layout_padding">
         <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="full">
                        <div className="heading_main text_align_left">
                          <div className="left">
                            <p className="section_count">01</p>
                          </div>
                          <div className="right">
                         
                           <h2><span className="theme_color">La liste des corps medicaux</span> </h2>
                           </div>	
                        </div>
                    </div>
                </div>
            </div>
         </div>
        </div>
      
    <div className="row">
    
        <div className="col-md-3"></div>
        <div className="col-md-4">
       
          <Paper >
          
          <ListItem >
               
                 <TextField 
                   fullWidth 
                   variant="outlined" 
                   type="text" 
                   placeholder="Rechercher..." 
                   onChange={this.searchHandler}
                   InputProps={{
                    endAdornment: <SearchIcon position="end"></SearchIcon>,
                  }}
                   />  
                    
          </ListItem>
        <List >
        <Typography variant="h6">Voici la liste de docteurs et ses spécialités</Typography>
        <hr/>
        <span style={{color:'red'}}>{this.state.nombre}</span>
        {this.state.postdata}  
        <br /> <br />
       
      	<div style={{marginLeft: '42%',marginRight: 'auto',textAlign:'center'}}>
				<ReactPaginate 
                    previousLabel={<KeyboardArrowLeftIcon fontSize="medium"/>}
                    nextLabel={<KeyboardArrowRightIcon fontSize="medium" />}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
			</div>		
        </List>
        </Paper>
        </div>
        
        <div className="col-md-4">
          <Paper>
        <Typography  variant="h5">
            Fiche de détail:
            
            <h3>{`Dr ${firstname} ${lastname} `}</h3>

         </Typography>
              <table>
                <tr>
                   <td> &nbsp;&nbsp;</td>
                   <td>&nbsp;</td>
                   <td>
                   <div >
                     
                      <Typography  style={{textAlign:'left',color:'#666363',fontSize:14}} variant="subtitle1">
                         <label style={{fontWeight:'bold'}}>&nbsp;&nbsp;Nom:</label> <span style={{color:'#4d4a4a'}}>{firstname}</span><br/>
                         <label style={{fontWeight:'bold'}}>&nbsp;&nbsp;Prénom(s):</label><span style={{color:'#4d4a4a'}}>{lastname}</span><br/>
                         <label style={{fontWeight:'bold'}}>&nbsp;&nbsp;Description:</label> <span style={{color:'#4d4a4a'}}>{description}</span><br/>
                         <label style={{fontWeight:'bold'}}>&nbsp;&nbsp;Lieu:</label> <span style={{color:'#4d4a4a'}}>{city}&nbsp;{town}</span><br/>
                         <label style={{fontWeight:'bold'}}>&nbsp;&nbsp;Tél:</label> <span style={{color:'#4d4a4a'}}>{telephone}</span><br/>
                         <label style={{fontWeight:'bold'}}>&nbsp;&nbsp;Adresse:</label> <span style={{color:'#4d4a4a'}}>{address}</span><br/>
                        </Typography>
                        
                        </div>
                   </td>
                </tr>
                <tr>
                  <td> &nbsp;&nbsp;</td>
                  <td></td>
                  <td><Avatar variant="rounded" src="/assets/img/outil2.jpg" style={{width:300,height:300}}/></td>
                </tr>
              </table>
              <br/>
              
       
  
        </Paper>
        </div>
        <div className="col-md-1"></div>
    </div>
    <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Connectez</DialogTitle>
        <form onSubmit={this.onSubmit}>
        <DialogContent>
          <DialogContentText>
            Pour t&eacute;l&eacute;charger cette application mobile, veuillez remplir les champs suivants avant de continuez
          </DialogContentText>
          
          <TextField
             margin="dense"
            id="name"
            label="Votre adresse Email"
            type="email"
            fullWidth
            value={this.state.email}
            onChange={this.onChangeEmail}
          />
           <TextField
            margin="dense"
            id="password"
            label="Votre mot de passe"
            type="password"
            fullWidth
            value={this.state.password}
            onChange={this.onChangePassword}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            Annuler
          </Button>
          <Button  type="submit" color="primary">
            Connectez
          </Button>
        
        </DialogActions>
        </form>
        <Grid container >
            <Grid item>
              <Link to="/inscription" className="nav-link">
               Vous n'avez pas un compte? Inscrire
              </Link>
            </Grid>
          </Grid>
          <br/>
      </Dialog>
    </div>
  )
}
}