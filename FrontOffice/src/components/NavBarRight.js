
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import GrandTitre from './GrandTitre'
import Galery from './Galery'
import { Alert, AlertTitle } from '@material-ui/lab';
import AndroidIcon from '@material-ui/icons/Android';
import {Link, Redirect} from 'react-router-dom' 
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export default function NavBarRight() {
    const classes = useStyles();
return(

<Grid item xs={3}>
            <Paper className={classes.paper}>
                <Typography variant="h6" gutterBottom>
                    Application DocFinder
                </Typography>
                <Divider component="li" />
                <ListItem>
                  <ListItemText secondary="A la recherche d'une application version mobile pour gérer vos santé?" />
                </ListItem>
                
                <Button variant="contained" color="secondary" style={{"backgroundColor":"#00a300"}}>
                <AndroidIcon></AndroidIcon> &nbsp;Télécharger apk
           </Button>
           <br/><br/>
           <label>Pour plus d'informations, veuillez  <Link  style={{"color":"crimson"}} to="/contact">nous contacter</Link> </label>
           <img src="assets/img/outil2.jpg" width="300px" style={{"borderRadius":"10px"}} />
            </Paper>
        </Grid>
       
   

    )
}