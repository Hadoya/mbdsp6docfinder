import React from 'react'
function TitleParagraph({sectionCount,smallTag,mainTitle,addtitle,subtitle}){
    
    return(
        
            <div className="row">
                <div className="col-md-12">
                    <div className="full">
                        <div className="heading_main text_align_left">
						   <div className="left">
						     <p className="section_count">{sectionCount}</p>
						   </div>
						   <div className="right">
						    <p className="small_tag">{smallTag}</p>
                                <h2><span className="theme_color">{mainTitle}</span><br/>{addtitle}</h2>
                            <p className="large">{subtitle}</p>
						  </div>	
                        </div>
                    </div>
                </div>
            </div>
       
    )      
}
export default  TitleParagraph
