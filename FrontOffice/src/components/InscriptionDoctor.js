import React,{Component,useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import GrandTitre from './GrandTitre';
import Axios from 'axios';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputLabel from '@material-ui/core/InputLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';


export default class InscriptionDoctor extends Component{
  constructor(props) {
    super(props)
    
    this.onChangeGender=this.onChangeGender.bind(this)
    this.onChangebirthdate=this.onChangebirthdate.bind(this)
    this.onChangeLastname=this.onChangeLastname.bind(this)
    this.onChangeFirstname=this.onChangeFirstname.bind(this)
    this.onChangeEmail=this.onChangeEmail.bind(this)
    this.onChangetelephone=this.onChangetelephone.bind(this)
    this.onChangeaddress=this.onChangeaddress.bind(this)
    this.onChangetown=this.onChangetown.bind(this)
    this.onChangecity=this.onChangecity.bind(this)
    this.onChangedescription=this.onChangedescription.bind(this)
    this.onChangetarif=this.onChangetarif.bind(this)
    this.onChangePassword=this.onChangePassword.bind(this)
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangeDoctorSpeciality=this.onChangeDoctorSpeciality.bind(this)
    

		this.state={
        lastname:'',
        firstname:'',
        idDoctorSpeciality:'',
        telephone:'',
        address:'',
        town:'',
        city:'',
        description:'',
        tarif:'',
        etatValidation:'',
        password:'',
        err:'',
        doctorSpeciality:[],
        email:'',
        idPatient:'',
        gender:'',
        birthdate:''
      
      };
    
    }
    onChangeGender(e){
      this.setState({
        gender:e.target.value
      });
    }
    onChangebirthdate(e){
      this.setState({
        birthdate:e.target.value
      });
    }
    onChangeLastname(e){
      this.setState({
        lastname:e.target.value
      });
    }
    onChangetelephone(e){
        this.setState({
            telephone:e.target.value
        });
      }
      onChangeaddress(e){
        this.setState({
            address:e.target.value
        });
      }
      onChangetown(e){
        this.setState({
            town:e.target.value
        });
      }
      onChangecity(e){
        this.setState({
            city:e.target.value
        });
      }
      onChangedescription(e){
        this.setState({
            description:e.target.value
        });
      }
      onChangetarif(e){
        this.setState({
            tarif:e.target.value
        });
      }
    onChangeFirstname(e){
        this.setState({
        firstname:e.target.value
      });
      //window.sessionStorage.setItem("sessionfirstname",e.target.value );
    }
    

    onChangeEmail(e){
      this.setState({
        email:e.target.value
      });
    }
    onChangeDoctorSpeciality(e){
        console.log("value="+e.target.value)
        this.setState({
          idDoctorSpeciality:e.target.value
        });
        
      }
    onChangePassword(e){
      this.setState({
        password:e.target.value
      });
    }
    onSubmit(e){
      e.preventDefault();
      const patient={
        lastname:this.state.lastname,
        firstname:this.state.firstname,
        email:this.state.email,
        password:this.state.password,
        gender:this.state.gender,
        telephone:this.state.telephone,
        birthdate:this.state.birthdate,
        estPatient:0
        
      }
      const patient1={
        lastname:this.state.lastname,
        firstname:this.state.firstname,
        email:this.state.email,
      }
      console.log(patient1)
      Axios.post('http://localhost:8080/patient/add',patient)
      .then(res=>{console.log("AddPatient"+res.data)
     
      Axios.post('http://localhost:8080/patient/findIdPatient',patient1)
      .then(response=>{
        console.log("idPatient="+response.data[0].id)
           this.setState({idPatient:response.data[0].id})
           if(this.state.idDoctorSpeciality!=''){
            console.log("id="+this.state.idDoctorSpeciality)
            const doctor={
                lastname:this.state.lastname,
                firstname:this.state.firstname,
                idDoctorSpeciality:this.state.idDoctorSpeciality,
                telephone:this.state.telephone,
                address:this.state.address,
                town:this.state.town,
                city:this.state.city,
                description:this.state.description,
                tarif:this.state.tarif,
                etatValidation:'0',
                password:this.state.password,
                email:this.state.email,
                idPatient:this.state.idPatient
                
              }
              console.log(doctor)
              Axios.post('http://localhost:8080/doctor/add',doctor)
              .then(res=>{ console.log(res.data)});
    
            
             alert('Votre compte a été crée avec succès! Veuillez cliquez sur le menu "Connectez" si vous voulez acceder à votre compte')
             
              window.location='/';
          }
          else{
            this.setState({err:'Veuillez choisir une spécialité!'})
          }
        
        });
      });
     
     
    }
    componentDidMount(){
        Axios.get('http://localhost:8080/doctorSpeciality/')
		.then(response=>{
		  this.setState({doctorSpeciality:response.data})
		})
		.catch(error=>{
		  console.log(error)
		})
    }
   
  render(){
    
  return (
    <div >
       <br/>
	   <GrandTitre name="inscription"/>
    <Container component="main" maxWidth="xs">
    
    
      <CssBaseline />
      <br/>
      <br/>
      <div >
        
        <Avatar >
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Inscription en tant que docteur
        </Typography>
        <form onSubmit={this.onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstname"
                variant="outlined"
                required
                fullWidth
                id="firstname"
                label="Nom "
                value={this.state.firstname}
                onChange={this.onChangeFirstname}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastname"
                label="Pr&eacute;nom(s)"
                name="lastname"
                value={this.state.lastname}
                onChange={this.onChangeLastname}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="email"
                label="email"
                type="email"
                id="email"
                value={this.state.email}
                onChange={this.onChangeEmail}
              />
            </Grid>
            <Grid item xs={12}>
         
            <InputLabel htmlFor="filled-age-native-simple"><span style={{color:'red'}}>{this.state.err}</span></InputLabel>
           
        <NativeSelect
          required
          fullWidth
          variant="outlined"
          value={this.state.idDoctorSpeciality}
          onChange={this.onChangeDoctorSpeciality}
          inputProps={{
            name: 'age',
            id: 'age-native-helper',
          }}
        >
              <option>Choisir une specialité</option>
             { this.state.doctorSpeciality.map(doc=>
                    <option value={doc.id}>{doc.name}</option>
                )}
    
        </NativeSelect>
          </Grid>
          <Grid item xs={12} sm={12}>
              <TextField
                autoComplete="fname"
                name="telephone"
                variant="outlined"
                required
                fullWidth
                id="telephone"
                label="telephone "
                value={this.state.telephone}
                onChange={this.onChangetelephone}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <label>Date de naissance</label>
              <TextField
                autoComplete="birthdate"
                name="birthdate"
                variant="outlined"
                required
                fullWidth
                type="date"
                id="birthdate"
               
                value={this.state.birthdate}
                onChange={this.onChangebirthdate}
              />
            </Grid>
            <Grid item xs={12}>
            <RadioGroup required row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeGender}>
          <FormControlLabel
            value="m"
            control={<Radio color="primary" />}
            label="Masculin"
            labelPlacement="start"
            
          />
          <FormControlLabel 
            value="w"
            control={<Radio color="primary" />}
            label="Féminin"
            labelPlacement="start"
          />
         </RadioGroup>
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="address"
                label="adresse"
                name="adresse"
                value={this.state.address}
                onChange={this.onChangeaddress}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="ftown"
                name="town"
                variant="outlined"
                required
                fullWidth
                id="town"
                label="Région"
                value={this.state.town}
                onChange={this.onChangetown}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                autoComplete="fcity"
                required
                fullWidth
                id="city"
                label="Ville"
                name="city"
                value={this.state.city}
                onChange={this.onChangecity}
              />
            </Grid>
            <Grid item xs={12}>
              <textarea
                className="form-control"
                fullWidth
                name="description"
                placeholder="Mettre votre description ici"
                type="text"
                id="description"
                value={this.state.description}
                onChange={this.onChangedescription}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="tarif"
                label="tarif"
                type="text"
                id="tarif"
                value={this.state.tarif}
                onChange={this.onChangetarif}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={this.state.password}
                onChange={this.onChangePassword}
              />
              
            </Grid>
            <Grid item xs={12}>
                <br/>

            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
          >
            S'inscrire
          </Button>
         
        </form>
      </div>
      <Box mt={5}>
        
      </Box>
    </Container>
    </div>
  );
}
}