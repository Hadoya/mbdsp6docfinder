import React,{Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Button from '@material-ui/core/Button';


export default class AddressForm extends Component {
  constructor(props) {
    super(props)
    
    this.onChangeAge=this.onChangeAge.bind(this)
    this.onChangeAutrecas=this.onChangeAutrecas.bind(this)
    this.onChangeCheckbox=this.onChangeCheckbox.bind(this)
    this.onChangeSexe=this.onChangeSexe.bind(this)
    this.onChangeSymptome=this.onChangeSymptome.bind(this)
    
    this.onSubmit=this.onSubmit.bind(this)

		this.state={
        age:'',
        autrecas:'',
        symptome:'',
        sexe:'',
        checkbox:'',
      
      };
    
    }
    onChangeAge(e){
      this.setState({
        age:e.target.value
      });
    }
    onChangeSexe(e){
      this.setState({
        sexe:e.target.value
      });
    }
    onChangeAutrecas(e){
      this.setState({
        autrecas:e.target.value
      });
    }
    onChangeCheckbox(e){
      this.setState({ checkbox:[e.target.name] });
    }
    onChangeSymptome(e){
      this.setState({
        symptome:e.target.value
      });
    }
    onSubmit(e){
      e.preventDefault();
      window.localStorage.setItem("age",this.state.age)
      window.localStorage.setItem("sexe",this.state.sexe)
      window.localStorage.setItem("symptom",this.state.symptome)
      window.localStorage.setItem("checkbox",this.state.checkbox)
    
    }
   

  componentDidMount(){
    
  }
  render(){
   
  return (
    <div>
     
      <Paper>
      <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Votre identité
      </Typography>
      <Grid container spacing={3}  style={{"padding": "5px","textAlign": 'center',"color": "theme.palette.text.secondary"}}>
      <form onSubmit={this.onSubmit}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="age"
            name="age"
            label="Votre age "
            fullWidth
            value={this.state.age}
            onChange={this.onChangeAge}
          
          />
        </Grid>
        <RadioGroup row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeSexe}>
          <FormControlLabel
            value="masculin"
            control={<Radio color="primary" />}
            label="Masculin"
            labelPlacement="start"
            
          />
          <FormControlLabel 
            value="feminin"
            control={<Radio color="primary" />}
            label="Féminin"
            labelPlacement="start"
          />
         </RadioGroup>
        <Grid item xs={12}>
          <TextField
            required
            id="symptome"
            name="symptome"
            label="Indiquez votre symptôme"
            fullWidth
            autoComplete="shipping address-line1"
            value={this.state.symptome}
            onChange={this.onChangeSymptome}
          />
        </Grid>
        <Grid item xs={12}>
        <Typography variant="subtitle1" style={{"textAlign":"left"}}>Vous avez peut-être:</Typography>
        <FormGroup>
        <FormControlLabel
            control={<Checkbox  name="fievre" />}
            label="Fièvre"
            onChange={this.onChangeCheckbox}
          />
           <FormControlLabel
            control={<Checkbox  name="nausee" />}
            label="Nausée"
            onChange={this.onChangeCheckbox}
          />
          
          <TextField
            required
            id="autre"
            name="autre"
            label="Autre cas "
            fullWidth
            autoComplete="shipping address-level2"
            value={this.state.autrecas}
            onChange={this.onChangeAutrecas}
          />
           </FormGroup>
           <Button variant="contained" color="secondary">
        Continuer
      </Button>
        </Grid>
        
        </form>
      </Grid>
    </React.Fragment>
    <br/>
          </Paper>
      
    </div>
  );
}
}