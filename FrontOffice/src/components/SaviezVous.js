
import React,{Component,useRef} from 'react';
import TitleParagraph from './TitleParagraph'
import GrandTitre from './GrandTitre'
import Galery from './Galery';
import Pagination from '@material-ui/lab/Pagination';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import ReactPaginate from 'react-paginate';
import  './style.css';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

class SaviezVous extends Component{
  
	constructor(props) {
		super(props)
		this.handlePageClick = this.handlePageClick.bind(this);
		this.state={
		  saviezVous:[],
		  result:'',
		  offset: 0,
		  perPage: 3,
		  currentPage: 0
		 
		};
	  }
	 componentDidMount(){
		axios.get('https://docfinder-backend.herokuapp.com/leSaviezVous/')
		.then(response=>{
			const data = response.data;
			const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
			const postdata = slice.map(saviezVousUn => <React.Fragment>
				 <div className="col-sm-6 col-md-4">
					
                    <div className="service_blog">
                        <div className="service_icons">
                        <a href={saviezVousUn.image}> <img width="150" style={{"borderRadius":"7px"}} height="150" src={saviezVousUn.image} alt="#"/></a>
                        </div>
                        <div className="full">
                        <h4 title={saviezVousUn.title}>{saviezVousUn.title}</h4>
                        </div>
                        <div className="full">
                        <p style={{"textAlign":"justify"}}>{saviezVousUn.description}</p>
                        </div>
                    </div>
			   </div>
			</React.Fragment>)
		  this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage)})
		})
		.catch(error=>{
		  console.log(error)
		})
	 }
	 handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset: offset
        }, () => {
            this.componentDidMount()
        });

    };
	 searchHandler=e=>{
		const saviezVousJson={
			wordToSearch:e.target.value,
		  
		}
		console.log(this.state)
		if(e.target.value!=null){
			axios.post('https://docfinder-backend.herokuapp.com/leSaviezVous/searchfromadmin',saviezVousJson)
		.then(response=>{
			const data = response.data;
			const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
			const postdata = slice.map(saviezVousUn => <React.Fragment>
				 <div className="col-sm-6 col-md-4">
					
                    <div className="service_blog">
                        <div className="service_icons">
                        <a href={saviezVousUn.image}> <img width="150" style={{"borderRadius":"7px"}} height="150" src={saviezVousUn.image} alt="#"/></a>
                        </div>
                        <div className="full">
                        <h4 title={saviezVousUn.title}>{saviezVousUn.title}</h4>
                        </div>
                        <div className="full">
                        <p style={{"textAlign":"justify"}}>{saviezVousUn.description}</p>
                        </div>
                    </div>
			   </div>
			</React.Fragment>)
		  this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),result:response.data.length+' résultat(s) trouvé(s)'})
		
		 
			//this.setState({posts:response.data})
		})
		.catch(error=>{
			console.log(error)
		   // this.setState({errorMsg:'Error retrieving data'})
		})
		}
		else{
			axios.get('https://docfinder-backend.herokuapp.com/leSaviezVous/')
			.then(response=>{
			  this.setState({postdata:response.data,result:''})
			})
			.catch(error=>{
			  console.log(error)
			})
		}
		
	  }
 render(){
	const {saviezVous,errorMsg}=this.state
	

    return(
	   <div>
		    <br/>
		<GrandTitre name="LE SAVIEZ-VOUS?"/>
        <div className="section layout_padding">
        <div className="container">
		
           <TitleParagraph
                    sectionCount="01" 
                    smallTag=""
                    mainTitle="Bienvenue dans notre site DocFinder" 
                    addtitle="LE SAVIEZ-VOUS ?"
                    subtitle="La connaissance est le fruit de l’expérience"
            /><br/>
			<React.Fragment >
			
				<IconButton style={{'padding': 10}} aria-label="menu">
					<MenuIcon />
				</IconButton>
				<TextField
				    type="text" 
				    onChange={this.searchHandler}
				    variant='standard'
					syle={{'marginLeft': '50px','width': '700px','flex':'1'}}
					placeholder="Rechecher dans LSV..."
					
				/>
				<IconButton type="submit" style={{'padding': 10}} aria-label="search">
					<SearchIcon />
				</IconButton>
				<br/><span style={{textAlign:'center',color:'red',alignItems:'center'}}>{this.state.result}</span>	
   			 </React.Fragment>
					
            <div className="row margin-top_30">
			
			{this.state.postdata}  
			   
			   
			</div>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp; 
			<div style={{marginLeft: '42%',marginRight: 'auto',textAlign:'center'}}>
				<ReactPaginate 
                    previousLabel={<KeyboardArrowLeftIcon fontSize="medium"/>}
                    nextLabel={<KeyboardArrowRightIcon fontSize="medium" />}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
			</div>		
        </div>

    </div>
	
	</div> 
    )      
 }
}

export default  SaviezVous
