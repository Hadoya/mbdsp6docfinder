import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import GrandTitre from './GrandTitre';
import axios from 'axios';
import { Alert, AlertTitle } from '@material-ui/lab';
import Galery from './Galery';
import TitleParagraph from './TitleParagraph';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {Link} from 'react-router-dom'
import EmailIcon from '@material-ui/icons/Email';



export default class ResultatDiagnostic extends Component{

  constructor(props) {
    super(props)
    
   
    this.state={
        resultat1:'',
        doctorList:[],
        doctorSpeciality:[],
        idspeciality:'',
        finalResult:[],
        age:window.localStorage.getItem("age"),
        symptome:window.localStorage.getItem("symptom"),
        sexe:window.localStorage.getItem("sexe"),
  
        sliderventre:window.localStorage.getItem("sliderventre"),
        resultat1:window.localStorage.getItem("resultat1"),
      };
      console.log('idDiseaseFromResultatC=',window.localStorage.getItem("resultat1"))
    }
    componentDidMount(){
      console.log('idDiseaseFromResultat=',window.localStorage.getItem("resultat1"))
		const idDisease={
         _id:window.localStorage.getItem("resultat1")
         
       }
      axios.post('https://docfinder-backend.herokuapp.com/disease/search',idDisease)
		.then(response=>{
         console.log('id=',response.data)
        this.setState({
           finalResult:response.data,
           idspeciality:response.data[0].idDoctorSpeciality
        })
               const doctorSpeciality={
                     idDoctorSpeciality:response.data[0].idDoctorSpeciality
                     
               }
               axios.post('https://docfinder-backend.herokuapp.com/doctor/searchByIdspecility',doctorSpeciality)
               .then(response=>{
                  console.log(response.data)
               this.setState({
                  doctorList:response.data,
                  })
                     
                     
                     console.log('idSpeciality',response.data[0].idDoctorSpeciality)
                     axios.get('https://docfinder-backend.herokuapp.com/doctorSpeciality/',response.data[0].idDoctorSpeciality)
                     .then(response=>{
                        console.log(response.data)
                     this.setState({
                        doctorSpeciality:response.data,
                        })
                     })
                     .catch(error=>{
                        console.log(error)
                     })
                     
               })
               .catch(error=>{
                  console.log(error)
               })
		})
		.catch(error=>{
			console.log(error)
      })
     
     
      
    }
   
 render(){
   let isLoggedIn;
   

   if(window.sessionStorage.getItem("sessionfirstname")=='undefined'){
       isLoggedIn= <Typography variant="subtitle2">Bonjour Anonyme</Typography>
   }
   else if(window.sessionStorage.getItem("sessionfirstname")==null){
       isLoggedIn= <Typography variant="subtitle2">Bonjour Anonyme</Typography>
   }
   else{
       isLoggedIn=<Typography variant="subtitle2"> {`Bonjour Mr./Mme. ${window.sessionStorage.getItem("sessionfirstname")}`}</Typography>
    
      
   }
  return (
    <div>
    
      <GrandTitre name="Diagnostic"/>
      <br/> 
      <Grid container xs={12} style={{"marginLeft":"10%"}}>
      <TitleParagraph
                 sectionCount="01" 
                 smallTag=""
                 mainTitle="Résultat du diagnostic" 
                 addtitle=""
                 subtitle="La santé et la raison sont les vrais trésors de l'homme "
            />
          
      </Grid>
      <br/>  <br/>
    <Grid container  xs={12} spacing={1}>
        
        <Grid item xs={1}></Grid>
        <Grid item xs={6}>
       
          <Paper>
         
         
          <ListItem>
          { this.state.finalResult.map(result=>
          <div>
        <ListItemText primary="Résultat du  diagnostic:"  />
        <ListItemText  secondary="La maladie qui correspond le plus aux symptômes que vous venez d'indiquer est la suivante:"  />
        <Alert severity="error">
                    <AlertTitle>{result.name}</AlertTitle>
                     — {result.description}
         </Alert>
        </div>
          )}
        </ListItem>
        <ListItem>
            <ListItemText primary="Conseil d'orientation" />
            
        </ListItem>
        <Alert severity="success">
        { this.state.doctorSpeciality.map(doc=>
            <AlertTitle style={{"textAlign":"justify"}}>Nous vous conseillons de consulter un(e)<strong> {doc.name} </strong> <br/>
                -{doc.description}
            </AlertTitle>
        )}           
         </Alert>
        <Divider component="li" />
        <ListItem>
            <ListItemText primary="Voici quelques suggestions"  />
        </ListItem>
      <Divider component="li" />
      
        <List >
        { this.state.doctorList.map(doc=>
        <ListItem alignItems="flex-start">
            <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
            </ListItemAvatar>
            <ListItemText
            primary={`Dr. ${doc.firstname} ${doc.lastname} `}
            secondary={
                <React.Fragment>
               {doc.description}
                </React.Fragment>
            }
            />
            <Button variant="outlined" color="secondary">
            <EmailIcon></EmailIcon>
           </Button>
        </ListItem>
        )}
        
        <Divider variant="inset" component="li" />
        <br/>
           <ListItemAvatar> 
           <Link to="/diagnostic2"> <Button variant="contained" size="small" color="secondary" ><ArrowBackIcon style={{"color":"white"}}> </ArrowBackIcon></Button></Link> 
               
           </ListItemAvatar>
           <br/>
        </List>
        </Paper>
        </Grid>
        <Grid item xs={4}>
            <Paper >
              
                <br/>
                <div style={{"paddingLeft":"10px","paddingRight":"10px"}}>
               {isLoggedIn}
                
               <ListItemText style={{"textAlign":"justify"}} secondary={`Vous êtes agé(es) de ${this.state.age} ans de sexe ${this.state.sexe}. Votre échelle visuelle de la douleur au ventre est  ${this.state.sliderventre} sur 10  `} />
               </div>
              <br/>
              <img src="assets/img/fond3.png"  style={{"borderRadius":"80px"}}/>
              <br/>   <br/>   <br/>
            </Paper>
        </Grid>
        
        <Grid item xs={1}></Grid>
    </Grid>
    <Galery/>
    </div>
  );
}
}