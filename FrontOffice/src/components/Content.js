import React from 'react'
import {Switch,Route} from "react-router-dom";
import SaviezVous from './SaviezVous';
import Inscription from './Inscription';
import Accueil from './Accueil';
import Banner from './Banner';
import Contact from './Contact';
import Diagnostic2 from './Diagnostic2';
import Diagnostic3 from './Diagnostic3';
import AddressForm from './AddressForm';
import Checkout from './Checkout';
import GalleryCircle from './GalleryCircle';
import ResultatDiagnostic from './ResultatDiagnostic';
import CorpsMedicaux from './CorpsMedicaux';
import DetailMedecin from './DetailMedecin';
import Paragraph from './Paragraph';
import NavBarRight from './NavBarRight';
import FinDiagnostic from './FinDiagnostic';
import ResultatDiagnosticNontrouve from './ResultatDiagnosticNontrouve';
import InscriptionDoctor from './InscriptionDoctor';
import RendezVous from './RendezVous';
import DemandeRDV from './DemandeRDV';
function Content(){
    
    return(
        <Switch>
              <Route  exact path="/" component={Accueil}/>
              <Route  path="/accueil" component={Accueil}/>
              <Route  path="/form" component={AddressForm}/>
              <Route  path="/saviezVous" component={SaviezVous}/>
              <Route  path="/inscription" component={Inscription}/>
              <Route  path="/inscriptionDoctor" component={InscriptionDoctor}/>
              <Route  path="/banner" component={Banner}/>
             <Route  path="/contact" component={Contact} />
             <Route  path="/diagnostic2" component={Diagnostic2}/>
             <Route  path="/diagnostic3" component={Diagnostic3}/>
             <Route  path="/findiagnostic" component={FinDiagnostic}/>
             <Route  path="/addressForm" component={AddressForm}/>
             <Route  path="/checkout" component={Checkout}/>
             <Route  path="/galeryCircle" component={GalleryCircle}/>
             <Route  path="/resultatDiagnostic" component={ResultatDiagnostic}/>
             <Route  path="/resultatDiagnosticNontrouve" component={ResultatDiagnosticNontrouve}/>
             <Route  path="/corpsMedicaux" component={CorpsMedicaux}/>
             <Route  path="/detailMedecin/:id" component={DetailMedecin}/>
             <Route  path="/demandeRDV/:id" component={DemandeRDV}/>
             <Route  path="/paragraph" component={Paragraph}/>
             <Route  path="/nav" component={NavBarRight}/>
             <Route  path="/rendezVous" component={RendezVous}/>
        
         </Switch>
    )      
}
export default Content