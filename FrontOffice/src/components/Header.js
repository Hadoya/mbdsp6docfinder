import React,{Component} from 'react'
import {Link, Redirect} from 'react-router-dom' 
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Axios from 'axios';
import './style.css'

class Header extends Component {
    constructor(props) {
       super(props);
       this.onChangeEmail=this.onChangeEmail.bind(this)
       this.onChangePassword=this.onChangePassword.bind(this)
       this.onSubmit=this.onSubmit.bind(this)

       this.state={
        open:false,
        email:'',
        password:'',
        onePatient:[],
        notconnected:false,
        error:'',
       }
      
    }
    searchHandler=e=>{
      e.preventDefault()
      console.log(this.state)
      Axios.post('https://docfinder-backend.herokuapp.com/doctor/search',e.target.value)
      .then(response=>{
          console.log('reponse=',response.data)
          this.props.history.push('/corpsMedicaux')
          //this.setState({posts:response.data})
      })
      .catch(error=>{
          console.log(error)
         // this.setState({errorMsg:'Error retrieving data'})
      })
  }
     handleClickOpen = () => {
        this.setState({
            open:true
        });
      };
    
       handleClose = () => {
        this.setState({
            open:false
        });
      };
      onChangeEmail(e){
        this.setState({
          email:e.target.value
        });
      }
      onChangePassword(e){
        this.setState({
          password:e.target.value
        });
      }
      onSubmit(e){
        e.preventDefault();
        const patient={
          email:this.state.email,
          password:this.state.password,
        }
        Axios.post('http://localhost:8080/patient/login',patient)
        .then(response=>{
          if(response.data.length>0){
            if(response.data[0].estPatient==1){
              console.log("valiny="+response.data[0].firstname);
              window.sessionStorage.setItem("sessionfirstname",response.data[0].firstname)
                window.sessionStorage.setItem("idPatient",response.data[0].id)
                window.sessionStorage.setItem("estPatient","1")
               console.log(response.data[0].firstname)
               this.setState({
                open:false
            });
             //  window.location="/"
            }
             if(response.data[0].estPatient==0){
              console.log("doc="+response.data[0].id);
              //Find from table doctor where idDoctor=id
             
                 Axios.post('http://localhost:8080/doctor/findIdPatient',{idPatient:response.data[0].id})
                 .then(res=>{
                   //alert("etatvalidation="+res.data[0].etatValidation)
                     //If valider ->effectuer l'operation
                     if(res.data[0].etatValidation==1){
                      console.log(res.data);
                      this.setState({onePatient:res.data,open:false})
                      window.sessionStorage.setItem("sessionfirstname",res.data[0].firstname)
                      window.sessionStorage.setItem("idPatient",res.data[0].idPatient)
                      window.sessionStorage.setItem("estPatient","0")
                     // console.log(this.state.onePatient.firstname)
                      window.location="/"
                     }
                        //sinon ->message d'erreur (en attente de validation)
                     else{
                      this.setState({open:false})
                        alert('Votre compte n\'est pas encore validé par l\'admin. Veuillez attrendre la validation!')
                     }
                 })
            }
           else{/*this.setState({notconnected:true,open:true,error:'Email inexistant ou mot de passe invalide. Veuillez réessayer!'})*/}
          }
          else{
            this.setState({notconnected:true,open:true,error:'Email inexistant ou mot de passe invalide. Veuillez réessayer!'}) 
          }
          })
          .catch(
           //this.setState({notconnected:true,open:true,error:'Email inexistant ou mot de passe invalide. Veuillez réessayer!'}) 
          )
        }
     
     handleClick = () => {
        window.sessionStorage.clear()
        window.location="/"
       
      };
    componentDidMount(){
		console.log('LOGIN',window.sessionStorage.getItem("sessionfirstname"))}
    render() {
        let isLoggedIn;
        let deconnexion;
        let rendezVous;
        let err;
    
        if(window.sessionStorage.getItem("sessionfirstname")=='undefined'){
            isLoggedIn=<Button className="nav-link" variant="outlined" onClick={this.handleClickOpen} to="/contact">Connexion</Button>
        }
        else if(window.sessionStorage.getItem("sessionfirstname")==null){
            isLoggedIn=<Button className="nav-link" variant="outlined" onClick={this.handleClickOpen} to="/contact">Connexion</Button>
        }
        else{
            isLoggedIn=  <Chip avatar={<Avatar><AccountCircleIcon></AccountCircleIcon></Avatar>}  color="secondary" variant="outlined" title={window.sessionStorage.getItem("sessionfirstname")} label={window.sessionStorage.getItem("sessionfirstname")} />
            deconnexion=<Link onClick={this.handleClick} className="nav-link" >Déconnexion </Link>
            rendezVous=<Link to="/rendezVous" className="nav-link" >Rendez-vous </Link>
        }
       
      
    return(
        <div>
        <header className="top-header">
        <nav className="navbar header-nav navbar-expand-lg">
            <div className="container-fluid">
                <a className="navbar-brand" href="accueil"><img src="assets/img/df.png" style={{"width":"200px","marginTop":"-7px"}} alt="image"/></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul className="navbar-nav">
                        <li><a className="nav-link active" href="/accueil">Accueil</a></li>
                        <li><Link className="nav-link" to="/saviezVous">Saviez-vous</Link></li>
                        <li><Link className="nav-link" to="/diagnostic2">Diagnostic</Link></li>
                        <li><Link className="nav-link" to="/corpsMedicaux">Médecins</Link></li>
						            <li  className="dropdown" style={{color:"black",fontSize:15,cursor:'pointer'}}>
                        <span className="nav-link active">INSCRIPTION</span>
                        <ul class="dropdown-content" >
                          <li style={{textTransform:'capitalize'}}><Link  to="/inscription">Patient</Link></li>
                          <li style={{textTransform:'capitalize'}}><Link  to="/inscriptionDoctor">Docteur</Link></li>
                        </ul>
                          
                          
                          </li>
                       
                        <li>{isLoggedIn} </li>
                        <li>{rendezVous} </li>
                        <li>{deconnexion} </li>
                        
                    </ul>
                </div>
               
            </div>
        </nav>
        
    </header>
    <br/><br/>
    <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Connectez</DialogTitle>
        <form onSubmit={this.onSubmit}>
        <DialogContent>
          <DialogContentText>
            Pour t&eacute;l&eacute;charger cette application mobile, veuillez remplir les champs suivants avant de continuez
           <br/> <span style={{color:'red',fontSize:13,fontFamily:'Arial'}}>{this.state.error}</span>
          </DialogContentText>
        
          <TextField
             margin="dense"
            id="name"
            label="Votre adresse Email"
            type="email"
            fullWidth
            value={this.state.email}
            onChange={this.onChangeEmail}
          />
           <TextField
            margin="dense"
            id="password"
            label="Votre mot de passe"
            type="password"
            fullWidth
            value={this.state.password}
            onChange={this.onChangePassword}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            Annuler
          </Button>
          <Button  type="submit" color="primary">
            Connectez
          </Button>
        
        </DialogActions>
        </form>
        <Grid container >
            <Grid item>
              <Link to="/inscription" className="nav-link">
               Vous n'avez pas un compte? Inscrire
              </Link>
            </Grid>
          </Grid>
          <br/>
      </Dialog>
    </div>
    )      
}
}
export default Header