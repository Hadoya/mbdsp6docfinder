import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import StarIcon from '@material-ui/icons/Star';
import Divider from '@material-ui/core/Divider';
import Slider from '@material-ui/core/Slider';
import {Link, Redirect} from 'react-router-dom' 


const useStyles = makeStyles((theme) => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
}));
function valuetext(value) {
  return `${value}°C`;
}
export default function Review() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
       Autres sympt&ocirc;mes?
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12}>
    
        <List component="nav" className={classes.root} aria-label="contacts">
      <ListItem >
            
            <ListItemText> 
                 Brûlure en urinant
                <RadioGroup row aria-label="position" name="position" defaultValue="top">
                  <FormControlLabel style={{"fontSize":"13px"}}
                    value="oui"
                    control={<Radio color="primary" />}
                    label="oui"
                    labelPlacement="start"
                  />
                  <FormControlLabel style={{"fontSize":"13px"}}
                    value="non"
                    control={<Radio color="primary" />}
                    label="non"
                    labelPlacement="start"
                  />
                  <FormControlLabel style={{"fontSize":"13px"}}
                    value="nesaitpas"
                    control={<Radio color="primary" />}
                    label="ne sait pas"
                    labelPlacement="start"
                  />
                </RadioGroup>
          </ListItemText>
      </ListItem>
      <ListItem >
        <ListItemText> 
                    Ingestion accidentelle de produit ménager
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
       
        <ListItem>
        
              <ListItemText>
                  <Typography id="discrete-slider" gutterBottom>
                  Mal au ventre
                  </Typography>
                  <Typography variant="subtitle2" >
                   Echelle visuelle de la douleur(0=absence de la douleur/ 10=douleur insupportable)
                  </Typography>
                  <Slider
                    defaultValue={0}
                    getAriaValueText={valuetext}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={1}
                    marks
                    min={0}
                    max={10}
                  />
                
              </ListItemText>
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Perte vaginales anormales
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Traumatisme ou plale récente
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Urine rouge
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <Typography variant="subtitle2" style={{"color":"crimson"}} >
          ANTECEDENTS, HABITUDES, MODE DE VIE,...
          <hr/>
        </Typography>

        <ListItem >
        <ListItemText> 
                    Voyage récent à l'étranger
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                   Antécedent de diabète insuline-dépendant
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Accouchement récent
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
        <ListItem >
        <ListItemText> 
                    Antécedent de calculs de la vésicule
                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="oui"
                        control={<Radio color="primary" />}
                        label="oui"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="non"
                        control={<Radio color="primary" />}
                        label="non"
                        labelPlacement="start"
                      />
                      <FormControlLabel style={{"fontSize":"13px"}}
                        value="nesaitpas"
                        control={<Radio color="primary" />}
                        label="ne sait pas"
                        labelPlacement="start"
                      />
                    </RadioGroup>
              </ListItemText>
              
        </ListItem>
    </List>
           
        </Grid>
        
      </Grid>
    </React.Fragment>
  );
}