import React from 'react'
import Banner from './Banner'
import SaviezVous from './SaviezVous'
import Galery from './Galery'
import Paragraph from './Paragraph'
import Footer from '../Footer'

function Accueil(){
    
    return(
        <div>
            <Banner/>
            <SaviezVous/>
            <Galery/>
            <Paragraph/>
            <Footer/>

         </div>
    )      
}
export default Accueil