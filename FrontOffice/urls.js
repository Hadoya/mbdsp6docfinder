module.exports = {
    local: 'http://localhost:3000/',
    prod: 'https://docfinder-front.herokuapp.com',
    qa:'https://docfinder-front-qa.herokuapp.com'
}
