package com.mbds.tpt.docfinder.dao;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mbds.tpt.docfinder.db.DbHelper;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.Map;

public abstract class BaseDao {

    protected DbHelper dbHelper;
    protected Activity activity;

    public BaseDao(DbHelper dbHelper, Activity activity) {
        this.dbHelper = dbHelper;
        this.activity = activity;
    }

    public void sendRequest(final int method, String url, final Map<String, String> params, final DataService.VolleyCallback callback) {
        try {
            BaseUtil.log("API: "+url);
            StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    BaseUtil.logSuccess("sendRequest",response);
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String msg;
                    try {
                        msg = new String(error.networkResponse.data, "UTF-8");
                    } catch (Exception e) {
                        msg = e.getMessage();
                    }
                    BaseUtil.logError("sendRequest",msg);
                    callback.onError(msg);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return params;
                }
            };

            Volley.newRequestQueue(activity).add(stringRequest);
        } catch (Exception e) {
            Log.e("sendRequest", e.getMessage());
            throw e;
        }
    }
}
