package com.mbds.tpt.docfinder.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.model.Patient;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class AppointmentRequestDialog extends DialogFragment implements View.OnClickListener {

    private MaterialButton closeBtn, submitBtn;
    private AppointmentRequestDialogListener appointmentRequestDialogListener;
    private DataService dataService;
    private Patient patient;
    private Doctor doctor;
    public interface AppointmentRequestDialogListener{
        void onSubmit(String content);
        void onError(String error);
    }

    public AppointmentRequestDialog(DataService dataService, Patient patient, Doctor doctor){
        this.dataService = dataService;
        this.patient = patient;
        this.doctor = doctor;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_appointment_request, container, false);
        initViews(view);
        initListeners();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            appointmentRequestDialogListener = (AppointmentRequestDialogListener) context;
        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement AppointmentRequestDialogListener");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_btn:
                dismiss();
                break;
            case R.id.submit_btn:
                sendAppointemntRequest();
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog mDialog = getDialog();
        if (mDialog != null) {
            mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void initViews(View view) {
        closeBtn = view.findViewById(R.id.close_btn);
        submitBtn = view.findViewById(R.id.submit_btn);
    }

    private void initListeners() {
        closeBtn.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
    }

    private void sendAppointemntRequest(){
        try {
            if (BaseUtil.isNetworkAvailable(getActivity())) {
                if(patient != null) {
                    final LoadingDialog ld = new LoadingDialog(getContext(), getResources().getString(R.string.sending) + "...");
                    ld.setCancelable(false);
                    ld.show();
                    Appointment appointment = new Appointment();
                    appointment.setIdDoctor(doctor.getId());
                    appointment.setIdPatient(patient.getId());
                    appointment.setLastNameDoctor(doctor.getLastName());
                    appointment.setFirstNameDoctor(doctor.getFirstName());
                    appointment.setDoctorSpeciality(doctor.getSpeciality());
                    appointment.setFirstNamePatient(patient.getFirstName());
                    appointment.setLastNamePatient(patient.getLastName());
                    appointment.setPatientGender(patient.getGender());
                    appointment.setValidation(0);

                    dataService.sendAppointmentRequest(appointment, new DataService.VolleyCallback() {
                        @Override
                        public void onSuccess(Object object) {
                            ld.dismiss();
                            BaseUtil.log("Appointemnt created!");
                            appointmentRequestDialogListener.onSubmit(BaseUtil.SUCCESS);
                        }

                        @Override
                        public void onError(String error) {
                            ld.dismiss();
                            BaseUtil.logError("sendAppointemntRequest:Dialog", error);
                            appointmentRequestDialogListener.onError(error);
                        }
                    });
                }else{
                    appointmentRequestDialogListener.onError(getResources().getString(R.string.you_need_login));
                }
            } else {
                appointmentRequestDialogListener.onError(getResources().getString(R.string.no_connection));
            }
        }catch (Exception e){
            BaseUtil.logError("sendAppointemntRequest:Dialog", e.getMessage());
            appointmentRequestDialogListener.onError(e.getMessage());
        }
    }
}
