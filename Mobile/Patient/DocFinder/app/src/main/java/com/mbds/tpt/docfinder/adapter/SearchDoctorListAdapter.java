package com.mbds.tpt.docfinder.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.model.DoctorWithDistance;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SearchDoctorListAdapter extends ArrayAdapter<DoctorWithDistance> {
    private List<DoctorWithDistance> doctorListCopy;
    private List<DoctorWithDistance> doctorListOriginal;
    private Context context;
    //private int lastPostion = -1;

    private static class ViewHolder {
        TextView nameView;
        TextView specialityView;
        TextView distanceView;
    }

    public SearchDoctorListAdapter(Context context, List<DoctorWithDistance> doctorList) {
        super(context, R.layout.list_item_search_doctor, doctorList);
        this.doctorListCopy = doctorList;
        doctorListOriginal = new ArrayList<>();
        doctorListOriginal.addAll(doctorList);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DoctorWithDistance doctor = getItem(position);
        ViewHolder viewHolder;
        final View result;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.list_item_search_doctor, parent, false);
            viewHolder.nameView = convertView.findViewById(R.id.name_view);
            viewHolder.specialityView = convertView.findViewById(R.id.speciality_view);
            viewHolder.distanceView = convertView.findViewById(R.id.distance_view);
            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        /*Animation animation = AnimationUtils.loadAnimation(context, (position>lastPostion)?R.anim.up_from_bottom:R.anim.down_from_top);
        result.startAnimation(animation);
        lastPostion = position;*/

        viewHolder.nameView.setText("Dr. " + doctor.getLastName() + " " + doctor.getFirstName());
        viewHolder.specialityView.setText(doctor.getSpeciality());
        viewHolder.distanceView.setText(doctor.getDistance() > 1000 ? (((int) doctor.getDistance()) / 1000 + " Km") : ((int) doctor.getDistance() + " m"));

        return convertView;
    }
/*
    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        Doctor doctor = (Doctor)getItem(position);
        switch (view.getId()){
            case R.id.name_view:
                Snackbar.make(view, "Dr. " +doctor.getLastName(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
        }

    }*/

    public void filterDoctor(String text, String speciality, String gender, double maxDistance) {
        doctorListCopy.clear();
        Log.e("MyError", "SearchDoctorDialogAdapter PARAMS text=" + text + " speciality=" + speciality + " gender=" + gender + " dist=" + maxDistance);
        if (text.isEmpty() && speciality.compareToIgnoreCase("all") == 0 && gender.compareToIgnoreCase("all") == 0 && maxDistance == 0) {
            doctorListCopy.addAll(doctorListOriginal);
        } else {
            text = text.toLowerCase();
            for (DoctorWithDistance doctor : doctorListOriginal) {
                if ((doctor.getFirstName().toLowerCase().contains(text) || doctor.getLastName().toLowerCase().contains(text))
                        && (speciality.compareToIgnoreCase("all") == 0 || doctor.getSpeciality().compareToIgnoreCase(speciality) == 0)
                        && (gender.compareToIgnoreCase("all") == 0 || doctor.getGender().compareToIgnoreCase(gender) == 0)
                        && (maxDistance == 0 || doctor.getDistance() / 1000 < maxDistance)) {
                    doctorListCopy.add(doctor);
                }
            }
        }
        notifyDataSetChanged();
    }
}
