package com.mbds.tpt.docfinder;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mbds.tpt.docfinder.dialog.MapBottomSheetDialog;
import com.mbds.tpt.docfinder.dialog.SearchDoctorDialog;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.model.DoctorWithDistance;
import com.mbds.tpt.docfinder.model.Speciality;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;
import com.mbds.tpt.docfinder.util.MarkerUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener, MapBottomSheetDialog.MapBottomSheetListener, SearchDoctorDialog.SearchDoctorDialogListener {

    private GoogleMap mMap;
    private DataService dataService;
    private int LOCATION_PERMISSION = 42;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location myLocation;
    private MapBottomSheetDialog mapBottomSheetDialog;
    private List<Marker> myMarkerList;
    private List<Doctor> doctorList;
    private List<Speciality> specialityList;
    private List<DoctorWithDistance> doctorWithDistanceList;
    private ImageButton searchBtn;
    private SearchDoctorDialog searchDoctorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        try {
            initModels();
            initDialogs();
            initViews();
            initListeners();
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            BaseUtil.toastException(this, e);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        initMyMarkers(googleMap, doctorList);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                for (Marker myMarker : myMarkerList)
                    if (marker.equals(myMarker)) {
                        mapBottomSheetDialog.setDoctor((Doctor) myMarker.getTag());
                        mapBottomSheetDialog.show(getSupportFragmentManager(), "Modal");
                    }
                return false;
            }
        });

        //verification de permission de localisation sinon on le demande
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            initMap();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                initMap();
            }
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
    }

    @Override
    public void onMapDialogCloseBtnClicked() {
    }

    @Override
    public void onMapDialogDetailBtnClicked(String doctorId) {
        startAboutDoctorActivity(doctorId);
    }

    @Override
    public void onSelectedDoctor(Doctor doctor) {
        for (Marker marker : myMarkerList) {
            if (((Doctor) marker.getTag()).getId().compareTo(doctor.getId()) == 0) {
                //BaseUtil.toastLong(MapsActivity.this,"tags:"+((Doctor) marker.getTag()).getFirstName()+" doc:"+doctor.getFirstName());
                Location location = new Location("");
                location.setLatitude(marker.getPosition().latitude);
                location.setLongitude(marker.getPosition().longitude);
                updateMapLocation(location);
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void buildDoctorListWithDistance() {
        try {
            for (Marker marker : myMarkerList) {
                doctorWithDistanceList.add(new DoctorWithDistance((Doctor) marker.getTag(), MarkerUtil.distance(myLocation, marker)));
            }
        } catch (Exception e) {
            BaseUtil.toastException(this, e);
        }
    }

    private void initDialogs() {
        mapBottomSheetDialog = new MapBottomSheetDialog();
        searchDoctorDialog = new SearchDoctorDialog();
        searchDoctorDialog.setSpecialityList(BaseUtil.getSpecialityName(this.specialityList, "All"));
    }

    private void initModels() {
        dataService = new DataService(this);
        doctorList = dataService.getAllDoctors();
        specialityList = dataService.getAllSpecialities();
        doctorWithDistanceList = new ArrayList<>(doctorList.size());
    }

    private void initListeners() {
        searchBtn.setOnClickListener(onSearchBtnListener);
    }

    public void initMap() {
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        //set my location
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    myLocation = location;
                    buildDoctorListWithDistance();
                    Collections.sort(doctorWithDistanceList);
                    searchDoctorDialog.setDoctorList(doctorWithDistanceList);
                    updateMapLocation(location);
                }
            });
        }
    }

    private void initMyMarkers(GoogleMap googleMap, List<Doctor> doctorList) {
        myMarkerList = new ArrayList<>();
        for (Doctor doctor : doctorList) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(doctor.getLatitude(), doctor.getLongitude()))
                    .title("Dr. " + doctor.getLastName() + " " + doctor.getFirstName()).snippet(doctor.getSpeciality()).zIndex(1.0f));
            marker.setTag(doctor);
            myMarkerList.add(marker);
        }
    }

    private void initViews() {
        searchBtn = findViewById(R.id.map_search_btn);
    }

    private View.OnClickListener onSearchBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                searchDoctorDialog.show(getSupportFragmentManager(), "Search");
            } catch (Exception e) {
                BaseUtil.toastException(MapsActivity.this, e);
            }
        }
    };

    private void startAboutDoctorActivity(String doctorId) {
        Intent intent = new Intent(this, AboutDoctorActivity.class);
        intent.putExtra(BaseUtil.EXTRA_DOCTOR_ID, doctorId);
        startActivity(intent);
    }

    private void updateMapLocation(Location location) {
        if (location != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(
                    location.getLatitude(), location.getLongitude())));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
        }
    }

}