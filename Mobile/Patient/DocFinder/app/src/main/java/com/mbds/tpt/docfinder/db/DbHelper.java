package com.mbds.tpt.docfinder.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "DocFinderPatient";
    public static final int DATABASE_VERSION = 1;
    private Context context;

    //TABLE SPECIALITY
    public static final String TABLE_SPECIALITY = "Speciality";
    public static final String COL_SPECIALITY_ID = "Id";
    public static final String COL_SPECIALITY_NAME = "Name";
    public static final String COL_SPECIALITY_DESCRIPTION = "Description";


    //TABLE DOCTOR
    public static final String TABLE_DOCTOR = "Doctor";
    public static final String COL_DOCTOR_ID = "Id";
    public static final String COL_DOCTOR_LASTNAME = "Last_Name";
    public static final String COL_DOCTOR_FIRSTNAME = "First_Name";
    public static final String COL_DOCTOR_SPECIALITY_ID = "SpecialityId";
    public static final String COL_DOCTOR_SPECIALITY = "Speciality";
    public static final String COL_DOCTOR_TELEPHONE = "Telephone";
    public static final String COL_DOCTOR_ADRESSE = "Adresse";
    public static final String COL_DOCTOR_TOWN = "Town";
    public static final String COL_DOCTOR_CITY = "City";
    public static final String COL_DOCTOR_DESCRIPTION = "Description";
    public static final String COL_DOCTOR_TARIF = "Tarif";
    public static final String COL_DOCTOR_ETATVALIDATION = "Etat_Validation";
    public static final String COL_DOCTOR_CREATEAT = "Create_At";
    public static final String COL_DOCTOR_UPDATEAT = "Update_At";
    public static final String COL_DOCTOR_EMAIL = "Email";
    public static final String COL_DOCTOR_PASSWORD = "Password";
    public static final String COL_DOCTOR_GENDER = "Gender";
    public static final String COL_DOCTOR_LATITUDE = "Latitude";
    public static final String COL_DOCTOR_LONGITUDE = "Longitude";

    //TABLE PATIENT
    public static final String TABLE_PATIENT = "Patient";
    public static final String COL_PATIENT_ID = "Id";
    public static final String COL_PATIENT_LASTNAME = "Last_Name";
    public static final String COL_PATIENT_FIRSTNAME = "First_Name";
    public static final String COL_PATIENT_EMAIL = "Email";
    public static final String COL_PATIENT_PASSWORD = "Password";
    public static final String COL_PATIENT_GENDER = "Gender";
    public static final String COL_PATIENT_TELEPHONE = "Telephone";
    public static final String COL_PATIENT_BIRTHDAY = "Birthday";
    public static final String COL_PATIENT_CREATEAT = "Create_At";
    public static final String COL_PATIENT_UPDATEAT = "Update_At";

    //TABLE APPOINTMENT
    public static final String TABLE_APPOINTMENT = "Appointment";
    public static final String COL_APPOINTMENT_ID = "Id";
    public static final String COL_APPOINTMENT_IDDOCTOR = "Id_Doctor";
    public static final String COL_APPOINTMENT_IDPATIENT = "Id_Patient";
    public static final String COL_APPOINTMENT_DOC_LASTNAME = "lastNameDoctor";
    public static final String COL_APPOINTMENT_DOC_FIRSTNAME = "firstNameDoctor";
    public static final String COL_APPOINTMENT_SPECIALITY = "speciality";
    public static final String COL_APPOINTMENT_PLASTNAME = "Patient_Last_Name";
    public static final String COL_APPOINTMENT_PFIRSTNAME = "Patient_First_Name";
    public static final String COL_APPOINTMENT_APPOINTMENT_DATE = "Appointment_Date";
    public static final String COL_APPOINTMENT_CREATEAT = "Create_At";
    public static final String COL_APPOINTMENT_PGENDER = "Patient_Gender";
    public static final String COL_APPOINTMENT_VALIDATION = "Validation";

    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE table " + TABLE_SPECIALITY + "("
                + COL_SPECIALITY_ID + " TEXT PRIMARY KEY,"
                + COL_SPECIALITY_NAME + " TEXT,"
                + COL_SPECIALITY_DESCRIPTION + " TEXT)");

        sqLiteDatabase.execSQL("CREATE table " + TABLE_DOCTOR + " ("
                + COL_DOCTOR_ID + " TEXT PRIMARY KEY,"
                + COL_DOCTOR_FIRSTNAME + " TEXT, "
                + COL_DOCTOR_LASTNAME + " TEXT, "
                + COL_DOCTOR_SPECIALITY_ID + " TEXT, "
                + COL_DOCTOR_SPECIALITY + " TEXT, "
                + COL_DOCTOR_GENDER + " TEXT,"
                + COL_DOCTOR_TELEPHONE + " TEXT, "
                + COL_DOCTOR_ADRESSE + " TEXT,"
                + COL_DOCTOR_CITY + " TEXT,"
                + COL_DOCTOR_TOWN + " TEXT,"
                + COL_DOCTOR_TARIF + " TEXT,"
                + COL_DOCTOR_EMAIL + " TEXT,"
                + COL_DOCTOR_LATITUDE + " NUMBER,"
                + COL_DOCTOR_LONGITUDE + " NUMBER)"
        );

        sqLiteDatabase.execSQL("CREATE table " + TABLE_PATIENT + " ("
                + COL_PATIENT_ID + " TEXT PRIMARY KEY, "
                + COL_PATIENT_FIRSTNAME + " TEXT,"
                + COL_DOCTOR_LASTNAME + " TEXT,"
                + COL_PATIENT_EMAIL + " TEXT,"
                + COL_PATIENT_PASSWORD + " TEXT,"
                + COL_PATIENT_GENDER + " TEXT,"
                + COL_PATIENT_TELEPHONE + " TEXT,"
                + COL_PATIENT_BIRTHDAY + " DATE,"
                + COL_PATIENT_CREATEAT + " DATE,"
                + COL_PATIENT_UPDATEAT + " DATE)"
        );

        sqLiteDatabase.execSQL("CREATE table " + TABLE_APPOINTMENT + " ("
                + COL_APPOINTMENT_ID + " TEXT PRIMARY KEY, "
                + COL_APPOINTMENT_IDDOCTOR + " INTEGER,"
                + COL_APPOINTMENT_IDPATIENT + " INTEGER,"
                + COL_APPOINTMENT_DOC_FIRSTNAME + " TEXT,"
                + COL_APPOINTMENT_DOC_LASTNAME + " TEXT,"
                + COL_APPOINTMENT_SPECIALITY + " TEXT,"
                + COL_APPOINTMENT_PFIRSTNAME + " TEXT,"
                + COL_APPOINTMENT_PLASTNAME + " TEXT,"
                + COL_APPOINTMENT_APPOINTMENT_DATE + " DATE,"
                + COL_APPOINTMENT_CREATEAT + " DATE,"
                + COL_APPOINTMENT_PGENDER + " TEXTE,"
                + COL_APPOINTMENT_VALIDATION + " INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PATIENT);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SPECIALITY);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCTOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_APPOINTMENT);
        onCreate(sqLiteDatabase);
    }

    public Context getContext() {
        return context;
    }
}
