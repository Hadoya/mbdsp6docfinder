package com.mbds.tpt.docfinder;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.mbds.tpt.docfinder.dialog.LoadingDialog;
import com.mbds.tpt.docfinder.model.Patient;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private MaterialButton loginBtn;
    private TextView emailView, passwordView, skipView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        initListeners();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.login_btn:
                login();
                break;
            case R.id.skip_view:
                logOut();
                getDataService().deleteAllAppointment();
                startMainActivity();
                break;
            default:
                break;
        }
    }

    private void initViews(){
        loginBtn = findViewById(R.id.login_btn);
        emailView = findViewById(R.id.email_input);
        passwordView = findViewById(R.id.pwd_input);
        skipView = findViewById(R.id.skip_view);
    }

    private void initListeners(){
        skipView.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
    }

    private void login(){
        if(BaseUtil.isNetworkAvailable(this)) {
            final LoadingDialog ld = new LoadingDialog(this, null);
            ld.setCancelable(false);
            ld.show();
            getDataService().authentificate(emailView.getText().toString(), passwordView.getText().toString(), new DataService.VolleyCallback() {
                @Override
                public void onSuccess(Object object) {
                    if (((Patient) object) != null) {
                        getDataService().updateAppointment(((Patient) object).getId(),null);
                        startMainActivity();
                    } else {
                        popupOups(BaseUtil.AUTH_FAILED,true);
                    }
                    ld.dismiss();
                }

                @Override
                public void onError(String error) {
                    ld.dismiss();
                    popupOups(error,true);
                }
            });
        }else{
            popupOupsNoNetwork();
        }
    }

    private void startMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}