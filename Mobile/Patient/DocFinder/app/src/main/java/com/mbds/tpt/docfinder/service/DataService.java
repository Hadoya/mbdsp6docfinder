package com.mbds.tpt.docfinder.service;

import android.app.Activity;
import android.content.Context;

import com.android.volley.Request;
import com.mbds.tpt.docfinder.dao.AppointmentDao;
import com.mbds.tpt.docfinder.dao.DoctorDao;
import com.mbds.tpt.docfinder.dao.PatientDao;
import com.mbds.tpt.docfinder.dao.SpecialityDao;
import com.mbds.tpt.docfinder.db.DbHelper;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.model.Patient;
import com.mbds.tpt.docfinder.model.Speciality;
import com.mbds.tpt.docfinder.util.BaseUtil;
import com.mbds.tpt.docfinder.util.ModelUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataService {

    //private static final String API_URL = "http://192.168.43.31:5000";
    private static final String API_URL = "https://docfinder-backend.herokuapp.com";
    private static final String APPOINTMENT_URL = API_URL + "/appointment";
    private static final String APPOINTMENT_ADD_URL = API_URL + "/appointment/add";
    private static final String APPOINTMENT_DELETE_URL = API_URL + "/appointment/delete";
    private static final String APPOINTMENT_BY_IDPATIENT_URL = APPOINTMENT_URL + "/searchByIdpatient";
    private static final String DOCTOR_URL = API_URL + "/doctor";
    private static final String PATIENT_URL = API_URL + "/patient";
    private static final String PATIENT_AUTH_URL = PATIENT_URL + "/login";
    private static final String SPECIALITY_URL = API_URL + "/doctorspeciality";

    private Activity activity;
    private DbHelper dbHelper;
    private AppointmentDao appointmentDao;
    private DoctorDao doctorDao;
    private PatientDao patientDao;
    private SpecialityDao specialityDao;

    public interface VolleyCallback {
        void onSuccess(Object object);

        void onError(String error);
    }

    public DataService(Activity activity) {
        this.activity = activity;
        this.dbHelper = new DbHelper(activity);
        doctorDao = new DoctorDao(dbHelper, activity);
        patientDao = new PatientDao(dbHelper, activity);
        appointmentDao = new AppointmentDao(dbHelper, activity);
        specialityDao = new SpecialityDao(dbHelper, activity);
    }

    public void authentificate(String email, String password, final VolleyCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        patientDao.sendRequest(Request.Method.POST, PATIENT_AUTH_URL, params, new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                String string = (String) object;
                if (string.compareToIgnoreCase("null") == 0) {
                    callback.onError(BaseUtil.AUTH_FAILED);
                } else {
                    try {
                        Patient patient = ModelUtil.JSONObjectToPatient(new JSONObject(string));
                        if (patient != null) {
                            patientDao.deleteAllPatients();
                            patientDao.addPatient(patient);
                            callback.onSuccess(patient);
                        } else {
                            callback.onError(BaseUtil.AUTH_FAILED);
                        }
                    } catch (JSONException | NullPointerException e) {
                        BaseUtil.logException("authentificateActivity", e);
                        callback.onError(BaseUtil.AUTH_FAILED);
                    } catch (Exception e) {
                        callback.onError(e.getMessage());
                    }
                }

            }

            @Override
            public void onError(String error) {
                callback.onError(error);
            }
        });
    }

    public void cancelAppointmentRequest(final String idPatient, String id, final VolleyCallback callback){
        appointmentDao.sendRequest(Request.Method.GET, APPOINTMENT_DELETE_URL + "/" + id, null, new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                updateAppointment(idPatient,callback);
            }

            @Override
            public void onError(String error) {
                if(callback != null)
                    callback.onError(error);
            }
        });
    }

    public void cleanDatabase(){
        doctorDao.deleteAllDoctors();
        specialityDao.deleteAllSpecialities();
        deleteAllAppointment();
    }

    public void deleteAllAppointment(){
        appointmentDao.deleteAllAppointment();
    }

    private List<Appointment> getAppointmentByValidation(List<Appointment> appointmentList, int validation) {
        List<Appointment> res = new ArrayList<>();
        for (Appointment appointment : appointmentList) {
            if (appointment.getValidation() == validation)
                res.add(appointment);
        }
        return res;
    }

    public List<Appointment> getAcceptedAppointment(List<Appointment> appointmentList) {
        return getAppointmentByValidation(appointmentList, 1);
    }

    public List<Appointment> getPendingAppointment(List<Appointment> appointmentList) {
        return getAppointmentByValidation(appointmentList, 0);
    }

    public List<Appointment> getAllAcceptedAppointment() {
        return appointmentDao.getAppointmentListByValidation("1");
    }

    public List<Appointment> getAllPengindAppointment() {
        return appointmentDao.getAppointmentListByValidation("0");
    }

    public List<Speciality> getAllSpecialities() {
        return specialityDao.getAllSpecialities();
    }

    public List<Doctor> getAllDoctors() {
        return doctorDao.getAllDoctors();
    }

    public List<Patient> getAllPatient() {
        return patientDao.getAllPatients();
    }

    public Patient getDefaultPatient() {
        return patientDao.getFirstPatient();
    }

    public Doctor getDoctorById(String id) {
        return doctorDao.getDoctorById(id);
    }

    public void logOut() {
        BaseUtil.log("logout");
        try {
            patientDao.deleteAllPatients();
            printAllDataTest();
        } catch (Exception e) {
            BaseUtil.logException("logOut", e);
        }
    }

    public void updateAppointment(String idPatient, final VolleyCallback callback) {
        appointmentDao.sendRequest(Request.Method.GET, APPOINTMENT_BY_IDPATIENT_URL + "/" + idPatient, null, new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                try {
                    List<Appointment> appointmentList = ModelUtil.JSONArrayToAppointment(new JSONArray((String) object));
                    if (appointmentList != null && !appointmentList.isEmpty()) {
                        appointmentDao.deleteAllAppointment();
                        appointmentDao.addAppointment(appointmentList);
                    }
                    if (callback != null)
                        callback.onSuccess(appointmentList);
                } catch (Exception e) {
                    if (callback != null)
                        callback.onError(e.getMessage());
                }
            }

            @Override
            public void onError(String error) {
                if (callback != null)
                    callback.onError(error);
            }
        });
    }

    public void updateDoctor(final VolleyCallback callback, boolean andSpeciality) {
        doctorDao.sendRequest(Request.Method.GET, DOCTOR_URL, null, new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                try {
                    List<Doctor> doctorList = ModelUtil.JSONArrayToDoctor(new JSONArray((String) object));
                    if (doctorList != null && !doctorList.isEmpty()) {
                        doctorDao.deleteAllDoctors();
                        doctorDao.addDoctor(doctorList);
                    }
                    if (callback != null)
                        callback.onSuccess(doctorList);
                } catch (Exception e) {
                    if (callback != null)
                        callback.onError(e.getMessage());
                }
            }

            @Override
            public void onError(String error) {
                if (callback != null)
                    callback.onError(error);
            }
        });

        if (andSpeciality) {
            updateSpeciality(new VolleyCallback() {
                @Override
                public void onSuccess(Object object) {

                }

                @Override
                public void onError(String error) {

                }
            });
        }
    }

    public void updateSpeciality(final VolleyCallback callback) {
        doctorDao.sendRequest(Request.Method.GET, SPECIALITY_URL, null, new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                try {
                    List<Speciality> specialityList = ModelUtil.JSONArrayToSpeciality(new JSONArray((String) object));
                    if (specialityList != null && !specialityList.isEmpty()) {
                        specialityDao.deleteAllSpecialities();
                        specialityDao.addSpeciality(specialityList);
                    }
                    if (callback != null)
                        callback.onSuccess(specialityList);
                } catch (Exception e) {
                    if (callback != null)
                        callback.onError(e.getMessage());
                }
            }

            @Override
            public void onError(String error) {
                if (callback != null)
                    callback.onError(error);
            }
        });
    }

    public void sendAppointmentRequest(final Appointment appointment, final VolleyCallback callback) {
        Map<String, String> params = ModelUtil.appointmentToParams(appointment);
        appointmentDao.sendRequest(Request.Method.POST, APPOINTMENT_ADD_URL , params, new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                try {
                    appointment.setId((String) object);
                    appointmentDao.addAppointment(appointment);
                } catch (Exception e) {
                    BaseUtil.log("erreur sendRequest:"+e.getMessage());
                    updateAppointment(appointment.getIdPatient(), null);
                }
                if (callback != null)
                    callback.onSuccess(appointment);
            }

            @Override
            public void onError(String error) {
                if (callback != null)
                    callback.onError(error);
            }
        });
    }

    public void createDataTest() {
        try {
            //---- SPECIALITY ----
            specialityDao.deleteAllSpecialities();
            specialityDao.addSpeciality(new Speciality("1", "Osteopathe", "Docteur spécialiste en ostéopathie"));
            specialityDao.addSpeciality(new Speciality("2", "Homeopathe", "Docteur spécialiste en homéopathie"));
            specialityDao.addSpeciality(new Speciality("3", "Dantiste", "Docteur spécialiste dans le soin dantaire"));
            specialityDao.addSpeciality(new Speciality("4", "Ophtalmologue", "Docteur spécialiste dans le soin de l'oeil"));

            //---- DOCTOR -----
            doctorDao.deleteAllDoctors();
            /*
            List<Speciality> specialities = getAllSpecialities();
            doctorDao.addDoctor(new Doctor("1", "1", "Rakoto", "Hernest", "0332424388", "rakoto@gmail.com", "m", "89 A bis", "Antananarivo", "Ikianja", 1, "", -18.893950920315536, 47.58234769105911, specialities.get(0).getName())); // ambohimangakely
            doctorDao.addDoctor(new Doctor("2", "2", "Fanja", "Rabemananjara", "0330628541", "fanja@gmail.com", "w", "22 A bis", "Antananarivo", "Ikianja", 1, "", -18.90622106798154, 47.57052518427372, specialities.get(1))); // ambatomaro
            doctorDao.addDoctor(new Doctor("3", "3", "Hervé", "Jean", "0327724231", "jeanherve@gmail.com", "m", "89 B Ter", "Antananarivo", "Ambohimangakely", 1, "", -18.910036883799567, 47.58103977888823, specialities.get(2))); // AMBOHIMANGAKELY
            doctorDao.addDoctor(new Doctor("4", "4", "Mark", "Rodrigez", "0344627390", "rodrigez@gmail.com", "m", "22 AE Bis", "Antananarivo", "Ankatso", 1, "", -18.915014010935103, 47.562712244689465, specialities.get(3)));// ANKATSO
            doctorDao.addDoctor(new Doctor("5", "1", "Tovo", "Razaka", "0334821301", "tovo@gmail.com", "m", "62 Z bis", "Antananarivo", "Tanjombato", 1, "", -18.95943937153844, 47.531601302325726, specialities.get(0)));// TANJOMBATO
            doctorDao.addDoctor(new Doctor("6", "2", "Lydia", "Rovasoa", "0331124331", "rovasoalydia@gmail.com", "w", "22 BE bis", "Antananarivo", "Andoharanofotsy", 1, "", -18.9880835010875, 47.53472942858935, specialities.get(1)));//androharanofotsy près ITU
            doctorDao.addDoctor(new Doctor("7", "3", "Celine", "Rianala", "0332624111", "celine@gmail.com", "w", "IC 303", "Antananarivo", "Andoharanofotsy", 1, "", -18.987610804956997, 47.52858750522137, specialities.get(2)));//andoharanofotsy près ITU
            doctorDao.addDoctor(new Doctor("8", "4", "Fred", "Rakoto", "0343224379", "fredrakoto@gmail.com", "m", "IB A 250", "Antananarivo", "Andoharanofotsy", 1, "", -18.96822562081143, 47.52800714224576, specialities.get(3)));//andoharanofotsy près Ekar Malaza
            doctorDao.addDoctor(new Doctor("9", "1", "Sophia", "Honorine", "0320644344", "sophia@gmail.com", "w", "MB 100", "Antananarivo", "Andoharanofotsy", 1, "", -18.97075328600753, 47.5338713336133, specialities.get(0)));//andoharanofotsy près Akany Karmela
            doctorDao.addDoctor(new Doctor("10", "2", "Lorie", "Razakason", "0336624031", "lorie@gmail.com", "w", "II N 51 A", "Antananarivo", "Analamahintsy", 1, "", -18.873746513958515, 47.54557393491268, specialities.get(1)));//ANALAMAHINTSY près dIVAGEL ET GASTRO PIZZA
            doctorDao.addDoctor(new Doctor("11", "3", "Bernard", "Rabezavana", "0333624333", "bernard@gmail.com", "m", "IIN 103 B", "Antananarivo", "Analamahintsy", 1, "", -18.870736423067186, 47.53962446004152, specialities.get(2)));//ANALAMAHINTSY près RUSSIAN EMBASSY
            doctorDao.addDoctor(new Doctor("12", "4", "Claris", "Rivolala", "0323625343", "rivolalalal@gmail.com", "w", "IIN 107 B", "Antananarivo", "Analamahintsy", 1, "", -18.862726433067186, 47.52182446804152, specialities.get(3)));//ANALAMAHINTSY près EPP Soavimasoandro
            doctorDao.addDoctor(new Doctor("13", "1", "Fitia", "Randrianjafy", "0323721338", "mitiarandri@gmail.com", "w", "IIN 107 B", "Antananarivo", "Ambohimalaza", 1, "", -18.860726433067186, 47.57182448804152, specialities.get(0)));//ANALAMAHINTSY près EPP Soavimasoandro
*/
            //---- PATIENT
            //patientDao.deleteAllPatients();
            //patientDao.addPatient(new Patient("5", "Louise", "Eva", "0330624331", "w", new Date(1991, 4, 17), "louise.eva@yahoo.com", "123", new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())));
            //patientDao.addPatient(getDefaultPatient());

            //--- APPOINTMENT
            appointmentDao.deleteAllAppointment();
            /*List<Appointment> appointmentList = new ArrayList<>();
            appointmentList.add(new Appointment("1", "5ee705a87af8a0057c3fe91d", "1", new Date(System.currentTimeMillis()), "Finaritra", "Tsiky", new Date(System.currentTimeMillis()), "w", 1));
            appointmentList.add(new Appointment("2", "5ee705a87af8a0057c3fe91d", "2", new Date(System.currentTimeMillis()), "Finaritra", "Tsiky", new Date(System.currentTimeMillis()), "w", 1));
            appointmentDao.addAppointment(appointmentList);*/

            BaseUtil.logSuccess("createDataTest", "Success");
            //printAllDataTest();
            BaseUtil.log("\n");
        } catch (Exception e) {
            BaseUtil.logException("createDataTest", e);
        }
    }


    public void printAllDataTest() {
        List<Speciality> specialities = getAllSpecialities();
        List<Doctor> doctors = getAllDoctors();
        List<Patient> patientList = getAllPatient();
        BaseUtil.log("-----SPECIALITIES----");
        for (Speciality speciality : specialities)
            BaseUtil.log(speciality.toString());
        BaseUtil.log("-----DOCTORS----");
        for (Doctor doctor : doctors)
            BaseUtil.log(doctor.toString());
        BaseUtil.log("-----PATIENT----");
        for (Patient patient : patientList)
            BaseUtil.log(patient.toString());
    }
}
