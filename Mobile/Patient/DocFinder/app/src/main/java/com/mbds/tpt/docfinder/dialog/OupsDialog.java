package com.mbds.tpt.docfinder.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mbds.tpt.docfinder.R;

import androidx.annotation.NonNull;

public class OupsDialog extends Dialog implements View.OnClickListener {
    private String msg;
    private TextView msgView;
    private Button okBtn;

    public OupsDialog(@NonNull Context context, String msg) {
        super(context);
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_oups);
        initViews();
        okBtn.setOnClickListener(this);
        msgView.setText(msg);
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }

    private void initViews(){
        msgView = findViewById(R.id.msg_view);
        okBtn = findViewById(R.id.ok_btn);
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
