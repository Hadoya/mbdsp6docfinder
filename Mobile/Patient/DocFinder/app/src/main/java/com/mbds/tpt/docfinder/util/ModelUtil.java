package com.mbds.tpt.docfinder.util;

import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.model.Patient;
import com.mbds.tpt.docfinder.model.Speciality;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelUtil extends BaseUtil {

    public static Map<String, String> appointmentToParams(Appointment appointment) {
        Map<String, String> params = new HashMap<>();
        params.put("idDoctor", appointment.getIdDoctor());
        params.put("idPatient", appointment.getIdPatient());
        params.put("lastNamePatient", appointment.getLastNamePatient());
        params.put("firstNamePatient", appointment.getFirstNamePatient());
        params.put("gender", appointment.getPatientGender());
        params.put("validation", String.valueOf(appointment.getValidation()));
        return params;
    }

    public static Appointment JSONObjectToAppointment(JSONObject object) throws JSONException {
        try {
            Appointment appointment = new Appointment();
            appointment.setId(object.getString("_id"));
            appointment.setIdDoctor(object.getString("idDoctor"));
            appointment.setIdPatient(object.getString("idPatient"));
            appointment.setFirstNameDoctor(object.getString("firstNameDoctor"));
            appointment.setLastNameDoctor(object.getString("lastNameDoctor"));
            appointment.setDoctorSpeciality(object.getString("speciality"));
            appointment.setLastNamePatient(object.getString("lastNamePatient"));
            appointment.setFirstNamePatient(object.getString("firstNamePatient"));
            appointment.setPatientGender(object.getString("gender"));
            appointment.setDate(BaseUtil.getMongoDbDate(object.getString("appointmentDate")));
            appointment.setCreatedAt(BaseUtil.getMongoDbDate(object.getString("createdAt")));
            appointment.setValidation(object.getInt("validation"));
            return  appointment;
        } catch (JSONException e) {
            BaseUtil.logException("JSONObjectToAppointment", e);
            throw e;
        }
    }

    public static List<Appointment> JSONArrayToAppointment(JSONArray array) throws JSONException {
        List<Appointment> appointmentList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                appointmentList.add(JSONObjectToAppointment(array.getJSONObject(i)));
            }catch (Exception e){
                continue;
            }
        }
        return appointmentList;
    }

    public static Patient JSONObjectToPatient(JSONObject object) throws JSONException {
        try {
            Patient patient = new Patient();
            patient.setId(object.getString("_id"));
            patient.setLastName(object.getString("lastname"));
            patient.setFirstName(object.getString("firstname"));
            patient.setGender(object.getString("gender"));
            patient.setEmail(object.getString("email"));
            //patient.setPassword(object.getString("password"));
            //patient.setTelephone(object.getString("telephone"));
            //patient.setBirthday(BaseUtil.getMongoDbDate(object.getString("birthdate")));
            //patient.setCreatedAt(BaseUtil.getMongoDbDate(object.getString("createdAt")));
            //patient.setUpdatedAt(BaseUtil.getMongoDbDate(object.getString("updatedAt")));
            return patient;
        } catch (JSONException e) {
            BaseUtil.logException("JSONObjectToPatient", e);
            throw e;
        }
    }

    public static List<Patient> JSONArrayToPatient(JSONArray array) throws JSONException {
        List<Patient> patients = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                patients.add(JSONObjectToPatient(array.getJSONObject(i)));
            }catch (Exception e){
                continue;
            }
        }
        return patients;
    }

    public static List<Doctor> JSONArrayToDoctor(JSONArray array) throws JSONException {
        List<Doctor> doctorList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                doctorList.add(JSONObjectToDoctor(array.getJSONObject(i)));
            }catch (Exception e){
                continue;
            }
        }
        return doctorList;
    }

    public static Doctor JSONObjectToDoctor(JSONObject object) throws JSONException {
        try {
            Doctor doc = new Doctor();
            doc.setId(object.getString("_id"));
            doc.setLastName(object.getString("lastname"));
            doc.setFirstName(object.getString("firstname"));
            doc.setIdSpeciality(object.getString("idDoctorSpeciality"));
            doc.setSpeciality(object.getString("speciality"));
            doc.setTelephone(object.getString("telephone"));
            doc.setEmail(object.getString("email"));
            doc.setGender(object.getString("gender"));
            doc.setAddress(object.getString("address"));
            doc.setTown(object.getString("town"));
            doc.setCity(object.getString("city"));
            doc.setTarif(object.getString("tarif"));
            doc.setLatitude(object.getDouble("latitude"));
            doc.setLongitude(object.getDouble("longitude"));
            //doc.setDescription(object.getString("description"));
            //doc.setValidationState(object.getInt("etatValidation"));
            //doc.setCreatedAt(BaseUtil.getMongoDbDate(object.getString("createdAt")));
            //doc.setUpdatedAt(BaseUtil.getMongoDbDate(object.getString("updatedAt")));
            return doc;
        } catch (JSONException e) {
            BaseUtil.logException("JSONObjectToDoctor", e);
            throw e;
        }
    }

    public static List<Speciality> JSONArrayToSpeciality(JSONArray array) throws JSONException {
        List<Speciality> specialityList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                specialityList.add(JSONObjectToSpeciality(array.getJSONObject(i)));
            }catch (Exception e){
                continue;
            }
        }
        return specialityList;
    }

    public static Speciality JSONObjectToSpeciality(JSONObject object) throws JSONException {
        try {
            Speciality speciality = new Speciality();
            speciality.setId(object.getString("_id"));
            speciality.setName(object.getString("name"));
            return speciality;
        } catch (JSONException e) {
            BaseUtil.logException("JSONObjectToSpeciality", e);
            throw e;
        }
    }

}
