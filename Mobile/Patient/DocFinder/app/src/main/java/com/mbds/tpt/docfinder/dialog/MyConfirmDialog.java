package com.mbds.tpt.docfinder.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.util.BaseUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class MyConfirmDialog extends DialogFragment implements View.OnClickListener {
    private String msg;
    private TextView msgView;
    private Button cancelBtn, confirmBtn;
    private MyConfirmDialogListener myConfirmDialogListener;

    public interface MyConfirmDialogListener{
        void onConfirmBtnClicked();
        void onCancelBtnClicked();
    }

    public MyConfirmDialog(String msg){
        this.msg = msg;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_confirm, container, false);
        initViews(view);
        initListeners();
        displayMessage();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            myConfirmDialogListener = (MyConfirmDialogListener) context;
        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement MyConfirmDialogListener");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_btn:
                myConfirmDialogListener.onConfirmBtnClicked();
                dismiss();
                break;
            default:
                myConfirmDialogListener.onCancelBtnClicked();
                dismiss();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog mDialog = getDialog();
        if (mDialog != null) {
            mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void displayMessage(){
        msgView.setText(msg);
    }

    private void initListeners() {
        confirmBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }

    private void initViews(View view) {
        msgView = view.findViewById(R.id.msg_view);
        cancelBtn = view.findViewById(R.id.cancel_btn);
        confirmBtn = view.findViewById(R.id.confirm_btn);
    }
}
