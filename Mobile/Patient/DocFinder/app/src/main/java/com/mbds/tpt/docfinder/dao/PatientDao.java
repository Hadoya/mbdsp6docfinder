package com.mbds.tpt.docfinder.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.mbds.tpt.docfinder.db.DbHelper;
import com.mbds.tpt.docfinder.model.Patient;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.ArrayList;
import java.util.List;

public class PatientDao extends BaseDao {
    public PatientDao(DbHelper dbHelper, Activity activity) {
        super(dbHelper, activity);
    }

    public void addPatient(Patient patient) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            putValues(values,patient);
            db.insert(dbHelper.TABLE_PATIENT, null, values);
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("addPatient",e);
            throw e;
        }
    }

    public void addPatient(List<Patient> patients) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (Patient patient : patients) {
                putValues(values,patient);
                db.insert(dbHelper.TABLE_PATIENT, null, values);
                values.clear();
            }
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("addPatient:List",e);
            throw e;
        }
    }

    public Patient getPatientById(String id) {
        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.query(dbHelper.TABLE_PATIENT,
                    getPatientCols(),
                    dbHelper.COL_PATIENT_ID + "=?",
                    new String[]{id},
                    null, null, null, null);
            if (cursor.moveToFirst()) {
                return getPatient(cursor);
            }
            return null;
        } catch (SQLException e) {
            BaseUtil.logException("getPatientById",e);
            throw e;
        }
    }

    public Patient getFirstPatient() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_PATIENT+" limit 1", null);
            Patient patient = null;
            if (cursor.moveToFirst()) {
                do {
                    patient = getPatient(cursor);
                    break;
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
            return patient;
        } catch (SQLException e) {
            BaseUtil.logException("getAllPatient",e);
            throw e;
        }
    }

    public List<Patient> getAllPatients() {
        try {
            List<Patient> patients = new ArrayList();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_PATIENT, null);
            if (cursor.moveToFirst()) {
                do {
                    Patient patient = getPatient(cursor);
                    patients.add(patient);
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
            return patients;
        } catch (SQLException e) {
            BaseUtil.logException("getAllPatient",e);
            throw e;
        }
    }

    public void deleteAllPatients() {
        try {
            BaseUtil.log("deleteAllPatient");
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_PATIENT, null, null);
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("deleteAllPatient",e);
            throw e;
        }
    }

    private String[] getPatientCols(){
        return new String[]{
                dbHelper.COL_PATIENT_ID,
                dbHelper.COL_PATIENT_FIRSTNAME,
                dbHelper.COL_PATIENT_LASTNAME,
                dbHelper.COL_PATIENT_EMAIL,
                dbHelper.COL_PATIENT_PASSWORD,
                dbHelper.COL_PATIENT_GENDER,
                dbHelper.COL_PATIENT_TELEPHONE
        };
    }

    private Patient getPatient(Cursor cursor){
        Patient patient = new Patient();
        patient.setId(cursor.getString(0));
        patient.setLastName(cursor.getString(1));
        patient.setFirstName(cursor.getString(2));
        patient.setEmail(cursor.getString(3));
        patient.setPassword(cursor.getString(4));
        patient.setGender(cursor.getString(5));
        patient.setTelephone(cursor.getString(6));
        //patient.setBirthday(BaseUtil.strToDate(cursor.getString(7)));
        return patient;
    }

    private void putValues(ContentValues values, Patient patient){
        values.put(dbHelper.COL_PATIENT_ID, patient.getId());
        values.put(dbHelper.COL_PATIENT_FIRSTNAME, patient.getFirstName());
        values.put(dbHelper.COL_PATIENT_LASTNAME, patient.getLastName());
        values.put(dbHelper.COL_PATIENT_EMAIL, patient.getEmail());
        values.put(dbHelper.COL_PATIENT_GENDER, patient.getGender());
        values.put(dbHelper.COL_PATIENT_TELEPHONE, patient.getTelephone());
        //values.put(dbHelper.COL_PATIENT_BIRTHDAY, BaseUtil.dateToString(patient.getBirthday()));
    }
}
