package com.mbds.tpt.docfinder.model;

import java.util.Date;

public class Appointment {
    private String id;
    private String idPatient;
    private String idDoctor;
    private String lastNameDoctor;
    private String firstNameDoctor;
    private String lastNamePatient;
    private String firstNamePatient;
    private String doctorSpeciality;
    private Date date;
    private Date createdAt;
    private String patientGender;
    private int validation;

    public Appointment(){

    }

    public Appointment(String id, String idPatient, String idDoctor, Date date, String lastNamePatient, String firstNamePatient, Date createdAt, String patientGender, int validation) {
        this.id = id;
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.date = date;
        this.lastNamePatient = lastNamePatient;
        this.firstNamePatient = firstNamePatient;
        this.createdAt = createdAt;
        this.patientGender = patientGender;
        this.validation = validation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(String idPatient) {
        this.idPatient = idPatient;
    }

    public String getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(String idDoctor) {
        this.idDoctor = idDoctor;
    }

    public Date getDate() {
        return date==null?new Date(System.currentTimeMillis()):date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLastNamePatient() {
        return lastNamePatient;
    }

    public void setLastNamePatient(String lastNamePatient) {
        this.lastNamePatient = lastNamePatient;
    }

    public String getFirstNamePatient() {
        return firstNamePatient;
    }

    public void setFirstNamePatient(String firstNamePatient) {
        this.firstNamePatient = firstNamePatient;
    }

    public String getLastNameDoctor() {
        return lastNameDoctor;
    }

    public void setLastNameDoctor(String lastNameDoctor) {
        this.lastNameDoctor = lastNameDoctor;
    }

    public String getFirstNameDoctor() {
        return firstNameDoctor;
    }

    public void setFirstNameDoctor(String firstNameDoctor) {
        this.firstNameDoctor = firstNameDoctor;
    }

    public String getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(String doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public int getValidation() {
        return validation;
    }

    public void setValidation(int validation) {
        this.validation = validation;
    }

    public String toString(){
        return "{firstNameDoctor:"+firstNameDoctor+
                ", lastNameDoctor:"+lastNameDoctor+
                ", speciality:"+doctorSpeciality+
                ", lastNamePatient:"+lastNamePatient+
                ", firstNamePatient:"+firstNamePatient+
                ", validation:"+validation+"}";
    }
}
