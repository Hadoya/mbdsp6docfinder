package com.mbds.tpt.docfinder.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mbds.tpt.docfinder.AboutDoctorActivity;
import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;
import com.mbds.tpt.docfinder.util.CallUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class AppointmentDetailDialog extends DialogFragment implements View.OnClickListener {
    private Activity activity;
    private DataService dataService;
    private Appointment appointment;
    private Doctor doctor;
    private TextView timeView, dateView, doctorNameView;
    private FloatingActionButton callBtn, msgBtn, deleteBtn, doctorBtn;
    private AppointmentDetailDialogListener appointmentDetailDialogListener;

    public interface AppointmentDetailDialogListener{
        void onAboutDoctorBtnClicked();
        void onCallBtnClicked();
        void onSmsBtnClicked();
        void onDeleteBtnClicked();
    }

    public AppointmentDetailDialog(Doctor doctor, Appointment appointment) {
        this.activity = activity;
        this.dataService = dataService;
        this.appointment = appointment;
        this.doctor = doctor;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_appointment_detail, container, false);
        initViews(view);
        initListeners();
        displayAppointment();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            appointmentDetailDialogListener = (AppointmentDetailDialogListener) context;
        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement AppointmentDetailDialogListener");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.call_btn:
                appointmentDetailDialogListener.onCallBtnClicked();
                break;
            case R.id.sms_btn:
                appointmentDetailDialogListener.onSmsBtnClicked();
                break;
            case R.id.delete_btn:
                dismiss();
                appointmentDetailDialogListener.onDeleteBtnClicked();
                break;
            case R.id.doctor_btn:
                appointmentDetailDialogListener.onAboutDoctorBtnClicked();
                break;
            default:
                dismiss();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog mDialog = getDialog();
        if (mDialog != null) {
            mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void displayAppointment(){
        if(appointment.getValidation() == 1) {
            timeView.setText(BaseUtil.dateToStringGetTime(appointment.getDate()));
            dateView.setText(BaseUtil.dateToStringGetDate(appointment.getDate()));
        }
        doctorNameView.setText("Dr. "+doctor.getLastName()+" "+doctor.getFirstName());
    }

    private void initListeners(){
        callBtn.setOnClickListener(this);
        msgBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
        doctorBtn.setOnClickListener(this);
    }

    private void initViews(View view){
        timeView = view.findViewById(R.id.time_view);
        dateView = view.findViewById(R.id.date_view);
        doctorNameView = view.findViewById(R.id.doctorName_view);
        callBtn = view.findViewById(R.id.call_btn);
        msgBtn = view.findViewById(R.id.sms_btn);
        deleteBtn = view.findViewById(R.id.delete_btn);
        doctorBtn = view.findViewById(R.id.doctor_btn);

        if(appointment.getValidation() == 0)
            deleteBtn.setVisibility(View.VISIBLE);
    }
}
