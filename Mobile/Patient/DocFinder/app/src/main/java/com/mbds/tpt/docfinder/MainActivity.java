package com.mbds.tpt.docfinder;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mbds.tpt.docfinder.dialog.MyConfirmDialog;
import com.mbds.tpt.docfinder.util.BaseUtil;

public class MainActivity extends BaseActivity implements View.OnClickListener, MyConfirmDialog.MyConfirmDialogListener {
    private FloatingActionButton logoutBtn;
    private MaterialCardView mapsView;
    private MaterialCardView doctorListView;
    private MaterialCardView appointmentListView;
    private MaterialCardView appointmentPendingView;
    private TextView firstNameView, lastNameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_main);
            initViews();
            initListeners();
            displayUserInfos();
        } catch (Exception e) {
            BaseUtil.logException("onCreate Main", e);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.map_view:
                startMapsActivity();
                break;
            case R.id.doctor_view:
                startDoctorListActivity();
                break;
            case R.id.appointment_view:
                startAppointmentListActivity();
                break;
            case R.id.pending_view:
                startAppointmentPendingActivity();
                break;
            case R.id.logout_btn:
                confirmLogout();
                break;
            default:
                break;
        }
    }

    @Override
    public void onConfirmBtnClicked() {
        startLoginActivity();
    }

    @Override
    public void onCancelBtnClicked() {
    }

    private void confirmLogout(){
        if(getPatient() == null){
            startLoginActivity();
        }else{
            showConfirmDialog(getResources().getString(R.string.sure_to_disconnect));
        }
    }

    private void displayUserInfos() {
        if (getPatient() != null) {
            firstNameView.setText(getPatient().getFirstName());
            lastNameView.setText(getPatient().getLastName());
        }
    }

    private void initViews() {
        mapsView = findViewById(R.id.map_view);
        doctorListView = findViewById(R.id.doctor_view);
        appointmentListView = findViewById(R.id.appointment_view);
        appointmentPendingView = findViewById(R.id.pending_view);
        firstNameView = findViewById(R.id.firstname_view);
        lastNameView = findViewById(R.id.lastname_view);
        logoutBtn = findViewById(R.id.logout_btn);
    }

    private void initListeners() {
        mapsView.setOnClickListener(this);
        doctorListView.setOnClickListener(this);
        appointmentListView.setOnClickListener(this);
        appointmentPendingView.setOnClickListener(this);
        logoutBtn.setOnClickListener(this);
    }

    private void startMapsActivity() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    private void startDoctorListActivity() {
        Intent intent = new Intent(this, DoctorListActivity.class);
        startActivity(intent);
    }

    private void startAppointmentListActivity() {
        if (getPatient() != null) {
            Intent intent = new Intent(this, AppointmentListActivity.class);
            startActivity(intent);
        } else {
            popupOups(getResources().getString(R.string.you_need_login), true);
        }
    }

    private void startAppointmentPendingActivity() {
        if (getPatient() != null) {
            Intent intent = new Intent(this, AppointmentPendingListActivity.class);
            startActivity(intent);
        } else {
            popupOups(getResources().getString(R.string.you_need_login), true);
        }
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}