package com.mbds.tpt.docfinder.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Toast;

import com.mbds.tpt.docfinder.R;

import androidx.appcompat.widget.AppCompatTextView;

public class DocFinderTextView extends AppCompatTextView {

    public DocFinderTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyStyle(context, attrs);
    }

    public DocFinderTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyStyle(context, attrs);
    }

    private void applyStyle(Context context, AttributeSet attrs) {
        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DocFinderTextView);
            int cf = a.getInteger(R.styleable.DocFinderTextView_fontName, 0);
            int fontName;
            switch (cf) {
                case 1:
                    fontName = R.string.Roboto_Black;
                    break;
                case 2:
                    fontName = R.string.Roboto_Bold;
                    break;
                case 3:
                    fontName = R.string.Roboto_Light;
                    break;
                case 4:
                    fontName = R.string.Roboto_Medium;
                    break;
                case 5:
                    fontName = R.string.Roboto_Thin;
                    break;
                case 6:
                    fontName = R.string.Helvetica_Bold;
                    break;
                case 7:
                    fontName = R.string.Helvetica_Light;
                    break;
                case 8:
                    fontName = R.string.Helvetica_Medium;
                    break;
                case 9:
                    fontName = R.string.Helvetica_Roman;
                    break;
                case 10:
                    fontName = R.string.Helvetica_Thin;
                    break;
                case 11:
                    fontName = R.string.Helvetica_Ultra_Light;
                    break;
                case 12:
                    fontName = R.string.OpenSans_Light;
                    break;
                default:
                    fontName = R.string.Roboto_Medium;
                    break;
            }

            String customFont = getResources().getString(fontName);

            Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/" + customFont + ".ttf");
            setTypeface(tf);
            a.recycle();
        }catch (Exception e){
            Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }
}
