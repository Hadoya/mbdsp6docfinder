package com.mbds.tpt.docfinder;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.mbds.tpt.docfinder.dialog.AppointmentDetailDialog;
import com.mbds.tpt.docfinder.dialog.LoadingDialog;
import com.mbds.tpt.docfinder.dialog.MyConfirmDialog;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;
import com.mbds.tpt.docfinder.util.CallUtil;

import java.util.List;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class AppointmentPendingListActivity extends AppointmentListActivity implements AppointmentDetailDialog.AppointmentDetailDialogListener, MyConfirmDialog.MyConfirmDialogListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_list);
        configureToolbar(getResources().getString(R.string.pending_appointment));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initDefaultModels();
        initModels();
        initAdapters();
        initViews();
        initListeners();
        setListViewAnimation(listView);
    }

    @Override
    public void onDeleteBtnClicked() {
        showConfirmDialog(getResources().getString(R.string.sure_to_cancel_appointment));
    }

    @Override
    public void onConfirmBtnClicked() {
        final LoadingDialog loadingDialog = new LoadingDialog(this, getResources().getString(R.string.please_wait));
        loadingDialog.setCancelable(false);
        try {
            loadingDialog.show();
            getDataService().cancelAppointmentRequest(getPatient().getId(), selectedAppointment.getId(), new DataService.VolleyCallback() {
                @Override
                public void onSuccess(Object object) {
                    setAppointmentList(getDataService().getPendingAppointment((List<Appointment>)object));
                    loadingDialog.dismiss();
                    appointmentListAdapter.notifyDataSetChanged();
                }

                @Override
                public void onError(String error) {
                    loadingDialog.dismiss();
                    popupOups(error,true);
                }
            });
        }catch (Exception e){
            if(loadingDialog.isShowing())
                loadingDialog.dismiss();
            popupOups(e.getMessage(),true);
        }
    }

    @Override
    public void onCancelBtnClicked() {

    }

    protected void initModels() {
        appointmentValidation = 0;
        appointmentList = getDataService().getAllPengindAppointment();
    }

}
