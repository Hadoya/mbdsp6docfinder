package com.mbds.tpt.docfinder;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mbds.tpt.docfinder.adapter.DoctorListAdapter;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.Collections;
import java.util.List;

public class DoctorListActivity extends BaseActivity {
    private DoctorListAdapter doctorListAdapter;
    private List<Doctor> doctorList;

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);
        configureToolbar(getResources().getString(R.string.doctors));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initDefaultModels();
        try {
            initModels();
            initAdapters();
            initViews();
            initListeners();
            setListViewAnimation(listView);
        } catch (Exception e) {
            BaseUtil.toastException(this, e);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void initAdapters() {
        Collections.sort(doctorList);
        doctorListAdapter = new DoctorListAdapter(this, doctorList);
    }

    public void initListeners() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startAboutDoctorActivity(doctorList.get(i).getId());
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                BaseUtil.log("Mise à jour appointment");
                if (BaseUtil.isNetworkAvailable(DoctorListActivity.this)) {
                    BaseUtil.log("Connexion api ...");
                    try {
                        getDataService().updateDoctor(new DataService.VolleyCallback() {
                            @Override
                            public void onSuccess(Object object) {
                                BaseUtil.log("Maj éffectué");
                                stopRefreshing();
                                try {
                                    setDoctorList((List<Doctor>) object);
                                    doctorListAdapter.notifyDataSetChanged();
                                }catch (Exception e){
                                    popupOups(e.getMessage(),true);
                                }
                            }

                            @Override
                            public void onError(String error) {
                                BaseUtil.log("Erreur: "+error);
                                stopRefreshing();
                                popupOups(error,true);
                            }
                        },true);
                    } catch (Exception e) {
                        stopRefreshing();
                        popupOups(e.getMessage(),true);
                    }
                }else{
                    popupOupsNoNetwork();
                    stopRefreshing();
                }
            }
        });
    }

    private void initModels() {
        doctorList = getDataService().getAllDoctors();
    }

    public void initViews() {
        listView = findViewById(R.id.list_view);
        listView.setAdapter(doctorListAdapter);
        swipeRefreshLayout = findViewById(R.id.swipe_layout);
    }



    private void setDoctorList(List<Doctor> doctors){
        this.doctorList.clear();
        this.doctorList.addAll(doctors);
        Collections.sort(doctorList);
    }

    private void startAboutDoctorActivity(String idDoctor) {
        Intent intent = new Intent(this, AboutDoctorActivity.class);
        intent.putExtra(BaseUtil.EXTRA_DOCTOR_ID, idDoctor);
        startActivity(intent);
    }

    private void stopRefreshing(){
        swipeRefreshLayout.setRefreshing(false);
    }
}