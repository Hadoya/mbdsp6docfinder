package com.mbds.tpt.docfinder.dialog;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.util.BaseUtil;
import com.mbds.tpt.docfinder.util.CallUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MapBottomSheetDialog extends BottomSheetDialogFragment {
    private ImageView callingView, messagingView;
    private TextView nameView;
    private TextView specialityView;
    private TextView addressView;
    private TextView telephoneView;
    private TextView closeBtn, detailBtn;
    private MapBottomSheetListener mapBottomSheetListener;
    private Doctor doctor;

    public interface MapBottomSheetListener {
        void onMapDialogCloseBtnClicked();

        void onMapDialogDetailBtnClicked(String doctorId);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_map_bottom_sheet, container, false);
        initViews(view);
        initListeners();
        if (doctor != null)
            displayDoctor();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mapBottomSheetListener = (MapBottomSheetListener) context;
        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement MapBottomSheetListener");
        }
    }

    private void displayDoctor() {
        nameView.setText("Dr " + doctor.getLastName() + " " + doctor.getFirstName());
        specialityView.setText(doctor.getSpeciality());
        addressView.setText(doctor.getAddress() + " " + doctor.getTown());
        telephoneView.setText(doctor.getTelephone());
    }

    private void initViews(View view) {
        callingView = view.findViewById(R.id.call);
        messagingView = view.findViewById(R.id.send_message);
        nameView = view.findViewById(R.id.name);
        specialityView = view.findViewById(R.id.speciality);
        addressView = view.findViewById(R.id.address);
        telephoneView = view.findViewById(R.id.telephone);
        closeBtn = view.findViewById(R.id.close_btn);
        detailBtn = view.findViewById(R.id.detail_btn);
    }

    private void initListeners() {
        callingView.setOnClickListener(onCallingListener);
        messagingView.setOnClickListener(onMessagingListener);
        detailBtn.setOnClickListener(onDetailListener);
        closeBtn.setOnClickListener(onCloseListener);
    }

    private View.OnClickListener onCloseListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismiss();
            mapBottomSheetListener.onMapDialogCloseBtnClicked();
        }
    };

    private View.OnClickListener onDetailListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismiss();
            mapBottomSheetListener.onMapDialogDetailBtnClicked(doctor.getId());
        }
    };

    private View.OnClickListener onCallingListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                CallUtil.call(getActivity(), doctor.getTelephone());
            } catch (Exception e) {
                dismiss();
                BaseUtil.toastException(getActivity(), e);
            }
        }
    };

    private View.OnClickListener onMessagingListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                CallUtil.sendMessage(getActivity(), doctor.getTelephone());
            } catch (Exception e) {
                dismiss();
                BaseUtil.toastException(getActivity(), e);
            }
        }
    };

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }
}
