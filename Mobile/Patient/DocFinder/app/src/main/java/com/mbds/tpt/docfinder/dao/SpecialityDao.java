package com.mbds.tpt.docfinder.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.mbds.tpt.docfinder.db.DbHelper;
import com.mbds.tpt.docfinder.model.Speciality;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.ArrayList;
import java.util.List;

public class SpecialityDao extends BaseDao {
    public SpecialityDao(DbHelper dbHelper, Activity activity) {
        super(dbHelper, activity);
    }

    public void addSpeciality(Speciality speciality) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            putValues(values,speciality);
            db.insert(dbHelper.TABLE_SPECIALITY, null, values);
            db.close();
        }catch (SQLException e){
            BaseUtil.logException("addSpeciality",e);
            throw e;
        }
    }

    public void addSpeciality(List<Speciality> specialityList) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (Speciality speciality : specialityList) {
                putValues(values,speciality);
                db.insert(dbHelper.TABLE_SPECIALITY, null, values);
                values.clear();
            }
            db.close();
        }catch (SQLException e) {
            BaseUtil.logException("addSpeciality:List", e);
            throw e;
        }
    }

    public List<Speciality> getAllSpecialities() {
        try {
            List<Speciality> specialityList = new ArrayList();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_SPECIALITY, null);
            if (cursor.moveToFirst()) {
                do {
                    Speciality speciality = getSpeciality(cursor);
                    specialityList.add(speciality);
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
            return specialityList;
        }catch (SQLException e){
            BaseUtil.logException("getAllSpeciality",e);
            throw e;
        }
    }

    public void deleteAllSpecialities() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_SPECIALITY, null, null);
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("deleteAllSpecialities",e);
            throw e;
        }
    }



    public Speciality getSpecialityById(String id) {
        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            return getSpecialityById(db,id);
        } catch (SQLException e) {
            BaseUtil.logException("getSpecialityById",e);
            throw e;
        }
    }

    public Speciality getSpecialityById(SQLiteDatabase db, String id) {
        try {
            Cursor cursor = db.query(dbHelper.TABLE_SPECIALITY,
                    getSpecialityCols(),
                    dbHelper.COL_SPECIALITY_ID + "=?",
                    new String[]{id},
                    null, null, null, null);
            if (cursor.moveToFirst()) {
                Speciality speciality = getSpeciality(cursor);
                return speciality;
            }
            return null;
        } catch (SQLException e) {
            BaseUtil.logException("getSpecialityById",e);
            throw e;
        }
    }

    private String[] getSpecialityCols(){
        return new String[]{dbHelper.COL_SPECIALITY_ID, dbHelper.COL_SPECIALITY_NAME, dbHelper.COL_SPECIALITY_DESCRIPTION};
    }

    private Speciality getSpeciality(Cursor cursor){
        Speciality speciality = new Speciality();
        speciality.setId(cursor.getString(0));
        speciality.setName(cursor.getString(1));
        speciality.setDescription(cursor.getString(2));
        return speciality;
    }

    private void putValues(ContentValues values, Speciality speciality){
        values.put(dbHelper.COL_SPECIALITY_ID, speciality.getId());
        values.put(dbHelper.COL_SPECIALITY_NAME, speciality.getName());
        values.put(dbHelper.COL_SPECIALITY_DESCRIPTION, speciality.getDescription());
    }
}
