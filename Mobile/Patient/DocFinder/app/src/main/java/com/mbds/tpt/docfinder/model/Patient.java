package com.mbds.tpt.docfinder.model;

import java.util.Date;

public class Patient {
    private String id;
    private String lastName;
    private String firstName;
    private String telephone;
    private String gender;
    private Date birthday;
    private int age;
    private String email;
    private String password;
    private Date createdAt;
    private Date updatedAt;

    public Patient(){

    }

    public Patient(String id, String lastName, String firstName, String telephone, String gender, Date birthday, String email, String password, Date createdAt, Date updatedAt) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.telephone = telephone;
        this.gender = gender;
        this.birthday = birthday;
        this.email = email;
        this.password = password;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String toString(){
        return "{id:"+id+", firstName:"+firstName+", lastName:"+lastName+", gender:"+gender+", email:"+email+"}";
    }
}
