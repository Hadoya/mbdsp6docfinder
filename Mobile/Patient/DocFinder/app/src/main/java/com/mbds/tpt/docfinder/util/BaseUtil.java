package com.mbds.tpt.docfinder.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Speciality;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BaseUtil {

    public final static String EXTRA_DOCTOR_ID = "docfinder.extra.docId";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public static final String AUTH_FAILED = "Email ou mot de passe incorrect!";
    public static final String SUCCESS = "success";
    public static final String ERROR = "Error";
    public static  final String FAILED = "Failed";

    public static void log(String msg){
        Log.e("DFLog","-"+ msg);
    }

    public static void logSuccess(String methodName, String msg){
        Log.e("DFLog","OK("+methodName+") -> "+ msg);
    }

    public static void logError(String methodName, String error){
        Log.e("DFLog","Error("+methodName+") -> "+ error);
    }

    public static void logException(String methodeName, Exception e){
        logError(methodeName, e.getMessage());
    }

    public static void toastException(Context activity, Exception e){
        toastLong(activity, "Error: "+e.getMessage());
    }

    public static void toastLong(Context activity, String msg){
        Toast.makeText(activity, msg,Toast.LENGTH_LONG).show();
    }

    public static void toastNoNetwork(Context activity){
        toastShort(activity,activity.getResources().getString(R.string.no_connection));
    }

    public static void toastShort(Context activity, String msg){
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }

    public static void toastSent(Context activity){
        Toast.makeText(activity, activity.getResources().getString(R.string.sent), Toast.LENGTH_SHORT).show();
    }

    public static List<String> getSpecialityName(List<Speciality> specialityList, String firstElement){
        List<String> specialities = new ArrayList<>(specialityList.size());
        if(firstElement != null && !firstElement.isEmpty())
            specialities.add(firstElement);
        for(Speciality speciality: specialityList)
            specialities.add(speciality.getName());
        return specialities;
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String[] getNomsPatients(List<Appointment> rendezVousList){
        String[] nomsPatients = new String[rendezVousList.size()];
        for(int i=0;i<rendezVousList.size();i++){
            nomsPatients[i] = rendezVousList.get(i).getFirstNamePatient() +" "+rendezVousList.get(i).getLastNamePatient();
        }
        return nomsPatients;
    }

    public static String dateToString(Date date){
        return dateFormat.format(date);
    }

    public static String dateToStringGetDate(Date date){
        return new SimpleDateFormat("dd-MM-yyyy").format(date);
    }

    public static String dateToStringGetTime(Date date){
        return new SimpleDateFormat("hh:mm").format(date);
    }

    public static Date strToDate(String str) {
        try {
            Log.e("Date",str);
            return dateFormat.parse(str);
        }catch (Exception e){
            return new Date(System.currentTimeMillis());
        }
    }

    public static String buildUrl(String ip, int port)  {
        return "http://"+ip+":"+port;
    }

    public static String dateToMongoDbDate(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss.000+00:00", Locale.getDefault());
        return dateFormat.format(date);
    }

    public static Date getMongoDbDate(String date){
        //1986-09-21T21:00:00.000+00:00
        return new Date(Integer.valueOf(date.substring(0,4))-1900,
                Integer.valueOf(date.substring(5,7))-1,
                Integer.valueOf(date.substring(8,10)),
                Integer.valueOf(date.substring(11,13)),
                Integer.valueOf(date.substring(14,16)));
    }
}
