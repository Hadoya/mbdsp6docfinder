package com.mbds.tpt.docfinder.model;

import java.util.Date;

public class Doctor implements Comparable<Doctor>{
    protected String id;
    protected String idSpeciality;
    protected String firstName;
    protected String lastName;
    protected String telephone;
    protected String email;
    protected String gender;
    protected String address;
    protected String speciality;
    protected String city;
    protected String town;
    protected String tarif;
    protected int validationState;
    protected String description;
    protected double latitude;
    protected double longitude;
    protected Date createdAt;
    protected Date updatedAt;

    //pour eviter que l'app se plante si les valeurs sont null;
    public Doctor(){
        gender = "m";
        description = "";
        speciality = "généraliste";
        email= "needmail@xx.xx";
        telephone = "XXX-XXX-XXX";
    }

    public Doctor(String id, String idSpeciality, String lastName, String firstName, String telephone, String email, String gender, String address, String city, String town, int validationState, String description, double latitude, double longitude, String speciality) {
        this.id = id;
        this.idSpeciality = idSpeciality;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
        this.email = email;
        this.gender = gender;
        this.address = address;
        this.city = city;
        this.town = town;
        this.validationState = validationState;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speciality = speciality;
        this.createdAt = new Date(System.currentTimeMillis());
        this.updatedAt = new Date(System.currentTimeMillis());
    }

    public Doctor(String id, String idSpeciality, String firstName, String lastName, String telephone, String email, String gender, String address, String city, String town, String tarif, int validationState, String description, double latitude, double longitude, String speciality, Date createdAt, Date updatedAt) {
        this.id = id;
        this.idSpeciality = idSpeciality;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
        this.email = email;
        this.gender = gender;
        this.address = address;
        this.city = city;
        this.town = town;
        this.tarif = tarif;
        this.validationState = validationState;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speciality = speciality;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdSpeciality() {
        return idSpeciality;
    }

    public void setIdSpeciality(String idSpeciality) {
        this.idSpeciality = idSpeciality;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName(){
        return lastName+" "+firstName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public int getValidationState() {
        return validationState;
    }

    public void setValidationState(int validationState) {
        this.validationState = validationState;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String toString(){
        return "{id:"+id+", firstName:"+firstName+", lastName:"+lastName+", gender:"+gender+", email:"+email+", lat:"+latitude+", long:"+longitude+", speciality:"+ speciality +"}";
    }

    @Override
    public int compareTo(Doctor doctor) {
        return this.lastName.compareTo(doctor.getLastName());
    }
}
