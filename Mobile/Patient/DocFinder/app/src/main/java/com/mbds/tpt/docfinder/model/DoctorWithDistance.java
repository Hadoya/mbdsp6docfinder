package com.mbds.tpt.docfinder.model;

public class DoctorWithDistance extends Doctor{
    private float distance;

    public DoctorWithDistance(Doctor doctor, float distance) {
        copy(doctor);
        this.distance = distance;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public void copy(Doctor doctor) {
        this.id = doctor.getId();
        this.idSpeciality = doctor.getIdSpeciality();
        this.firstName = doctor.getFirstName();
        this.lastName = doctor.getLastName();
        this.telephone = doctor.getTelephone();
        this.email = doctor.getEmail();
        this.gender = doctor.getGender();
        this.address = doctor.getAddress();
        this.city = doctor.getCity();
        this.town = doctor.getTown();
        this.validationState = doctor.getValidationState();
        this.description = doctor.getDescription();
        this.latitude = doctor.getLatitude();
        this.longitude = doctor.getLongitude();
        if (doctor.getSpeciality() != null)
            speciality = doctor.getSpeciality();

    }

    public int compareTo(DoctorWithDistance doctorWithDistance) {
        //return this.lastName.compareTo(doctorWithDistance.getLastName());

        if (distance == doctorWithDistance.getDistance()) return 0;
        if (distance > doctorWithDistance.getDistance()) return 1;
        return -1;
    }
}
