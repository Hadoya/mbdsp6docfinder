package com.mbds.tpt.docfinder.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.mbds.tpt.docfinder.db.DbHelper;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.ArrayList;
import java.util.List;

public class AppointmentDao extends BaseDao {
    public AppointmentDao(DbHelper dbHelper, Activity activity) {
        super(dbHelper, activity);
    }

    public Appointment getAppointmentById(String id) {
        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.query(dbHelper.TABLE_APPOINTMENT,
                    getAppointmentCols(),
                    dbHelper.COL_APPOINTMENT_ID + "=?",
                    new String[]{id},
                    null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();
            Appointment appointment = getAppointment(cursor);
            cursor.close();
            db.close();
            return appointment;
        } catch (SQLException e) {
            BaseUtil.logException("getAppointmentById", e);
            throw e;
        }
    }

    public void addAppointment(Appointment appointment) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            putValues(values, appointment);
            db.insert(dbHelper.TABLE_APPOINTMENT, null, values);
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("addAppointment", e);
            throw e;
        }
    }

    public void addAppointment(List<Appointment> appointments) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (Appointment appointment : appointments) {
                putValues(values, appointment);
                db.insert(dbHelper.TABLE_APPOINTMENT, null, values);
                values.clear();
            }
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("addAppointment:List", e);
            throw e;
        }
    }

    public List<Appointment> searchAppointment(List<Appointment> liste, String mot) {
        mot = mot.trim();
        List<Appointment> appointments = new ArrayList();
        for (Appointment appointment : liste) {
            if ((appointment.getFirstNamePatient() + appointment.getLastNamePatient()).toLowerCase().contains(mot))
                appointments.add(appointment);
        }
        return appointments;
    }


    public List<Appointment> getAllAppointment() {
        try {
            List<Appointment> appointments = new ArrayList();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_APPOINTMENT, null);
            if (cursor.moveToFirst()) {
                do {
                    Appointment appointment = getAppointment(cursor);
                    appointments.add(appointment);
                } while (cursor.moveToNext());
            }
            return appointments;
        } catch (SQLException e) {
            BaseUtil.logException("getAllAppointment", e);
            throw e;
        }
    }

    public List<Appointment> getAppointmentListByValidation(String validation) {
        try {
            List<Appointment> appointments = new ArrayList();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor cursor = db.query(dbHelper.TABLE_APPOINTMENT, null,
                    "validation=?", new String[]{validation},
                    null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    Appointment appointment = getAppointment(cursor);
                    appointments.add(appointment);
                } while (cursor.moveToNext());
            }
            return appointments;
        } catch (SQLException e) {
            BaseUtil.logException("getAllAppointment", e);
            throw e;
        }
    }

    public void deleteAppointmentById(String id) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_APPOINTMENT, dbHelper.COL_APPOINTMENT_ID + "=?", new String[]{id});
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("deleteAppointementById", e);
            throw e;
        }
    }

    public void deleteAllAppointment() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_APPOINTMENT, null, null);
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("deleteAllAppointment", e);
            throw e;
        }
    }

    private void putValues(ContentValues values, Appointment appointment) {
        values.put(dbHelper.COL_APPOINTMENT_ID, appointment.getId());
        values.put(dbHelper.COL_APPOINTMENT_IDDOCTOR, appointment.getIdDoctor());
        values.put(dbHelper.COL_APPOINTMENT_DOC_FIRSTNAME, appointment.getFirstNameDoctor());
        values.put(dbHelper.COL_APPOINTMENT_DOC_LASTNAME, appointment.getLastNameDoctor());
        values.put(dbHelper.COL_APPOINTMENT_SPECIALITY, appointment.getDoctorSpeciality());
        values.put(dbHelper.COL_APPOINTMENT_IDPATIENT, appointment.getIdPatient());
        values.put(dbHelper.COL_APPOINTMENT_PFIRSTNAME, appointment.getFirstNamePatient());
        values.put(dbHelper.COL_APPOINTMENT_PLASTNAME, appointment.getLastNamePatient());
        values.put(dbHelper.COL_APPOINTMENT_PGENDER, appointment.getPatientGender());
        values.put(dbHelper.COL_APPOINTMENT_APPOINTMENT_DATE, BaseUtil.dateToString(appointment.getDate()));
        values.put(dbHelper.COL_APPOINTMENT_VALIDATION, appointment.getValidation());
    }

    private String[] getAppointmentCols() {
        return new String[]{
                dbHelper.COL_APPOINTMENT_ID,
                dbHelper.COL_APPOINTMENT_IDDOCTOR,
                dbHelper.COL_APPOINTMENT_IDPATIENT,
                dbHelper.COL_APPOINTMENT_DOC_FIRSTNAME,
                dbHelper.COL_APPOINTMENT_DOC_LASTNAME,
                dbHelper.COL_APPOINTMENT_SPECIALITY,
                dbHelper.COL_APPOINTMENT_PFIRSTNAME,
                dbHelper.COL_APPOINTMENT_PLASTNAME,
                dbHelper.COL_APPOINTMENT_APPOINTMENT_DATE,
                dbHelper.COL_APPOINTMENT_CREATEAT,
                dbHelper.COL_APPOINTMENT_PGENDER,
                dbHelper.COL_APPOINTMENT_VALIDATION};
    }

    private Appointment getAppointment(Cursor cursor){
        Appointment appointment = new Appointment();
        appointment.setId(cursor.getString(0));
        appointment.setIdDoctor(cursor.getString(1));
        appointment.setIdPatient(cursor.getString(2));
        appointment.setFirstNameDoctor(cursor.getString(3));
        appointment.setLastNameDoctor(cursor.getString(4));
        appointment.setDoctorSpeciality(cursor.getString(5));
        appointment.setLastNamePatient(cursor.getString(6));
        appointment.setFirstNamePatient(cursor.getString(7));
        appointment.setDate(BaseUtil.strToDate(cursor.getString(8)));
        appointment.setCreatedAt(BaseUtil.strToDate(cursor.getString(9)));
        appointment.setPatientGender(cursor.getString(10));
        appointment.setValidation(cursor.getInt(11));
        return appointment;
    }
}
