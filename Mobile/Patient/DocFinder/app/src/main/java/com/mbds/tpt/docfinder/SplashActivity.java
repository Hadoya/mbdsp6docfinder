package com.mbds.tpt.docfinder;

import androidx.annotation.Nullable;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

public class SplashActivity extends BaseActivity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private ProgressBar progressBar;
    private TextView initTextView;
    private static int MAX_PROGRESS = 2;
    private int PROGRESS = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_splash);
        initViews();

        if(BaseUtil.isNetworkAvailable(this)){
            initTextView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            setInitTextViewProgress();
            updateDb();
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    startLoginActivity();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    private void initViews(){
        try {
            initTextView = findViewById(R.id.init_view);
            progressBar = findViewById(R.id.progress_splash);
        }catch (Exception e){
            BaseUtil.logException("initViews",e);
        }
    }

    private void setInitTextViewProgress(){
        initTextView.setText(getResources().getString(R.string.updating_data)+"...("+PROGRESS+"/"+ MAX_PROGRESS +")");
        if(PROGRESS == MAX_PROGRESS){
            startLoginActivity();
        }
    }

    private void startLoginActivity() {
        Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(mainIntent);
        finish();
    }

    private void updateDb(){
        getDataService().updateDoctor(new DataService.VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                PROGRESS++;
                setInitTextViewProgress();
            }

            @Override
            public void onError(String error) {
                PROGRESS++;
                setInitTextViewProgress();
            }
        },false);

        getDataService().updateSpeciality(new DataService.VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                PROGRESS++;
                setInitTextViewProgress();
            }

            @Override
            public void onError(String error) {
                PROGRESS++;
                setInitTextViewProgress();
            }
        });
    }
}