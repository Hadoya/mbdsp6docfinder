package com.mbds.tpt.docfinder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mbds.tpt.docfinder.adapter.AppointmentListAdapter;
import com.mbds.tpt.docfinder.dialog.AppointmentDetailDialog;
import com.mbds.tpt.docfinder.dialog.OupsDialog;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;
import com.mbds.tpt.docfinder.util.CallUtil;

import java.util.List;

public class AppointmentListActivity extends BaseActivity implements AppointmentDetailDialog.AppointmentDetailDialogListener {
    protected AppointmentListAdapter appointmentListAdapter;
    protected List<Appointment> appointmentList;
    protected int appointmentValidation = 1;

    protected ListView listView;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected AppointmentDetailDialog appointmentDetailDialog;

    protected Doctor selectedDoctor;
    protected Appointment selectedAppointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_list);
        configureToolbar(getResources().getString(R.string.appointments));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initDefaultModels();
        initModels();
        initAdapters();
        initViews();
        initListeners();
        setListViewAnimation(listView);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onAboutDoctorBtnClicked() {
        startAboutDoctorActivity();
    }

    @Override
    public void onCallBtnClicked() {
        CallUtil.call(this, selectedDoctor.getTelephone());
    }

    @Override
    public void onSmsBtnClicked() {
        CallUtil.sendMessage(this, selectedDoctor.getTelephone());
    }

    @Override
    public void onDeleteBtnClicked() {

    }

    protected void initAdapters() {
        appointmentListAdapter = new AppointmentListAdapter(this, appointmentList);
    }

    protected void initListeners() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedAppointment = appointmentList.get(i);
                selectedDoctor = getDataService().getDoctorById(appointmentList.get(i).getIdDoctor());
                appointmentDetailDialog = new AppointmentDetailDialog(selectedDoctor, selectedAppointment);
                appointmentDetailDialog.show(getSupportFragmentManager(), "detailAppointment");
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                BaseUtil.log("Mise à jour appointment");
                if (BaseUtil.isNetworkAvailable(AppointmentListActivity.this)) {
                    BaseUtil.log("Connexion api ...");
                    try {
                        getDataService().updateAppointment(getPatient().getId(), new DataService.VolleyCallback() {
                            @Override
                            public void onSuccess(Object object) {
                                if (appointmentValidation == 0)
                                    setAppointmentList(getDataService().getPendingAppointment((List<Appointment>) object));
                                else
                                    setAppointmentList(getDataService().getAcceptedAppointment((List<Appointment>) object));
                                stopRefreshing();
                            }

                            @Override
                            public void onError(String error) {
                                popupOups(error,true);
                                stopRefreshing();
                            }
                        });
                    } catch (Exception e) {
                        popupOups(e.getMessage(),true);
                        stopRefreshing();
                    }
                } else {
                    BaseUtil.log("Aucun connexion internet ...");
                    popupOupsNoNetwork();
                    stopRefreshing();
                }
            }
        });
    }

    protected void initModels() {
        appointmentList = getDataService().getAllAcceptedAppointment();
    }

    protected void initViews() {
        listView = findViewById(R.id.list_view);
        listView.setAdapter(appointmentListAdapter);
        swipeRefreshLayout = findViewById(R.id.swipe_layout);
    }

    protected void setAppointmentList(List<Appointment> appointmentList) {
        this.appointmentList.clear();
        this.appointmentList.addAll(appointmentList);
        appointmentListAdapter.notifyDataSetChanged();
    }

    protected void startAboutDoctorActivity() {
        Intent intent = new Intent(this, AboutDoctorActivity.class);
        intent.putExtra(BaseUtil.EXTRA_DOCTOR_ID, selectedDoctor.getId());
        startActivity(intent);
    }

    protected void stopRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }


}