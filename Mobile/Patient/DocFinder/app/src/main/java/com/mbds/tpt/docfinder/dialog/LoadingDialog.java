package com.mbds.tpt.docfinder.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.views.DocFinderTextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class LoadingDialog extends Dialog {
    private DocFinderTextView msgView;
    private String msg;

    public LoadingDialog(@NonNull Context context,String msg){
        super(context);
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_loading);
        msgView = findViewById(R.id.dialog_text);
        msgView.setText(msg);
    }

}
