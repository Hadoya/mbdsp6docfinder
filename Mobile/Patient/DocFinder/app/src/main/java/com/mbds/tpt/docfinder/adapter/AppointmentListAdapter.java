package com.mbds.tpt.docfinder.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DateSorter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.model.Appointment;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AppointmentListAdapter extends ArrayAdapter<Appointment> {
    private List<Appointment> appointmentListCopy;
    private List<Appointment> appointmentListOriginal;
    private Context context;

    private static class ViewHolder {
        TextView nameView;
        TextView specialityView;
        TextView dateView;
        TextView timeView;
    }

    public AppointmentListAdapter(@NonNull Context context, List<Appointment> appointmentList) {
        super(context, R.layout.list_item_appointment, appointmentList);
        this.appointmentListCopy = appointmentList;
        appointmentListOriginal = new ArrayList<>();
        appointmentListOriginal.addAll(appointmentList);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Appointment appointment = getItem(position);
        AppointmentListAdapter.ViewHolder viewHolder;
        final View result;
        if (convertView == null) {
            viewHolder = new AppointmentListAdapter.ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.list_item_appointment, parent, false);
            viewHolder.nameView = convertView.findViewById(R.id.name_view);
            viewHolder.specialityView = convertView.findViewById(R.id.speciality_view);
            viewHolder.timeView = convertView.findViewById(R.id.time_view);
            viewHolder.dateView = convertView.findViewById(R.id.date_view);
            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AppointmentListAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        try {
            viewHolder.nameView.setText("Dr. " + appointment.getLastNameDoctor() + " " + appointment.getFirstNameDoctor());
            viewHolder.specialityView.setText(appointment.getDoctorSpeciality());
            if(appointment.getValidation() == 1) {
                viewHolder.timeView.setText(BaseUtil.dateToStringGetTime(appointment.getDate()));
                viewHolder.dateView.setText(BaseUtil.dateToStringGetDate(appointment.getDate()));
            }
        }catch (Exception e){
            BaseUtil.logError("adapterAppointment",e.getMessage());
            BaseUtil.toastLong(getContext(),"Erreur: "+e.getMessage());
        }

        return convertView;
    }

    public void filterAppointment(String text) {
        /*
        appointmentListCopy.clear();
        if (text.isEmpty()) {
            appointmentListCopy.addAll(appointmentListOriginal);
        } else {
            text = text.toLowerCase();
            for (Appointment appointment : appointmentListOriginal) {
                if ()
                        && (speciality.compareToIgnoreCase("all") == 0 || appointment.getSpeciality().getName().compareToIgnoreCase(speciality) == 0)
                        && (gender.compareToIgnoreCase("all") == 0 || appointment.getGender().compareToIgnoreCase(gender) == 0)
                        && (maxDistance == 0 || appointment.getDistance() / 1000 < maxDistance)) {
                    appointmentListCopy.add(appointment);
                }
            }
        }
        notifyDataSetChanged();*/
    }

    public void changeAndRefreshList(List<Appointment> appointmentList) {
        this.appointmentListCopy.clear();
        this.appointmentListCopy.addAll(appointmentList);
        super.notifyDataSetChanged();
    }
}
