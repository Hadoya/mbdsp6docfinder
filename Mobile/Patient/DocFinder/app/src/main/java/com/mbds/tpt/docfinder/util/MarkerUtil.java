package com.mbds.tpt.docfinder.util;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class MarkerUtil extends BaseUtil{
    public static float distance(Location location, Marker marker){
        LatLng markerLatLng = marker.getPosition();
        Location markerLocation = new Location("");
        markerLocation.setLatitude(markerLatLng.latitude);
        markerLocation.setLongitude(markerLatLng.longitude);

        return location.distanceTo(markerLocation);
    }
}
