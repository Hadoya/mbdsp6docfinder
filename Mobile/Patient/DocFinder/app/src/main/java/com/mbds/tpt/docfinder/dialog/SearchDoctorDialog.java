package com.mbds.tpt.docfinder.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.slider.Slider;
import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.adapter.SearchDoctorListAdapter;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.model.DoctorWithDistance;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

public class SearchDoctorDialog extends DialogFragment {
    private ArrayAdapter<String> specialityAdapter;
    private Dialog mDialog;
    private FloatingActionButton expandBtn;
    private LinearLayout advanceSearchLayout;
    private List<DoctorWithDistance> doctorList;
    private List<String> specialityList;
    private List<DoctorWithDistance> doctorWithDistancesCopy;
    private ListView listView;
    private SearchView searchView;
    private SearchDoctorListAdapter adapter;
    private SearchDoctorDialogListener searchDoctorDialogListener;
    private Spinner specialitySpinner, genderSpinner;
    private Slider distanceSlider;

    public interface SearchDoctorDialogListener {
        void onSelectedDoctor(Doctor doctor);
    }

    public SearchDoctorDialog() {
        Log.e("MyError", "SearchDoctorDialog Constructor");
        doctorWithDistancesCopy = new ArrayList<>();
    }

    /*public SearchDoctorDialog(@NonNull Context context, List<Doctor> doctorList) {
        super(context);
        setContentView(R.layout.dialog_search_doctor);
        initViews(null);
        this.doctorList = doctorList;
        adapter = new SearchDoctorListAdapter(context, this.doctorList);
        listView.setAdapter(adapter);
        initListeners();
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }*/

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_search_doctor, container, false);
        initViews(view);
        initSpinners();
        doctorWithDistancesCopy.clear();
        doctorWithDistancesCopy.addAll(doctorList);
        adapter = new SearchDoctorListAdapter(getContext(), doctorWithDistancesCopy);
        listView.setAdapter(adapter);
        initListeners();
        setListViewAnimation();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mDialog = getDialog();
        if (mDialog != null) {
            mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            searchDoctorDialogListener = (SearchDoctorDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onViewSelected");
        }
    }

    public void setDoctorList(List<DoctorWithDistance> doctorList) {
        this.doctorList = doctorList;
    }

    private void filterDoctor(String text) {
        adapter.filterDoctor(text,
                specialitySpinner.getSelectedItem().toString(),
                genderSpinner.getSelectedItem().toString().compareToIgnoreCase("all") == 0 ? "all" : genderSpinner.getSelectedItem().toString().substring(0, 1),
                distanceSlider.getValue());
    }

    private void initListeners() {
        expandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(advanceSearchLayout.getVisibility() == View.GONE) {
                    expandBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_expand_less_24));
                    advanceSearchLayout.setVisibility(View.VISIBLE);
                }else{
                    expandBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_expand_more_24));
                    advanceSearchLayout.setVisibility(View.GONE);
                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterDoctor(s);
                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Doctor doctor = doctorWithDistancesCopy.get(i);
                dismiss();
                searchDoctorDialogListener.onSelectedDoctor(doctor);
            }
        });

        specialitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                filterDoctor(searchView.getQuery().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                filterDoctor(searchView.getQuery().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        distanceSlider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                filterDoctor(searchView.getQuery().toString());
            }
        });
    }

    private void initViews(View view) {
        advanceSearchLayout = view.findViewById(R.id.advance_search_layout);
        expandBtn = view.findViewById(R.id.fab);
        listView = view.findViewById(R.id.list_view);
        searchView = view.findViewById(R.id.search_view);
        searchView.onActionViewExpanded();
        specialitySpinner = view.findViewById(R.id.speciality_spinner);
        genderSpinner = view.findViewById(R.id.gender_spinner);
        distanceSlider = view.findViewById(R.id.distance_slider);
        float maxDistanceValue = 1;
        for (DoctorWithDistance doc : doctorList) {
            if (maxDistanceValue < doc.getDistance())
                maxDistanceValue = doc.getDistance();
        }
        maxDistanceValue = maxDistanceValue / 1000; // convert to Km
        //make distance max divisible by 10 for design
        maxDistanceValue = maxDistanceValue%10==0?maxDistanceValue:maxDistanceValue + (10-maxDistanceValue%10);
        distanceSlider.setValueTo(maxDistanceValue);
        distanceSlider.setValue(maxDistanceValue);
    }

    private void initSpinners() {
        specialityAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, specialityList);
        specialityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        specialitySpinner.setAdapter(specialityAdapter);
    }

    public void setSpecialityList(List<String> specialityList){
        this.specialityList = specialityList;
    }

    public void setPosition(int x, int y) {
        /*WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        wmlp.x = x;
        wmlp.y = y;*/
    }


    private void setListViewAnimation(){
        AnimationSet set = new AnimationSet(true);
        Animation fadin = new AlphaAnimation(0.0f,1.0f);
        fadin.setDuration(800);
        fadin.setFillAfter(true);
        set.addAnimation(fadin);
        LayoutAnimationController controller = new LayoutAnimationController(set,0.2f);
        listView.setLayoutAnimation(controller);
    }
}
