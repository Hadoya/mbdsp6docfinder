package com.mbds.tpt.docfinder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mbds.tpt.docfinder.R;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DoctorListAdapter extends ArrayAdapter<Doctor> {
    private List<Doctor> doctorListCopy;
    private List<Doctor> doctorListOriginal;
    private Context context;
    private static class ViewHolder {
        ImageView iconView;
        TextView nameView;
        TextView specialityView;
    }

    public DoctorListAdapter(@NonNull Context context, List<Doctor> doctorList) {
        super(context, R.layout.list_item_doctor, doctorList);
        this.doctorListCopy = doctorList;
        doctorListOriginal = new ArrayList<>();
        doctorListOriginal.addAll(doctorList);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Doctor doctor = getItem(position);
        DoctorListAdapter.ViewHolder viewHolder;
        final View result;
        if (convertView == null) {
            viewHolder = new DoctorListAdapter.ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.list_item_doctor, parent, false);
            viewHolder.nameView = convertView.findViewById(R.id.name_view);
            viewHolder.specialityView = convertView.findViewById(R.id.speciality_view);
            viewHolder.iconView = convertView.findViewById(R.id.icon);
            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (DoctorListAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        Integer idMan = R.drawable.profil_man;
        Integer idWoman = R.drawable.profil_woman;

        viewHolder.nameView.setText("Dr. "+doctor.getFullName());
        viewHolder.specialityView.setText(doctor.getSpeciality());
        viewHolder.iconView.setImageResource(doctor.getGender().compareToIgnoreCase("m")==0?idMan:idWoman);

        return convertView;
    }

    public void changeAndRefreshList(List<Doctor> doctorList){
        this.doctorListCopy.clear();
        doctorListCopy.addAll(doctorList);
        notifyDataSetChanged();
    }
}
