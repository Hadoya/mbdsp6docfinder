package com.mbds.tpt.docfinder.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.mbds.tpt.docfinder.db.DbHelper;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.ArrayList;
import java.util.List;

public class DoctorDao extends BaseDao {
    private SpecialityDao specialityDao;
    public DoctorDao(DbHelper dbHelper, Activity activity) {
        super(dbHelper, activity);
        this.specialityDao = new SpecialityDao(dbHelper, activity);
    }


    public DoctorDao(DbHelper dbHelper, Activity activity, SpecialityDao specialityDao) {
        super(dbHelper, activity);
        this.specialityDao = specialityDao;
    }

    public void addDoctor(Doctor doctor) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            putValues(values,doctor);
            db.insert(dbHelper.TABLE_DOCTOR, null, values);
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("addDoctor",e);
            throw e;
        }
    }

    public void addDoctor(List<Doctor> doctors) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (Doctor doctor : doctors) {
                putValues(values,doctor);
                db.insert(dbHelper.TABLE_DOCTOR, null, values);
                values.clear();
            }
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("addDoctor:List",e);
            throw e;
        }
    }

    public Doctor getDoctorById(String id) {
        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.query(dbHelper.TABLE_DOCTOR,
                    getDoctorCols(),
                    dbHelper.COL_SPECIALITY_ID + "=?",
                    new String[]{id},
                    null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                Doctor doctor = getDoctor(cursor);
                return doctor;
            }
            return null;
        } catch (SQLException e) {
            BaseUtil.logException("getDoctorById",e);
            throw e;
        }
    }

    public List<Doctor> getAllDoctors() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_DOCTOR, null);
            List<Doctor> doctorList = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    Doctor doctor = getDoctor(cursor);
                    doctorList.add(doctor);
                } while (cursor.moveToNext());
            }
            return doctorList;
        } catch (SQLException e) {
            BaseUtil.logException("getAllDoctors",e);
            throw e;
        }
    }

    public int updateDoctor(Doctor doctor) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            putValues(values,doctor);
            return db.update(dbHelper.TABLE_DOCTOR, values, dbHelper.COL_DOCTOR_ID + "=?", new String[]{doctor.getId()});
        }catch (SQLException e){
            BaseUtil.logException("getAllPatient",e);
            throw e;
        }
    }


    public void deleteAllDoctors() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_DOCTOR, null, null);
            db.close();
        } catch (SQLException e) {
            BaseUtil.logException("deleteAllDoctors",e);
            throw e;
        }
    }

    private void putValues(ContentValues values, Doctor doctor){
        values.put(dbHelper.COL_DOCTOR_ID, doctor.getId());
        values.put(dbHelper.COL_DOCTOR_FIRSTNAME, doctor.getFirstName());
        values.put(dbHelper.COL_DOCTOR_LASTNAME, doctor.getLastName());
        values.put(dbHelper.COL_DOCTOR_SPECIALITY_ID, doctor.getIdSpeciality());
        values.put(dbHelper.COL_DOCTOR_SPECIALITY, doctor.getSpeciality());
        values.put(dbHelper.COL_DOCTOR_GENDER, doctor.getGender());
        values.put(dbHelper.COL_DOCTOR_TELEPHONE, doctor.getTelephone());
        values.put(dbHelper.COL_DOCTOR_ADRESSE, doctor.getAddress());
        values.put(dbHelper.COL_DOCTOR_CITY, doctor.getCity());
        values.put(dbHelper.COL_DOCTOR_TOWN, doctor.getTown());
        values.put(dbHelper.COL_DOCTOR_TARIF, doctor.getTarif());
        values.put(dbHelper.COL_DOCTOR_EMAIL, doctor.getEmail());
        values.put(dbHelper.COL_DOCTOR_LATITUDE, doctor.getLatitude());
        values.put(dbHelper.COL_DOCTOR_LONGITUDE, doctor.getLongitude());
    }

    private Doctor getDoctor(Cursor cursor){
        Doctor doctor = new Doctor();
        doctor.setId(cursor.getString(0));
        doctor.setFirstName(cursor.getString(1));
        doctor.setLastName(cursor.getString(2));
        doctor.setIdSpeciality(cursor.getString(3));
        doctor.setSpeciality(cursor.getString(4));
        doctor.setGender(cursor.getString(5));
        doctor.setTelephone(cursor.getString(6));
        doctor.setAddress(cursor.getString(7));
        doctor.setCity(cursor.getString(8));
        doctor.setTown(cursor.getString(9));
        doctor.setTarif(cursor.getString(10));
        doctor.setEmail(cursor.getString(11));
        doctor.setLatitude(Float.valueOf(cursor.getString(12)));
        doctor.setLongitude(Float.valueOf(cursor.getString(13)));
        return doctor;
    }

    private String[] getDoctorCols(){
        return new String[]{
                dbHelper.COL_DOCTOR_ID,
                dbHelper.COL_DOCTOR_FIRSTNAME,
                dbHelper.COL_DOCTOR_LASTNAME,
                dbHelper.COL_DOCTOR_SPECIALITY_ID,
                dbHelper.COL_DOCTOR_SPECIALITY,
                dbHelper.COL_DOCTOR_GENDER,
                dbHelper.COL_DOCTOR_TELEPHONE,
                dbHelper.COL_DOCTOR_ADRESSE,
                dbHelper.COL_DOCTOR_CITY,
                dbHelper.COL_DOCTOR_TOWN,
                dbHelper.COL_DOCTOR_TARIF,
                dbHelper.COL_DOCTOR_EMAIL,
                dbHelper.COL_DOCTOR_LATITUDE,
                dbHelper.COL_DOCTOR_LONGITUDE
        };
    }
}
