package com.mbds.tpt.docfinder;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;

import com.mbds.tpt.docfinder.dialog.MyConfirmDialog;
import com.mbds.tpt.docfinder.dialog.OupsDialog;
import com.mbds.tpt.docfinder.model.Patient;
import com.mbds.tpt.docfinder.service.DataService;
import com.mbds.tpt.docfinder.util.BaseUtil;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseActivity extends AppCompatActivity {
    private static Patient patient;
    private static DataService dataService;
    protected static OupsDialog oupsDialog;
    protected static MyConfirmDialog myConfirmDialog;

    protected void hideStatusBar() {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {

        }
    }

    protected void initDefaultModels() {
        if (dataService == null)
            dataService = new DataService(this);
        if (patient == null)
            patient = dataService.getDefaultPatient();
    }

    protected Patient getPatient() {
        if (patient == null)
            patient = dataService.getDefaultPatient();
        return patient;
    }

    protected DataService getDataService() {
        if (dataService == null)
            dataService = new DataService(this);
        return dataService;
    }

    protected void popupOups(String msg, boolean cancelable) {
        oupsDialog = new OupsDialog(this, msg);
        oupsDialog.setCancelable(cancelable);
        oupsDialog.show();
    }

    protected void popupOupsNoNetwork() {
        popupOups(getResources().getString(R.string.no_connection), true);
    }

    protected void configureToolbar(String title) {
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle(title.toUpperCase());
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            BaseUtil.toastException(this, e);
        }
    }

    protected void logOut() {
        patient = null;
        getDataService().logOut();
    }

    protected void setListViewAnimation(ListView listView) {
        AnimationSet set = new AnimationSet(true);
        Animation fadin = new AlphaAnimation(0.0f, 1.0f);
        fadin.setDuration(800);
        fadin.setFillAfter(true);
        set.addAnimation(fadin);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.2f);
        listView.setLayoutAnimation(controller);
    }

    protected void showConfirmDialog(String msg) {
        myConfirmDialog = new MyConfirmDialog(msg);
        myConfirmDialog.show(getSupportFragmentManager(), "confirm");
    }
}
