package com.mbds.tpt.docfinder;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mbds.tpt.docfinder.dialog.AppointmentRequestDialog;
import com.mbds.tpt.docfinder.model.Doctor;
import com.mbds.tpt.docfinder.util.BaseUtil;
import com.mbds.tpt.docfinder.util.CallUtil;
import com.mbds.tpt.docfinder.views.DocFinderTextView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutDoctorActivity extends BaseActivity implements View.OnClickListener, AppointmentRequestDialog.AppointmentRequestDialogListener {

    private AppointmentRequestDialog appointmentRequestDialog;
    private FloatingActionButton appointmentBtn, callBtn, smsBtn;
    private DocFinderTextView nameView, specialityView, addressView, cityTownView, phoneNumberView;
    private Doctor doctor;
    private TextView descriptionView;
    private ImageView profilView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_doctor);
        initDefaultModels();
        String docId = getIntent().getStringExtra(BaseUtil.EXTRA_DOCTOR_ID);
        initViews();
        initListeners();
        initDefaultModels();

        if(!docId.isEmpty()) {
            doctor = getDataService().getDoctorById(docId);
            displayDoctor();
            initDialogs();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.appointment_btn:
                appointmentRequestDialog.show(getSupportFragmentManager(),"requestApp");
                break;
            case R.id.call_btn:
                CallUtil.call(this,doctor.getTelephone());
                break;
            case R.id.sms_btn:
                CallUtil.sendMessage(this,doctor.getTelephone());
                break;
            default:
                BaseUtil.toastShort(this,"Unknown command");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onSubmit(String content) {
        BaseUtil.toastSent(this);
    }

    @Override
    public void onError(String error) {
        popupOups(error,true);
    }

    public void setDoctor(Doctor doctor){
        this.doctor = doctor;
    }

    public Doctor getDoctor(){
        return doctor;
    }

    public void displayDoctor(){
        if(doctor.getGender().compareTo("w")==0)
            profilView.setImageResource(R.drawable.profil_woman);
        this.nameView.setText("Dr. " + doctor.getFullName());
        this.specialityView.setText(doctor.getSpeciality());
        this.addressView.setText(doctor.getAddress());
        this.cityTownView.setText(doctor.getCity()+" - "+doctor.getTown());
        this.phoneNumberView.setText(doctor.getTelephone());
        this.descriptionView.setText(String.format(getResources().getString(R.string.description_doctor),
                doctor.getLastName()+" "+doctor.getFirstName(),"1972",doctor.getCity()+"-"+doctor.getTown(),doctor.getLastName()));
    }

    private void initDialogs(){
        appointmentRequestDialog = new AppointmentRequestDialog(getDataService(), getPatient(), doctor);
    }

    private void initListeners(){
        appointmentBtn.setOnClickListener(this);
        callBtn.setOnClickListener(this);
        smsBtn.setOnClickListener(this);
    }

    private void initViews(){
        profilView = findViewById(R.id.profil_view);
        appointmentBtn = findViewById(R.id.appointment_btn);
        callBtn = findViewById(R.id.call_btn);
        smsBtn = findViewById(R.id.sms_btn);
        nameView = findViewById(R.id.name_view);
        specialityView = findViewById(R.id.speciality_view);
        addressView = findViewById(R.id.address_view);
        cityTownView = findViewById(R.id.city_town_view);
        phoneNumberView = findViewById(R.id.telephone_view);
        descriptionView = findViewById(R.id.description_view);
    }

}