package com.mbds.appdoctor.table;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RendezVous {
    private String id;
    private String idPatient;
    private String idDoctor;
    private String nomDoctor;
    private String prenomDoctor;
    private String speciality;
    private Date date;
    private String nomPatient;
    private String prenomPatient;
    private Date createAt;
    private String patientSexe;
    private int validation;

    public RendezVous(){

    }

    public RendezVous(String id, String idPatient, String idDoctor, Date date, String nomPatient, String prenomPatient, Date createAt, String sexe){
        this.id = id;
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.date = date;
        this.nomPatient = nomPatient;
        this.prenomPatient = prenomPatient;
        this.createAt = createAt;
        this.patientSexe = sexe;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return id;
    }

    public void setIdPatient(String id){
        idPatient = id;
    }
    public String getIdPatient(){
        return idPatient;
    }

    public void setDate(Date date){
        this.date = date;
    }
    public Date getDate(){
        return date;
    }

    public String getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(String idDoctor) {
        this.idDoctor = idDoctor;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getNomPatient() {
        return nomPatient;
    }

    public void setNomPatient(String nomPatient) {
        this.nomPatient = nomPatient;
    }

    public String getPrenomPatient() {
        return prenomPatient;
    }

    public void setPrenomPatient(String prenomPatient) {
        this.prenomPatient = prenomPatient;
    }

    public String getPatientSexe() {
        return patientSexe;
    }

    public void setPatientSexe(String patientSexe) {
        this.patientSexe = patientSexe;
    }

    public int getValidation() {
        return validation;
    }

    public void setValidation(int validation) {
        this.validation = validation;
    }

    public String getNomDoctor() {
        return nomDoctor;
    }

    public void setNomDoctor(String nomDoctor) {
        this.nomDoctor = nomDoctor;
    }

    public String getPrenomDoctor() {
        return prenomDoctor;
    }

    public void setPrenomDoctor(String prenomDoctor) {
        this.prenomDoctor = prenomDoctor;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String toString(){
        return "RDV - {nomPatient:"+nomPatient+", validation:"+validation+"}";
    }
}
