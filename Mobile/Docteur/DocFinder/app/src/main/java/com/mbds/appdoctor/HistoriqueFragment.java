package com.mbds.appdoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;
import com.mbds.appdoctor.utils.RdvListAdapter;

import java.util.ArrayList;
import java.util.List;

public class HistoriqueFragment extends RendezVousFragment {
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rendezvous, container, false);
        initViews(view);
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        rdvListRecherche = new ArrayList<>();
        dataService = ((AccueilActivity) getActivity()).getDataService();

        rdvList = dataService.getListeOldRdvs();
        initListView(rdvList);
        initListeners();
        return view;
    }
}
