package com.mbds.appdoctor.dao;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mbds.appdoctor.table.Patient;

import org.json.JSONException;
import org.json.JSONObject;

public class OnlinePatientDao extends OnlineDao{
    private static String patientUrl =  "/patient";

    public OnlinePatientDao(Activity activity, String serverUrl) {
        super(activity, serverUrl);
    }


    public void getAllPatients(VolleyCallback callback){
        sendRequest(Request.Method.GET, serverUrl + patientUrl, null, callback);
    }

    public void getPatient(String id, VolleyCallback callback){
        sendRequest(Request.Method.GET, serverUrl + patientUrl+"/"+id, null, callback);
    }
}
