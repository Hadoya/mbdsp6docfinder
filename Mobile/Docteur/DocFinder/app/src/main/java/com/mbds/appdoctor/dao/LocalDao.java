package com.mbds.appdoctor.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mbds.appdoctor.db.DbHelper;
import com.mbds.appdoctor.table.Config;
import com.mbds.appdoctor.table.Docteur;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.utils.BaseUtils;

import java.util.Date;

public abstract class LocalDao{
    protected DbHelper dbHelper;

    public LocalDao(DbHelper dbHelper){
        this.dbHelper = dbHelper;
    }

    public Config getConfig(int id){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(dbHelper.TABLE_CONFIG,
                new String[] { dbHelper.COL_CONFIG_ID, dbHelper.COL_CONFIG_SERVER, dbHelper.COL_CONFIG_MAJ, dbHelper.COL_CONFIG_IP, dbHelper.COL_CONFIG_PORT},
                dbHelper.COL_CONFIG_ID+"=?",
                new String[] {String.valueOf(id)},
                null, null, null, null);
        if(cursor != null)
            cursor.moveToFirst();

        Config conf = new Config();
        conf.setId(cursor.getInt(0));
        conf.setServerLocale(cursor.getInt(1)==0?false:true);
        conf.setMajAuto(cursor.getInt(2)==0?false:true);
        conf.setIp(cursor.getString(3));
        conf.setPort(cursor.getInt(4));

        return conf;
    }

    public int updateConfig(Config config){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(dbHelper.COL_CONFIG_SERVER, config.isServerLocale()?1:0);
        values.put(dbHelper.COL_CONFIG_MAJ, config.isMajAuto()?1:0);
        values.put(dbHelper.COL_CONFIG_IP, config.getIp());
        values.put(dbHelper.COL_CONFIG_PORT, config.getPort());
        return db.update(dbHelper.TABLE_CONFIG,values,dbHelper.COL_CONFIG_ID+"=?",new String[]{String.valueOf(config.getId())});
    }
}
