package com.mbds.appdoctor.dao;

import android.app.Activity;
import com.android.volley.Request;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;

import java.util.HashMap;
import java.util.Map;

public class OnlineRdvDao extends OnlineDao {
    private static String rdvUrl =  "/appointment";
    private static String rdvDuDocUrl = rdvUrl + "/doctor";
    private static String ajoutUrl = rdvUrl + "/add";
    private static String validerUrl = rdvUrl + "/valider";
    private static String rejeterUrl = rdvUrl + "/rejeter";

    public OnlineRdvDao(Activity activity, String serverUrl) {
        super(activity, serverUrl);
    }

    public void getAllRendezVous(String idDoctor, VolleyCallback callback){
        sendRequest(Request.Method.GET,  serverUrl + rdvDuDocUrl + "/"+idDoctor,null, callback);
    }

    public void getRendezVous(String id, VolleyCallback callback){
        sendRequest(Request.Method.GET, rdvUrl+"/"+id,null, callback);
    }

    public void addRendezVous(RendezVous rdv, VolleyCallback callback){
        Map<String,String> params = new HashMap<>();
        params.put("idDoctor",rdv.getIdDoctor());
        params.put("idPatient",rdv.getIdPatient());
        params.put("lastNamePatient",rdv.getNomPatient());
        params.put("firstNamePatient",rdv.getPrenomPatient());
        params.put("gender",rdv.getPatientSexe());
        params.put("appointmentDate", BaseUtils.dateToString(rdv.getDate()));
        params.put("validation", "1");
        sendRequest(Request.Method.POST, serverUrl + ajoutUrl,params, callback);
    }

    public void validerRendezVous(RendezVous rdv, VolleyCallback callback){
        Map<String,String> params = new HashMap<>();
        params.put("id", rdv.getId());
        params.put("appointmentDate", BaseUtils.dateToString(rdv.getDate()));
        sendRequest(Request.Method.POST, serverUrl + validerUrl,params, callback);
    }

    public void rejeterRendezVous(RendezVous rdv, VolleyCallback callback){
        Map<String,String> params = new HashMap<>();
        params.put("id", rdv.getId());
        sendRequest(Request.Method.POST, serverUrl + rejeterUrl,params, callback);
    }
}
