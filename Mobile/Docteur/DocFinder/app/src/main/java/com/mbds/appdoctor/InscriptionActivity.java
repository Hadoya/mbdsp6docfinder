package com.mbds.appdoctor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InscriptionActivity extends AppCompatActivity {
    private Button register_btn;
    private TextView login_link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        getSupportActionBar().hide();

        register_btn = findViewById(R.id.register_btn);
        login_link = findViewById(R.id.login_link);

        login_link.setOnClickListener(login_link_listener);
    }

    private View.OnClickListener login_link_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

}
