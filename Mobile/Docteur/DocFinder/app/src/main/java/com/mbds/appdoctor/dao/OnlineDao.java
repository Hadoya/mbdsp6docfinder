package com.mbds.appdoctor.dao;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class OnlineDao {
    protected static String serverUrl;

    protected Activity activity;

    public OnlineDao(Activity activity, String serverUrl) {
        this.activity = activity;
        this.serverUrl = serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    protected void sendRequest(final int method, String url, final Map<String, String> params, final VolleyCallback callback) {
        try {
            Log.e("DFLog", url);
            Log.e("DFLog", "params length="+((params!=null)?params.size():"is Null"));
            StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("DFLog","onResponse: "+response);
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("DFLog", "onErrorResponse: "+error.getMessage());
                    callback.onError(error);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return params;
                }
            };

            if(activity==null)
                Log.e("DFLog","sendRequest activity is null");
            if(stringRequest == null)
                Log.e("DFLog","sendRequest stringRequest is null");


            Volley.newRequestQueue(activity).add(stringRequest);
        } catch (Exception e) {
            Log.e("sendRequest", e.getMessage());
            throw e;
        }
    }
}
