package com.mbds.appdoctor.utils;

import android.util.Log;

import com.mbds.appdoctor.table.Docteur;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.table.RendezVous;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VolleyUtils {
    public static RendezVous JSONObjectToRdv(JSONObject object) throws JSONException {
        try {
            if (object.length() > 0) {
                RendezVous rdv = new RendezVous();
                rdv.setId(object.getString("_id"));
                rdv.setIdDoctor(object.getString("idDoctor"));
                rdv.setIdPatient(object.getString("idPatient"));
                rdv.setNomPatient(object.getString("lastNamePatient"));
                rdv.setPrenomPatient(object.getString("firstNamePatient"));
                rdv.setPatientSexe(object.getString("gender"));
                Log.e("RdvMongo", object.getString("appointmentDate"));
                rdv.setDate(BaseUtils.getMongoDbDate(object.getString("appointmentDate")));
                rdv.setCreateAt(BaseUtils.getMongoDbDate(object.getString("createdAt")));
                rdv.setValidation(object.getInt("validation"));
                return rdv;
            }
            return null;
        } catch (Exception e) {
            Log.e("VolleyUtils", e.getMessage());
            throw e;
        }
    }

    public static List<RendezVous> JSONArrayToRdv(JSONArray array) throws JSONException {
        List<RendezVous> rdvs = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            rdvs.add(JSONObjectToRdv(array.getJSONObject(i)));
        }
        return rdvs;
    }

    public static Patient JSONObjectToPatient(JSONObject object) throws JSONException {
        try {
            if (object.length() > 0) {
                Patient patient = new Patient();
                patient.setId(object.getString("_id"));
                patient.setNom(object.getString("lastname"));
                patient.setPrenom(object.getString("firstname"));
                patient.setSexe(object.getString("gender"));
                patient.setEmail(object.getString("email"));
                patient.setPassword(object.getString("password"));
                patient.setTelephone(object.getString("telephone"));
                patient.setDateNaissance(BaseUtils.getMongoDbDate(object.getString("birthdate")));
                patient.setCreateAt(BaseUtils.getMongoDbDate(object.getString("createdAt")));
                patient.setUpdateAt(BaseUtils.getMongoDbDate(object.getString("updatedAt")));
                return patient;
            }
            return null;
        } catch (JSONException e) {
            Log.e("VolleyUtils", e.getMessage());
            throw new JSONException("erreur de convertion patient");
        }
    }

    public static List<Patient> JSONArrayToPatient(JSONArray array) throws Exception {
        List<Patient> patients = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            patients.add(JSONObjectToPatient(array.getJSONObject(i)));
        }
        return patients;
    }

    public static Docteur JSONObjectToDocteur(JSONObject object) throws Exception {
        try {
            if (object.length() > 0) {
                Docteur doc = new Docteur();
                doc.setId(object.getString("_id"));
                doc.setNom(object.getString("lastname"));
                doc.setPrenom(object.getString("firstname"));
                doc.setIdSpecialite(object.getString("idDoctorSpeciality"));
                doc.setTelephone(object.getString("telephone"));
                doc.setAdresse(object.getString("address"));
                doc.setTown(object.getString("town"));
                doc.setCity(object.getString("city"));
                doc.setDescription(object.getString("description"));
                doc.setTarif(object.getString("tarif"));
                doc.setEtatValidation(object.getInt("etatValidation"));
                doc.setCreateAt(BaseUtils.getMongoDbDate(object.getString("createdAt")));
                doc.setUpdateAt(BaseUtils.getMongoDbDate(object.getString("updatedAt")));
                doc.setEmail(object.getString("email"));
                doc.setPassword(object.getString("password"));
                return doc;
            }
            return null;
        } catch (Exception e) {
            throw new Exception("Erreur convertion doc: " + e.getMessage());
        }
    }
}
