package com.mbds.appdoctor.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {
    private static SharedPreferences prefs;

    public Session(Context ctx){
        prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public void setUserId(String userId) {
        prefs.edit().putString("userId", userId).commit();
    }

    public String getUserId() {
        return prefs.getString("userId","");
    }
}
