package com.mbds.appdoctor.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mbds.appdoctor.db.DbHelper;
import com.mbds.appdoctor.table.Docteur;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.utils.BaseUtils;

import java.util.ArrayList;
import java.util.List;

public class LocalDoctorDao extends LocalDao {

    public LocalDoctorDao(DbHelper dbHelper) {
        super(dbHelper);
    }

    public void addDocteur(Docteur docteur) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(dbHelper.COL_DOCTOR_ID, docteur.getId());
            values.put(dbHelper.COL_DOCTOR_FIRSTNAME, docteur.getNom());
            values.put(dbHelper.COL_DOCTOR_LASTNAME, docteur.getPrenom());
            values.put(dbHelper.COL_DOCTOR_IDDOCSPEC, docteur.getIdSpecialite());
            values.put(dbHelper.COL_DOCTOR_TELEPHONE, docteur.getTelephone());
            values.put(dbHelper.COL_DOCTOR_ADRESSE, docteur.getAdresse());
            values.put(dbHelper.COL_DOCTOR_CITY, docteur.getCity());
            values.put(dbHelper.COL_DOCTOR_TOWN, docteur.getTown());
            values.put(dbHelper.COL_DOCTOR_DESCRIPTION, docteur.getDescription());
            values.put(dbHelper.COL_DOCTOR_TARIF, docteur.getTarif());
            values.put(dbHelper.COL_DOCTOR_ETATVALIDATION, docteur.getEtatValidation());
            values.put(dbHelper.COL_DOCTOR_CREATEAT, BaseUtils.dateToString(docteur.getCreateAt()));
            values.put(dbHelper.COL_DOCTOR_UPDATEAT, BaseUtils.dateToString(docteur.getUpdateAt()));
            db.insert(dbHelper.TABLE_DOCTOR, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("InsertDoctor", e.getMessage());
        }
    }

    public void addDocteur(List<Docteur> docteurs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (Docteur docteur : docteurs) {
            values.put(dbHelper.COL_DOCTOR_ID, docteur.getId());
            values.put(dbHelper.COL_DOCTOR_FIRSTNAME, docteur.getNom());
            values.put(dbHelper.COL_DOCTOR_LASTNAME, docteur.getPrenom());
            values.put(dbHelper.COL_DOCTOR_IDDOCSPEC, docteur.getIdSpecialite());
            values.put(dbHelper.COL_DOCTOR_TELEPHONE, docteur.getTelephone());
            values.put(dbHelper.COL_DOCTOR_ADRESSE, docteur.getAdresse());
            values.put(dbHelper.COL_DOCTOR_CITY, docteur.getCity());
            values.put(dbHelper.COL_DOCTOR_TOWN, docteur.getTown());
            values.put(dbHelper.COL_DOCTOR_DESCRIPTION, docteur.getDescription());
            values.put(dbHelper.COL_DOCTOR_TARIF, docteur.getTarif());
            values.put(dbHelper.COL_DOCTOR_ETATVALIDATION, docteur.getEtatValidation());
            values.put(dbHelper.COL_DOCTOR_CREATEAT, BaseUtils.dateToString(docteur.getCreateAt()));
            values.put(dbHelper.COL_DOCTOR_UPDATEAT, BaseUtils.dateToString(docteur.getUpdateAt()));
            db.insert(dbHelper.TABLE_DOCTOR, null, values);
            values.clear();
        }
        db.close();
    }

    public Docteur getDocteur(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(dbHelper.TABLE_DOCTOR,
                new String[]{dbHelper.COL_DOCTOR_ID, dbHelper.COL_DOCTOR_FIRSTNAME, dbHelper.COL_DOCTOR_LASTNAME, dbHelper.COL_DOCTOR_IDDOCSPEC, dbHelper.COL_DOCTOR_TELEPHONE,
                        dbHelper.COL_DOCTOR_ADRESSE, dbHelper.COL_DOCTOR_CITY, dbHelper.COL_DOCTOR_TOWN, dbHelper.COL_DOCTOR_DESCRIPTION, dbHelper.COL_DOCTOR_TARIF,
                        dbHelper.COL_DOCTOR_ETATVALIDATION, dbHelper.COL_DOCTOR_CREATEAT, dbHelper.COL_DOCTOR_UPDATEAT, dbHelper.COL_DOCTOR_EMAIL, dbHelper.COL_DOCTOR_PASSWORD},
                null,
                null,
                null, null, null, null);
        if (cursor != null && cursor.moveToFirst()){
            Docteur docteur = new Docteur();
            docteur.setId(cursor.getString(0));
            docteur.setNom(cursor.getString(1));
            docteur.setPrenom(cursor.getString(2));
            docteur.setIdSpecialite(cursor.getString(3));
            docteur.setTelephone(cursor.getString(4));
            docteur.setAdresse(cursor.getString(5));
            docteur.setCity(cursor.getString(6));
            docteur.setTown(cursor.getString(7));
            docteur.setDescription(cursor.getString(8));
            docteur.setTarif(cursor.getString(9));
            docteur.setEtatValidation(cursor.getInt(10));
            docteur.setCreateAt(BaseUtils.strToDate(cursor.getString(11)));
            docteur.setUpdateAt(BaseUtils.strToDate(cursor.getString(12)));
            docteur.setEmail(cursor.getString(13));
            docteur.setPassword(cursor.getString(14));
            return docteur;
        }
        return null;
    }

    public Docteur getDocteur(String email, String pwd) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(dbHelper.TABLE_DOCTOR,
                new String[]{dbHelper.COL_DOCTOR_ID, dbHelper.COL_DOCTOR_FIRSTNAME, dbHelper.COL_DOCTOR_LASTNAME, dbHelper.COL_DOCTOR_IDDOCSPEC, dbHelper.COL_DOCTOR_TELEPHONE,
                        dbHelper.COL_DOCTOR_ADRESSE, dbHelper.COL_DOCTOR_CITY, dbHelper.COL_DOCTOR_TOWN, dbHelper.COL_DOCTOR_DESCRIPTION, dbHelper.COL_DOCTOR_TARIF,
                        dbHelper.COL_DOCTOR_ETATVALIDATION, dbHelper.COL_DOCTOR_CREATEAT, dbHelper.COL_DOCTOR_UPDATEAT, dbHelper.COL_DOCTOR_EMAIL, dbHelper.COL_DOCTOR_PASSWORD},
                dbHelper.COL_DOCTOR_EMAIL + "=? and " + dbHelper.COL_DOCTOR_PASSWORD + "=?",
                new String[]{email, pwd},
                null, null, null, null);
        if (cursor != null && cursor.moveToFirst()){
            Docteur docteur = new Docteur();
            docteur.setId(cursor.getString(0));
            docteur.setNom(cursor.getString(1));
            docteur.setPrenom(cursor.getString(2));
            docteur.setIdSpecialite(cursor.getString(3));
            docteur.setTelephone(cursor.getString(4));
            docteur.setAdresse(cursor.getString(5));
            docteur.setCity(cursor.getString(6));
            docteur.setTown(cursor.getString(7));
            docteur.setDescription(cursor.getString(8));
            docteur.setTarif(cursor.getString(9));
            docteur.setEtatValidation(cursor.getInt(10));
            docteur.setCreateAt(BaseUtils.strToDate(cursor.getString(11)));
            docteur.setUpdateAt(BaseUtils.strToDate(cursor.getString(12)));
            docteur.setEmail(cursor.getString(13));
            docteur.setPassword(cursor.getString(14));
            return docteur;
        }
        return null;
    }

    public int updateDocteur(Docteur docteur) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(dbHelper.COL_DOCTOR_ID, docteur.getId());
        values.put(dbHelper.COL_DOCTOR_FIRSTNAME, docteur.getNom());
        values.put(dbHelper.COL_DOCTOR_LASTNAME, docteur.getPrenom());
        values.put(dbHelper.COL_DOCTOR_IDDOCSPEC, docteur.getIdSpecialite());
        values.put(dbHelper.COL_DOCTOR_TELEPHONE, docteur.getTelephone());
        values.put(dbHelper.COL_DOCTOR_ADRESSE, docteur.getAdresse());
        values.put(dbHelper.COL_DOCTOR_CITY, docteur.getCity());
        values.put(dbHelper.COL_DOCTOR_TOWN, docteur.getTown());
        values.put(dbHelper.COL_DOCTOR_DESCRIPTION, docteur.getDescription());
        values.put(dbHelper.COL_DOCTOR_TARIF, docteur.getTarif());
        values.put(dbHelper.COL_DOCTOR_ETATVALIDATION, docteur.getEtatValidation());
        values.put(dbHelper.COL_DOCTOR_CREATEAT, BaseUtils.dateToString(docteur.getCreateAt()));
        values.put(dbHelper.COL_DOCTOR_UPDATEAT, BaseUtils.dateToString(docteur.getUpdateAt()));
        return db.update(dbHelper.TABLE_DOCTOR, values, dbHelper.COL_DOCTOR_ID + "=?", new String[]{docteur.getId()});
    }

    public void deleteDocteur(String id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(dbHelper.TABLE_DOCTOR, dbHelper.COL_DOCTOR_ID + "=?", new String[]{id});
        db.close();
    }

    public void deleteAllDocteurs() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_DOCTOR, null, null);
            db.close();
        } catch (Exception e) {
            Log.e("DeleteDoctor", e.getMessage());
        }
    }
}
