package com.mbds.appdoctor.dao;

import android.app.Activity;

import com.android.volley.Request;
import com.mbds.appdoctor.table.Docteur;

import java.util.HashMap;
import java.util.Map;

public class OnlineDocDao extends OnlineDao{
    private static String doctorUrl = "/doctor";
    private final static String authUrl = doctorUrl + "/authentificate";
    public OnlineDocDao(Activity activity, String serverUrl) {
        super(activity, serverUrl);
    }

    public void getDocteur(String email, String pwd, VolleyCallback callback){
        Map<String,String> params = new HashMap<>();
        params.put("email",email);
        params.put("pwd",pwd);
        sendRequest(Request.Method.POST, serverUrl + authUrl, params, callback);
    }
}
