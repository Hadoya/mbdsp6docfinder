package com.mbds.appdoctor.service;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mbds.appdoctor.AccueilActivity;
import com.mbds.appdoctor.dao.LocalDoctorDao;
import com.mbds.appdoctor.dao.LocalPatientDao;
import com.mbds.appdoctor.dao.LocalRdvDao;
import com.mbds.appdoctor.dao.OnlineDocDao;
import com.mbds.appdoctor.dao.OnlinePatientDao;
import com.mbds.appdoctor.dao.OnlineRdvDao;
import com.mbds.appdoctor.dao.VolleyCallback;
import com.mbds.appdoctor.db.DbHelper;
import com.mbds.appdoctor.table.Config;
import com.mbds.appdoctor.table.Docteur;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;
import com.mbds.appdoctor.utils.VolleyUtils;

import org.json.JSONArray;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class DataService {
    private static final String onlineServerUrl = "https://docfinder-backend.herokuapp.com";
    private DbHelper dbHelper;
    private LocalRdvDao localRdvDao;
    private OnlineRdvDao onlineRdvDao;
    private LocalDoctorDao localDoctorDao;
    private LocalPatientDao localPatientDao;
    private OnlinePatientDao onlinePatientDao;
    private OnlineDocDao onlineDocDao;
    private Activity activity;
    private Config config;

    public DataService(Activity activity) {
        this.activity = activity;
        this.dbHelper = new DbHelper(activity);
        localDoctorDao = new LocalDoctorDao(dbHelper);
        localPatientDao = new LocalPatientDao(dbHelper);
        onlinePatientDao = new OnlinePatientDao(activity, onlineServerUrl);
        localRdvDao = new LocalRdvDao(dbHelper);
        onlineRdvDao = new OnlineRdvDao(activity, onlineServerUrl);
        onlineDocDao = new OnlineDocDao(activity, onlineServerUrl);
    }

    public void updateLocalDb(final VolleyCallback callback) throws Exception {
        try {
            verifyAndConfiServerUrl();
            Docteur doc = localDoctorDao.getDocteur();
            if (doc != null) {
                onlineRdvDao.getAllRendezVous(doc.getId(), new VolleyCallback() {
                    @Override
                    public void onSuccess(String reponse) {
                        try {
                            List<RendezVous> rdvs = VolleyUtils.JSONArrayToRdv(new JSONArray(reponse));
                            if (rdvs != null) {
                                try {
                                    localRdvDao.deleteAllRendezVous();
                                    localRdvDao.addRendezVous(rdvs);
                                    callback.onSuccess("ok");
                                }catch (Exception e){
                                    Log.e("DFLog","erreur:"+e.getMessage());
                                    callback.onError(e);
                                }
                            }
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("ErrorGetAllRdv", e.getMessage());
                        callback.onError(e);
                    }
                });

                onlinePatientDao.getAllPatients(new VolleyCallback() {
                    @Override
                    public void onSuccess(String reponse)  {
                        try {
                            List<Patient> patients = VolleyUtils.JSONArrayToPatient(new JSONArray(reponse));
                            if (patients != null) {
                                localPatientDao.deleteAllPatients();
                                localPatientDao.addPatient(patients);
                                callback.onSuccess("ok");
                            }
                        }catch (Exception e){
                            callback.onError(e);
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("ErrorGetAllPatient", e.getMessage());
                        callback.onError(e);
                    }
                });
            } else {
                throw new Exception("Aucun docteur enregistré dans la base de donnée");
            }
        } catch (Exception e) {
            Log.e("updateLocalDb Error", e.getMessage());
            throw e;
        }
    }

    public void authentificate(String email, String pwd, VolleyCallback callback) {
        verifyAndConfiServerUrl();
        onlineDocDao.getDocteur(email, pwd, callback);
    }

    public Docteur authentificateOffline(String email, String pwd) {
        return localDoctorDao.getDocteur(email, pwd);
    }

    private void verifyAndConfiServerUrl() {
        config = getConfig();
        if (config.isServerLocale()) {
            onlinePatientDao.setServerUrl(BaseUtils.buildUrl(config.getIp(), config.getPort()));
        }
    }

    public void saveDoctor(final Docteur doc) throws Exception {
        try {
            Docteur docFromDb = localDoctorDao.getDocteur(doc.getEmail(), doc.getPassword());
            if (docFromDb != null) {
                if (docFromDb.getId().compareTo(doc.getId()) == 0) {
                    localDoctorDao.updateDocteur(doc);
                } else {
                    localDoctorDao.deleteAllDocteurs();
                    localDoctorDao.addDocteur(doc);
                }
            } else {
                localDoctorDao.addDocteur(doc);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addRendezVous(RendezVous rendezVous, VolleyCallback callback) {
        verifyAndConfiServerUrl();
        onlineRdvDao.addRendezVous(rendezVous, callback);
    }

    public void validerRendezVous(RendezVous rendezVous, VolleyCallback callback) {
        verifyAndConfiServerUrl();
        onlineRdvDao.validerRendezVous(rendezVous, callback);
    }

    public void rejeterRendezVous(RendezVous rendezVous, VolleyCallback callback) {
        verifyAndConfiServerUrl();
        onlineRdvDao.rejeterRendezVous(rendezVous, callback);
    }

    public void validerLocalRendezVous(RendezVous rdv) {
        localRdvDao.validerRendezVous(rdv,1);
    }

    public void rejeterLocalRendezVous(RendezVous rdv) {
        localRdvDao.validerRendezVous(rdv,-1);
    }

    public void addLocalRendezVous(RendezVous rdv) {
        localRdvDao.addRendezVous(rdv);
    }

    public List<RendezVous> getListeRdvs() {
        return localRdvDao.getAllRendezVous();
    }

    public List<RendezVous> getListeNewRdvs() {
        return localRdvDao.getAllNewRendezVous();
    }

    public List<RendezVous> getListeAttenteRdvs() {
        return localRdvDao.getAllAttenteRendezVous();
    }

    public List<RendezVous> getListeOldRdvs() {
        return localRdvDao.getAllOldRendezVous();
    }

    public List<Patient> getListePatient() {
        return localPatientDao.getAllPatients();
    }

    public RendezVous getRdvById(String id) {
        return localRdvDao.getRendezVous(id);
    }

    public Patient getPatientById(String id) {
        return localPatientDao.getPatient(id);
    }

    public List<RendezVous> getRdvByPatient(List<RendezVous> rdvs, String mot) {
        return localRdvDao.searchRendezVous(rdvs, mot);
    }

    public List<Patient> getPatientByName(List<Patient> patientList, String mot) {
        return localRdvDao.searchPatient(patientList, mot);
    }

    public Config getConfig() {
        return localRdvDao.getConfig(1);
    }

    public int updateConfig(Config config) {
        return localRdvDao.updateConfig(config);
    }


    public void initDataTest() {
        Log.d("Init Data", "initTestData Begin...");
        try {
            //-----------------------PATIENT--------------------------
            ArrayList<Patient> patients = new ArrayList<>();
            patients.add(new Patient("1", "Yves", "Jean", "yves.jean@gmail.com", "123", "m", "0330624331", new Date(1993, 2, 10), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())));
            patients.add(new Patient("2", "Charles", "Frederic", "charles.frederic@yahoo.fr", "123", "m", "0330624331", new Date(1993, 2, 10), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())));
            patients.add(new Patient("3", "Dupont", "Sarah", "dupont.sarah@gmail.com", "123", "w", "0330624331", new Date(1985, 11, 20), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())));
            patients.add(new Patient("4", "Laroche", "Martin", "laroche.martin@yahoo.com", "123", "0330624331", "m", new Date(1995, 2, 17), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())));
            patients.add(new Patient("5", "Louise", "Eva", "louise.eva@yahoo.com", "123", "0330624331", "w", new Date(1991, 4, 17), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())));
            patients.add(new Patient("6", "Lerand", "Xavier", "lerand.xavier@gmail.com", "123", "0330624331", "m", new Date(1972, 11, 3), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())));
            try {
                localPatientDao.deleteAllPatients();
                localPatientDao.addPatient(patients);
            } catch (Exception e) {
                throw new Exception("Insert patient ERROR");
            }

            //---------------------DOCTOR-----------------------------
            Docteur doc = new Docteur();
            doc.setId("1");
            doc.setNom("Rafanoharantsoa");
            doc.setPrenom("Lala Harilanto");
            doc.setEmail("lalaharilanto@yahoo.fr");
            doc.setPassword("123");
            doc.setEtatValidation(1);
            doc.setTarif("13500");
            doc.setCity("Antananarivo");
            doc.setTown("Ikianja Ambohimangakely");
            doc.setAdresse("84 Abis Ikianja Ambohimangakely");
            doc.setTelephone("0331271652");
            doc.setDescription("Docteur spécialiste en homeopatie e osteopatie");
            doc.setCreateAt(new java.util.Date());
            doc.setUpdateAt(new java.util.Date());
            try {
                localDoctorDao.deleteAllDocteurs();
                localDoctorDao.addDocteur(doc);
            } catch (Exception e) {
                throw new Exception("Insert DOCTOR ERROR");
            }

            //----------------------RENDEZ VOUS-----------------------
            ArrayList<RendezVous> rdvs = new ArrayList();
            rdvs.add(new RendezVous("1", "6", "1", new Date(2020, 06, 13), "Lerand", "Xavier", new Date(System.currentTimeMillis()), "m"));
            rdvs.add(new RendezVous("2", "5", "1", new Date(2020, 06, 14), "Louise", "Eva", new Date(System.currentTimeMillis()), "w"));
            rdvs.add(new RendezVous("3", "3", "1", new Date(2020, 06, 14), "Dupont", "Sarah", new Date(System.currentTimeMillis()), "w"));
            rdvs.add(new RendezVous("4", "4", "1", new Date(2020, 06, 16), "Laroche", "Martin", new Date(System.currentTimeMillis()), "m"));
            rdvs.add(new RendezVous("5", "2", "1", new Date(2020, 06, 17), "Charles", "Frederic", new Date(System.currentTimeMillis()), "m"));
            rdvs.add(new RendezVous("6", "5", "1", new Date(2020, 06, 18), "Louise", "Eva", new Date(System.currentTimeMillis()), "w"));
            rdvs.add(new RendezVous("7", "1", "1", new Date(2020, 06, 19), "Lerand", "Xavier", new Date(System.currentTimeMillis()), "m"));
            rdvs.add(new RendezVous("8", "3", "1", new Date(2020, 06, 20), "Dupont", "Sarah", new Date(System.currentTimeMillis()), "w"));
            rdvs.add(new RendezVous("9", "4", "1", new Date(2020, 06, 20), "Laroche", "Martin", new Date(System.currentTimeMillis()), "m"));
            rdvs.add(new RendezVous("10", "1", "1", new Date(2020, 06, 24), "Lerand", "Xavier", new Date(System.currentTimeMillis()), "m"));
            rdvs.add(new RendezVous("11", "5", "1", new Date(2020, 06, 27), "Charles", "Frederic", new Date(System.currentTimeMillis()), "m"));
            try {
                localRdvDao.deleteAllRendezVous();
                localRdvDao.addRendezVous(rdvs);
            } catch (Exception e) {
                throw new Exception("Insert RDV ERROR");
            }
            Log.d("Init Data", "init OK");
        } catch (Exception e) {
            Log.d("Init Data ERROR", e.getMessage());
            Log.e("Init Data ERROR", e.getMessage());
        }
    }


    public DbHelper getDbHelper() {
        return dbHelper;
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }
}
