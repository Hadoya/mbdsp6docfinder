package com.mbds.appdoctor.table;

public class Config {
    private int id;
    private boolean serverLocale;
    private boolean majAuto;
    private String ip;
    private int port;

    public Config(){}

    public Config(int id, boolean serverLocale, boolean majAuto, String ip, int port) {
        this.id = id;
        this.serverLocale = serverLocale;
        this.majAuto = majAuto;
        this.ip = ip;
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isServerLocale() {
        return serverLocale;
    }

    public void setServerLocale(boolean serverLocale) {
        this.serverLocale = serverLocale;
    }

    public boolean isMajAuto() {
        return majAuto;
    }

    public void setMajAuto(boolean majAuto) {
        this.majAuto = majAuto;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
