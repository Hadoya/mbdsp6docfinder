package com.mbds.appdoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import com.mbds.appdoctor.dialog.MyConfirmDialog;

import java.util.ArrayList;

public class PendingFragment extends RendezVousFragment {

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rendezvous, container, false);
        initViews(view);
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        rdvListRecherche = new ArrayList<>();
        dataService = ((AccueilActivity) getActivity()).getDataService();

        rdvList = dataService.getListeAttenteRdvs();
        initListView(rdvList);
        initListeners();
        return view;
    }
}