package com.mbds.appdoctor.table;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Patient implements Serializable, Comparable<Patient> {
    private String id;
    private String nom;
    private String prenom;
    private String telephone;
    private String sexe;
    private Date dateNaissance;
    private int age;
    private String email;
    private String password;
    private Date createAt;
    private Date updateAt;

    public  Patient(){

    }

    public Patient(String id, String nom, String prenom, String email, String password, String sexe, String telephone, Date dateNaissance, Date createAt, Date updateAt){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.password = password;
        this.sexe = sexe;
        this.telephone = telephone;
        this.dateNaissance = dateNaissance;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return id;
    }

    public void setNom(String nom){
        this.nom = nom;
    }
    public String getNom(){
        return nom;
    }

    public void setPrenom(String prenom){
        this.prenom = prenom;
    }
    public String getPrenom(){
        return prenom;
    }

    public void setTelephone(String telephone){
        this.telephone = telephone;
    }
    public String getTelephone(){
        return telephone;
    }

    public void setSexe(String sexe){
        this.sexe = sexe;
    }
    public String getSexe(){
        return sexe;
    }

    public int getAge(){
        if(dateNaissance != null){
            DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            return (Integer.parseInt(formatter.format(new Date())) - Integer.parseInt(formatter.format(dateNaissance)))/10000;
        }
        return 0;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }


    @Override
    public int compareTo(Patient patient) {
        return this.nom.compareTo(patient.getNom());
    }
}
