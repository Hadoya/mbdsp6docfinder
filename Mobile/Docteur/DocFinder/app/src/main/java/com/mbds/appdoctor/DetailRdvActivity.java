package com.mbds.appdoctor;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mbds.appdoctor.dao.VolleyCallback;
import com.mbds.appdoctor.dialog.MyConfirmDialog;
import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DetailRdvActivity extends AppCompatActivity implements MyConfirmDialog.MyConfirmDialogListener, View.OnClickListener {
    private ImageView icon;
    private TextView nomView;
    private TextView dateView;
    private TextView heureView;
    private TextView msgView;
    private String idRdv;
    private RendezVous rdv;
    private DataService dataService;
    private LinearLayout actionBtnLayout;
    private Button rejetBtn, donnerBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rdv);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlue)));
        getSupportActionBar().setTitle("Détail rendez-vous");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initViews();
        initListeners();

        dataService = new DataService(this);
        idRdv = getIntent().getStringExtra("idRdv");
        rdv = dataService.getRdvById(idRdv);

        displayRdv();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rejeter_btn:
                showConfirmDialog();
                break;
            default:
                startDetailPatientActivity();
                break;
        }
    }

    private void initViews(){
        icon = findViewById(R.id.icon);
        nomView = findViewById(R.id.nom);
        dateView = findViewById(R.id.date);
        heureView = findViewById(R.id.heure);
        msgView = findViewById(R.id.msg_view);
        actionBtnLayout = findViewById(R.id.actionBtn_layout);
        rejetBtn = findViewById(R.id.rejeter_btn);
        donnerBtn = findViewById(R.id.donner_btn);
    }

    private void displayRdv(){
        if(rdv != null) {
            icon.setImageResource(rdv.getPatientSexe().compareToIgnoreCase("m")==0?R.drawable.homme_ico:R.drawable.femme_ico);
            nomView.setText(rdv.getPrenomPatient()+ " " + rdv.getNomPatient());
            if(rdv.getValidation() == 1) {
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                DateFormat heureFormat = new SimpleDateFormat("hh:mm");
                dateView.setVisibility(View.VISIBLE);
                heureView.setVisibility(View.VISIBLE);
                dateView.setText(dateFormat.format(rdv.getDate()));
                heureView.setText(heureFormat.format(rdv.getDate()));
            }else{
                msgView.setText("VOUS DEMANDE UN RENDEZ-VOUS");
                actionBtnLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showConfirmDialog(){
        MyConfirmDialog confirmDialog = new MyConfirmDialog("Voulez vous rejeter cette demande?");
        confirmDialog.setCancelable(true);
        confirmDialog.show(getSupportFragmentManager(),"confirm");
    }

    private void initListeners(){
        icon.setOnClickListener(this);
        nomView.setOnClickListener(this);
        rejetBtn.setOnClickListener(this);
        donnerBtn.setOnClickListener(this);
    }

    @Override
    public void onConfirmBtnClicked() {
        if(BaseUtils.isNetworkAvailable(this)) {
            final UpdateDialog ud = new UpdateDialog(this, "");
            ud.setCancelable(false);
            dataService.rejeterRendezVous(rdv, new VolleyCallback() {
                @Override
                public void onSuccess(String reponse) {
                    rdv.setId(reponse);
                    dataService.rejeterLocalRendezVous(rdv);
                    ud.dismiss();
                    startRendezVousActivity();
                }

                @Override
                public void onError(Exception e) {
                    ud.dismiss();
                    Toast.makeText(DetailRdvActivity.this, "Erreur:" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else{
            Toast.makeText(this, "Aucun connexion internet", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCancelBtnClicked() {

    }

    private void startDetailPatientActivity(){
        Intent intent = new Intent(DetailRdvActivity.this,ProfilPatientActivity.class);
        intent.putExtra("idPatient",rdv.getIdPatient());
        if(rdv.getValidation()==0)
            intent.putExtra("idAppointment",rdv.getId());
        startActivity(intent);
    }

    private void startRendezVousActivity(){
        Intent intent = new Intent(this,RendezVousFragment.class);
        startActivity(intent);
        finish();
    }
}
