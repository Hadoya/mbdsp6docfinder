package com.mbds.appdoctor;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.mbds.appdoctor.dao.VolleyCallback;
import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.utils.BaseUtils;
import com.mbds.appdoctor.utils.Session;
import com.mbds.appdoctor.utils.TabAdapter;

public class AccueilActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabAdapter adapter;
    private DataService dataService;
    private Session session;

    private TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            viewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlue)));

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        tabLayout.addTab(tabLayout.newTab()/*.setText("Rendez-vous")*/);
        tabLayout.addTab(tabLayout.newTab()/*.setText("Demande")*/);
        tabLayout.addTab(tabLayout.newTab()/*.setText("Liste des patients")*/);
        tabLayout.addTab(tabLayout.newTab()/*.setText("Historiques")*/);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        adapter = new TabAdapter(this,getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        dataService = new DataService(this);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(tabSelectedListener);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_baseline_event_note_24);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_baseline_notifications_active_24);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_baseline_how_to_reg_24);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_baseline_history_24);

        session = new Session(this);

        if(dataService.getConfig().isMajAuto()){
            updateDb();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_accueil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deconnexion_item:
                Intent intent = new Intent(AccueilActivity.this, ConnexionActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.search_item:
                displaySearch(true);
                return true;
            case R.id.update_item:
                updateDb();
                return true;
            case R.id.config_item:
                Intent intentC = new Intent(AccueilActivity.this, ConfigActivity.class);
                startActivity(intentC);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AccueilActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Quitter DocFinder ?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void displaySearch(boolean test){
        try {
            //int tabPosition = tabLayout.getSelectedTabPosition();
            for(Fragment f:getSupportFragmentManager().getFragments()) {
                if (f instanceof RendezVousFragment)
                    ((RendezVousFragment) f).setSearchVisibility(true);
                if(f instanceof PatientListFragment)
                    ((PatientListFragment)f).setSearchVisibility(true);
            }
        }catch (Exception e){
            Toast.makeText(AccueilActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    public DataService getDataService() {
        return dataService;
    }

    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    public void updateDb(){
        try {
            if(BaseUtils.isNetworkAvailable(this)) {
                final UpdateDialog ud = new UpdateDialog(this,"Mise à jour des données");
                ud.setCancelable(false);
                ud.show();
                dataService.updateLocalDb(new VolleyCallback() {
                    int success = 0;
                    String error = "";
                    @Override
                    public void onSuccess(String reponse) {
                        success++;
                        ud.dismiss();
                        if(success > 1) {
                            try {
                                viewPager.getAdapter().notifyDataSetChanged();
                                Toast.makeText(AccueilActivity.this,"Màj effectué",Toast.LENGTH_SHORT).show();
                                ud.dismiss();
                            }catch (Exception e){
                                finish();
                                startActivity(getIntent());
                            }
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("UpdateError",e.getMessage());
                        ud.dismiss();
                        if (error.isEmpty()) {
                            error = e.getMessage();
                            if (e.getMessage().toLowerCase().contains("failed to connect to") || e.getMessage().toLowerCase().contains("bad url")) {
                                Snackbar snackbar = Snackbar.make(viewPager, "Impossible de se connecté au server", Snackbar.LENGTH_LONG).setAction("Action", null);
                                snackbar.getView().setBackgroundColor(ContextCompat.getColor(AccueilActivity.this, R.color.colorRed));
                                snackbar.show();
                            } else Toast.makeText(AccueilActivity.this, "error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }else{
                Snackbar snackbar = Snackbar.make(viewPager, "MAJ Echoué, aucun connexion internet!", Snackbar.LENGTH_LONG).setAction("Action", null);
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorRed));
                snackbar.show();
            }
        }catch (Exception e){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage(e.getMessage());
            builder1.setCancelable(true).create().show();
        }
    }
}
