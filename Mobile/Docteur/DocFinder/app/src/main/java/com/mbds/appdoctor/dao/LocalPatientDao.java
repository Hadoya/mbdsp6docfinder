package com.mbds.appdoctor.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mbds.appdoctor.db.DbHelper;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.utils.BaseUtils;

import java.util.ArrayList;
import java.util.List;

public class LocalPatientDao extends LocalDao {
    public LocalPatientDao(DbHelper dbHelper) {
        super(dbHelper);
    }

    public void addPatient(List<Patient> patients){
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (Patient patient : patients) {
                values.put(dbHelper.COL_PATIENT_ID, patient.getId());
                values.put(dbHelper.COL_PATIENT_FIRSTNAME, patient.getPrenom());
                values.put(dbHelper.COL_PATIENT_LASTNAME, patient.getNom());
                values.put(dbHelper.COL_PATIENT_EMAIL, patient.getEmail());
                values.put(dbHelper.COL_PATIENT_PASSWORD, patient.getPassword());
                values.put(dbHelper.COL_PATIENT_GENDER, patient.getSexe());
                values.put(dbHelper.COL_PATIENT_TELEPHONE, patient.getTelephone());
                values.put(dbHelper.COL_PATIENT_BIRTHDAY, BaseUtils.dateToString(patient.getDateNaissance()));

                db.insert(dbHelper.TABLE_PATIENT, null, values);
                values.clear();
            }
            db.close();
        }catch (SQLException e){
            Log.e("SQLiteERROR",e.getMessage());
        }
    }

    public Patient getPatient(String id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(dbHelper.TABLE_PATIENT,
                new String[] { dbHelper.COL_PATIENT_ID, dbHelper.COL_PATIENT_FIRSTNAME, dbHelper.COL_PATIENT_LASTNAME, dbHelper.COL_PATIENT_EMAIL,dbHelper.COL_PATIENT_PASSWORD, dbHelper.COL_PATIENT_GENDER,dbHelper.COL_PATIENT_TELEPHONE,dbHelper.COL_PATIENT_BIRTHDAY},
                dbHelper.COL_PATIENT_ID+"=?",
                new String[] {id},
                null, null, null, null);
        Log.d("getPatient","cursor ok");
        if(cursor.moveToFirst()) {
            Patient patient = new Patient();
            patient.setId(cursor.getString(0));
            patient.setNom(cursor.getString(1));
            patient.setPrenom(cursor.getString(2));
            patient.setEmail(cursor.getString(3));
            patient.setPassword(cursor.getString(4));
            patient.setSexe(cursor.getString(5));
            patient.setTelephone(cursor.getString(6));
            patient.setDateNaissance(BaseUtils.strToDate(cursor.getString(7)));
            Log.e("PatientPrenom", patient.getPrenom());
            return patient;
        }
        return null;
    }

    public List<Patient> getAllPatients() {
        List<Patient> patients = new ArrayList();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_PATIENT, null);
        if (cursor.moveToFirst()) {
            do {
                Patient patient = new Patient();
                patient.setId(cursor.getString(0));
                patient.setNom(cursor.getString(1));
                patient.setPrenom(cursor.getString(2));
                patient.setEmail(cursor.getString(3));
                patient.setPassword(cursor.getString(4));
                patient.setSexe(cursor.getString(5));
                patient.setTelephone(cursor.getString(6));
                patient.setDateNaissance(BaseUtils.strToDate(cursor.getString(7)));
                patients.add(patient);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return patients;
    }

    public void deletePatient(String id){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(dbHelper.TABLE_PATIENT,dbHelper.COL_PATIENT_ID+"=?",new String[]{id});
        db.close();
    }

    public void deleteAllPatients(){
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_PATIENT, null, null);
            db.close();
        }catch (Exception e){
            Log.e("DeletePatients",e.getMessage());
        }
    }
}
