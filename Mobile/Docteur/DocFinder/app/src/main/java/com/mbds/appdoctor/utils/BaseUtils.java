package com.mbds.appdoctor.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.mbds.appdoctor.table.RendezVous;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BaseUtils {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String[] getNomsPatients(List<RendezVous> rendezVousList){
        String[] nomsPatients = new String[rendezVousList.size()];
        for(int i=0;i<rendezVousList.size();i++){
            nomsPatients[i] = rendezVousList.get(i).getPrenomPatient() +" "+rendezVousList.get(i).getNomPatient();
        }
        return nomsPatients;
    }

    public static String dateToString(Date date){
        return dateFormat.format(date);
    }

    public static Date strToDate(String str) {
        try {
            Log.e("Date",str);
            return dateFormat.parse(str);
        }catch (Exception e){
            return new Date(System.currentTimeMillis());
        }
    }

    public static String buildUrl(String ip, int port)  {
        return "http://"+ip+":"+port;
    }

    public static String dateToMongoDbDate(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss.000+00:00", Locale.getDefault());
        return dateFormat.format(date);
    }

    public static Date getMongoDbDate(String date){
        //1986-09-21T21:00:00.000+00:00
        return new Date(Integer.valueOf(date.substring(0,4))-1900,
                Integer.valueOf(date.substring(5,7))-1,
                Integer.valueOf(date.substring(8,10)),
                Integer.valueOf(date.substring(11,13)),
                Integer.valueOf(date.substring(14,16)));
    }
}
