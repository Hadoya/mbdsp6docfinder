package com.mbds.appdoctor;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.table.Config;

public class ConfigActivity extends AppCompatActivity {
    DataService dataService;
    Config config;

    RadioButton enligneRadio;
    RadioButton localRadio;
    CheckBox majCheckbox;
    TextView ipTextView;
    EditText ipEdit;
    TextView portTextView;
    EditText portEdit;
    Button modifierBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlue)));
        getSupportActionBar().setTitle("Configuration");

        enligneRadio = findViewById(R.id.enligne_radio);
        localRadio = findViewById(R.id.local_radio);
        majCheckbox = findViewById(R.id.maj_checkbox);
        ipTextView = findViewById(R.id.ip_textView);
        ipEdit = findViewById(R.id.ip_input);
        portTextView = findViewById(R.id.port_textView);
        portEdit = findViewById(R.id.port_input);
        modifierBtn = findViewById(R.id.modif_btn);

        dataService = new DataService(this);
        config = dataService.getConfig();

        displayConfig(config);

        ipEdit.addTextChangedListener(onEditTextListener);
        portEdit.addTextChangedListener(onEditTextListener);
    }

    public void displayConfig(Config config){
        if(config != null){
            if(config.isServerLocale()){
                localRadio.setChecked(true);
            }else{
                enligneRadio.setChecked(true);
                ipEdit.setEnabled(false);
                portEdit.setEnabled(false);
            }
            ipEdit.setText(config.getIp());
            portEdit.setText(config.getPort()+"");
            majCheckbox.setChecked(config.isMajAuto());
        }
    }

    public void onRadioButtonClicked(View view){
        enableModifBtn(leConfigEstModifie());
        switch (view.getId()){
            case R.id.local_radio:
                ipEdit.setEnabled(true);
                portEdit.setEnabled(true);
                ipTextView.setTextColor(getResources().getColor(R.color.colorDarkGray));
                ipEdit.setTextColor(getResources().getColor(R.color.colorDarkGray));
                portTextView.setTextColor(getResources().getColor(R.color.colorDarkGray));
                portEdit.setTextColor(getResources().getColor(R.color.colorDarkGray));
                break;
            default:
                ipEdit.setEnabled(false);
                portEdit.setEnabled(false);
                ipTextView.setTextColor(getResources().getColor(R.color.colorLightGray));
                ipEdit.setTextColor(getResources().getColor(R.color.colorLightGray));
                portTextView.setTextColor(getResources().getColor(R.color.colorLightGray));
                portEdit.setTextColor(getResources().getColor(R.color.colorLightGray));
                break;
        }
    }

    public void onCheckboxClicked(View view){
        enableModifBtn(leConfigEstModifie());
    }

    public TextWatcher onEditTextListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!editable.toString().isEmpty() && !ipEdit.getText().toString().isEmpty() && !portEdit.getText().toString().isEmpty()) {
                enableModifBtn(leConfigEstModifie());
            } else {
                enableModifBtn(false);
            }
        }
    };

    public void enableModifBtn(boolean test){
        if(modifierBtn.isEnabled() != test) {
            modifierBtn.setEnabled(test);
            modifierBtn.setBackgroundColor(test ? getResources().getColor(R.color.colorBlue) : getResources().getColor(R.color.colorLightGray));
            modifierBtn.setTextColor(test ? getResources().getColor(R.color.colorWhite) : getResources().getColor(R.color.colorGray));
        }
    }

    public boolean leConfigEstModifie(){
        return config.isServerLocale() != localRadio.isChecked() || config.isMajAuto() != majCheckbox.isChecked()
                || ( config.isServerLocale() == localRadio.isChecked()
                        && config.getIp().compareTo(ipEdit.getText().toString()) != 0
                        || config.getPort() != Integer.valueOf(portEdit.getText().toString()));
    }

    public void onModifClicked(View view){
        config.setMajAuto(majCheckbox.isChecked());
        config.setServerLocale(localRadio.isChecked());
        if(localRadio.isChecked()){
            config.setIp(ipEdit.getText().toString());
            config.setPort(Integer.valueOf(portEdit.getText().toString()));
        }
        try {
            dataService.updateConfig(config);
            finish();
        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }



    @Override
    public void onBackPressed() {
        finish();
    }
}
