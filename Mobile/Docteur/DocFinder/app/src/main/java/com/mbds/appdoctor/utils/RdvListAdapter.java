package com.mbds.appdoctor.utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mbds.appdoctor.R;
import com.mbds.appdoctor.table.RendezVous;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class RdvListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private List<RendezVous> rendezVousList;
    private String[] listeNom;
    private String[] listeDate;
    private String[] listeHeure;
    private String[] listeSexe;
    private Integer[] imgid;


    public RdvListAdapter(Activity context, String[] listeNom, List<RendezVous> rendezVousList) {
        super(context, R.layout.item_list_rdv,listeNom);
        this.context = context;
        initList(listeNom,rendezVousList);
    }

    public void initList(String[] listeNom,List<RendezVous> rendezVousList){
        this.listeNom = listeNom;
        this.rendezVousList = rendezVousList;
        this.listeDate = new String[rendezVousList.size()];
        this.listeHeure = new String[rendezVousList.size()];
        this.listeSexe = new String[rendezVousList.size()];
        this.imgid = new Integer[rendezVousList.size()];

        Integer idH = R.drawable.homme_ico;
        Integer idF = R.drawable.femme_ico;
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat heureFormat = new SimpleDateFormat("hh:mm");
        for (int i = 0; i < this.listeNom.length; i++) {
            RendezVous rdv = rendezVousList.get(i);
            if(rendezVousList.get(i).getValidation() == 1) {
                this.listeDate[i] = dateFormat.format(rdv.getDate());
                this.listeHeure[i] = heureFormat.format(rdv.getDate());
            }
            this.listeSexe[i] = rdv.getPatientSexe();
            this.imgid[i] = rdv.getPatientSexe().compareToIgnoreCase("m") == 0 ? idH : idF;
        }
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.item_list_rdv, null,true);

        TextView nomText = (TextView) rowView.findViewById(R.id.nom);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView dateText = (TextView) rowView.findViewById(R.id.date);
        TextView heureText = (TextView) rowView.findViewById(R.id.heure);
        TextView sexeText = (TextView) rowView.findViewById(R.id.sexe);

        nomText.setText(listeNom[position]);
        imageView.setImageResource(imgid[position]);
        dateText.setText(listeDate[position]);
        heureText.setText(listeHeure[position]);
        sexeText.setText(listeSexe[position].compareToIgnoreCase("m")==0?"Homme":"Femme");

        return rowView;
    }
}
