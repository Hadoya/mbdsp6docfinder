package com.mbds.appdoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;
import com.mbds.appdoctor.utils.PatientListAdapter;
import com.mbds.appdoctor.utils.RdvListAdapter;

import java.util.Collections;
import java.util.List;

public class PatientListFragment extends Fragment implements View.OnClickListener {

    protected ListView listView;
    protected List<Patient> patientList;
    protected List<Patient> patientListRecherche;
    protected DataService dataService;
    protected PatientListAdapter patientListAdapter;
    private FloatingActionButton scanBtn;

    protected EditText bareRecherche;
    protected LinearLayout searchLayout;
    protected boolean isSearching;
    protected InputMethodManager imm;
    protected ImageView closeSearchBtn;

    public PatientListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_list, container, false);
        try {
            initModels();
            initAdapters();
            initViews(view);
            initListeners();
        } catch (Exception e) {
            Log.e("DFLog", "onCreateView PFragm error: " + e.getMessage());
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scanBtn:
                startScannerActivity();
                break;
            default:
                break;
        }
    }

    protected void initListView(List<Patient> list) {
        if (list == null)
            list = patientList;
        patientListAdapter = new PatientListAdapter(getActivity(), list);
        listView.setAdapter(patientListAdapter);
    }

    private void initAdapters() {
        Collections.sort(patientList);
        patientListAdapter = new PatientListAdapter(getActivity(), patientList);
    }

    private void initModels() {
        dataService = new DataService(getActivity());
        patientList = dataService.getListePatient();
    }

    private void initListeners() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (isSearching)
                    startProfilPatientActivity(patientListRecherche.get(i).getId());
                else
                    startProfilPatientActivity(patientList.get(i).getId());
            }
        });

        scanBtn.setOnClickListener(this);

        bareRecherche.addTextChangedListener(onSearchListener);
        closeSearchBtn.setOnClickListener(closeSearchListener);
    }

    private void initViews(View view) {
        listView = view.findViewById(R.id.listView);
        listView.setAdapter(patientListAdapter);
        scanBtn = view.findViewById(R.id.scanBtn);

        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        bareRecherche = view.findViewById(R.id.bare_recherche);
        searchLayout = view.findViewById(R.id.search_layout);
        closeSearchBtn = view.findViewById(R.id.close_search_btn);
    }

    private void startProfilPatientActivity(String idPatient) {
        Intent intent = new Intent(getActivity(), ProfilPatientActivity.class);
        intent.putExtra("idPatient", idPatient);
        startActivity(intent);
    }

    private void startScannerActivity() {
        Intent intent = new Intent(getActivity(), ScannerActivity.class);
        startActivity(intent);
    }

    protected void setSearchVisibility(boolean test) {
        if (test) {
            searchLayout.setVisibility(View.VISIBLE);
            bareRecherche.requestFocus();
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
            params.setMargins(0, 140, 0, 0);
            listView.setLayoutParams(params);

            //show keyboard on open
            imm.showSoftInput(bareRecherche, InputMethodManager.SHOW_IMPLICIT);
        } else {
            searchLayout.setVisibility(View.INVISIBLE);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            listView.setLayoutParams(params);
            isSearching = false;
            bareRecherche.setText("");
            initListView(patientList);

            //hide keyboard on close
            imm.hideSoftInputFromWindow(bareRecherche.getWindowToken(), 0);
        }
    }

    protected View.OnClickListener closeSearchListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setSearchVisibility(false);
        }
    };

    protected TextWatcher onSearchListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence != "") {
                isSearching = true;
                patientListRecherche = dataService.getPatientByName(patientList, charSequence + "");
                if (patientListRecherche != null) {
                    initListView(patientListRecherche);
                }
            } else {
                isSearching = false;
                initListView(patientList);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };
}