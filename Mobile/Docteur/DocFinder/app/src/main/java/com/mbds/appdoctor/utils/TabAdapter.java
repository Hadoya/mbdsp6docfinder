package com.mbds.appdoctor.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.mbds.appdoctor.PatientListFragment;
import com.mbds.appdoctor.PendingFragment;
import com.mbds.appdoctor.HistoriqueFragment;
import com.mbds.appdoctor.RendezVousFragment;

public class TabAdapter extends FragmentPagerAdapter {
    private RendezVousFragment rendezVousFragment;
    private PendingFragment pendingFragment;
    private PatientListFragment patientListFragment;
    private HistoriqueFragment histFragment;

    Context context;
    int totalTabs;
    public TabAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        try {
            rendezVousFragment = new RendezVousFragment();
            pendingFragment = new PendingFragment();
            patientListFragment = new PatientListFragment();
            histFragment = new HistoriqueFragment();
        }catch (Exception e){
            Log.e("DFLog","Init TabAdapter failed: "+e.getMessage());
        }
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Log.e("DFLog","TabAdapter getItem pos="+position);
        switch (position) {
            case 0:
                return rendezVousFragment;
            case 1:
                return pendingFragment;
            case 2:
                return patientListFragment;
            case 3:
                return histFragment;
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }

    public RendezVousFragment getRendezVousFragment() {
        return rendezVousFragment;
    }

    public void setRendezVousFragment(RendezVousFragment rendezVousFragment) {
        this.rendezVousFragment = rendezVousFragment;
    }

    public PatientListFragment getPatientListFragment() {
        return patientListFragment;
    }

    public void setPatientListFragment(PatientListFragment patientListFragment) {
        this.patientListFragment = patientListFragment;
    }

    public HistoriqueFragment getHistFragment() {
        return histFragment;
    }

    public void setHistFragment(HistoriqueFragment histFragment) {
        this.histFragment = histFragment;
    }

    public PendingFragment getPendingFragment() {
        return pendingFragment;
    }

    public void setPendingFragment(PendingFragment pendingFragment) {
        this.pendingFragment = pendingFragment;
    }
}
