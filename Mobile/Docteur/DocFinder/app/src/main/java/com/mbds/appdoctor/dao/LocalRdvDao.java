package com.mbds.appdoctor.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mbds.appdoctor.db.DbHelper;
import com.mbds.appdoctor.table.Docteur;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LocalRdvDao extends LocalDao {
    public LocalRdvDao(DbHelper dbHelper) {
        super(dbHelper);
    }


    public RendezVous getRendezVous(String id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(dbHelper.TABLE_APPOINTMENT,
                getRdvCols(),
                dbHelper.COL_APPOINTMENT_ID + "=?",
                new String[]{id},
                null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        RendezVous rdv = getRendezVous(cursor);
        cursor.close();
        db.close();
        return rdv;
    }

    public void addRendezVous(RendezVous rdv) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        putValues(values, rdv);
        db.insert(dbHelper.TABLE_APPOINTMENT, null, values);
        db.close();
    }

    public void addRendezVous(List<RendezVous> rdvs) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (RendezVous rdv : rdvs) {
                putValues(values, rdv);
                db.insert(dbHelper.TABLE_APPOINTMENT, null, values);
                values.clear();
            }
            db.close();
        } catch (Exception e) {
            Log.e("InsertAllRdv", e.getMessage());
        }
    }

    public List<RendezVous> searchRendezVous(List<RendezVous> liste, String mot) {
        mot = mot.trim();
        List<RendezVous> rdvs = new ArrayList();
        for (RendezVous rdv : liste) {
            if ((rdv.getPrenomPatient() + rdv.getNomPatient()).toLowerCase().contains(mot))
                rdvs.add(rdv);
        }
        return rdvs;
    }

    public List<Patient> searchPatient(List<Patient> liste, String mot) {
        mot = mot.trim();
        List<Patient> patientList = new ArrayList();
        for (Patient patient : liste) {
            if ((patient.getNom() + patient.getPrenom()).toLowerCase().contains(mot))
                patientList.add(patient);
        }
        return patientList;
    }

    public List<RendezVous> getAllAttenteRendezVous() {
        List<RendezVous> rdvs = new ArrayList();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_APPOINTMENT + " where "
                + dbHelper.COL_APPOINTMENT_VALIDATION + "=0 ", null);
        if (cursor.moveToFirst()) {
            do {
                RendezVous rdv = getRendezVous(cursor);
                Log.e("DFLog", "attente " + rdv.toString());
                rdvs.add(rdv);
            } while (cursor.moveToNext());
        }
        return rdvs;
    }

    public List<RendezVous> getAllNewRendezVous() {
        List<RendezVous> rdvs = new ArrayList();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_APPOINTMENT + " where "
                + dbHelper.COL_APPOINTMENT_APPOINTMENTAT + ">=datetime('now','localtime') and "
                + dbHelper.COL_APPOINTMENT_VALIDATION + "=1"
                + " order by " + dbHelper.COL_APPOINTMENT_APPOINTMENTAT + " asc", null);
        if (cursor.moveToFirst()) {
            do {
                RendezVous rdv = getRendezVous(cursor);
                Log.e("DFLog", "nouveau " + rdv.toString());
                rdvs.add(rdv);
            } while (cursor.moveToNext());
        }
        return rdvs;
    }

    public List<RendezVous> getAllOldRendezVous() {
        List<RendezVous> rdvs = new ArrayList();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_APPOINTMENT + " where "
                + dbHelper.COL_APPOINTMENT_APPOINTMENTAT + "<datetime('now','localtime') and "
                + dbHelper.COL_APPOINTMENT_VALIDATION + "=1"
                + " order by " + dbHelper.COL_APPOINTMENT_APPOINTMENTAT + " asc", null);
        if (cursor.moveToFirst()) {
            do {
                RendezVous rdv = getRendezVous(cursor);
                Log.e("DFLog", "historique " + rdv.toString());
                rdvs.add(rdv);
            } while (cursor.moveToNext());
        }
        return rdvs;
    }

    public List<RendezVous> getAllRendezVous() {
        List<RendezVous> rdvs = new ArrayList();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * from " + dbHelper.TABLE_APPOINTMENT, null);
        if (cursor.moveToFirst()) {
            do {
                RendezVous rdv = getRendezVous(cursor);
                rdvs.add(rdv);
            } while (cursor.moveToNext());
        }
        return rdvs;
    }

    public void deleteRendezVous(String id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(dbHelper.TABLE_APPOINTMENT, dbHelper.COL_APPOINTMENT_ID + "=?", new String[]{id});
        db.close();
    }

    public void deleteAllRendezVous() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(dbHelper.TABLE_APPOINTMENT, null, null);
            db.close();
        } catch (Exception e) {
            Log.e("DeleteRdv", e.getMessage());
        }
    }

    public int validerRendezVous(RendezVous rdv, int validation) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(dbHelper.COL_APPOINTMENT_VALIDATION, validation);
        if (validation == 1)
            values.put(dbHelper.COL_APPOINTMENT_APPOINTMENTAT, BaseUtils.dateToString(rdv.getDate()));
        return db.update(dbHelper.TABLE_APPOINTMENT, values, dbHelper.COL_APPOINTMENT_ID + "=?", new String[]{rdv.getId()});
    }

    private void putValues(ContentValues values, RendezVous rdv) {
        values.put(dbHelper.COL_APPOINTMENT_ID, rdv.getId());
        values.put(dbHelper.COL_APPOINTMENT_IDDOCTOR, rdv.getIdDoctor());
        values.put(dbHelper.COL_APPOINTMENT_IDPATIENT, rdv.getIdPatient());
        values.put(dbHelper.COL_APPOINTMENT_APPOINTMENTAT, BaseUtils.dateToString(rdv.getDate()));
        values.put(dbHelper.COL_APPOINTMENT_CREATEAT, BaseUtils.dateToString(rdv.getCreateAt()));
        values.put(dbHelper.COL_APPOINTMENT_PFIRSTNAME, rdv.getPrenomPatient());
        values.put(dbHelper.COL_APPOINTMENT_PLASTNAME, rdv.getNomPatient());
        values.put(dbHelper.COL_APPOINTMENT_PGENDER, rdv.getPatientSexe());
        values.put(dbHelper.COL_APPOINTMENT_VALIDATION, rdv.getValidation());
    }

    private RendezVous getRendezVous(Cursor cursor) {
        RendezVous rdv = new RendezVous();
        rdv.setId(cursor.getString(0));
        rdv.setIdDoctor(cursor.getString(1));
        rdv.setIdPatient(cursor.getString(2));
        rdv.setNomPatient(cursor.getString(3));
        rdv.setPrenomPatient(cursor.getString(4));
        rdv.setDate(BaseUtils.strToDate(cursor.getString(5)));
        rdv.setCreateAt(BaseUtils.strToDate(cursor.getString(6)));
        rdv.setPatientSexe(cursor.getString(7));
        rdv.setValidation(cursor.getInt(8));
        return rdv;
    }

    private String[] getRdvCols() {
        return new String[]{
                dbHelper.COL_APPOINTMENT_ID,
                dbHelper.COL_APPOINTMENT_IDDOCTOR,
                dbHelper.COL_APPOINTMENT_IDPATIENT,
                dbHelper.COL_APPOINTMENT_PLASTNAME,
                dbHelper.COL_APPOINTMENT_PFIRSTNAME,
                dbHelper.COL_APPOINTMENT_APPOINTMENTAT,
                dbHelper.COL_APPOINTMENT_CREATEAT,
                dbHelper.COL_APPOINTMENT_PGENDER,
                dbHelper.COL_APPOINTMENT_VALIDATION
        };
    }
}
