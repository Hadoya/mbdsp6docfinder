package com.mbds.appdoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;
import com.mbds.appdoctor.utils.RdvListAdapter;

import java.util.ArrayList;
import java.util.List;

public class RendezVousFragment extends Fragment {
    protected View view;
    protected ListView listView;
    protected EditText bareRecherche;
    protected LinearLayout searchLayout;
    protected RdvListAdapter adapter;
    protected List<RendezVous> rdvList;
    protected List<RendezVous> rdvListRecherche;
    protected LinearLayout listViewLayout;
    protected ImageView closeSearchBtn;
    protected boolean isSearching;
    protected InputMethodManager imm;
    protected DataService dataService;

    public RendezVousFragment() {

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rendezvous, container, false);
        initViews(view);
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        rdvListRecherche = new ArrayList<>();
        dataService = ((AccueilActivity) getActivity()).getDataService();

        rdvList = dataService.getListeNewRdvs();
        initListView(rdvList);
        initListeners();
        return view;
    }

    protected void initViews(View view) {
        listView = view.findViewById(R.id.rdv_list);
        bareRecherche = view.findViewById(R.id.bare_recherche);
        searchLayout = view.findViewById(R.id.search_layout);
        listViewLayout = view.findViewById(R.id.listViewLayout);
        closeSearchBtn = view.findViewById(R.id.close_search_btn);
    }

    protected void initListeners() {
        bareRecherche.addTextChangedListener(onSearchListener);
        listView.setOnItemClickListener(itemClickListener);
        closeSearchBtn.setOnClickListener(closeSearchListener);
    }

    protected TextWatcher onSearchListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence != "") {
                isSearching = true;
                rdvListRecherche = dataService.getRdvByPatient(rdvList, charSequence + "");
                if (rdvListRecherche != null) {
                    initListView(rdvListRecherche);
                }
            } else {
                isSearching = false;
                initListView(rdvList);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    protected void initListView(List<RendezVous> listRdv) {
        if (listRdv == null)
            listRdv = rdvList;
        adapter = new RdvListAdapter(getActivity(), BaseUtils.getNomsPatients(listRdv), listRdv);
        listView.setAdapter(adapter);
    }

    protected AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent(view.getContext(), DetailRdvActivity.class);
            intent.putExtra("idRdv", isSearching ? rdvListRecherche.get(i).getId() : rdvList.get(i).getId());
            startActivity(intent);
        }
    };

    protected View.OnClickListener closeSearchListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setSearchVisibility(false);
        }
    };

    protected void setSearchVisibility(boolean test) {
        if (test) {
            searchLayout.setVisibility(View.VISIBLE);
            bareRecherche.requestFocus();
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
            params.setMargins(0, 140, 0, 0);
            listView.setLayoutParams(params);

            //show keyboard on open
            imm.showSoftInput(bareRecherche, InputMethodManager.SHOW_IMPLICIT);
        } else {
            searchLayout.setVisibility(View.INVISIBLE);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            listView.setLayoutParams(params);
            isSearching = false;
            bareRecherche.setText("");
            initListView(rdvList);

            //hide keyboard on close
            imm.hideSoftInputFromWindow(bareRecherche.getWindowToken(), 0);
        }
    }
}
