package com.mbds.appdoctor.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {
    public  static final String DATABASE_NAME = "DocFinderDoc";
    public  static final int DATABASE_VERSION = 1;

    //TABLE DOCTOR
    public static final String TABLE_DOCTOR = "Doctor";
    public static final String COL_DOCTOR_ID = "Id";
    public static final String COL_DOCTOR_LASTNAME = "Last_Name";
    public static final String COL_DOCTOR_FIRSTNAME = "First_Name";
    public static final String COL_DOCTOR_IDDOCSPEC = "Id_Doctor_Speciality";
    public static final String COL_DOCTOR_TELEPHONE = "Telephone";
    public static final String COL_DOCTOR_ADRESSE = "Adresse";
    public static final String COL_DOCTOR_TOWN = "Town";
    public static final String COL_DOCTOR_CITY = "City";
    public static final String COL_DOCTOR_DESCRIPTION = "Description";
    public static final String COL_DOCTOR_TARIF = "Tarif";
    public static final String COL_DOCTOR_ETATVALIDATION = "Etat_Validation";
    public static final String COL_DOCTOR_CREATEAT = "Create_At";
    public static final String COL_DOCTOR_UPDATEAT = "Update_At";
    public static final String COL_DOCTOR_EMAIL = "Email";
    public static final String COL_DOCTOR_PASSWORD = "Password";

    //TABLE PATIENT
    public static final String TABLE_PATIENT = "Patient";
    public static final String COL_PATIENT_ID = "Id";
    public static final String COL_PATIENT_LASTNAME = "Last_Name";
    public static final String COL_PATIENT_FIRSTNAME ="First_Name";
    public static final String COL_PATIENT_EMAIL = "Email";
    public static final String COL_PATIENT_PASSWORD = "Password";
    public static final String COL_PATIENT_GENDER = "Gender";
    public static final String COL_PATIENT_TELEPHONE = "Telephone";
    public static final String COL_PATIENT_BIRTHDAY = "Birthday";
    public static final String COL_PATIENT_CREATEAT = "Create_At";
    public static final String COL_PATIENT_UPDATEAT = "Update_At";

    //TABLE APPOINTMENT
    public static final String TABLE_APPOINTMENT = "Appointment";
    public static final String COL_APPOINTMENT_ID = "Id";
    public static final String COL_APPOINTMENT_IDDOCTOR = "Id_Doctor";
    public static final String COL_APPOINTMENT_IDPATIENT = "Id_Patient";
    public static final String COL_APPOINTMENT_PLASTNAME = "Patient_Last_Name";
    public static final String COL_APPOINTMENT_PFIRSTNAME = "Patient_First_Name";
    public static final String COL_APPOINTMENT_APPOINTMENTAT = "Appointment_At";
    public static final String COL_APPOINTMENT_CREATEAT = "Create_At";
    public static final String COL_APPOINTMENT_PGENDER = "Patient_Gender";
    public static final String COL_APPOINTMENT_VALIDATION = "Validation";

    //TABLE CONFIGURATION
    public static final String TABLE_CONFIG = "Config";
    public static final String COL_CONFIG_ID = "Id";
    public static final String COL_CONFIG_MAJ = "Maj_Auto";
    public static final String COL_CONFIG_SERVER = "Local_Server";
    public static final String COL_CONFIG_IP = "Ip";
    public static final String COL_CONFIG_PORT = "Port";


    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE table " + TABLE_DOCTOR + " ("
                + COL_DOCTOR_ID+" TEXT PRIMARY KEY,"
                + COL_DOCTOR_LASTNAME+" TEXT, "
                + COL_DOCTOR_FIRSTNAME+" TEXT, "
                + COL_DOCTOR_IDDOCSPEC+" TEXT, "
                + COL_DOCTOR_TELEPHONE+" INTEGER, "
                + COL_DOCTOR_ADRESSE+" TEXT,"
                + COL_DOCTOR_TOWN+" TEXT,"
                + COL_DOCTOR_CITY+" TEXT,"
                + COL_DOCTOR_DESCRIPTION+" TEXT,"
                + COL_DOCTOR_TARIF+" TEXT,"
                + COL_DOCTOR_ETATVALIDATION+" INTEGER,"
                + COL_DOCTOR_CREATEAT+" DATE,"
                + COL_DOCTOR_UPDATEAT+" DATE,"
                + COL_DOCTOR_EMAIL + " TEXT,"
                + COL_DOCTOR_PASSWORD + " TEXT)"
        );

        sqLiteDatabase.execSQL("CREATE table " + TABLE_PATIENT + " ("
                + COL_PATIENT_ID +" TEXT PRIMARY KEY, "
                + COL_PATIENT_FIRSTNAME +" TEXT,"
                + COL_DOCTOR_LASTNAME + " TEXT,"
                + COL_PATIENT_EMAIL + " TEXT,"
                + COL_PATIENT_PASSWORD + " TEXT,"
                + COL_PATIENT_GENDER + " TEXT,"
                + COL_PATIENT_TELEPHONE + " TEXT,"
                + COL_PATIENT_BIRTHDAY + " DATE,"
                + COL_PATIENT_CREATEAT + " DATE,"
                + COL_PATIENT_UPDATEAT + " DATE)"
        );

        sqLiteDatabase.execSQL("CREATE table " + TABLE_APPOINTMENT + " ("
                + COL_APPOINTMENT_ID +" TEXT PRIMARY KEY, "
                + COL_APPOINTMENT_IDDOCTOR +" INTEGER,"
                + COL_APPOINTMENT_IDPATIENT + " INTEGER,"
                + COL_APPOINTMENT_PLASTNAME + " TEXT,"
                + COL_APPOINTMENT_PFIRSTNAME + " TEXT,"
                + COL_APPOINTMENT_APPOINTMENTAT + " DATE,"
                + COL_APPOINTMENT_CREATEAT + " DATE,"
                + COL_APPOINTMENT_PGENDER + " TEXTE,"
                + COL_APPOINTMENT_VALIDATION + " INTEGER)"
        );

        sqLiteDatabase.execSQL("CREATE table "+ TABLE_CONFIG + " ("
                + COL_CONFIG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COL_CONFIG_SERVER +" INTEGER DEFAULT 0,"
                + COL_CONFIG_IP + " TEXT,"
                + COL_CONFIG_PORT + " INTEGER,"
                + COL_CONFIG_MAJ + " INTEGER DEFAULT 0)"
        );

        sqLiteDatabase.execSQL("INSERT INTO "+TABLE_CONFIG+" "
                + "(" + COL_CONFIG_ID + "," + COL_CONFIG_SERVER + "," + COL_CONFIG_IP + "," + COL_CONFIG_PORT + "," + COL_CONFIG_MAJ + ")"
                + " VALUES (1,0,'192.168.1.101',5000,0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PATIENT);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCTOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_APPOINTMENT);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CONFIG);
        onCreate(sqLiteDatabase);
    }
}
