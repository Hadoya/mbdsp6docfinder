package com.mbds.appdoctor;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import org.w3c.dom.Text;

public class UpdateDialog extends Dialog {
    TextView contentText;
    String text = "";
    public UpdateDialog(@NonNull Context context, String text) {
        super(context);
        this.text = text;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_update_db);
        contentText = (TextView)findViewById(R.id.dialog_text);
        contentText.setText(text);
        Log.e("UpdateDialog","onCreate : contentText is "+ ((contentText==null)?"":"not ")+"null");
    }

    public void setText(String text){
        this.text = text;
    }

}
