package com.mbds.appdoctor.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mbds.appdoctor.R;
import com.mbds.appdoctor.table.Patient;

import java.util.ArrayList;
import java.util.List;

public class PatientListAdapter extends ArrayAdapter<Patient> {
    private List<Patient> patientListCopy;
    private List<Patient> patientListOriginal;
    private Context context;

    private static class ViewHolder {
        ImageView iconView;
        TextView nameView;
        TextView emailView;
        TextView telephoneView;
    }

    public PatientListAdapter(@NonNull Context context, List<Patient> patientList) {
        super(context, R.layout.item_list_patient, patientList);
        this.patientListCopy = patientList;
        patientListOriginal = new ArrayList<>();
        patientListOriginal.addAll(patientList);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Patient patient = getItem(position);
        PatientListAdapter.ViewHolder viewHolder;
        final View result;
        if (convertView == null) {
            viewHolder = new PatientListAdapter.ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.item_list_patient, parent, false);
            viewHolder.nameView = convertView.findViewById(R.id.nameView);
            viewHolder.emailView = convertView.findViewById(R.id.emailView);
            viewHolder.telephoneView = convertView.findViewById(R.id.telephoneView);
            viewHolder.iconView = convertView.findViewById(R.id.iconView);
            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (PatientListAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        Integer idMan = R.drawable.homme_ico;
        Integer idWoman = R.drawable.femme_ico;

        viewHolder.nameView.setText("Dr. "+patient.getNom()+" "+patient.getPrenom());
        viewHolder.emailView.setText(patient.getEmail());
        viewHolder.telephoneView.setText(patient.getTelephone());
        viewHolder.iconView.setImageResource(patient.getSexe().compareToIgnoreCase("m")==0?idMan:idWoman);

        return convertView;
    }
}
