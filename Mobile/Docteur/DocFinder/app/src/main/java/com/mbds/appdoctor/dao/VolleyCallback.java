package com.mbds.appdoctor.dao;

import com.mbds.appdoctor.table.RendezVous;

import org.json.JSONException;

import java.util.List;

public interface VolleyCallback {
     void  onSuccess(String reponse);
     void onError(Exception e);
}
