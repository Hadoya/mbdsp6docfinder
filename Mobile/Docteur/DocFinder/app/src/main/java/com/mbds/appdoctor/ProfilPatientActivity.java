package com.mbds.appdoctor;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mbds.appdoctor.dao.VolleyCallback;
import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.table.Patient;
import com.mbds.appdoctor.table.RendezVous;
import com.mbds.appdoctor.utils.BaseUtils;
import com.mbds.appdoctor.utils.Session;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ProfilPatientActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView icon;
    private TextView nomView;
    private TextView prenomView;
    private TextView sexeView;
    private TextView contanctView;
    private TextView ageView;
    private Patient patient;
    private DataService dataService;
    private EditText dateEdit, timeEdit;
    private Button ajouterRdvBtn;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private RendezVous rdv;
    private Session session;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_patient);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlue)));
        getSupportActionBar().setTitle("Détail patient");

        initViews();

        dataService = new DataService(this);
        session = new Session(this);

        String idPatient = getIntent().getStringExtra("idPatient");
        String idAppointment = getIntent().getStringExtra("idAppointment");
        Log.e("DFLog","initRdv avec id="+idAppointment);
        initRdv(idAppointment);
        patient = dataService.getPatientById(idPatient);
        displayPatient();
    }

    private void displayPatient() {
        icon.setImageResource(patient.getSexe().compareToIgnoreCase("m") == 0 ? R.drawable.homme_ico : R.drawable.femme_ico);
        nomView.setText(patient.getNom());
        prenomView.setText(patient.getPrenom());
        sexeView.setText(patient.getSexe().compareToIgnoreCase("m") == 0 ? "Homme" : "Femme");
        ageView.setText(patient.getAge() + " ans");
        contanctView.setText(patient.getTelephone());
    }

    private void initViews() {
        icon = findViewById(R.id.iconPatient);
        nomView = findViewById(R.id.nom);
        prenomView = findViewById(R.id.prenom);
        sexeView = findViewById(R.id.sexe);
        ageView = findViewById(R.id.age);
        contanctView = findViewById(R.id.contact);
        dateEdit = findViewById(R.id.date_edit);
        timeEdit = findViewById(R.id.time_edit);
        ajouterRdvBtn = findViewById(R.id.ajouter_rdv_btn);
    }

    private void initRdv(String id) {
        Log.e("DFLog","initRdv id:");
        if (id != null) {
            rdv = dataService.getRdvById(id);
        }
    }

    public void onClick(View view) {
        if (view == dateEdit) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DatePickerTheme,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            dateEdit.setText((dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "-" + ((monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : (monthOfYear + 1)) + "-" + year);
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (view == timeEdit) {
            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.DatePickerTheme,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            timeEdit.setText((hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute));
                        }
                    }, 12, 0, true);
            timePickerDialog.show();
        }
        if (view == ajouterRdvBtn) {
            if (BaseUtils.isNetworkAvailable(this)) {
                if (rdv == null || rdv.getId() == null)
                    ajouterRdv();
                else
                    updateRdv();
            } else {
                Toast.makeText(this, "Aucun connexion internet", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void ajouterRdv() {
        final UpdateDialog ud = new UpdateDialog(this, "");
        ud.setCancelable(false);
        try {
            Log.e("DFLog", "ADD date==" + (dateEdit.getText().toString() + " " + timeEdit.getText().toString() + ":00"));
            if (dateEdit.getText().toString().compareTo("") != 0 && timeEdit.getText().toString().compareTo("") != 0) {
                Date date = dateFormat.parse(dateEdit.getText().toString() + " " + timeEdit.getText().toString() + ":00");
                if (date.after(new Date(System.currentTimeMillis()))) {
                    ud.show();
                    rdv = new RendezVous();
                    rdv.setIdDoctor(session.getUserId());
                    rdv.setIdPatient(patient.getId());
                    rdv.setNomPatient(patient.getNom());
                    rdv.setPrenomPatient(patient.getPrenom());
                    rdv.setDate(date);
                    rdv.setPatientSexe(patient.getSexe());
                    rdv.setValidation(1);
                    dataService.addRendezVous(rdv, new VolleyCallback() {
                        @Override
                        public void onSuccess(String reponse) {
                            rdv.setId(reponse);
                            dataService.addLocalRendezVous(rdv);
                            ud.dismiss();
                            launchDetailRdvActivity();
                        }

                        @Override
                        public void onError(Exception e) {
                            ud.dismiss();
                            Toast.makeText(ProfilPatientActivity.this, "Erreur:" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    throw new Exception("La date doit être supérieur à aujourd'hui");
                }
            } else {
                Toast.makeText(this, "Veuillez choisir la date-heure du rendez-vous", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            ud.dismiss();
            Toast.makeText(this, "Erreur: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void updateRdv() {

        final UpdateDialog ud = new UpdateDialog(this, "");
        ud.setCancelable(false);
        Log.e("DFLog", "UPDATE date==" + (dateEdit.getText().toString() + " " + timeEdit.getText().toString() + ":00"));
        if (dateEdit.getText().toString().compareTo("") != 0 && timeEdit.getText().toString().compareTo("") != 0) {
            try {
                Date date = dateFormat.parse(dateEdit.getText().toString() + " " + timeEdit.getText().toString() + ":00");
                if (date.after(new Date(System.currentTimeMillis()))) {
                    Log.e("DFLog","init");
                    ud.show();
                    rdv.setDate(date);
                    Log.e("DFLog","set Date OK");
                    rdv.setValidation(1);

                    dataService.validerRendezVous(rdv, new VolleyCallback() {
                        @Override
                        public void onSuccess(String reponse) {
                            rdv.setValidation(1);
                            dataService.validerLocalRendezVous(rdv);
                            ud.dismiss();
                            launchDetailRdvActivity();
                        }

                        @Override
                        public void onError(Exception e) {
                            ud.dismiss();
                            Toast.makeText(ProfilPatientActivity.this, "Erreur:" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

                } else {
                    throw new Exception("La date doit être supérieur à aujourd'hui");
                }
            } catch (Exception e) {
                ud.dismiss();
                Log.e("DFLog", "Exception:" + e.getMessage());
                Toast.makeText(this, "Exception:" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Veuillez choisir la date-heure du rendez-vous", Toast.LENGTH_LONG).show();
        }
    }

    private void launchDetailRdvActivity() {
        Intent intent = new Intent(this, DetailRdvActivity.class);
        intent.putExtra("idRdv", rdv.getId());
        startActivity(intent);
        finish();
    }
}
