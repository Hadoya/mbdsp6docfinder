package com.mbds.appdoctor;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mbds.appdoctor.dao.VolleyCallback;
import com.mbds.appdoctor.db.DbHelper;
import com.mbds.appdoctor.service.DataService;
import com.mbds.appdoctor.table.Config;
import com.mbds.appdoctor.table.Docteur;
import com.mbds.appdoctor.utils.BaseUtils;
import com.mbds.appdoctor.utils.Session;
import com.mbds.appdoctor.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class ConnexionActivity extends AppCompatActivity {
    private Button login_btn;
    private TextView register_link;
    private TextView config_link;
    private Session session;
    private DataService dataService;
    private EditText emailEdit, pwdEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        getSupportActionBar().hide();

        login_btn = findViewById(R.id.login_btn);
        register_link = findViewById(R.id.register_link);
        config_link = findViewById(R.id.config_link);
        emailEdit = findViewById(R.id.email_input);
        pwdEdit = findViewById(R.id.pwd_input);

        login_btn.setOnClickListener(login_btn_listener);
        register_link.setOnClickListener(register_link_listener);
        config_link.setOnClickListener(config_link_listener);

        session = new Session(this);
        session.setUserId("bonjour activity");
        dataService = new DataService(this);
    }

    private View.OnClickListener login_btn_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final UpdateDialog ud = new UpdateDialog(ConnexionActivity.this, "");
            ud.setCancelable(false);
            ud.show();
            if (BaseUtils.isNetworkAvailable(ConnexionActivity.this)) {
                dataService.authentificate(emailEdit.getText().toString(), pwdEdit.getText().toString(), new VolleyCallback() {
                    @Override
                    public void onSuccess(String reponse) {
                        if (reponse.compareToIgnoreCase("null") == 0) {
                            ud.dismiss();
                            toastConnexionFailed();
                        } else {
                            try {
                                Docteur doc = VolleyUtils.JSONObjectToDocteur((reponse.charAt(0)=='{')?new JSONObject(reponse):(new JSONArray(reponse)).getJSONObject(0));
                                if (doc != null) {
                                    session.setUserId(doc.getId());
                                    dataService.saveDoctor(doc);
                                    Config conf = dataService.getConfig();
                                    if (!conf.isMajAuto())
                                        conf.setMajAuto(true);
                                    dataService.updateConfig(conf);//forcer la maj pour un nouveau doc
                                    goToAccueilActivity();
                                } else {
                                    toastConnexionFailed();
                                }
                                ud.dismiss();
                            } catch (Exception e) {
                                Toast.makeText(ConnexionActivity.this, "Erreur:" + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        ud.dismiss();
                        Toast.makeText(ConnexionActivity.this, "erreur: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                try {
                    if (dataService.authentificateOffline(emailEdit.getText().toString(), pwdEdit.getText().toString()) != null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ConnexionActivity.this);
                        builder.setTitle("Connexion hors ligne");
                        builder.setMessage("Vous allez utilisé des données hors ligne.\nPensez à activer votre connexion internet pour obtenir les mise à jour")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        goToAccueilActivity();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        toastConnexionFailed();
                    }
                } catch (Exception e) {
                    Toast.makeText(ConnexionActivity.this, "Erreur: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                ud.dismiss();
            }
        }
    };

    public void toastConnexionFailed() {
        Toast.makeText(this, "Email ou mot de passe incorrect", Toast.LENGTH_LONG).show();
    }

    private View.OnClickListener register_link_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ConnexionActivity.this, InscriptionActivity.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener config_link_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ConnexionActivity.this, ConfigActivity.class);
            startActivity(intent);
        }
    };

    public void goToAccueilActivity() {
        Intent intent = new Intent(ConnexionActivity.this, AccueilActivity.class);
        startActivity(intent);
        finish();
    }

}
