package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.Doctor;
import com.docfinderserver.docfinderserver.model.SymptomPerDisease;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SymptomPerDiseaseRepository extends JpaRepository<SymptomPerDisease, Long> {
    List<SymptomPerDisease> findByIdDisease(Long idDisease); 
    
    @Query("select d from SymptomPerDisease d where d.idSymptom=?1 ")
    List<SymptomPerDisease> searchIdSymptom(Long idSymptom);
}
 