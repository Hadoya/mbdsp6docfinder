package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}