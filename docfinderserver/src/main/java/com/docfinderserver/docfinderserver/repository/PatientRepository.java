package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.Doctor;
import com.docfinderserver.docfinderserver.model.Patient;
import com.docfinderserver.docfinderserver.model.simplemodel.StatPerMonth;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    @Query("select p from Patient p where (UPPER(p.lastname) like UPPER(?1) or UPPER(p.firstname) like UPPER(?1) or UPPER(p.telephone) like UPPER(?1) or UPPER(p.email) like UPPER(?1) or UPPER(p.gender) like UPPER(?1) ) and p.estPatient=1 ")
    List<Patient> searchFromBO(String wordToSearch);
    
    List<Patient> findByEstPatient(int estPatient);
    
    
    @Query("select count(p) from Patient p where p.estPatient=1")
    int countAllPatient();
    
     @Query("select new com.docfinderserver.docfinderserver.model.simplemodel.StatPerMonth(Month(p.createdAt), count(p)) from Patient p where p.estPatient=1 and YEAR(p.createdAt)=?1 group by Month(p.createdAt)")
    List<StatPerMonth> findStatPerMonth(int currentYear);
    
   @Query("select d from Patient d where (UPPER(d.lastname) like UPPER(?1) and UPPER(d.firstname) like UPPER(?2) and  UPPER(d.email) like UPPER(?3))")
    List<Patient> findIdPatient(String lastname,String firstname,String email);
    
     @Query("select d from Patient d where (UPPER(d.email) like UPPER(?1) and UPPER(d.password) like UPPER(?2) )")
    List<Patient> login(String email,String password);

}