package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.Doctor;
import com.docfinderserver.docfinderserver.model.simplemodel.DoctorStatisticPerSpeciality;
import com.docfinderserver.docfinderserver.model.simplemodel.StatPerMonth;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    List<Doctor> findByEtatValidation(int etatValidation);
    List<Doctor> findByLastnameLikeOrFirstnameLikeOrTelephoneLikeOrAddressLikeOrTownLikeOrCityLikeOrDescriptionLikeOrEmailLikeOrDoctorSpeciality_NameLikeOrDoctorSpeciality_DescriptionAndEtatValidation(
            String lastname,String firstname,String telephone,String address, String town, String city, String description, String email,String doctorSpecialityName, String doctorSpecialityDescription, int EtatValidation
    );
    
    @Query("select d from Doctor d where (UPPER(d.lastname) like UPPER(?1) or UPPER(d.firstname) like UPPER(?1) or UPPER(d.telephone) like UPPER(?1) or UPPER(d.address) like UPPER(?1) or UPPER(d.town) like UPPER(?1) or UPPER(d.city) like UPPER(?1) or UPPER(d.description) like UPPER(?1) or UPPER(d.email) like UPPER(?1) or UPPER(d.doctorSpeciality.name) like UPPER(?1)or UPPER(d.doctorSpeciality.description) like UPPER(?1)) and d.etatValidation=?2 ")
    List<Doctor> searchByEtatValidation(String wordToSearch,int etatValidation);
    
    @Query("select count(d) from Doctor d where d.etatValidation=?1")
    int countByEtatValidation(int etatValidation);
    
    @Query("select new com.docfinderserver.docfinderserver.model.simplemodel.DoctorStatisticPerSpeciality(d.doctorSpeciality.name, count(d)) from Doctor d where d.etatValidation=1 group by d.doctorSpeciality.name ")
    List<DoctorStatisticPerSpeciality> getStatisticsPerDocSpeciality();
    
    @Query("select new com.docfinderserver.docfinderserver.model.simplemodel.StatPerMonth(Month(d.createdAt), count(d)) from Doctor d where d.etatValidation=1 and YEAR(d.createdAt)=?1 group by Month(d.createdAt)")
    List<StatPerMonth> findStatPerMonth(int currentYear);
    
        @Query("select d from Doctor d where d.idDoctorSpeciality=?1 and d.etatValidation=1 ")
    List<Doctor> searchIdSymptom(Long idDoctorSpeciality);
    
     @Query("select d from Doctor d where d.idPatient=?1")
    List<Doctor> findIdPatient(Long idPatient);
    
}