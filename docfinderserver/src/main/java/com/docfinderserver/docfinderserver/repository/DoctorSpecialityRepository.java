package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.DoctorSpeciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorSpecialityRepository extends JpaRepository<DoctorSpeciality, Long> {
}