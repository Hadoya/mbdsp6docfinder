package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.Appointment;
import com.docfinderserver.docfinderserver.model.SymptomPerDisease;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
      @Query("select d from Appointment d where d.idPatient=?1 ")
    List<Appointment> searchByIdpatient(Long idPatient);
    
      @Query("select d from Appointment d where UPPER(d.appointmentDateString) like UPPER(?1)")
    List<Appointment> searchByDate(String wordToSearch);
    
     @Query("select d from Appointment d where (UPPER(d.firstNamePatient) like UPPER(?1) or UPPER(d.lastNamePatient) like UPPER(?1) or UPPER(d.appointmentDateString) like UPPER(?1))")
    List<Appointment> searchAll(String wordToSearch);
    
}