package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.Disease;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DiseaseRepository extends JpaRepository<Disease, Long> {
    @Query("Select d from Disease d order by d.name asc")
    List<Disease> findWithOrder();
    //List<Disease> findTop50OrderByNameAsc();
    
     @Query("select d from Disease d where UPPER(d.name) like UPPER(?1) or UPPER(d.description) like UPPER(?1)  ")
    List<Disease> searchFromBO(String wordToSearch);
 
}