package com.docfinderserver.docfinderserver.repository;

import com.docfinderserver.docfinderserver.model.Symptom;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SymptomRepository extends JpaRepository<Symptom, Long> {
    @Query("Select s from Symptom s order by s.name asc")
    List<Symptom> findOrder();
    
     @Query("select s from Symptom s where UPPER(s.name) like UPPER(?1) or UPPER(s.description) like UPPER(?1)")
    List<Symptom> search(String wordToSearch);
 
}