package com.docfinderserver.docfinderserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "symptomperdiseases")
public class SymptomPerDisease extends DocFinderModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private int begininterval;

    @Column
    private int endinterval;

    @Column
    @Size(min = 0, max = 20)
    private String unit;
    
    
    @Column(name = "id_disease")
    private Long idDisease;
    
    @Column(name = "id_symptom")
    private Long idSymptom;
    
    //For the table in database
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_belonging_disease", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Disease disease;
    

    //For the table in database
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_belonging_symptom", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Symptom symptom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
  

    public int getBegininterval() {
        return begininterval;
    }

    public void setBegininterval(int begininterval) {
        this.begininterval = begininterval;
    }

    public int getEndinterval() {
        return endinterval;
    }

    public void setEndinterval(int endinterval) {
        this.endinterval = endinterval;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    
    public Long getIdDisease() {
        return idDisease;
    }

    public void setIdDisease(Long idDisease) {
        this.idDisease = idDisease;
    }

    public Long getIdSymptom() {
        return idSymptom;
    }

    public void setIdSymptom(Long idSymptom) {
        this.idSymptom = idSymptom;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
        this.idDisease=disease.getId();
    }

    public Symptom getSymptom() {
        return symptom;
    }

    public void setSymptom(Symptom symptom) {
        this.symptom = symptom;
        this.idSymptom=symptom.getId();
    }
  }