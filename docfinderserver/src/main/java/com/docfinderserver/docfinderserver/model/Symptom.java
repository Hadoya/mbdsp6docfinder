package com.docfinderserver.docfinderserver.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "symptoms")
public class Symptom extends DocFinderModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    @Size(min = 0, max = 200)
    private String name;

    @Column
    @Size(min = 0, max = 500)
    private String description;
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

   
}