package com.docfinderserver.docfinderserver.model;

import com.docfinderserver.docfinderserver.model.simplemodel.ObjectId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "diseases")
public class Disease extends DocFinderModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    @Size(min = 0, max = 200)
    private String name;

    @Column
    @Size(min = 0, max = 500)
    private String description;
    
    @Column(name = "id_doctor_speciality")
    private Long idDoctorSpeciality;
    
    
    //For the table in database
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_belonging_doctor_speciality", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private DoctorSpeciality doctorSpeciality;
    
    @Transient
    private ArrayList<ObjectId> symptoms;

    
     
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getIdDoctorSpeciality() {
        return idDoctorSpeciality;
    }

    public void setIdDoctorSpeciality(Long idDoctorSpeciality) {
        this.idDoctorSpeciality = idDoctorSpeciality;
    }
    public void setIdDoctorSpeciality() {
        if(this.doctorSpeciality!=null){
            this.idDoctorSpeciality = this.doctorSpeciality.getId();
        }
    }

    public DoctorSpeciality getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(DoctorSpeciality doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
        this.idDoctorSpeciality = doctorSpeciality.getId();
    }

    public ArrayList<ObjectId> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(ArrayList<ObjectId> symptoms) {
        this.symptoms = symptoms;
    }

   
    
    

   
}