/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.docfinderserver.docfinderserver.model.simplemodel;

/**
 *
 * @author hajavola
 */
public class StatPerMonth {
    private int month;
    private Long number;

    public StatPerMonth() {
    }

    public StatPerMonth(int month, Long number) {
        this.month = month;
        this.number = number;
    }
    
    
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }
    
     
    
}
