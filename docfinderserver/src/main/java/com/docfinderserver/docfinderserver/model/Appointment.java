package com.docfinderserver.docfinderserver.model;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "appointments")
public class Appointment extends DocFinderModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    @Size(min = 0, max = 100)
    private String lastNamePatient;

    @Column
    @Size(min = 0, max = 200)
    private String firstNamePatient;

    @Column
    @Size(min = 0, max = 1)
    private String gender;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "appointment_date", nullable = false)
    private Date appointmentDate;
    
    
    @Column
    @Size(min = 0, max = 500)
    private String appointmentDateString;

    @Column
    private int validation;
    
    @Column(name = "id_doctor")
    private Long idDoctor;
    
    @Column(name = "id_patient")
    private Long idPatient;
    
    //For the table in database
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_belonging_doctor", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Doctor doctor;
    

    //For the table in database
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_belonging_patient", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Patient patient;
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getLastNamePatient() {
        return lastNamePatient;
    }

    public void setLastNamePatient(String lastNamePatient) {
        this.lastNamePatient = lastNamePatient;
    }

    public String getFirstNamePatient() {
        return firstNamePatient;
    }

    public void setFirstNamePatient(String firstNamePatient) {
        this.firstNamePatient = firstNamePatient;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public int getValidation() {
        return validation;
    }

    public void setValidation(int validation) {
        this.validation = validation;
    }

    public Long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Long idDoctor) {
        this.idDoctor = idDoctor;
    }
    public void setIdDoctor() {
        if(this.doctor!=null){
            this.idDoctor=this.doctor.getId(); 
        }
    }


    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
        this.idDoctor =doctor.getId();
    }
    
    public Long getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Long idPatient) {
        this.idPatient = idPatient;
    }
     public void setIdPatient() {
        if(this.patient!=null){
            this.idPatient = this.patient.getId();
        }
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
        this.idPatient=patient.getId();
      
    }

    public String getAppointmentDateString() {
        return appointmentDateString;
    }

    public void setAppointmentDateString(String appointmentDateString) {
        this.appointmentDateString = appointmentDateString;
    }
    
    
    
    
    
    
    

    
    
    
}