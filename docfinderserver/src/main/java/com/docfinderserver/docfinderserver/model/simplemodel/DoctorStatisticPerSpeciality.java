/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.docfinderserver.docfinderserver.model.simplemodel;

import java.lang.reflect.Field;

/**
 *
 * @author hajavola
 */
public class DoctorStatisticPerSpeciality {
    private String specialityName;
    private Long number;

    public DoctorStatisticPerSpeciality() {
    }

    public DoctorStatisticPerSpeciality(String specialityName, Long number) {
        this.specialityName = specialityName;
        this.number = number;
    }
    
    public DoctorStatisticPerSpeciality(Object o){
        Class<?> oClass = o.getClass();
        Field[] fields=oClass.getFields();
        
        
        

        /*for(Field field : clazz.getDeclaredFields()) {
            //you can also use .toGenericString() instead of .getName(). This will
            //give you the type information as well.
            System.out.println(field.getName());
            
        }*/
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }
    
    
    
}
