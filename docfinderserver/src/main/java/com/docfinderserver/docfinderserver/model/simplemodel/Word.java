/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.docfinderserver.docfinderserver.model.simplemodel;

/**
 *
 * @author hajavola
 */
public class Word {
    private String wordToSearch;
      private String appointmentDateString;

    public String getWordToSearch() {
        return wordToSearch;
    }

    public void setWordToSearch(String wordToSearch) {
        this.wordToSearch = wordToSearch;
    }

    public String getAppointmentDateString() {
        return appointmentDateString;
    }

    public void setAppointmentDateString(String appointmentDateString) {
        this.appointmentDateString = appointmentDateString;
    }
    
}
