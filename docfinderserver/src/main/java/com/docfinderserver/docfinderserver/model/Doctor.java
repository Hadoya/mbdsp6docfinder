package com.docfinderserver.docfinderserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "doctors")
public class Doctor extends DocFinderModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    @Size(min = 0, max = 300)
    private String lastname;

    @Column
    @Size(min = 0, max = 300)
    private String firstname;

    @Column
    @Size(min = 0, max = 20)
    private String telephone;
    
    @Column
    @Size(min = 0, max = 200)
    private String address;
    
    @Column
    @Size(min = 0, max = 200)
    private String town;
    
    @Column
    @Size(min = 0, max = 200)
    private String city;
    
    @Column
    @Size(min = 0, max = 300)
    private String description;
    
    @Column
    private String tarif;
    
    @Column
    private int etatValidation;
    
    @Column
    @Size(min = 0, max = 300)
    private String email;
    
    @Column
    @Size(min = 0, max = 300)
    private String password;
    
    @Column(name = "id_doctor_speciality")
    private Long idDoctorSpeciality;
    
     @Column(name = "id_patient")
    private Long idPatient;
    
    //For the table in database
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_belonging_doctor_speciality", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private DoctorSpeciality doctorSpeciality;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_belonging_patient", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Patient patient;
    
    
    public void initAttributes(){
        this.setIdDoctorSpeciality();
        this.setIdPatient();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


   
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public int getEtatValidation() {
        return etatValidation;
    }

    public void setEtatValidation(int etatValidation) {
        this.etatValidation = etatValidation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getIdDoctorSpeciality() {
        return idDoctorSpeciality;
    }

    public void setIdDoctorSpeciality(Long idDoctorSpeciality) {
        this.idDoctorSpeciality = idDoctorSpeciality;
    }
    public void setIdDoctorSpeciality() {
        if(this.doctorSpeciality!=null){
            this.idDoctorSpeciality = this.doctorSpeciality.getId();
        }
    }

    public DoctorSpeciality getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(DoctorSpeciality doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
        this.idDoctorSpeciality=doctorSpeciality.getId();
    }

    public Long getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Long idPatient) {
        this.idPatient = idPatient;
    }
    public void setIdPatient() {
       if(this.patient!=null){
            this.idPatient = this.patient.getId();
        }
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
        this.idPatient=patient.getId();
    }
    
    
   
    

   
}