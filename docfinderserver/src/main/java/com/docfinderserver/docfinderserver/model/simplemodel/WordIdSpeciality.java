/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.docfinderserver.docfinderserver.model.simplemodel;

/**
 *
 * @author hajavola
 */
public class WordIdSpeciality {
    private Long idDoctorSpeciality;

    public Long getIdDoctorSpeciality() {
        return idDoctorSpeciality;
    }

    public void setIdDoctorSpeciality(Long idDoctorSpeciality) {
        this.idDoctorSpeciality = idDoctorSpeciality;
    }
    
}
