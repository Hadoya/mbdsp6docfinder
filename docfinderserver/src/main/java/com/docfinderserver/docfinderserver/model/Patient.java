package com.docfinderserver.docfinderserver.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "patients")
public class Patient extends DocFinderModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    @Size(min = 0, max = 100)
    private String lastname;

    @Column
    @Size(min = 0, max = 200)
    private String firstname;
    
    @Column
    @Size(min = 0, max = 300)
    private String email;
    
    @Column
    @Size(min = 0, max = 300)
    private String password;
    
    
    @Column
    @Size(min = 0, max = 1)
    private String gender;
    
    @Column
    @Size(min = 0, max = 100)
    private String telephone;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "birthdate", nullable = false)
    private Date birthdate;
    
    @Column
    private int estPatient;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public int getEstPatient() {
        return estPatient;
    }

    public void setEstPatient(int estPatient) {
        this.estPatient = estPatient;
    }
    
    
    
    
}