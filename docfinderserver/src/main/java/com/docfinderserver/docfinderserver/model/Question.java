package com.docfinderserver.docfinderserver.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "question")
public class Question extends DocFinderModel {
    @Id
    @GeneratedValue(generator = "question_generator")
    @SequenceGenerator(
            name = "question_generator",
            sequenceName = "question_sequence",
            initialValue = 1000
    )
    private Long id;

    @NotBlank
    @Size(min = 3, max = 100)
    private String title;

    @Column(columnDefinition = "Varchar(300)")
    private String description;

    @Column(name = "idTest")
    private Long _idTest;
    
    @Transient
    private String desc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this._idTest=id;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.desc=description;
        this.description = description;
    }

    public Long getIdTest() {
        return _idTest;
    }

    public void setIdTest(Long _idTest) {
        this._idTest = _idTest;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
}