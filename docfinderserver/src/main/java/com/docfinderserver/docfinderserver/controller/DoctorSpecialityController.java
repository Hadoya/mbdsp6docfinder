package com.docfinderserver.docfinderserver.controller;

import com.docfinderserver.docfinderserver.exception.ResourceNotFoundException;
import com.docfinderserver.docfinderserver.model.DoctorSpeciality;
import com.docfinderserver.docfinderserver.repository.DoctorSpecialityRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
public class DoctorSpecialityController {

    @Autowired
    private DoctorSpecialityRepository doctorSpecialityRepository;

    @GetMapping("/doctorSpeciality")
    public List<DoctorSpeciality> getDoctorSpecialitys() {
        return doctorSpecialityRepository.findAll();
    }
    
    @GetMapping("/doctorSpeciality/{doctorSpecialityId}")
    public DoctorSpeciality getDoctorSpecialityById(@PathVariable Long doctorSpecialityId) { 
        return doctorSpecialityRepository.findById(doctorSpecialityId)
                .map(doctorSpeciality -> {
                    return doctorSpeciality;
                }).orElseThrow(() -> new ResourceNotFoundException("DoctorSpeciality not found with id " + doctorSpecialityId));
         
    }


    @PostMapping("/doctorSpeciality/add")
    public DoctorSpeciality createDoctorSpeciality(@Valid @RequestBody DoctorSpeciality doctorSpeciality) {
        return doctorSpecialityRepository.save(doctorSpeciality);
    }

    @PostMapping("/doctorSpeciality/{doctorSpecialityId}")
    public DoctorSpeciality updateDoctorSpeciality(@PathVariable Long doctorSpecialityId,
                                   @Valid @RequestBody DoctorSpeciality doctorSpecialityRequest) {
        return doctorSpecialityRepository.findById(doctorSpecialityId)
                .map(doctorSpeciality -> {
                    doctorSpeciality.setName(doctorSpecialityRequest.getName());
                    doctorSpeciality.setDescription(doctorSpecialityRequest.getDescription());
                    return doctorSpecialityRepository.save(doctorSpeciality);
                }).orElseThrow(() -> new ResourceNotFoundException("DoctorSpeciality not found with id " + doctorSpecialityId));
    }


    @DeleteMapping("/doctorSpeciality/{doctorSpecialityId}")
    public ResponseEntity<?> deleteDoctorSpeciality(@PathVariable Long doctorSpecialityId) {
        return doctorSpecialityRepository.findById(doctorSpecialityId)
                .map(doctorSpeciality -> {
                    doctorSpecialityRepository.delete(doctorSpeciality);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("DoctorSpeciality not found with id " + doctorSpecialityId));
    }
}