package com.docfinderserver.docfinderserver.controller;

import com.docfinderserver.docfinderserver.exception.ResourceNotFoundException;
import com.docfinderserver.docfinderserver.model.Disease;
import com.docfinderserver.docfinderserver.model.DoctorSpeciality;
import com.docfinderserver.docfinderserver.model.Symptom;
import com.docfinderserver.docfinderserver.model.SymptomPerDisease;
import com.docfinderserver.docfinderserver.model.simplemodel.ObjectId;
import com.docfinderserver.docfinderserver.model.simplemodel.Word;
import com.docfinderserver.docfinderserver.repository.DiseaseRepository;
import com.docfinderserver.docfinderserver.repository.DoctorSpecialityRepository;
import com.docfinderserver.docfinderserver.repository.SymptomPerDiseaseRepository;
import com.docfinderserver.docfinderserver.repository.SymptomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
public class DiseaseController {

    @Autowired
    private DiseaseRepository diseaseRepository;
    @Autowired
    private DoctorSpecialityRepository doctorSpecialityRepository;
    @Autowired 
    private SymptomPerDiseaseRepository symptomPerDiseaseRepository;
    @Autowired 
    private SymptomRepository symptomRepository;
    
    @GetMapping("/disease")
    public List<Disease> getDiseases() {
        return diseaseRepository.findAll();
    }
    
    @GetMapping("/disease/{diseaseId}")
    public Disease getDiseaseById(@PathVariable Long diseaseId) { 
        return diseaseRepository.findById(diseaseId)
                .map(disease -> {
                    return disease;
                }).orElseThrow(() -> new ResourceNotFoundException("Disease not found with id " + diseaseId));
         
    }


    @PostMapping("/disease/add")
    public Disease createDisease(@Valid @RequestBody Disease disease) {
        return doctorSpecialityRepository.findById(disease.getIdDoctorSpeciality())
                .map(docSpec -> {
                     disease.setDoctorSpeciality(docSpec);
                    return diseaseRepository.save(disease);
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor Speciality not found with id " + disease.getIdDoctorSpeciality()));
    }

    @PostMapping("/disease/update/{diseaseId}")
    public Disease updateDisease(@PathVariable Long diseaseId,
                                   @Valid @RequestBody Disease diseaseRequest) {
        return diseaseRepository.findById(diseaseId)
                .map(disease -> {
                    disease.setDoctorSpeciality(doctorSpecialityRepository.findById(disease.getIdDoctorSpeciality())
                    .map(doc -> {
                        return doc;
                    }).orElseThrow(() -> new ResourceNotFoundException("DoctorSpeciality not found with id " + disease.getIdDoctorSpeciality())));
                    
                    disease.setName(diseaseRequest.getName());
                    disease.setDescription(diseaseRequest.getDescription());
                   
                    return diseaseRepository.save(disease);
                }).orElseThrow(() -> new ResourceNotFoundException("Disease not found with id " + diseaseId));
    }


    @DeleteMapping("/disease/{diseaseId}")
    public ResponseEntity<?> deleteDisease(@PathVariable Long diseaseId) {
        //delete the previous diseaseSymptoms from table SymptomPerDisease
        List<SymptomPerDisease> allSymptoms=symptomPerDiseaseRepository.findByIdDisease(diseaseId);
        for(int i=0;i<allSymptoms.size();i++){
            symptomPerDiseaseRepository.delete(allSymptoms.get(i));
        }
        //delete the main Disease
        return diseaseRepository.findById(diseaseId)
                .map(disease -> {
                    diseaseRepository.delete(disease);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Disease not found with id " + diseaseId));
    }
    
    @PostMapping("/disease/addwithsymptoms")
    public Disease createDiseaseWithSymptoms(@Valid @RequestBody Disease disease) {
        List<ObjectId> idSymptoms=disease.getSymptoms();
        Disease currentDisease= doctorSpecialityRepository.findById(disease.getIdDoctorSpeciality())
                .map(docSpec -> {
                     disease.setDoctorSpeciality(docSpec);
                    return diseaseRepository.save(disease);
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor Speciality not found with id " + disease.getIdDoctorSpeciality()));
        
        SymptomPerDisease symptomPerDisease=new SymptomPerDisease();
        Symptom symptom=new Symptom();
        for(int i=0;i<idSymptoms.size();i++){
            symptomPerDisease=new SymptomPerDisease();
            
            symptom=symptomRepository.findById(idSymptoms.get(i).getId())
                    .map(symp -> {
                        return symp;
                     }).orElseThrow(() -> new ResourceNotFoundException("symptom not found"));
            symptomPerDisease.setSymptom(symptom);
            symptomPerDisease.setDisease(currentDisease);
            symptomPerDisease.setBegininterval(0);
            symptomPerDisease.setEndinterval(1);
            symptomPerDisease.setUnit("unit");
            symptomPerDiseaseRepository.save(symptomPerDisease);
        }
        return currentDisease;
    }
    
    @PostMapping("/disease/updatewithsymptoms/{idDisease}")
    public Disease updateDiseaseWithSymptoms(@PathVariable Long idDisease, @Valid @RequestBody Disease disease) {
        //delete the previous diseaseSymptoms from table SymptomPerDisease
        List<SymptomPerDisease> allSymptoms=symptomPerDiseaseRepository.findByIdDisease(idDisease);
        for(int i=0;i<allSymptoms.size();i++){
            symptomPerDiseaseRepository.delete(allSymptoms.get(i));
        }
        
        //start the update
        List<ObjectId> idSymptoms=disease.getSymptoms();
        DoctorSpeciality currentDoctorSpeciality= doctorSpecialityRepository.findById(disease.getIdDoctorSpeciality())
                .map(docSpec -> {
                     return docSpec;
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor Speciality not found with id " + disease.getIdDoctorSpeciality()));
        
        Disease currentDisease= diseaseRepository.findById(idDisease).map(dis -> {
                    return dis;
                }).orElseThrow(() -> new ResourceNotFoundException("Disease not found with id " + idDisease));
         
        currentDisease.setDoctorSpeciality(currentDoctorSpeciality);
        currentDisease.setName(disease.getName());
        currentDisease.setDescription(disease.getDescription());
        diseaseRepository.save(currentDisease);
        
        SymptomPerDisease symptomPerDisease=new SymptomPerDisease();
        Symptom symptom=new Symptom();
        for(int i=0;i<idSymptoms.size();i++){
            symptomPerDisease=new SymptomPerDisease();
            
            symptom=symptomRepository.findById(idSymptoms.get(i).getId())
                    .map(symp -> {
                        return symp;
                     }).orElseThrow(() -> new ResourceNotFoundException("symptom not found"));
            symptomPerDisease.setSymptom(symptom);
            symptomPerDisease.setDisease(currentDisease);
            symptomPerDisease.setBegininterval(0);
            symptomPerDisease.setEndinterval(1);
            symptomPerDisease.setUnit("unit");
            symptomPerDiseaseRepository.save(symptomPerDisease);
        }
        return currentDisease;
    }
    
    @GetMapping("/disease/list")
    public List<Disease> getListDiseases() {
        return diseaseRepository.findWithOrder();
    }
    
    @PostMapping("/disease/searchfrombo")
    public List<Disease> searchDisease(@Valid @RequestBody Word word) {
       String wordToSearch="%"+word.getWordToSearch()+"%";
       return diseaseRepository.searchFromBO(wordToSearch);
    }
    

}