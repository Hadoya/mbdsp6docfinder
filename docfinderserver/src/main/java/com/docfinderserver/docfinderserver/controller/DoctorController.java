package com.docfinderserver.docfinderserver.controller;

import com.docfinderserver.docfinderserver.exception.ResourceNotFoundException;
import com.docfinderserver.docfinderserver.model.Doctor;
import com.docfinderserver.docfinderserver.model.DoctorSpeciality;
import com.docfinderserver.docfinderserver.model.Patient;
import com.docfinderserver.docfinderserver.model.SymptomPerDisease;
import com.docfinderserver.docfinderserver.model.simplemodel.DoctorStatisticPerSpeciality;
import com.docfinderserver.docfinderserver.model.simplemodel.StatPerMonth;
import com.docfinderserver.docfinderserver.repository.DoctorRepository;
import com.docfinderserver.docfinderserver.repository.DoctorSpecialityRepository;
import com.docfinderserver.docfinderserver.repository.PatientRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import com.docfinderserver.docfinderserver.model.simplemodel.Word;
import com.docfinderserver.docfinderserver.model.simplemodel.WordIdSpeciality;
import com.docfinderserver.docfinderserver.model.simplemodel.Wordpatient;
import java.util.ArrayList;

@RestController
@CrossOrigin(origins = "*")
public class DoctorController {

    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private DoctorSpecialityRepository doctorSpecialityRepository;
    
    @Autowired
    private PatientRepository patientRepository;
   
    @GetMapping("/doctor/findall/")
    public List<Doctor> getDoctors() {
        return doctorRepository.findAll();
    }
    
    @GetMapping("/doctor/{doctorId}")
    public Doctor getDoctorById(@PathVariable Long doctorId) { 
        return doctorRepository.findById(doctorId)
                .map(doctor -> {
                    return doctor;
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor not found with id " + doctorId));
         
    }


    @PostMapping("/doctor/add")
    public Doctor createDoctor(@Valid @RequestBody Doctor doctor) {
        DoctorSpeciality doctorS=doctorSpecialityRepository.findById(doctor.getIdDoctorSpeciality())
                .map(docSpec -> {      
                    return docSpec;
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor Speciality not found with id " + doctor.getIdDoctorSpeciality()));
        Patient doctorP=patientRepository.findById(doctor.getIdPatient())
                .map(docP -> {      
                    return docP;
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + doctor.getIdPatient()));
        
        
       
        doctor.setDoctorSpeciality(doctorS);
        doctor.setPatient(doctorP);
        return doctorRepository.save(doctor);
               
    }

    @PostMapping("/doctor/update/{doctorId}")
    public Doctor updateDoctor(@PathVariable Long doctorId,
                                   @Valid @RequestBody Doctor doctorRequest) {
        return doctorRepository.findById(doctorId)
                .map(doctor -> {
                    doctor.setDoctorSpeciality(doctorSpecialityRepository.findById(doctor.getIdDoctorSpeciality())
                    .map(doc -> {
                        return doc;
                    }).orElseThrow(() -> new ResourceNotFoundException("DoctorSpeciality not found with id " + doctor.getIdDoctorSpeciality())));
                    doctor.setPatient(patientRepository.findById(doctor.getIdPatient())
                    .map(pat -> {
                        return pat;
                    }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + doctor.getIdPatient())));
                    
                    
                    doctor.setLastname(doctorRequest.getLastname());
                    doctor.setFirstname(doctorRequest.getFirstname());
                    doctor.setTelephone(doctorRequest.getTelephone());
                    doctor.setAddress(doctorRequest.getAddress());
                    doctor.setTown(doctorRequest.getTown());
                    doctor.setCity(doctorRequest.getCity());
                    doctor.setDescription(doctorRequest.getDescription());
                    doctor.setTarif(doctorRequest.getTarif());
                    doctor.setEtatValidation(doctorRequest.getEtatValidation());
                    doctor.setEmail(doctorRequest.getEmail());
                    doctor.setPassword(doctorRequest.getPassword());
                    return doctorRepository.save(doctor);
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor not found with id " + doctorId));
    }


    @DeleteMapping("/doctor/{doctorId}")
    public ResponseEntity<?> deleteDoctor(@PathVariable Long doctorId) {
        return doctorRepository.findById(doctorId)
                .map(doctor -> {
                    doctorRepository.delete(doctor);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor not found with id " + doctorId));
    }
    
    @PostMapping("/doctor/validate/{doctorId}")
    public Doctor validateDoctor(@PathVariable Long doctorId) { 
        Doctor doctor=doctorRepository.findById(doctorId)
                .map(doc -> {
                    return doc;
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor not found with id " + doctorId));
        doctor.setEtatValidation(1);
        return doctorRepository.save(doctor);
    }
    
    @GetMapping("/doctor/okdoctors")
    public List<Doctor> getOkDoctors() {
        return doctorRepository.findByEtatValidation(1);
    }
    
    @GetMapping("/doctor/nokdoctors")
    public List<Doctor> getNokDoctors() {
        return doctorRepository.findByEtatValidation(0);
    }
    
    @PostMapping("/doctor/okdoctorssearch")
    public List<Doctor> okDoctorSearch(@Valid @RequestBody Word word) {
       String wordToSearch="%"+word.getWordToSearch()+"%";
       //return doctorRepository.findByLastnameLikeOrFirstnameLikeOrTelephoneLikeOrAddressLikeOrTownLikeOrCityLikeOrDescriptionLikeOrEmailLikeOrDoctorSpeciality_NameLikeOrDoctorSpeciality_DescriptionAndEtatValidation(wordToSearch, wordToSearch, wordToSearch, wordToSearch, wordToSearch, wordToSearch, wordToSearch, wordToSearch, wordToSearch, wordToSearch, 1);
       return doctorRepository.searchByEtatValidation(wordToSearch, 1);
    }
    @PostMapping("/doctor/nokdoctorssearch")
    public List<Doctor> nokDoctorSearch(@Valid @RequestBody Word word) {
       String wordToSearch="%"+word.getWordToSearch()+"%";
         return doctorRepository.searchByEtatValidation(wordToSearch, 0);      
    }
    
    @GetMapping("/doctor/countallokdoctors")
    public int countAllOkDoctors() {
        return doctorRepository.countByEtatValidation(1);
    }
    
    @GetMapping("/doctor/countallnokdoctors")
    public int countAllNokDoctors() {
        return doctorRepository.countByEtatValidation(0);
    }
    
    @GetMapping("/doctor/statperdocspeciality")
    public List<DoctorStatisticPerSpeciality> getStatisticsPerDocSpeciality() {
        List<DoctorStatisticPerSpeciality> answer=doctorRepository.getStatisticsPerDocSpeciality();
        return answer ;
    }
    
    @GetMapping("/doctor/statpermonth/{year}")
    public List<Long> getStatDoctorPerMonth(@PathVariable int year) { 
        List<StatPerMonth> statPerMonth=doctorRepository.findStatPerMonth(year);
        List<Long> answer=new ArrayList<Long>();
        
        Long number=Long.parseLong("0");
        for(int i=1;i<=12;i++){
            number=Long.parseLong("0");
            for(int j=0;j<statPerMonth.size();j++){
                if(statPerMonth.get(j).getMonth()==i){
                    number=statPerMonth.get(j).getNumber();
                }
            }
            answer.add(number);
        }
        return answer;
    }
    
     @PostMapping("/doctor/searchByIdspecility")
    public List<Doctor> searchIdSymptom(@Valid @RequestBody WordIdSpeciality idDoctorSpeciality) {
         return doctorRepository.searchIdSymptom(idDoctorSpeciality.getIdDoctorSpeciality());      
    }
   
      @PostMapping("/doctor/findIdPatient")
     public List<Doctor> findIdPatient(@Valid @RequestBody Wordpatient patient) {
         return doctorRepository.findIdPatient(patient.getIdPatient());      
    }
    
}