package com.docfinderserver.docfinderserver.controller;

import com.docfinderserver.docfinderserver.exception.ResourceNotFoundException;
import com.docfinderserver.docfinderserver.model.Patient;
import com.docfinderserver.docfinderserver.model.simplemodel.StatPerMonth;
import com.docfinderserver.docfinderserver.model.simplemodel.Word;
import com.docfinderserver.docfinderserver.model.simplemodel.Wordpatient;
import com.docfinderserver.docfinderserver.repository.PatientRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
public class PatientController {

    @Autowired
    private PatientRepository patientRepository;


    @GetMapping("/patient")
    public List<Patient> getPatients() {
        return patientRepository.findAll();
    }
    @GetMapping("/patient/allfrombo")
    public List<Patient> getPatientsFromBO() {
        return patientRepository.findByEstPatient(1);

    }
  
    
    
    @GetMapping("/patient/{patientId}")
    public Patient getPatientById(@PathVariable Long patientId) { 
        return patientRepository.findById(patientId)
                .map(patient -> {
                    return patient;
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + patientId));
         
    }


    @PostMapping("/patient/add")
    public Patient createPatient(@Valid @RequestBody Patient patient) {
        return patientRepository.save(patient);
    }

    @PostMapping("/patient/{patientId}")
    public Patient updatePatient(@PathVariable Long patientId,
                                   @Valid @RequestBody Patient patientRequest) {
        return patientRepository.findById(patientId)
                .map(patient -> {
                    patient.setLastname(patientRequest.getLastname());
                    patient.setFirstname(patientRequest.getFirstname());
                    patient.setEmail(patientRequest.getEmail());
                    patient.setPassword(patientRequest.getPassword());
                    patient.setGender(patientRequest.getGender());
                    patient.setTelephone(patientRequest.getTelephone());
                    patient.setBirthdate(patientRequest.getBirthdate());
                    patient.setEstPatient(patientRequest.getEstPatient());
                  
                    return patientRepository.save(patient);
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + patientId));
    }


    @DeleteMapping("/patient/{patientId}")
    public ResponseEntity<?> deletePatient(@PathVariable Long patientId) {
        return patientRepository.findById(patientId)
                .map(patient -> {
                    patientRepository.delete(patient);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + patientId));
    }
    
    @PostMapping("/patient/searchfrombo")
    public List<Patient> searchPatient(@Valid @RequestBody Word word) {
       String wordToSearch="%"+word.getWordToSearch()+"%";
       return patientRepository.searchFromBO(wordToSearch);
    }
    
    @GetMapping("/patient/countallpatients")
    public int countAllPatients() {
        return patientRepository.countAllPatient();
    }
    
    @GetMapping("/patient/statpermonth/{year}")
    public List<Long> getStatPatientPerMonth(@PathVariable int year) { 
        List<StatPerMonth> statPerMonth=patientRepository.findStatPerMonth(year);
        List<Long> answer=new ArrayList<Long>();
        
        Long number=Long.parseLong("0");
        for(int i=1;i<=12;i++){
            number=Long.parseLong("0");
            for(int j=0;j<statPerMonth.size();j++){
                if(statPerMonth.get(j).getMonth()==i){
                    number=statPerMonth.get(j).getNumber();
                }
            }
            answer.add(number);
        }
        return answer;
    }
      @PostMapping("/patient/findIdPatient")
     public List<Patient> searchIdSymptom(@Valid @RequestBody Wordpatient patient) {
         return patientRepository.findIdPatient(patient.getLastname(),patient.getFirstname(),patient.getEmail());      
    }
     @PostMapping("/patient/login")
     public List<Patient> login(@Valid @RequestBody Wordpatient patient) {
         return patientRepository.login(patient.getEmail(),patient.getPassword());      
    }
}