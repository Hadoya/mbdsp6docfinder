package com.docfinderserver.docfinderserver.controller;

import com.docfinderserver.docfinderserver.exception.ResourceNotFoundException;
import com.docfinderserver.docfinderserver.model.Appointment;
import com.docfinderserver.docfinderserver.model.Doctor;
import com.docfinderserver.docfinderserver.model.Patient;
import com.docfinderserver.docfinderserver.model.simplemodel.Word;
import com.docfinderserver.docfinderserver.repository.AppointmentRepository;
import com.docfinderserver.docfinderserver.repository.DoctorRepository;
import com.docfinderserver.docfinderserver.repository.PatientRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
public class AppointmentController {

    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private PatientRepository patientRepository;

    @GetMapping("/appointment")
    public List<Appointment> getAppointments() {
        return appointmentRepository.findAll();
    }
    
    @GetMapping("/appointment/{appointmentId}")
    public Appointment getAppointmentById(@PathVariable Long appointmentId) { 
        return appointmentRepository.findById(appointmentId)
                .map(appointment -> {
                    return appointment;
                }).orElseThrow(() -> new ResourceNotFoundException("Appointment not found with id " + appointmentId));
         
    }


    @PostMapping("/appointment/add")
    public Appointment createAppointment(@Valid @RequestBody Appointment appointment) {
        Doctor doctor=doctorRepository.findById(appointment.getIdDoctor())
                .map(doc -> {
                    return doc;
                }).orElseThrow(() -> new ResourceNotFoundException("Doctor not found with id " + appointment.getIdDoctor()));
        
        Patient patient=patientRepository.findById(appointment.getIdPatient())
                .map(pat -> {
                    return pat;
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + appointment.getIdDoctor()));
        
        appointment.setDoctor(doctor);
        appointment.setPatient(patient);
        return appointmentRepository.save(appointment);
        
    }

    @PostMapping("/appointment/update/{appointmentId}")
    public Appointment updateAppointment(@PathVariable Long appointmentId,
                                   @Valid @RequestBody Appointment appointmentRequest) {
        return appointmentRepository.findById(appointmentId)
                .map(appointment -> {
                    appointment.setDoctor(doctorRepository.findById(appointment.getIdDoctor())
                    .map(doc -> {
                        return doc;
                    }).orElseThrow(() -> new ResourceNotFoundException("Doctor not found with id " + appointment.getIdDoctor())));
                    appointment.setPatient(patientRepository.findById(appointment.getIdDoctor())
                    .map(pat -> {
                        return pat;
                    }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + appointment.getIdDoctor())));

                    appointment.setLastNamePatient(appointmentRequest.getLastNamePatient());
                    appointment.setFirstNamePatient(appointmentRequest.getFirstNamePatient());
                    appointment.setGender(appointmentRequest.getGender());
                    appointment.setAppointmentDate(appointmentRequest.getAppointmentDate());
                    appointment.setValidation(appointmentRequest.getValidation());
                    appointment.setAppointmentDateString(appointmentRequest.getAppointmentDateString());
                    return appointmentRepository.save(appointment);
                }).orElseThrow(() -> new ResourceNotFoundException("Appointment not found with id " + appointmentId));
    }


    @DeleteMapping("/appointment/{appointmentId}")
    public ResponseEntity<?> deleteAppointment(@PathVariable Long appointmentId) {
        return appointmentRepository.findById(appointmentId)
                .map(appointment -> {
                    appointmentRepository.delete(appointment);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Appointment not found with id " + appointmentId));
    }
     @GetMapping("/appointment/searchByIdpatient/{patienId}")
    public List<Appointment> searchByIdpatient(@PathVariable Long patienId) { 
        return appointmentRepository.searchByIdpatient(patienId);
    }
      @PostMapping("/appointment/searchByDate")
    public List<Appointment> searchIdSymptom(@Valid @RequestBody Word word) {
        String wordToSearch="%"+word.getWordToSearch()+"%";
         return appointmentRepository.searchByDate(wordToSearch);      
    }
      @PostMapping("/appointment/searchAll")
    public List<Appointment> searchAll(@Valid @RequestBody Word word) {
        String wordToSearch="%"+word.getWordToSearch()+"%";
         return appointmentRepository.searchAll(wordToSearch);      
    }
}