package com.docfinderserver.docfinderserver.controller;

import com.docfinderserver.docfinderserver.exception.ResourceNotFoundException;
import com.docfinderserver.docfinderserver.model.Disease;
import com.docfinderserver.docfinderserver.model.SymptomPerDisease;
import com.docfinderserver.docfinderserver.model.Symptom;
import com.docfinderserver.docfinderserver.model.simplemodel.Word;
import com.docfinderserver.docfinderserver.model.simplemodel.Word1;
import com.docfinderserver.docfinderserver.repository.SymptomPerDiseaseRepository;
import com.docfinderserver.docfinderserver.repository.DiseaseRepository;
import com.docfinderserver.docfinderserver.repository.SymptomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
public class SymptomPerDiseaseController {

    @Autowired
    private SymptomPerDiseaseRepository symptomPerDiseaseRepository;
    @Autowired
    private DiseaseRepository diseaseRepository;
    @Autowired
    private SymptomRepository symptomRepository;

    @GetMapping("/symptomPerDisease")
    public List<SymptomPerDisease> getSymptomPerDiseases() {
        return symptomPerDiseaseRepository.findAll();
    }
    
    @GetMapping("/symptomPerDisease/{symptomPerDiseaseId}")
    public SymptomPerDisease getSymptomPerDiseaseById(@PathVariable Long symptomPerDiseaseId) { 
        return symptomPerDiseaseRepository.findById(symptomPerDiseaseId)
                .map(symptomPerDisease -> {
                    return symptomPerDisease;
                }).orElseThrow(() -> new ResourceNotFoundException("SymptomPerDisease not found with id " + symptomPerDiseaseId));
         
    }


    @PostMapping("/symptomPerDisease/add")
    public SymptomPerDisease createSymptomPerDisease(@Valid @RequestBody SymptomPerDisease symptomPerDisease) {
        Disease disease=diseaseRepository.findById(symptomPerDisease.getIdDisease())
                .map(dis -> {
                    return dis;
                }).orElseThrow(() -> new ResourceNotFoundException("Disease not found with id " + symptomPerDisease.getIdDisease()));
        
        Symptom symptom=symptomRepository.findById(symptomPerDisease.getIdSymptom())
                .map(sym -> {
                    return sym;
                }).orElseThrow(() -> new ResourceNotFoundException("Symptom not found with id " + symptomPerDisease.getIdSymptom()));
        
        symptomPerDisease.setDisease(disease);
        symptomPerDisease.setSymptom(symptom);
        symptomPerDisease.setBegininterval(0);
        symptomPerDisease.setEndinterval(0);
        symptomPerDisease.setUnit("");
        
        return symptomPerDiseaseRepository.save(symptomPerDisease);
        
    }

    @PostMapping("/symptomPerDisease/update/{symptomPerDiseaseId}")
    public SymptomPerDisease updateSymptomPerDisease(@PathVariable Long symptomPerDiseaseId,
                                   @Valid @RequestBody SymptomPerDisease symptomPerDiseaseRequest) {
        return symptomPerDiseaseRepository.findById(symptomPerDiseaseId)
                .map(symptomPerDisease -> {
                    symptomPerDisease.setDisease(diseaseRepository.findById(symptomPerDisease.getIdDisease())
                    .map(dis -> {
                        return dis;
                    }).orElseThrow(() -> new ResourceNotFoundException("Disease not found with id " + symptomPerDisease.getIdDisease())));
                    symptomPerDisease.setSymptom(symptomRepository.findById(symptomPerDisease.getIdSymptom())
                    .map(sym -> {
                        return sym;
                    }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + symptomPerDisease.getIdSymptom())));
                    symptomPerDisease.setBegininterval(0);
                    symptomPerDisease.setEndinterval(0);
                    symptomPerDisease.setUnit("");
                    return symptomPerDiseaseRepository.save(symptomPerDisease);
                }).orElseThrow(() -> new ResourceNotFoundException("SymptomPerDisease not found with id " + symptomPerDiseaseId));
    }


    @DeleteMapping("/symptomPerDisease/{symptomPerDiseaseId}")
    public ResponseEntity<?> deleteSymptomPerDisease(@PathVariable Long symptomPerDiseaseId) {
        return symptomPerDiseaseRepository.findById(symptomPerDiseaseId)
                .map(symptomPerDisease -> {
                    symptomPerDiseaseRepository.delete(symptomPerDisease);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("SymptomPerDisease not found with id " + symptomPerDiseaseId));
    }
    
    @GetMapping("/symptomPerDisease/findbyiddisease/{idDisease}")
    public List<SymptomPerDisease> getSymptomPerDiseases(@PathVariable Long idDisease) {
        return symptomPerDiseaseRepository.findByIdDisease(idDisease);
    }
     @PostMapping("/symptomPerDisease/searchIdSymptom")
    public List<SymptomPerDisease> searchIdSymptom(@Valid @RequestBody Word1 idSymptom1) {
         return symptomPerDiseaseRepository.searchIdSymptom(idSymptom1.getIdSymptom());      
    }
}