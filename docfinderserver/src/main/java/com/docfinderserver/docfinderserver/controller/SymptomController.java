package com.docfinderserver.docfinderserver.controller;

import com.docfinderserver.docfinderserver.exception.ResourceNotFoundException;
import com.docfinderserver.docfinderserver.model.Symptom;
import com.docfinderserver.docfinderserver.model.simplemodel.Word;
import com.docfinderserver.docfinderserver.repository.SymptomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
public class SymptomController {

    @Autowired
    private SymptomRepository symptomRepository;

    @GetMapping("/symptom")
    public List<Symptom> getSymptoms() {
        return symptomRepository.findAll();
    }
    
    @GetMapping("/symptom/{symptomId}")
    public Symptom getSymptomById(@PathVariable Long symptomId) { 
        return symptomRepository.findById(symptomId)
                .map(symptom -> {
                    return symptom;
                }).orElseThrow(() -> new ResourceNotFoundException("Symptom not found with id " + symptomId));
         
    }


    @PostMapping("/symptom/add")
    public Symptom createSymptom(@Valid @RequestBody Symptom symptom) {
        return symptomRepository.save(symptom);
    }

    @PostMapping("/symptom/update/{symptomId}")
    public Symptom updateSymptom(@PathVariable Long symptomId,
                                   @Valid @RequestBody Symptom symptomRequest) {
        return symptomRepository.findById(symptomId)
                .map(symptom -> {
                    symptom.setName(symptomRequest.getName());
                    symptom.setDescription(symptomRequest.getDescription());
                    return symptomRepository.save(symptom);
                }).orElseThrow(() -> new ResourceNotFoundException("Symptom not found with id " + symptomId));
    }


    @DeleteMapping("/symptom/{symptomId}")
    public ResponseEntity<?> deleteSymptom(@PathVariable Long symptomId) {
        return symptomRepository.findById(symptomId)
                .map(symptom -> {
                    symptomRepository.delete(symptom);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Symptom not found with id " + symptomId));
    }
    
    @GetMapping("/symptom/byname")
    public List<Symptom> getSymptomsByName() {
        return symptomRepository.findOrder();
    }
    
    @PostMapping("/symptom/searchfrombo")
    public List<Symptom> searchSymptom(@Valid @RequestBody Word word) {
       String wordToSearch="%"+word.getWordToSearch()+"%";
       return symptomRepository.search(wordToSearch);
    }
}