package com.docfinderserver.docfinderserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DocfinderserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocfinderserverApplication.class, args);
	}

}
