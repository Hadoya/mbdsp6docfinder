import React, { Component } from 'react'
import "./MedicalTeamStatistics.css";
import * as Config from './../Config/Config';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {    makeStyles  } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';


//select
import Select from 'react-select';
import SearchIcon from '@material-ui/icons/Search';

//Chart
import Paper from '@material-ui/core/Paper';
//Chart 2
import {Line} from 'react-chartjs-2';



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
 
  


export default class MedicalTeamStatistics extends Component {
    constructor(props){  
      super(props); 
 
      this.state = {
        optionsyear:[], 
        dataChart:[],
        yearToShow:0,
        beginningURL: Config.beginningURLJava
      };
      this.onSubmitShowStatistics=this.onSubmitShowStatistics.bind(this);  
      this.searchCorrespondentStatistics=this.searchCorrespondentStatistics.bind(this);
        
    }  
    componentDidMount(){
      //select
      let years=[];
      let currentYear= new Date().getFullYear();
      let i=currentYear;
      for(i=currentYear;i>=2019;i--){
        years.push({value:i, label:i});
      }
      this.setState({optionsyear:years});
      //Year to show
      this.setState({yearToShow:currentYear});
      //the chart for the year
      axios.get(this.state.beginningURL+'doctor/statpermonth/'+currentYear)
            .then(res => {
              this.setState({dataChart:res.data});
            });
      //this.searchCorrespondentStatistics();
    }
    //select (combobox) 
    state = {
        selectedOption: null,
      };
      handleChange = selectedOption => {
        this.setState({ selectedOption });
        //console.log(`Option selected:`, selectedOption);
        this.setState({yearToShow:selectedOption.value});
         // search the correspondent statistics
         axios.get(this.state.beginningURL+'doctor/statpermonth/'+selectedOption.value)
              .then(res => {
                this.setState({dataChart:res.data});
              });
         //this.searchCorrespondentStatistics();
      };

    onSubmitShowStatistics(e){
        e.preventDefault();
        axios.get(this.state.beginningURL+'doctor/statpermonth/'+this.state.selectedOption.value)
              .then(res => {
                this.setState({dataChart:res.data});
              });
        //this.searchCorrespondentStatistics();
           
    }

    searchCorrespondentStatistics(){
      if(this.state.selectedOption!=null){
        this.setState({yearToShow:this.state.selectedOption.value});
      } 
      //find the statistics corresponding to 'yearToShow'  through the web service
      axios.get(this.state.beginningURL+'doctor/statpermonth/'+this.state.yearToShow)
            .then(res => {
              this.setState({dataChart:res.data});
            });
      
      //data for the chart(example)
      //let data=[65, 59, 80, 81, 56, 55, 40,90,25,45,63,80];
      //this.setState({dataChart:data}); 
    }


    render() {
        //for the url
        //const {match} = this.props;
     
        //form 
        const classesForm = useStylesForm;
        //select (combobox)
        const { selectedOption } = this.state; 
  
        //chart 2
        const dataChart = {
          labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
          datasets: [
            {
              label: 'Nombres de corps médicaux enregistrés',
              fill: false,
              lineTension: 0.1,
              backgroundColor: 'rgba(75,192,192,0.4)',
              borderColor: 'rgba(75,192,192,1)',
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: 'rgba(75,192,192,1)',
              pointBackgroundColor: '#fff',
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: 'rgba(75,192,192,1)',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.state.dataChart
            }
          ]
        };

        // end chart 2 

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} >

                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5 >
                                        Statistiques des corps m&eacute;dicaux 
                                    </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main"  style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    <div className={classesForm.paper} >
                        <div style={{textAlign:'right' }}>
                            <br/>
                            <form onSubmit={this.onSubmitShowStatistics} noValidate>
                            <Grid container >
                                <Grid item xs={8} >
                                </Grid>
                                <Grid item xs={3}>
                                <Select
                                        placeholder="Année"
                                        value={selectedOption}
                                        onChange={this.handleChange}
                                        options={this.state.optionsyear}
                                        style={{maxWidth:'20px'}}
                                        
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                    >
                                        <SearchIcon />
                                    </Button>
                                   
                                </Grid>
                            </Grid>
                            </form>
                        </div>
                        {/* Chart 2 */}
                        <div  className="contentGraphe">
                          <Paper>
                            <h3>Corps M&eacute;dicaux Ann&eacute;e {this.state.yearToShow}</h3>
                            <Line data={dataChart} />
                          </Paper>
                        </div>
                        {/* End Chart 2 */}
                         <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
