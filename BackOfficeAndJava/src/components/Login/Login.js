import React, { Component } from 'react';
import * as Config from './../Config/Config';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import "./Login.css";
import Footer from '../Template/Footer';
import {Redirect} from 'react-router-dom'
//connect web service
import axios from 'axios';


//data userAdmin
/*const users=[
    {lastname:'Rakoto',firstname:'Fy', username:'fy', email:'fy@gmail.com',password:'fy'},
    {lastname:'Randria',firstname:'Andry', username:'andry', email:'andry@gmail.com',password:'andry'}
] */

export default class Login extends Component {
    constructor(props){
        super(props)
        
        let loggedIn= false;
        let isSubmitted=false;//if the person has already pressed the submit button
        this.state={
            beginningURL: Config.beginningURL,
            errorMessage:'',
            username:'',
            password:'',
            currentUser:null,
            loggedIn,
            isSubmitted
        }
        this.onChange=this.onChange.bind(this);
        this.submitForm=this.submitForm.bind(this);
        this.showErrorMessage=this.showErrorMessage.bind(this);
  
    }
    componentDidMount(){
        if(localStorage.getItem("loggedIn")==="true"){
            this.setState({
                loggedIn:true
            })
        }
    }
    onChange(e){
        this.setState({
            [e.target.name]:e.target.value
        })
    }
    submitForm(e){
        e.preventDefault()
        this.setState({isSubmitted:true});
        const {username, password} = this.state;
        //login magic
        //new change
        //find in database 
        const userAdmin ={
            username:username,
            password:password,
        };

        axios.post(this.state.beginningURL+'userAdmin/login',userAdmin)
        .then(res =>{ 
            this.setState({currentUser:res.data})
            if(res.data.msg){ 
                this.setState({currentUser:null, errorMessage:"Nom d'utilisateur et/ou mot de passe invalide."});
            }
            //alert(this.state.currentUser.username);
            if(this.state.currentUser!=null){
                localStorage.setItem("userAdmin",JSON.stringify(this.state.currentUser));
                localStorage.setItem("token","hgdljhqflhlkjhlkjhqqq");
                localStorage.setItem("reload","true");
                localStorage.setItem("loggedIn","true");
                this.setState({
                    loggedIn:true
                })
            }
        });
        if((this.state.isSubmitted) && (!this.state.loggedIn)){
            //alert("Votre \"nom d'ualertilisateur\" ou \"mot de passe\" est incorrect, veuillez vous reconnecter.");
            this.setState({isSubmitted:false});
        }

    }
   

    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={{marginTop:'20px',color:'red',backgroundColor:'yellow'}}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }
    
    render() {
        const useStyles = makeStyles((theme) => ({
            paper: {
              marginTop: theme.spacing(8),
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
             
            },
            avatar: {
              margin: theme.spacing(1),
              backgroundColor: theme.palette.secondary.main,
              textAlign:'center'
            },
            form: {
              width: '100%', // Fix IE 11 issue.
              marginTop: theme.spacing(1),
            },
            submit: {
              margin: theme.spacing(3, 0, 2),
            },
          }));

        const classes = useStyles;

        if(this.state.loggedIn){
            return  <Redirect to="/dashboard" />

        }
        
        return (
            <Container component="main" maxWidth="xs" className="mainContainer">
                <CssBaseline />
                <div className={classes.paper}>
                    <br/>
                    <img className="imgLogo" src="assets/images/logos/LogoDocFinder4.png" alt="Doc Finder" />
                    <br/>
                    <Typography component="h1" variant="h5" style={{marginTop:'20px'}}>
                            Bienvenue ! 
                            <br/>
                            <LockOutlinedIcon style={{marginTop:'20px'}} color="secondary" />
                       
                    </Typography>
                   {this.showErrorMessage()}
                    
                    
                    
                    <form className={classes.form} onSubmit={this.submitForm} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Nom d'utilisateur"
                        autoComplete="Nom d'utilisateur"
                        autoFocus

                        name="username" 
                        value={this.state.username} 
                        onChange={this.onChange}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        
                        label="Mot de passe"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        name="password" 
                        value={this.state.pasword} 
                        onChange={this.onChange}
                    />
                
                    
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    
                        
                    >
                        Se connecter 
                    </Button>
                    
                   
                    </form>
                    
                </div>
                <Box mt={8} variant="body2" color="textSecondary" align="center">
                    <Footer/>
                </Box>
                
            </Container>
        )
    }
}
