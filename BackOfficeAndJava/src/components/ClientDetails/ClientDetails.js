import React, { Component } from 'react'
import "./ClientDetails.css";
import * as Config from './../Config/Config';
import axios from 'axios';

//Table
import { Link } from 'react-router-dom';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  createMuiTheme, makeStyles, ThemeProvider  } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { green } from '@material-ui/core/colors';




// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  //theme button 
  const theme = createMuiTheme({
    palette: {
      primary: green,
    },
  });
 


export default class ClientDetails extends Component {
    constructor(props){  
        super(props); 
        //upload photo 
     
        this.state={
            lastname:'',
            firstname:'',
            email:'',
            dateInscription:'',
            beginningURL: Config.beginningURLJava
        };       
    }  
    componentDidMount(){
      axios.get(this.state.beginningURL+'patient/'+this.props.match.params.id)
      .then(response=>{
          this.setState({
              lastname:response.data.lastname,
              firstname:response.data.firstname,
              email:response.data.email,
              dateInscription:response.data.createdAt
          })
      })
      .catch(function(error){
          console.log(error);
      });

    }
  


    render() {
        //for the url
        //const {match} = this.props;
     
        //form 
        const classesForm = useStylesForm;
        //select (combobox)
        

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} >

                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5 >
                                        Client : {this.state.lastname+' '+this.state.firstname}
                                    </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    <div className={classesForm.paper} >
                       
                        <div className="contentLeSaviezVousDetails" >
                            <img className="imagePerson" src="/assets/images/logos/logoPerson3.png" alt="Le Saviez-vous"/>
                            
                            
                            <div className="row ">
                                <h4 className="labelContent">Nom:</h4> 
                            </div>
                            <div className=" row">
                                {this.state.lastname}<br/>
                            </div>
                            <div className="row">
                                <h4 className="labelContent">Prénom(s): </h4>
                            </div>
                            <div className=" row">
                                 {this.state.firstname}<br/>
                            </div>
                            <div className="row">
                                <h4 className="labelContent">Email: </h4> 
                            </div> 
                            <div className=" row">
                                {this.state.email}<br/>
                            </div> 
                            <div className="row">
                                <h4 className="labelContent">Date et heure d'inscription: </h4> 
                            </div>  
                            <div className=" row">
                                {this.state.dateInscription.substring(0,10)+" à "+this.state.dateInscription.substring(11,22)}<br/>
                            </div>  
                            
                            <Grid container >
                                <Grid item xs={5} >
                                
                                </Grid>
                                <Grid item xs={2}>

                                </Grid>
                                <Grid item xs={5}>
                                <ThemeProvider theme={theme}>
                                    <Link to='/clientslist'>
                                    <Button variant="contained" color="primary" className={classesForm.submit}>
                                    Revenir liste
                                    </Button>
                                  </Link>
                                </ThemeProvider>
                                </Grid>
                            </Grid>
                        </div>
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
