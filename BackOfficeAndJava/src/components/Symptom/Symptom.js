import React, { Component } from 'react'
import "./Symptom.css";
import Footer from "./../Template/Footer";
import {  Switch, Route } from 'react-router-dom'
//import { makeStyles } from '@material-ui/core/styles';
//Search
import SymptomList from './SymptomList';
import NewSymptom from './NewSymptom';
import EditSymptom from './EditSymptom';


export default class Symptom extends Component {
    constructor(props){  
        super(props);  
        this.state = {  
         }  
        
         
    }  
    

    

    render() {
        //for the url
        //const {match} = this.props;    

        
        return (
            <div className="page-wrapper">  
                
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Sympt&ocirc;mes</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Sympt&ocirc;mes</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                
                <Switch>
                    <Route path='/symptomlist' component={SymptomList}  exact/>
                    <Route path='/editsymptom/:id' component={EditSymptom} exact />
                    <Route path='/newsymptom' component={NewSymptom} exact />


                </Switch>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}

                {/* <Scripts/>  */}
            </div>
        )
    }
}
