import React, { Component } from 'react'
import "./MedicalTeam.css";
import Footer from "./../Template/Footer";
import {  Switch, Route } from 'react-router-dom'
//import { makeStyles } from '@material-ui/core/styles';
//Search

import MedicalTeamList from '../MedicalTeamList/MedicalTeamList';
import MedicalTeamValidation from '../MedicalTeamValidation/MedicalTeamValidation';
import MedicalTeamStatistics from '../MedicalTeamStatistics/MedicalTeamStatistics';
import MedicalTeamDetails from '../MedicalTeamDetails/MedicalTeamDetails';



export default class MedicalTeam extends Component {
    constructor(props){  
        super(props);  
        this.state = {  
           page:0,
           rowsPerPage:10,
        }     
    }  

    

    render() {
        //for the url
        //const {match} = this.props;
       
        
        return (
            <div className="page-wrapper">  
                
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Corps m&eacute;dicaux</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Corps m&eacute;dicaux</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                
                <Switch>
                   
                    <Route path='/medicalteamvalidation' component={MedicalTeamValidation} exact />
                    <Route path='/medicalteamlist' component={MedicalTeamList} exact />
                    <Route path='/medicalteamdetails/:id' component={MedicalTeamDetails} exact />
                    <Route path='/medicalteamstatistics' component={MedicalTeamStatistics} exact />
                     
                </Switch>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}

                {/* <Scripts/>  */}
            </div>
        )
    }
}
