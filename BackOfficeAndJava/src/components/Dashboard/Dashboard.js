import React, { Component } from 'react'
import "./Dashboard.css";
import * as Config from './../Config/Config';
import '@devexpress/dx-react-chart-bootstrap4/dist/dx-react-chart-bootstrap4.css';
import Footer from "./../Template/Footer";
//connect mongodb
import axios from 'axios';
//Pie 
import {Pie} from 'react-chartjs-2';
import Paper from '@material-ui/core/Paper';




export default class Dashboard extends Component {
    constructor(props) {
        //reload the page so as to get the javascript
        if(localStorage.getItem("reload") === "true"){
            window.location.reload(false);
            localStorage.setItem("reload","false")
        }
        super(props);
        let docPerSpec=[
            {specialityName: 'Dentiste', number:5},
            {specialityName: 'Généraliste', number:20},
            {specialityName: 'Dermatologue', number:3},
            {specialityName: 'Ophtalmo', number:10}
        ];
    
        this.state = {
          nbClients:0,
          nbOkDoctors:0,
          nbNokDoctors:0,
          doctorPerSpeciality:[],
          highlightedSlice: null,
          labelsPie:[],
          dataPie:[],
          pieColorArray:[],
          beginningURL: Config.beginningURLJava
        };
        //pie 
        this.setPieData=this.setPieData.bind(this); 

      }
    

    componentDidMount(){
        // number of clients (patients)
        axios.get(this.state.beginningURL+'patient/countallpatients')
        .then(response=>{
          this.setState({nbClients:response.data})
        })
        .catch(error=>{
          console.log(error)
        })
        //validate doctor
        axios.get(this.state.beginningURL+'doctor/countallokdoctors')
        .then(response=>{
          this.setState({nbOkDoctors:response.data})
        })
        .catch(error=>{
          console.log(error)
        })
        //non validate doctor
        axios.get(this.state.beginningURL+'doctor/countallnokdoctors')
        .then(response=>{
          this.setState({nbNokDoctors:response.data})
        })
        .catch(error=>{
          console.log(error)
        });
        // doctor per speciality (pie)
        axios.get(this.state.beginningURL+'doctor/statperdocspeciality')
        .then(response=>{
          this.setState({doctorPerSpeciality:response.data})
          this.setPieData();
        })
        .catch(error=>{
          console.log(error)
        });

        
        

    }

    //Pie 
    setPieData(){
        let allColors= [];
        let allLabels=[];
        let arrayData=[];
        let colorIsPresent=true;
        let randomColor="";
        
        let i=0;
        for (i = 0; i < this.state.doctorPerSpeciality.length; i++) {
           //take a random color
           //make sure all color inside the array are not the same 
            colorIsPresent=true;
            while (colorIsPresent) {
                randomColor="#" +Math.floor(Math.random()*16777215).toString(16);
                colorIsPresent=allColors.some(el => el === randomColor);               
            }
            allColors.push(randomColor);
            //insert labels
            allLabels.push(this.state.doctorPerSpeciality[i].specialityName);
            //insert data
            arrayData.push(this.state.doctorPerSpeciality[i].number);
            
        }
        this.setState({
            labelsPie:allLabels,
            dataPie:arrayData,
            pieColorArray:allColors
        });
        
      }
    //End Pie 


    render() {

        //Pie 
        const alldataPie3 = {
            labels:this.state.labelsPie,
            datasets: [{
                data: this.state.dataPie,
                backgroundColor: this.state.pieColorArray,
                hoverBackgroundColor: this.state.pieColorArray
            }]
        };

        //End Pie 
      

        return (
            <div className="page-wrapper">  
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Tableau de bord</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Tableau de bord</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid">
                    {/*-- The content of the page-->*/}
                    {/*-- ============================================================== -->*/}
                    {/*-- Sales Cards  -->*/}
                    {/*-- ============================================================== -->*/}
                    <div className="row">
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-cyan text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">Clients inscrits: <br/>{this.state.nbClients}</h6>

                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-success text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">M&eacute;decins inscrits: <br/>{this.state.nbOkDoctors}</h6>
                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-warning text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">M&eacute;decins en attente de validation:<br/>{this.state.nbNokDoctors}</h6>
                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-12 col-lg-12 col-xlg-3 divPie" >
                            {/*<div className="card card-hover">
                                <div className="box bg-danger text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-border-outside"></i></h1>
                                    <h6 className="text-white">Corps m&eacute;dicaux par sp&eacute;cialit&eacute;s</h6>
                                </div>
                            </div>*/}
                           
                            {/* Pie  */}
                            <Paper>
                                <h3>Corps m&eacute;dicaux par sp&eacute;cialit&eacute;s</h3>
                                <Pie data={alldataPie3} />
                            </Paper>
                            {/* End Pie  */}


                        </div>
                        
                        

                        
                    </div>
                    {/*-- ============================================================== -->*/}
                    {/*-- Sales chart -->*/}
                    {/*-- ============================================================== -->*/}

        
                    {/*-- End content of the page-->*/}
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}
            </div>
        )
    }
}
