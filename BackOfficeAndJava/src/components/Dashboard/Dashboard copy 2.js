import React, { Component } from 'react'
import "./Dashboard.css";
import '@devexpress/dx-react-chart-bootstrap4/dist/dx-react-chart-bootstrap4.css';
import Footer from "./../Template/Footer";
//connect mongodb
import axios from 'axios';
//Pie
import Paper from '@material-ui/core/Paper';
import {
  Chart,
  PieSeries,
  Title
} from '@devexpress/dx-react-chart-material-ui';
import  {

    Label,
    Font,
    Connector
  } from 'devextreme-react/pie-chart';

  

import { Animation } from '@devexpress/dx-react-chart';
const data = [
    { country: 'Russia', area: 12 },
    { country: 'Canada', area: 7 },
    { country: 'USA', area: 7 },
    { country: 'China', area: 7 },
    { country: 'Brazil', area: 6 },
    { country: 'Australia', area: 5 },
    { country: 'India', area: 2 },
    { country: 'Others', area: 55 },
  ];
//End pie


export default class Dashboard extends Component {
    //For the pie that represents the statistics of medical team 
    constructor(props) {
        //reload the page so as to get the javascript
        if(localStorage.getItem("reload") === "true"){
            window.location.reload(false);
            localStorage.setItem("reload","false")
        }
        super(props);
    
        this.state = {
          data,
          nbClients:0,
          nbOkDoctors:0,
          nbNokDoctors:0,
          beginningURL: 'http://localhost:5000/'
        };
        this.customizeText=this.customizeText.bind(this);  
        

      }
    //end pie

    componentDidMount(){
        // number of clients (patients)
        axios.get(this.state.beginningURL+'patient/countallpatients')
        .then(response=>{
          this.setState({nbClients:response.data})
        })
        .catch(error=>{
          console.log(error)
        })
        //validate doctor
        axios.get(this.state.beginningURL+'doctor/countallokdoctors')
        .then(response=>{
          this.setState({nbOkDoctors:response.data})
        })
        .catch(error=>{
          console.log(error)
        })
        //non validate doctor
        axios.get(this.state.beginningURL+'doctor/countallnokdoctors')
        .then(response=>{
          this.setState({nbNokDoctors:response.data})
        })
        .catch(error=>{
          console.log(error)
        })


    }

     customizeText(arg) {
        return `${arg.valueText} (${arg.percentText})`;
      }


    render() {
        //pie
        const { data: chartData } = this.state;
        //end pie
        return (
            <div className="page-wrapper">  
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Tableau de bord</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Tableau de bord</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid">
                    {/*-- The content of the page-->*/}
                    {/*-- ============================================================== -->*/}
                    {/*-- Sales Cards  -->*/}
                    {/*-- ============================================================== -->*/}
                    <div className="row">
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-cyan text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">Clients inscrits: <br/>{this.state.nbClients}</h6>

                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-success text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">M&eacute;decins inscrits: <br/>{this.state.nbOkDoctors}</h6>
                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-warning text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">M&eacute;decins en attente de validation:<br/>{this.state.nbNokDoctors}</h6>
                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-12 col-lg-12 col-xlg-3">
                            {/*<div className="card card-hover">
                                <div className="box bg-danger text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-border-outside"></i></h1>
                                    <h6 className="text-white">Corps m&eacute;dicaux par sp&eacute;cialit&eacute;s</h6>
                                </div>
                            </div>*/}
                            {/* pie */}
                            <Paper>
                                <Chart
                                data={chartData}
                                >
                                <PieSeries
                                    valueField="area"
                                    argumentField="country"
                                    
                                >
                                       <Label
                                            visible={true}
                                            position="columns"
                                            customizeText={this.customizeText}>
                                            <Font size={16} />
                                            <Connector visible={true} width={0.5} />
                                        </Label> 
                                </PieSeries>

                                
                                <Title
                                    text="Corps m&eacute;dicaux par sp&eacute;cialit&eacute;s"
                                />
                                <Animation />
                                </Chart>
                            </Paper>
                            {/* end pie */}
                        </div>
                        
                    </div>
                    {/*-- ============================================================== -->*/}
                    {/*-- Sales chart -->*/}
                    {/*-- ============================================================== -->*/}

        
                    {/*-- End content of the page-->*/}
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}
            </div>
        )
    }
}
