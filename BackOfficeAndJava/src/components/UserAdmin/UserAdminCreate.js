import React, { Component } from 'react';
import "./UserAdminCreate.css";
import axios from 'axios';
import * as Config from './../Config/Config';
import {Link} from 'react-router-dom';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  makeStyles } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';


// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  
// end form


export default class UserAdminCreate extends Component {
    constructor(props){
        super(props);
        this.state={
            errorMessage:'',
            lastname:"",
            firstname:'',
            username:'',
            email:'',
            password:'',
            passwordConfirmation:'',
            beginningURL: Config.beginningURL

        }
    

        this.showErrorMessage=this.showErrorMessage.bind(this);

        this.onChangeLastname=this.onChangeLastname.bind(this);
        this.onChangeFirstname=this.onChangeFirstname.bind(this);
        this.onChangeUsername=this.onChangeUsername.bind(this);
        this.onChangeEmail=this.onChangeEmail.bind(this);
        this.onChangePassword=this.onChangePassword.bind(this);
        this.onChangePasswordConfirmation=this.onChangePasswordConfirmation.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
    }

    componentDidMount(){}

    onChangeLastname(e){
        this.setState({
            lastname:e.target.value
        });
    }
    onChangeFirstname(e){
        this.setState({
            firstname:e.target.value
        });
    }
    onChangeUsername(e){
        this.setState({
            username:e.target.value
        });
    }
    onChangeEmail(e){
        this.setState({
            email:e.target.value
        });
    }
    onChangePassword(e){
        this.setState({
            password:e.target.value
        });
    }
    onChangePasswordConfirmation(e){
        this.setState({
            passwordConfirmation:e.target.value
        });
    }
    //if we want to change a date(if we have a date among the states)
    /*onChangeDate(date){
        this.setState({
            date:date
        })
    }*/

    onSubmit(e){
        e.preventDefault();
        //verify if all the required field are not empty
        if((this.state.username==='' )||(this.state.password==='')||(this.state.passwordConfirmation==='')){
            this.setState({errorMessage:'Tous les champs requis(*) doivent être remplis'});
        }
        else{
            if(this.state.password===this.state.passwordConfirmation){
                const userAdmin ={
                    lastname:this.state.lastname,
                    firstname:this.state.firstname,
                    username:this.state.username,
                    email:this.state.email,
                    password:this.state.password
                };
                console.log(userAdmin);
        
                axios.post(this.state.beginningURL+'userAdmin/add',userAdmin)
                 .then(res => console.log(res.data));
        
                 window.location="/useradminlist";
            }
            else{
                this.setState({errorMessage:'Les mots de passes ne sont par conformes.'})
            }

        }

        

        
    }

    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={{marginTop:'20px',color:'red',backgroundColor:'yellow'}}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }

    render() {
        //form 
        const classesForm = useStylesForm;
        return (
            <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} style={{textAlign:'left'}}>
                                        <Link to='/useradminlist' >
                                            <Button variant="contained" color="secondary">
                                                Voir liste
                                            </Button>
                                        </Link>
                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5  style={{marginTop:'10px'}}>
                                        Nouveau administrateur
                                    </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
                 {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    {this.showErrorMessage()}
                    <div className={classesForm.paper} >
                       
                        
                    <form className={classesForm.form} onSubmit={this.onSubmit} noValidate>                       
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="nom"
                            label="Nom"
                            name="nom"
                            autoComplete="nom"
                            autoFocus 
                            type="text" value={this.state.lastname} onChange={this.onChangeLastname}/>

                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="prenoms"
                            label="Prénom(s)"
                            name="prenoms"
                            autoComplete="prenoms"
                             
                            type="text" value={this.state.firstname} onChange={this.onChangeFirstname}/>
                  
                    
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="username"
                            label="Nom d'utilisateur"
                            name="username"
                            autoComplete="username"
                             
                            type="text" required  value={this.state.username} onChange={this.onChangeUsername}/>
                    
                   
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="email"
                            label="Email"
                            name="email"
                            autoComplete="email"
                             
                            type="email"   value={this.state.email} onChange={this.onChangeEmail}/>
                 
                    
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="password"
                            label="Mot de passe"
                            name="password"
                            autoComplete="password"
                             
                            type="password" required  value={this.state.password} onChange={this.onChangePassword}/>
                  
                  
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="passwordConfirmation"
                            label="Confirmer mot de passe"
                            name="passwordConfirmation"
                            autoComplete="passwordConfirmation"
                             
                            type="password" required value={this.state.passwordConfirmation} onChange={this.onChangePasswordConfirmation}/>
                    
                        <Grid container style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                                <Link to="/useradminlist">
                                    <Button
                                    type="button"
                                    fullWidth
                                    variant="contained" 
                                    style={{marginRight:'8px'}}                                 
                                    > 
                                        Annuler
                                    </Button>
                                </Link>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={{marginLeft:'8px'}} 
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                        </form>
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
            
                
            </div>
        )
    }
}
