import React, { Component } from 'react'
import "./Diagnostic.css";
import Footer from "./../Template/Footer";
import {  Switch, Route } from 'react-router-dom'

import DiagnosticList from '../DiagnosticList/DiagnosticList';
import NewDiagnostic from '../NewDiagnostic/NewDiagnostic';
import DiagnosticDetails from '../DiagnosticDetails/DiagnosticDetails';
import DiagnosticEdit from '../DiagnosticEdit/DiagnosticEdit';




export default class Diagnostic extends Component {
    constructor(props){  
        super(props);  
        this.state = {
         }  

    }  

    

    render() {
        //for the url
        //const {match} = this.props;
        
      

        
        return (
            <div className="page-wrapper">  
                
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Diagnostic</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Diagnostic</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                
                <Switch>
                    <Route path='/diagnosticlist' component={DiagnosticList} exact />
                    <Route path='/diagnosticdetails/:id' component={DiagnosticDetails} exact />
                    <Route path='/diagnosticedit/:id' component={DiagnosticEdit} exact />
                    <Route path='/newdiagnostic' component={NewDiagnostic} exact />

                    
                    

                </Switch>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}

                {/* <Scripts/>  */}
            </div>
        )
    }
}
