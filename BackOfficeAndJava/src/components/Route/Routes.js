
import React, { Component } from 'react'
import {  Switch, Route } from 'react-router-dom'
import Template from '../Template/Template'
import Login from '../Login/Login'



export default class Routes extends Component {
    render() {
   
        return (
           
            <Switch>
                    <Route path='/' component={Login} exact/>
                    <Route path='/dashboard' component={Template} exact/>
                    <Route path='/lesaviezvouslist' component={Template} exact/>
                    {/* inside le saviez-vous */}
                    <Route path='/newlesaviezvous' component={Template} exact />
                    <Route path='/lesaviezvousdetails/:id' component={Template} exact />
                    <Route path='/lesaviezvousedit/:id' component={Template} exact />
                    {/* end inside le saviez-vous */}
                    {/* <Route path='/diagnostic' component={Diagnostic} /> */}
                    {/* inside diagnostic */}
                    <Route path='/diagnosticlist' component={Template} exact />
                    <Route path='/diagnosticdetails/:id' component={Template} exact />
                    <Route path='/diagnosticedit/:id' component={Template} exact />
                    <Route path='/newdiagnostic' component={Template} exact />
                    {/* Symptoms */}
                    <Route path='/symptomlist' component={Template}  exact/>
                    <Route path='/editsymptom/:id' component={Template} exact />
                    <Route path='/newsymptom' component={Template} exact />
                    {/* End Symptoms */}
                    {/* end inside diagnostic */}
                    <Route path='/clients' component={Template} />
                    {/* inside clients  */}
                    <Route path='/clientslist' component={Template} exact />
                    <Route path='/clientdetails/:id' component={Template} exact />
                    <Route path='/clientsstatistics' component={Template} exact /> 
                    {/* end inside clients */}
                    {/* <Route path='/clients/clientsstatistics' component={Clients} /> */}
                    {/* <Route path='/medicalteam' component={MedicalTeam} /> */}
                    {/* <Route path='/medicalteamlist' component={MedicalTeamList} /> */}
                    {/* <Route path='/medicalteamstatistics' component={MedicalTeamStatistics} /> */}
                    {/* inside medicalteam */}
                    <Route path='/medicalteamvalidation' component={Template} exact />
                    <Route path='/medicalteamlist' component={Template} exact />
                    <Route path='/medicalteamdetails/:id' component={Template} exact />
                    <Route path='/medicalteamstatistics' component={Template} exact />
                    {/* end inside medicalteam */}
                    {/* UserAdmin */}
                    <Route path="/useradminlist" component={Template} exact/>
                    <Route path="/useradminedit/:id" component={Template} exact/>
                    <Route path="/useradmincreate" component={Template} exact/> 
                    {/* end userAdmin */}
            </Switch>
            
           
        
        )
    }
}
