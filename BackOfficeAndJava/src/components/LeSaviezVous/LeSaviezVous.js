import React, { Component } from 'react'
import "./LeSaviezVous.css";
import Footer from "./../Template/Footer";
import {  Switch, Route } from 'react-router-dom'
//import { makeStyles } from '@material-ui/core/styles';
//Search
import LeSaviezVousDetails from '../LeSaviezVousDetails/LeSaviezVousDetails';
import LeSaviezVousList from '../LeSaviezVousList/LeSaviezVousList';
import NewLeSaviezVous from '../NewLeSaviezVous/NewLeSaviezVous';
import LeSaviezEdit from '../LeSaviezVousEdit/LeSaviezVousEdit';

export default class LeSaviezVous extends Component {
    constructor(props){  
        super(props);  
        this.state = {  
           page:0,
           rowsPerPage:10,
          
         }  
        
         
    }  
    

    

    render() {
        //for the url
        //const {match} = this.props;    

        
        return (
            <div className="page-wrapper">  
                
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Le Saviez-Vous</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Le Saviez-Vous</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                
                <Switch>
                    <Route path='/lesaviezvouslist' component={LeSaviezVousList}  exact/>
                    <Route path='/newlesaviezvous' component={NewLeSaviezVous} exact />
                    <Route path='/lesaviezvousdetails/:id' component={LeSaviezVousDetails} exact />
                    <Route path='/lesaviezvousedit/:id' component={LeSaviezEdit} exact />

                </Switch>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}

                {/* <Scripts/>  */}
            </div>
        )
    }
}
