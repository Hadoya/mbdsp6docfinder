import React, { Component } from 'react'
import "./ClientsList.css";
import * as Config from './../Config/Config';
import axios from 'axios';
//Table
import { Link } from 'react-router-dom';
//import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import {  makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
//grid for the search
import Grid from '@material-ui/core/Grid';
//button
import Button from '@material-ui/core/Button';




  const Client = props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.patient.code}>
        <TableCell>{props.numero}</TableCell>
        <TableCell>{props.patient.lastname}</TableCell>
        <TableCell>{props.patient.firstname}</TableCell>
        <TableCell style={{textAlign:'center'}}><Link to={"/clientdetails/"+props.patient.id}><img className="imageModifier" alt="edit" src="assets/images/logos/logoAdd.png"/></Link></TableCell>
    </TableRow>
    )
  
  const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },
  });
//end Table


export default class ClientsList extends Component {
    constructor(props){  
        super(props);  
        this.state = {  
            clients:[],
           page:0,
           rowsPerPage:10,
           wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
           beginningURL: Config.beginningURLJava
         }
    
        this.clientsList=this.clientsList.bind(this);  
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearchClients=this.onSubmitSearchClients.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
         
    }  

    componentDidMount(){
        axios.get(this.state.beginningURL+'patient/allfrombo')
            .then(response =>{
                this.setState({
                    clients:response.data
                });
            })
            .catch(error=>{
                console.log(error);
            })
    }
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
    }
    onSubmitSearchClients(e){
        e.preventDefault();
        const word ={
            wordToSearch:this.state.wordToSearch,
  
        };
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            axios.post(this.state.beginningURL+'patient/searchfrombo',word)
            .then(res => {
                this.setState({
                    clients:res.data,
                    nbSearchResult:res.data.length,
                    isSubmit:true
                });
            });
        }    
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
           return <div><br/><b className="searchResult">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }

    clientsList(){
        let i=0;
        return this.state.clients.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentClient) => {
            i++;
            return <Client numero={i}  patient={currentClient}  key={currentClient.id}/>;
           
        })
     
    }

    

    render() {
        //for the url
        //const {match} = this.props;
        //Table
        const classes = useStyles;
       
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };

        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table
      

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                                <Grid container spacing={3}>
                                    <Grid item xs={4} style={{textAlign:'left'}}>
                                        <h5 style={{marginTop:'10px'}}>Liste de tous les clients inscrits</h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        <div >
                                        <form onSubmit={this.onSubmitSearchClients} noValidate>
                                            <div className="searchDiv">
                                            <InputBase
                                                placeholder=" Rechercher…" 
                                                className="searchInput"  
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}
                                            />
                                            <Button variant="contained" className="searchButton"><SearchIcon /></Button>
                                            </div>
                                        </form>
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid" style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Paper className={classes.root}>
                    {this.displaySearchResult()}
                    <TableContainer className={classes.container}>
                        <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Num&eacute;ro</TableCell>
                                <TableCell>Nom</TableCell>
                                <TableCell>Pr&eacute;nom(s)</TableCell>
                                {/* <TableCell>Date d'entr&eacute;e</TableCell> */}
                                <TableCell style={{textAlign:'center'}}>D&eacute;tails</TableCell>
                            </TableRow>
                            
                        </TableHead>
                        <TableBody>
                            {this.clientsList()}
                        </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={this.state.clients.length}
                        rowsPerPage={this.state.rowsPerPage}
                        page={this.state.page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                    </Paper>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
