import React, { Component }  from 'react';
import ScriptTag from 'react-script-tag';
const Scripts = props => (
    <div>   
        <ScriptTag src="assets/libs/jquery/dist/jquery.min.js"></ScriptTag>
        {/*<!-- Bootstrap tether Core JavaScript -->*/}
        <ScriptTag src="assets/libs/popper.js/dist/umd/popper.min.js"></ScriptTag>
        <ScriptTag src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></ScriptTag>
        <ScriptTag src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></ScriptTag>
        <ScriptTag src="assets/extra-libs/sparkline/sparkline.js"></ScriptTag>
        {/* <!--Wave Effects --> */}
        <ScriptTag src="dist/js/waves.js"></ScriptTag>
        {/* <!--Menu sidebar --> */}
        <ScriptTag src="dist/js/sidebarmenu.js"></ScriptTag>
        {/* <!--Custom JavaScript --> */}
        <ScriptTag src="dist/js/custom.min.js"></ScriptTag>
        {/* <!--This page JavaScript --> */}
        {/* <!-- <script src="dist/js/pages/dashboards/dashboard1.js"></script> --> */}
        {/* <!-- Charts js Files --> */}
        <ScriptTag src="assets/libs/flot/excanvas.js"></ScriptTag>
        <ScriptTag src="assets/libs/flot/jquery.flot.js"></ScriptTag>
        <ScriptTag src="assets/libs/flot/jquery.flot.pie.js"></ScriptTag>
        <ScriptTag src="assets/libs/flot/jquery.flot.time.js"></ScriptTag>
        <ScriptTag src="assets/libs/flot/jquery.flot.stack.js"></ScriptTag>
        <ScriptTag src="assets/libs/flot/jquery.flot.crosshair.js"></ScriptTag>
        <ScriptTag src="assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></ScriptTag>
        <ScriptTag src="dist/js/pages/chart/chart-page-init.js"></ScriptTag> 
    </div>
)



export default class DefaultScripts extends Component {
    componentDidMount() {
         /* <!-- ============================================================== --> 
        /* <!-- This page plugin js --><!--Scripts for the login page. --> */
        /* <!-- ============================================================== --> 
        $('[data-toggle="tooltip"]').tooltip();
        $(".preloader").fadeOut();
         {/* ==============================================================  
        {/* // Login and Recover Password  
        {/* // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
            });
        $('#to-login').click(function(){
            
            $("#recoverform").hide();
            $("#loginform").fadeIn();
        });*/
      }
    
    render(){
        return (
           <div>
               <Scripts/>
           </div>
        );
    }
}


