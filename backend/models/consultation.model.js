const mongoose = require('mongoose')
const Schema = mongoose.Schema

const consultationShema = new Schema(
    {
        datetimeconsultation: { type: Date, required: true },
       
    },
    { timestamps: true },
)

module.exports = mongoose.model('Consultation', consultationShema)