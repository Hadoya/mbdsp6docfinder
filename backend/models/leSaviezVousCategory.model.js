const mongoose = require('mongoose')
const Schema = mongoose.Schema

const leSaviezVousCategorySchema = new Schema(
    {
        category: { type: String, required: true },
    },
    { timestamps: true },
)

module.exports = mongoose.model('LeSaviezVousCategory', leSaviezVousCategorySchema)