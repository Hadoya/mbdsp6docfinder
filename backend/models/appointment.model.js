const mongoose = require('mongoose')
const Schema = mongoose.Schema

const appointmentShema = new Schema(
    {
        idDoctor:{type:String, required:true},
        idPatient : {type:String},
        firstNameDoctor:{type:String, default:"inconnu"},
        lastNameDoctor:{type:String, default:"inconnu"},
        speciality:{type:String, default:"généraliste"},
        firstNamePatient: { type: String, required: true },
        lastNamePatient: { type: String, required:true },
        gender:{type:String, required:true},
        appointmentDate: { type: Date, default:new Date() },
        validation: {type:Number, default:0},
        createdAt: {type:Date},
        appointmentDateString : {type:String}
    },
    { timestamps: true },
)

module.exports = mongoose.model('Appointment', appointmentShema)