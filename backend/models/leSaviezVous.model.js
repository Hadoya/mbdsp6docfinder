const mongoose = require('mongoose')
const Schema = mongoose.Schema

const leSaviezVousShema = new Schema(
    {
        title: { type: String },
        idCategory: { type: String, required: true },
        image: { type: String},
        description: { type: String},
        insertdate: { type: Date},
        
     
       
    },
    { timestamps: true },
)

module.exports = mongoose.model('LeSaviezVous', leSaviezVousShema)