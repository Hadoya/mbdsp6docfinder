const mongoose = require('mongoose')
const Schema = mongoose.Schema

const symptomPerDiseaseShema = new Schema(
    {
        idDisease: { type: String },
        idSymptom: { type: String },
        begininterval: { type: Number },
        endinterval: { type: Number },
        unit: { type: String },
     
    },
    { timestamps: true },
)

module.exports = mongoose.model('SymptomPerDisease', symptomPerDiseaseShema)