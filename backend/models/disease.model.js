const mongoose = require('mongoose')
const Schema = mongoose.Schema

const diseaseShema = new Schema(
    {
        name: { type: String },
        description: { type: String },
        idDoctorSpeciality: { type: String },
        
       
    },
    { timestamps: true },
)

module.exports = mongoose.model('Disease', diseaseShema)