const mongoose = require('mongoose')
const Schema = mongoose.Schema

const patientDetailsShema = new Schema(
    {
        birthdate: { type: Date },
        height: { type: Number },
        weight: { type: Number },
        description: { type: String },
        
       
    },
    { timestamps: true },
)

module.exports = mongoose.model('PatientDetails', patientDetailsShema)