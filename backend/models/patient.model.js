const mongoose = require('mongoose')
const Schema = mongoose.Schema

const patientShema = new Schema(
    {
        lastname: { type: String },
        firstname: { type: String },
        email: { type: String },
        password: { type: String },
        gender:{type: String},
        estPatient: { type: Number }
       
    },
    { timestamps: true },
)

module.exports = mongoose.model('Patient', patientShema)