const router=require('express').Router()
let Doctor=require('../models/doctor.model') 

router.route('').get((req,res)=>{
     Doctor.find()
     .then(doctor=>res.json(doctor))
     .catch(err=>res.status(400).json('Error:' +err));
});


router.route('/nokdoctors').get((req,res)=>{
    Doctor.find({etatValidation:0}).sort({createdAt:-1})
    .then(doctor=>res.json(doctor))
    .catch(err=>res.status(400).json('Error:' +err));
});
router.route('/okdoctors').get((req,res)=>{
    Doctor.find({etatValidation:1}).sort({createdAt:-1})
    .then(doctor=>res.json(doctor))
    .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/okdoctorssearch').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
     Doctor.find({$and: [{$or: [{ lastname: regex }, { firstname: regex }, { telephone: regex }, { address: regex }, { town: regex }, { city: regex }, { description: regex }, { tarif: regex }]}, { etatValidation: 1 }]})
     .then(doct=>res.json(doct))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

router.route('/nokdoctorssearch').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
    Doctor.find({$and: [{$or: [{ lastname: regex }, { firstname: regex }, { telephone: regex }, { address: regex }, { town: regex }, { city: regex }, { description: regex }, { tarif: regex }]}, { etatValidation: 0 }]})
     .then(doc=>res.json(doc))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

router.route('/countallokdoctors').get((req,res)=>{
    Doctor.countDocuments({ etatValidation: 1 })
    .then(doctor=>res.json(doctor))
    .catch(err=>res.status(400).json('Error:' +err));
});
router.route('/countallnokdoctors').get((req,res)=>{
    Doctor.countDocuments({ etatValidation: 0 })
    .then(doctor=>res.json(doctor))
    .catch(err=>res.status(400).json('Error:' +err));
});






router.route('/add').post((req,res)=>{
    const lastname=req.body.lastname
    const firstname=req.body.firstname
    const idDoctorSpeciality=req.body.idDoctorSpeciality
    const telephone=req.body.telephone
    const address=req.body.address
    const town=req.body.town
    const city=req.body.city
    const description=req.body.description
    const tarif=req.body.tarif
    const etatValidation=req.body.etatValidation 
    const password=req.body.password
    const email=req.body.email
    const idPatient=req.body.idPatient
    
    const newDoctor=new Doctor({
        lastname,
        firstname,
        idDoctorSpeciality,
        telephone,
        address,
        town,
        city,
        description,
        tarif,
        etatValidation,
        password,
        email,
        idPatient
    })
    
    newDoctor.save()
    .then(doctor=>res.json('Doctor added'))
    .catch(err=>res.status(400).json('Error:' +err));

});
router.route('/:id').get((req,res)=>{
    Doctor.findById(req.params.id)
    .then(doctor=>res.json(doctor))
    .catch(err=>res.status(400).json('Error:'+err))
});

router.route('/findByIdpatient').post((req,res)=>{
     Doctor.find({idPatient: req.body.idPatient})
     .then(doctor=>res.json(doctor))
     .catch(err=>res.status(400).json('Error:' +err));
    
});


router.route('/update/:id').post((req,res)=>{
    Doctor.findById(req.params.id)
    .then(doctor=>{
        doctor.lastname=req.body.lastname
        doctor.firstname=req.body.firstname
        doctor.idDoctorSpeciality=req.body.idDoctorSpeciality
        doctor.telephone=req.body.telephone
        doctor.address=req.body.address
        doctor.town=req.body.town
        doctor.city=req.body.city
        doctor.description=req.body.description
        doctor.tarif=req.body.tarif


        doctor.save()
            .then(()=>res.json('Doctor updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
});

router.route('/validate/:id').post((req,res)=>{
    Doctor.findById(req.params.id)
    .then(doctor=>{
        doctor.etatValidation=1;
        doctor.save()
            .then(()=>res.json('Doctor validation success'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
});

router.route('/:id').delete((req,res)=>{
    Doctor.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Doctor deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/search').post((req,res)=>{
    const regex = new RegExp(req.body.description, 'i');  // 'i' makes it case insensitive
     Doctor.find({description: regex})
     .then(doctor=>res.json(doctor))
     .catch(err=>res.status(400).json('Error:' +err));
    
});
router.route('/search').post((req,res)=>{
    const regex = new RegExp(req.body.description, 'i');  // 'i' makes it case insensitive
     Doctor.find({description: regex})
     .then(doctor=>res.json(doctor))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

router.route('/searchByIdspecility').post((req,res)=>{
   Doctor.find({idDoctorSpeciality: req.body.idDoctorSpeciality})
     .then(doctor=>res.json(doctor))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

router.route('/authentificate').post((req,res)=>{
    const regexEmail = new RegExp(req.body.email, 'i');
    const regexPwd =  new RegExp(req.body.pwd);
    Doctor.find({$and: [{ email: regexEmail }, { password: regexPwd }, { etatValidation: 1 }]})
     .then(doct=>res.json(doct))
     .catch(err=>res.status(400).json('Error:' +err));
    
    Doctor.findOne({$and: [{email: req.body.email , password: req.body.pwd , etatValidation: 1}]})
    .then(doct=>res.json(doct))
    .catch(err=>res.status(400).json('Error:' +err));
});


module.exports=router;