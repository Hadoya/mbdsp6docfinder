const router=require('express').Router()
let Consultation=require('../models/consultation.model') 

router.route('').get((req,res)=>{
     Consultation.find()
     .then(consultation=>res.json(consultation))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
   
    const datetimeconsultation=Date.parse(req.body.datetimeconsultation)
   
    
    const newConsultation=new Consultation({
      
        datetimeconsultation,
     
    })
    
    newConsultation.save()
    .then(consultation=>res.json('Consultation added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    Consultation.findById(req.params.id)
    .then(consultation=>res.json(consultation))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    Consultation.findById(req.params.id)
    .then(consultation=>{
         consultation.datetimeconsultation=Date.parse(req.body.datetimeconsultation)
       
        consultation.save()
            .then(()=>res.json('Consultation updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    Consultation.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Consultation deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


module.exports=router;