const router=require('express').Router()
let Disease=require('../models/disease.model') 
let SymptomPerDisease=require('../models/symptomPerDisease.model') 

router.route('').get((req,res)=>{
     Disease.find()
     .then(disease=>res.json(disease))
     .catch(err=>res.status(400).json('Error:' +err));
});
router.route('/list').get((req,res)=>{
    Disease.find().sort({createdAt:-1}).limit(50)
    .then(disease=>res.json(disease))
    .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const name=req.body.name
    const description=req.body.description
    const idDoctorSpeciality=req.body.idDoctorSpeciality
  
    
    const newDisease=new Disease({
       name,
        description,
        idDoctorSpeciality,
 
    })
    
    newDisease.save()
    .then(disease=>res.json('Disease added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/addwithsymptoms').post((req,res)=>{
    const name=req.body.name
    const description=req.body.description
    const idDoctorSpeciality=req.body.idDoctorSpeciality
    const symptoms=req.body.symptoms;
    
    
    const newDisease=new Disease({
       name,
        description,
        idDoctorSpeciality,
 
    })
    
    newDisease.save()
    .then(disease=>{
        //res.json('Disease added')
        const idDisease=disease._id;
        let i=0;
        for(i=0;i<symptoms.length;i++){
            const idSymptom=symptoms[i].id;
            const begininterval=0;
            const endinterval=1;
            const unit='';
  
    
            const newSymptomPerDisease=new SymptomPerDisease({
                idDisease,
                idSymptom,
                begininterval,
                endinterval,
                unit
            
            })
            
            newSymptomPerDisease.save()
            .then(symptomPerDisease=>{})
            .catch(err=>res.status(400).json('Error:' +err));

        }
        res.json('Disease added')

    })
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    Disease.findById(req.params.id)
    .then(disease=>res.json(disease))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    Disease.findById(req.params.id)
    .then(disease=>{
         disease.name=req.body.name
         disease.description=req.body.description
          disease.idDoctorSpeciality=req.body.idDoctorSpeciality

        disease.save()
            .then(()=>res.json('Disease updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/updatewithsymptoms/:id').post((req,res)=>{
       //find all the symptoms that corresponds to the disease and delete them first
         SymptomPerDisease.find({idDisease:req.params.id})
         .then(symptomPerDisease=>{
             for(let j=0;j<symptomPerDisease.length;j++){
                 SymptomPerDisease.findByIdAndDelete(symptomPerDisease[j]._id)
                 .then(()=>{})
                 .catch(err=>res.status(400).json('Error:'+err))
             }

         })
         .catch(err=>res.status(400).json('Error:'+err))

    Disease.findById(req.params.id)
    .then(disease=>{
         disease.name=req.body.name;
         disease.description=req.body.description;
         disease.idDoctorSpeciality=req.body.idDoctorSpeciality;
     
        //update the disease
        disease.save()
        .then(disease=>{
            //res.json('Disease added')
            const idDisease=disease._id;
            const symptoms=req.body.symptoms;
            let i=0;
            
            //insert new symptoms
            for(i=0;i<symptoms.length;i++){
                const idSymptom=symptoms[i].id;
                const begininterval=0;
                const endinterval=1;
                const unit='';
      
        
                const newSymptomPerDisease=new SymptomPerDisease({
                    idDisease,
                    idSymptom,
                    begininterval,
                    endinterval,
                    unit
                
                })
                
                newSymptomPerDisease.save()
                .then(symptomPerDisease=>{})
                .catch(err=>res.status(400).json('Error:' +err));
    
            }
            res.json('Disease updated')
    
            })
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    //find all the symptoms that corresponds to the disease and delete them first
    SymptomPerDisease.find({idDisease:req.params.id})
    .then(symptomPerDisease=>{
        for(let j=0;j<symptomPerDisease.length;j++){
            SymptomPerDisease.findByIdAndDelete(symptomPerDisease[j]._id)
            .then(()=>{})
            .catch(err=>res.status(400).json('Error:'+err))
        }

    })
    .catch(err=>res.status(400).json('Error:'+err))

    //delete the disease
    Disease.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Disease deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/search').post((req,res)=>{
    Disease.find({_id: req.body._id})
     .then(disease=>res.json(disease))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

router.route('/searchfrombo').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
     Disease.find({$or: [{ name: regex }, { description: regex }]} )
     .then(dss=>res.json(dss))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

module.exports=router;