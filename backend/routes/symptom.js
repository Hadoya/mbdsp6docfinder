const router=require('express').Router()
let Symptom=require('../models/symptom.model') 

router.route('').get((req,res)=>{
     Symptom.find()
     .then(symptom=>res.json(symptom))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/byname').get((req,res)=>{
    Symptom.find().sort({name:'ascending'})
    .then(symptom=>res.json(symptom))
    .catch(err=>res.status(400).json('Error:' +err));

    
});

router.route('/add').post((req,res)=>{
    const name=req.body.name
    const description=req.body.description
  
    
    const newSymptom=new Symptom({
      name,
       description,
 
    })
    
    newSymptom.save()
    .then(symptom=>res.json('Symptom added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    Symptom.findById(req.params.id)
    .then(symptom=>res.json(symptom))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    Symptom.findById(req.params.id)
    .then(symptom=>{
        symptom.name=req.body.name
          symptom.description=req.body.description
        

        symptom.save()
            .then(()=>res.json('Symptom updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    Symptom.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Symptom deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/searchfrombo').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
     Symptom.find({$or: [{ name: regex }, { description: regex }]} )
     .then(spt=>res.json(spt))
     .catch(err=>res.status(400).json('Error:' +err));
    
});


module.exports=router;