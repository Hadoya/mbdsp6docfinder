const router=require('express').Router()
let Patient=require('../models/patient.model') 

router.route('').get((req,res)=>{
     Patient.find().sort({createdAt:-1}).limit(50)
     .then(patient=>res.json(patient))
     .catch(err=>res.status(400).json('Error:' +err));
});
router.route('/countallpatients').get((req,res)=>{
    Patient.countDocuments()
    .then(patient=>res.json(patient))
    .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const lastname=req.body.lastname;
    const firstname=req.body.firstname;
    const email=req.body.email;
    const password=req.body.password;
    const estPatient=req.body.estPatient;
    
    const newPatient=new Patient({
        lastname,
        firstname,
        email,
        password,
        estPatient,
    })
    
    newPatient.save()
    .then(patient=>res.json('Patient added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    Patient.findById(req.params.id)
    .then(patient=>res.json(patient))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    Patient.findById(req.params.id)
    .then(patient=>{
         patient.lastname=req.body.lastname
         patient.firstname=req.body.firstname
         patient.email=req.body.email
         patient.password=req.body.password

        patient.save()
            .then(()=>res.json('Patient updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    Patient.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Patient deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/login').post((req,res)=>{
   
        const email=req.body.email;
        const password=req.body.password;
        
        Patient.findOne({email:req.body.email})
        .then(patient=>{
            if(patient){
                if(password==patient.password){
                   const payload={
                       _id:patient._id,
                       firstname:patient.firstname,
                       lastname:patient.lastname,
                       email:patient.email,
                       gender:patient.gender,
                       estPatient:patient.estPatient

                   } 
                   res.send(payload)
                }
                else{
                    res.json({msg:"Invalid email or password"})
                }
            }
            else{
                res.json({msg:"User does not exist"})
            }
        })
        .catch(err=>res.send('Error:'+err))
     
       /* if(!email ||!password)
         return res.status(400).json({msg:"Not all fields are entered"})
         const patient=Patient.findOne({email:email})
         if(!patient)
          return res.status(400).json({msg:"No account with this email"})
          const passwordmatch=Patient.findOne({passwordmatch:password})
          if(!passwordmatch)
           return res.status(400).json({msg:"Invalid credentials"})*/
           
     
   
});
router.route('/findIdPatient').post((req,res)=>{

    Patient.find({lastname: req.body.lastname,firstname: req.body.firstname,email: req.body.email})
    .then(patient=>res.json(patient))
    .catch(err=>res.status(400).json('Error:' +err));
   
});
router.route('/searchfrombo').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
     Patient.find({$or: [{ lastname: regex }, { firstname: regex }, { email: regex }]} )
     .then(usr=>res.json(usr))
     .catch(err=>res.status(400).json('Error:' +err));
    
});






module.exports=router;