const router=require('express').Router()
let LeSaviezVousCategory=require('../models/leSaviezVousCategory.model') 

router.route('').get((req,res)=>{
     LeSaviezVousCategory.find()
     .then(leSaviezVousCategory=>res.json(leSaviezVousCategory))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const category=req.body.category
     const newLeSaviezVousCategory=new LeSaviezVousCategory({
       category,
   })
    
    newLeSaviezVousCategory.save()
    .then(leSaviezVousCategory=>res.json('LeSaviezVousCategory added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    LeSaviezVousCategory.findById(req.params.id)
    .then(leSaviezVousCategory=>res.json(leSaviezVousCategory))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    LeSaviezVousCategory.findById(req.params.id)
    .then(leSaviezVousCategory=>{
         leSaviezVousCategory.category=req.body.category
      

        leSaviezVousCategory.save()
            .then(()=>res.json('LeSaviezVousCategory updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    LeSaviezVousCategory.findByIdAndDelete(req.params.id)
    .then(()=>res.json('LeSaviezVousCategory deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


module.exports=router;