const router=require('express').Router()
let Appointment=require('../models/appointment.model') 
let Doctor=require('../models/doctor.model') 

router.route('').get((req,res)=>{
     Appointment.find()
     .then(appointment=>res.json(appointment))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const newAppointment=new Appointment({
        idDoctor:req.body.idDoctor,
        idPatient:req.body.idPatient,
        firstNamePatient:req.body.firstNamePatient,
        lastNamePatient: req.body.lastNamePatient,
        gender:req.body.gender,
        appointmentDate:req.body.appointmentDate,
        validation:req.body.validation,
        appointmentDateString:req.body.appointmentDateString
    })
    //pour les demandes (validation=0)
    if(req.body.validation == 0 || req.body.validation == "undefined"){ // veut dire demande de rendez-vous       
        Appointment.find({$and:[{idPatient:req.body.idPatient, idDoctor:req.body.idDoctor,validation:0}]})
        .then(appointment=>{
            if(appointment.length == 0){
                Doctor.findById(newAppointment.idDoctor).then(doctor=>{
                    console.log(newAppointment.idDoctor+" "+doctor["lastname"]+" "+doctor.firstname+" ("+doctor.speciality+")");
                    newAppointment.lastNameDoctor=doctor.lastname;
                    newAppointment.firstNameDoctor=doctor.firstname;
                    newAppointment.speciality=doctor.speciality;
                    newAppointment.save()
                    .then(appointment=>res.json(appointment._id))
                    .catch(err=>{
                        res.status(400).json('Error:' +err)
                    });
                }).catch(error=>console.log("error getDoctor info: "+error));
            }else{
                console.log("Vous avez encore un rendez-vous en attente!");
                res.status(400).json('Vous avez encore un rendez-vous en attente!');
                return
            }
        })
        .catch(err => {
            console.log("Error: findExistant"+err);
            res.status(400).json("Error:"+err);
        });
    }
    //pour l'ajout direct (validation=1)
    else{
        Doctor.findById(newAppointment.idDoctor).then(doctor=>{
            console.log(newAppointment.idDoctor+" "+doctor["lastname"]+" "+doctor.firstname+" ("+doctor.speciality+")");
            newAppointment.lastNameDoctor=doctor.lastname;
            newAppointment.firstNameDoctor=doctor.firstname;
            newAppointment.speciality=doctor.speciality;
            newAppointment.save()
            .then(appointment=>res.json(appointment._id))
            .catch(err=>{
                res.status(400).json('Error:' +err)
            });
        }).catch(error=>console.log("error getDoctor info: "+error));
    }

})

router.route('/:id').get((req,res)=>{
    Appointment.findById(req.params.id)
    .then(appointment=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/delete/:id').get((req,res)=>{
    Appointment.findByIdAndRemove(req.params.id)
    .then(res.json("effacé"))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/update/:id').post((req,res)=>{
    Appointment.findById(req.params.id)
    .then(appointment=>{
        appointment.idDoctor = req.body.idDoctor,
        appointment.idPatient = req.body.idPatient,
        appointment.firstNamePatient = req.body.firstNamePatient,
        appointment.lastNamePatient = req.body.lastNamePatient,
        appointment.gender = req.body.gender,
        appointment.appointmentDate=req.body.appointmentDate,
        appointment.validation=req.body.validation,
        appointment.appointmentDateString=req.body.appointmentDateString
        appointment.save()
            .then(()=>res.json('Appointment updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    Appointment.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Appointment deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/doctor/:id').get((req,res)=>{
    Appointment.find({idDoctor:req.params.id})
    .then(appointment=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/searchAll').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
    Appointment.find({$or: [{ firstNamePatient: regex }, { lastNamePatient: regex },{ appointmentDateString: regex }]} )
     .then(lsv=>res.json(lsv))
     .catch(err=>res.status(400).json('Error:' +err));
    
});
router.route('/searchByIdpatient/:id').get((req,res)=>{
    Appointment.find({idPatient:req.params.id})
    .then(appointment=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/attente/searchByIdpatient/:id').get((req,res)=>{
    Appointment.find({idPatient:req.params.id, validation:0})
    .then(appointment=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/valide/searchByIdpatient/:id').get((req,res)=>{
    Appointment.find({idPatient:req.params.id, validation:1})
    .then(appointment=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/rejet/searchByIdpatient/:id').get((req,res)=>{
    Appointment.find({idPatient:req.params.id, validation:-1})
    .then(appointment=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/searchByDate').post((req,res)=>{
       const regex = new RegExp(req.body.appointmentDateString, 'i');
	   Appointment.find({appointmentDateString: regex})
     .then(appointment=>res.json(appointment))
     .catch(err=>res.status(400).json('Error:' +err)); 
});

router.route('/valider').post((req,res)=>{
    const date = req.body.appointmentDate;
    const id = req.body.id;
    Appointment.findByIdAndUpdate(
        id,
        {
            appointmentDate : date,
            validation:1
        },
        {new:true}
    )
    .then((appointment)=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/rejeter').post((req,res)=>{
    const id = req.body.id;
    Appointment.findByIdAndUpdate(
        id,
        {
            validation:-1
        },
        {new:true}
    )
    .then((appointment)=>res.json(appointment))
    .catch(err=>res.status(400).json('Error:'+err))
})

module.exports=router;