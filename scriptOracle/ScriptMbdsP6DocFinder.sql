
cmd
-- Lancer sqlplus sans se logger
sqlplus /nolog


-- D�finir la variable contenant le nom de l'instance
define MYINSTANCE=orcl.unice.fr

-- D�finir la vairiable qui va contenir le nom r�seau de votre base PDB.
-- Le nom r�seau se dans le fichier tnsnames.ora
-- Il est disponible dans le dossier : %ORACLE_HOME%\network\admin
define DBALIASPDB=ORCLPDB

-- D�finir la vairiable qui va contenir le nom r�seau de votre base CDB.
-- Le nom r�seau se dans le fichier tnsnames.ora
-- Il est disponible dans le dossier : %ORACLE_HOME%\network\admin
define DBALIASCDB=ORCL

-- D�finir la variable contenant le nom de l'utilisateur que vous allez 
-- utiliser au niveau CDB. 
define MYCDBUSER=SYSTEM
 
-- D�finir la variable contenant le pass de l'utilisateur que vous allez 
-- utiliser au niveau CDB.
define MYCDBUSERPASS=Dbamanager1

-- D�finir la variable contenant le nom de l'utilisateur que vous allez 
-- cr�er au niveau PDB ou utiliser s'il existe d�j�. 
define MYPDBUSER=DOCFINDER
 
-- D�finir la variable contenant le pass de l'utilisateur que vous allez 
-- cr�er au niveau PDB ou utiliser s'il existe d�j�.
define MYPDBUSERPASS=DOCFINDER

-- D�finir la variable contenant la trace que vous souhaitez :
-- ON : si affiche r�sultat+plan
-- TRACEONLY : si affichage plan uniquement
define TRACEOPTION=TRACEONLY

-- Se connecter avec votre compte CDB dans la PDB pour cr�er l'utilisateur 
-- &MYPDBUSER
-- 
connect &MYCDBUSER@&DBALIASPDB/&MYCDBUSERPASS

-- Si erreur de mot de passe: 
--lsnrctl status

-- suprimer l'utilisateur s'il existe d�j�
drop user &MYPDBUSER cascade;

--Pour ne pas utiliser le c##
alter session set "_ORACLE_SCRIPT"=true;

-- Cr�ation de l'utilisateur. 
create user &MYPDBUSER identified by &MYPDBUSERPASS
default tablespace users
temporary tablespace temp;
-- Ma R�ponse---------------------
--Utilisateur cr��.
-- Fin R�ponse -------------------


-- affecter et enlever des droits
grant dba to &MYPDBUSER;
-- Ma R�ponse---------------------
--Autorisation de privil�ges (GRANT) accept�e.
-- Fin R�ponse -------------------

revoke unlimited tablespace from &MYPDBUSER;
-- Ma R�ponse---------------------
--Suppression de privil�ges (REVOKE) accept�e.
-- Fin R�ponse -------------------


alter user &MYPDBUSER quota unlimited on users;
-- Ma R�ponse---------------------
--Utilisateur modifi�.
-- Fin R�ponse -------------------

-- Connexion avec le nouvel utilisateur ou un utilisateur existant au niveau
-- PDB. 
connect &MYPDBUSER@&DBALIASPDB/&MYPDBUSERPASS
-- Ma R�ponse---------------------
--Connect�.
-- Fin R�ponse ------------------- 
-- CREATE table 
--test table
-- create table question
-- (
--    id             int ,
--    createdAt            Date,
--    updateAt          Date,
--    title          varchar(50),
--    description          varchar(300),
--    primary key (id)
-- );
-- create table answer
-- (
--    id             int ,
--    createdAt            Date,
--    updateAt          Date,
--    text          varchar(300),
--    primary key (id)
-- );
-- end test table
create table question
(
   id             int ,
   primary key (id)
);
create table answer
(
   id             int ,
   primary key (id)
);

create table appointments
(	
	id INTEGER GENERATED  AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
	primary key (id)
);
create table diseases
(	
	id INTEGER GENERATED  AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
	primary key (id)
);
create table doctors
(	
	id INTEGER GENERATED  AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
	primary key (id)
);
create table doctorspecialities
(	
	id INTEGER GENERATED  AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
	primary key (id)
);
create table patients
(	
	id INTEGER GENERATED  AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
	primary key (id)
);
create table symptomperdiseases
(	
	id INTEGER GENERATED  AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
	primary key (id)
);
create table symptoms
(	
	id INTEGER GENERATED  AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
	primary key (id)
);



--POUR LES DELETE
drop table appointments;
drop table symptomperdiseases;
drop table symptoms;
drop table doctors;
drop table patients;
drop table diseases;
drop table doctorspecialities;