import React, { Component } from 'react'
import "./Clients.css";
import Footer from "./../Template/Footer";
//Table
import {  Switch, Route } from 'react-router-dom'
//import { makeStyles } from '@material-ui/core/styles';
//grid for the search
import ClientsList from '../ClientsList/ClientsList';
import ClientDetails from '../ClientDetails/ClientDetails';
import ClientsStatistics from '../ClientsStatistics/ClientsStatistics';



export default class Clients extends Component {
    constructor(props){  
        super(props);  
        this.state = {  
           page:0,
           rowsPerPage:10,
         }  
        
         
    }  

    

    render() {
        //for the url
        //const {match} = this.props;
       
        return (
            <div className="page-wrapper">  
                
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Clients</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Clients</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                
                <Switch>
                    <Route path='/clientslist' component={ClientsList} exact />
                    <Route path='/clientdetails/:id' component={ClientDetails} exact />
                    <Route path='/clientsstatistics' component={ClientsStatistics} exact /> 
                </Switch>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}

                {/* <Scripts/>  */}
            </div>
        )
    }
}
