import React, { Component } from 'react'
import "./LeSaviezVousEdit.css";
import {Link} from "react-router-dom";
import axios from 'axios';
import * as Config from './../Config/Config';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  makeStyles } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Select from 'react-select';
import { MDBInput } from 'mdbreact';
import Typography from '@material-ui/core/Typography';





// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  


export default class NewLeSaviezVous extends Component {
    constructor(props){  
        super(props); 
        //upload photo 
        this.state = { 
          pictures: [],
          categories:[],
          optionsCategory:[],
          errorMessage:'',
          title:'',
          idCategory:'',
          description:'',
          image:'',
          urlImage:'',
          beginningURL: Config.beginningURL
         };
        
         this.onChangeTitle = this.onChangeTitle.bind(this);  
         this.onChangeDescription = this.onChangeDescription.bind(this); 
         this.onChangeImage = this.onChangeImage.bind(this); 
         this.imageUploaded=this.imageUploaded.bind(this);
         this.showErrorMessage=this.showErrorMessage.bind(this);
         this.deleteLeSaviezVous=this.deleteLeSaviezVous.bind(this);
        
         this.onSubmit = this.onSubmit.bind(this);    
    } 

     //select (combobox) 
     state = {
      selectedOption: null,
    };
    handleChange = selectedOption => {
      this.setState({ selectedOption });
      //console.log(`Option selected:`, selectedOption);
      this.setState({idCategory:selectedOption.value});
    };
    
    componentDidMount(){
      // categories of LeSaviezVous
      axios.get(this.state.beginningURL+'leSaviezVousCategory/')
      .then(response=>{
        this.setState({categories:response.data});
        let i=0;
        let categories=response.data;
        let choicesCategory=[];
        for(i=0;i<categories.length;i++){
          choicesCategory.push({value:categories[i]._id, label:categories[i].category});
        }
        this.setState({optionsCategory:choicesCategory});

         //Get the LeSaviezVous to update 
        axios.get(this.state.beginningURL+'leSaviezVous/'+this.props.match.params.id)
        .then(response=>{
            this.setState({
                title:response.data.title,
                idCategory:response.data.idCategory,
                urlImage:response.data.image,
                description:response.data.description
            });

            
            let j=0;
            let allCategories=this.state.optionsCategory;
            for(j=0;j<allCategories.length;j++){
                if(allCategories[j].value===this.state.idCategory){
                  this.setState({ selectedOption:allCategories[j] });
                  break;
                }
            }
        })
        .catch(function(error){
            console.log(error);
        });

      })
      .catch(error=>{
        console.log(error)
      })
     

    }
   

    
    

      
    
      //form
    onChangeTitle(e){
        this.setState({
            title:e.target.value
        });
    }
    onChangeDescription(e){
      this.setState({
          description:e.target.value
      });
    }
    onChangeImage(e){
      this.setState({
          image:e.target.files[0]
      });
      console.log(e.target.files[0]);

      //postImage
      //upload image to the cloud
      const data=new FormData();
      data.append("file",e.target.files[0]);
      data.append("upload_preset","docfinder");
      data.append("cloud_name","hadoyacloud");
      fetch("https://api.cloudinary.com/v1_1/hadoyacloud/image/upload",{
        method:"post",
        body:data
      })
      .then(res=>res.json())
      .then(data=>{
        this.setState({urlImage:data.url});
        //console.log(data);
      })
      .catch(err=>{
        console.log(err);
      })
    }
    onSubmit(e){
      e.preventDefault();
   
      //verify if all the required field are not empty
      if((this.state.title==='' )||(this.state.idCategory==='')||(this.state.description==='')||(this.state.urlImage==='')){
          this.setState({errorMessage:'Tous les champs doivent être remplis, y compris l\'image.'});
      }
      else{
   
              const leSaviezVous ={
                  title:this.state.title,
                  idCategory:this.state.idCategory,
                  image:this.state.urlImage,
                  description:this.state.description
              
              };
              console.log(leSaviezVous);
      
              axios.post(this.state.beginningURL+'leSaviezVous/update/'+this.props.match.params.id,leSaviezVous)
               .then(res => console.log(res.data));
      
              window.location="/lesaviezvouslist";
         

        }   
    }
    deleteLeSaviezVous(){
      axios.delete(this.state.beginningURL+'leSaviezVous/'+this.props.match.params.id)
      .then(res => {
        console.log(res.data);
        window.location="/lesaviezvouslist";
      });
    
      
  }

    showErrorMessage(){
      if(this.state.errorMessage!==''){
          return(
              <Typography component="h5"  style={{marginTop:'20px',color:'red',backgroundColor:'yellow'}}>
                          {this.state.errorMessage} 
                  </Typography>
          );
      }
      else{
          return
      }
      
  }


    imageUploaded(){
      if(this.state.urlImage!==''){
        return(<img src={this.state.urlImage} alt="uploaded" className="imageUploaded"/>)
      }
      else{
        return
      }
    }
    render() {
        //for the url
        //const {match} = this.props;
     
        //form 
        const classesForm = useStylesForm;
        //select (combobox)
        const { selectedOption } = this.state; 

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} >

                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5 >
                                        Modification Le Saviez Vous
                                    </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                              <Button
                                onClick={()=> {this.deleteLeSaviezVous()}}
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={{marginRight:'5px',marginLeft:'5px', backgroundColor:'red',color:'white'}}                                  
                              >
                                <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>
                                  &nbsp; Supprimer l'article
                              </Button>
                        
                    <br/>
                    {this.showErrorMessage()}
                    <div className={classesForm.paper} >
                       

                        
                        <form className={classesForm.form} onSubmit={this.onSubmit} noValidate> 
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="titre"
                            label="Titre"
                            name="titre"
                            autoComplete="titre"
                            autoFocus
                            value={this.state.title} onChange={this.onChangeTitle}
                        />
                        <Select
                            placeholder="Catégorie"
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={this.state.optionsCategory}
                        />
                        <label style={{marginTop:'10px'}}>Description</label>
                        <MDBInput type="textarea" placeholder="Description" rows="5" value={this.state.description} onChange={this.onChangeDescription} />

                        <label >Importer une image</label>
                      
                        <br/>
                        <input type="file" onChange={this.onChangeImage} />
                        <br/>
                        {this.imageUploaded()}
                        <br/>

                        <Grid container >
                            <Grid item xs={6} >
                              <Link to="/lesaviezvouslist"> 
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={{marginRight:'8px'}}                                  
                              >
                                  Annuler
                              </Button>
                              </Link>
                            </Grid>
                            
                         
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={{marginLeft:'8px'}} 
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                        </form>
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
