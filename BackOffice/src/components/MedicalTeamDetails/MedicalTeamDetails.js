import React, { Component } from 'react'
import "./MedicalTeamDetails.css";
import axios from 'axios';
import * as Config from './../Config/Config';

//Table
import { Link } from 'react-router-dom';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  createMuiTheme,  makeStyles, ThemeProvider  } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { green } from '@material-ui/core/colors';


const IsValid = props =>(
  <Grid container >
    <Grid item xs={5} ></Grid>
    <Grid item xs={2}></Grid>
    <Grid item xs={5}>
    <ThemeProvider theme={theme}>
      <Link to='/medicalteamlist'>
        <Button variant="contained" color="primary" > Revenir liste </Button>
      </Link>
    </ThemeProvider>
    </Grid>
  </Grid>
);

const IsNotValid = props =>(
  <Grid container >
  <Grid item xs={2} >
  </Grid>
  <Grid item xs={5}>
  <Link to='/medicalteamvalidation'>
      <Button variant="contained"  >
        Revenir liste
      </Button>
    </Link>
  </Grid>
  <Grid item xs={5}>
  <ThemeProvider theme={theme}>
      <Button onClick={()=> {props.validateDoctor(props.beginningURL, props.idDoctor)}} variant="contained" color="primary" >
       <img style={{width:'25px'}}  alt="Valider" src="/assets/images/logos/logovalidation.png"/>&nbsp;Valider 
      </Button>
  </ThemeProvider>
  </Grid>
</Grid>
);


// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  //theme button 
  const theme = createMuiTheme({
    palette: {
      primary: green,
    },
  });
 


export default class MedicalTeamDetails extends Component {
    constructor(props){  
        super(props); 
        //upload photo 
        this.state = { 
            idDoctor:'',
            lastname:'',
            firstname:'',
            telephone:'',
            idSpeciality:'',
            specialityName:'',
            specialityDescription:'',
            address:'',
            city:'',
            town:'',
            tarif:'',
            description:'',
            etatValidation:5,
            beginningURL: Config.beginningURL
          };
          //local: http://localhost:5000/
          // en ligne: 

          this.validateDoctor=this.validateDoctor.bind(this);
          this.affichageDependingOnValidation=this.affichageDependingOnValidation.bind(this);
      
       
    }
    componentDidMount(){
      axios.get(this.state.beginningURL+'doctor/'+this.props.match.params.id)
      .then(response=>{
          this.setState({
              idDoctor:response.data._id,
              lastname:response.data.lastname,
              firstname:response.data.firstname,
              telephone:response.data.telephone,
              idSpeciality:response.data.idDoctorSpeciality,
              address:response.data.address,
              city:response.data.city,
              town:response.data.town,
              tarif:response.data.tarif,
              description:response.data.description,
              etatValidation:response.data.etatValidation
          })

          axios.get(this.state.beginningURL+'doctorSpeciality/'+this.state.idSpeciality)
          .then(response2=>{
              this.setState({
                specialityName:response2.data.name,
                specialityDescription:response2.data.description
              })
              
            
          })
          .catch(function(error2){
              console.log(error2);
          });
      })
      .catch(function(error){
          console.log(error);
      });
      
    }

    validateDoctor(beginningURL,idDoctor){
      axios.post(beginningURL+'doctor/validate/'+idDoctor)
      .then(res => {console.log(res.data)
        this.props.history.push('/medicalteamlist'); 
      });
        
    }

    affichageDependingOnValidation() {
      if (this.state.etatValidation===1) {
        return <IsValid/>
      
      }
      if (this.state.etatValidation===0) {
        return <IsNotValid  beginningURL={this.state.beginningURL} idDoctor={this.state.idDoctor} validateDoctor={this.validateDoctor} />;

      }
      
    }
   
   

    render() {
        //for the url
        //const {match} = this.props;
     
        //form 
        const classesForm = useStylesForm;
        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} >

                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5 >
                                        Corps médical : {this.state.lastname+' '+this.state.firstname}
                                    </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    <div className={classesForm.paper} >
                       
                        <div className="contentLeSaviezVousDetails" >
                            <img className="imagePerson" src="/assets/images/logos/logocorpsmedical.jpg" alt="Corps medical"/>
                            
                            
                            <div className=" row">
                                <h4 className="labelContent">Nom: </h4>
                            </div>  
                            <div className=" row">
                                 {this.state.lastname}<br/>
                            </div> 
                            <div className=" row">
                                <h4 className="labelContent">Prénom(s): </h4> 
                            </div> 
                            <div className=" row">
                                {this.state.firstname}<br/>
                            </div> 
                            <div className=" row">
                                <h4 className="labelContent">Téléphone: </h4>
                            </div>
                            <div className=" row">
                                 {this.state.telephone}<br/>
                            </div>
                            <div className=" row">
                                <h4 className="labelContent">Spécialisation: </h4> 
                               
                            </div>
                            <div className="descriptionContent row">
                                  <strong>{this.state.specialityName}:</strong><br/>
                                  {this.state.specialityDescription}<br/>
                            </div>
                            <div className=" row">
                                <h4 className="labelContent">Adresse: </h4> 
                            </div>
                            <div className=" row">
                                {this.state.address+', '+this.state.city+', '+this.state.town}<br/>
                            </div>
                            <div className=" row">
                                <h4 className="labelContent">Tarif (MGA): </h4> 
                            </div>
                            <div className=" row">
                                 {this.state.tarif} <br/>
                            </div>
                            <div className=" row" >
                                <h4 className="labelContent">Description: </h4> 
                            </div>
                            <div className=" row">
                                {this.state.description}<br/>
                            </div>
                            {this.affichageDependingOnValidation()}
                            
                        </div>
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
