import React, { Component } from 'react'
import "./DiagnosticDetails.css";
import axios from 'axios';
import {Link} from "react-router-dom";
import * as Config from './../Config/Config';



//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  createMuiTheme,  makeStyles, ThemeProvider  } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { green } from '@material-ui/core/colors';




// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  //theme button 
  const theme = createMuiTheme({
    palette: {
      primary: green,
    },
  });
  //options for select



export default class DiagnosticDetails extends Component {
    constructor(props){  
        super(props); 
        //upload photo 
        this.state = {
          idDisease:'',
          name:'',
          description:'',
          idDoctorSpeciality:'',
          nameSpeciality:'',
          descriptionSpeciality:'',
          idSymptoms:[],
          symptoms:[],
          beginningURL: Config.beginningURL
         };
     
    }  
    componentDidMount(){
      axios.get(this.state.beginningURL+'disease/'+this.props.match.params.id)
      .then(response=>{
          this.setState({
            idDisease:response.data._id,
            name:response.data.name,
            description:response.data.description,
            idDoctorSpeciality:response.data.idDoctorSpeciality,
          })
          //doctor speciality
          axios.get(this.state.beginningURL+'doctorSpeciality/'+this.state.idDoctorSpeciality)
          .then(response2=>{
              this.setState({
                nameSpeciality:response2.data.name,
                descriptionSpeciality:response2.data.description
              }) 
              //id symptoms of the disease (from table symptoms per disease)
              axios.get(this.state.beginningURL+'symptomPerDisease/findbyiddisease/'+this.state.idDisease)
              .then(response3=>{
                  this.setState({
                    idSymptoms:response3.data                 
                  }) ;
                  //the symptoms from the table symptoms 
                  this.state.idSymptoms.map((idSymptom)=>{
                    axios.get(this.state.beginningURL+'symptom/'+idSymptom.idSymptom)
                    .then(response4=>{
                        let symptoms = this.state.symptoms;
                        symptoms.push(response4.data);
                        this.setState({
                          symptoms: symptoms 
                                       
                        }) ;                 
                    })
                    .catch(function(error4){
                        console.log(error4);
                    });
                  });
              })
              .catch(function(error3){
                  console.log(error3);
              });
          })
          .catch(function(error2){
              console.log(error2);
          });
      })
      .catch(function(error){
          console.log(error);
      });
      
    }
   


    render() {
        //for the url
        //const {match} = this.props;
     
        //form 
        const classesForm = useStylesForm;
        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} >

                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5 >
                                        Maladie: {this.state.name}
                                     </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    <div className={classesForm.paper} >
                       
                        <div className="contentDiagnisticDetails" >
                            
                            <div className="descriptionContent row">
                              <div className="categorieContent row"><h4>Description:</h4></div>
                                <div className="descriptionContent1 row">
                                {this.state.description}
                              </div>
                            
                            </div>
                            <div className="descriptionContent row">
                                <h4>Corps m&eacute;dical correspondant: </h4><br/>
                                <div>
                                <strong>{this.state.nameSpeciality}</strong><br/><br/>
                                {this.state.descriptionSpeciality}
                                </div>
                            
                            </div>
                            <div className="descriptionContent row">
                                <h4>Sympt&ocirc;mes: </h4>
                                {this.state.symptoms.map(symptom=>
                                 <div key={symptom._id}> 
                                   <p>
                                     <strong>-  {symptom.name}: </strong><br/> 
                                     {symptom.description} <br/><br/> 
                                    </p>
                                  </div>
                                )}
                            
                            </div>
                           
                            <Grid container >
                                <Grid item xs={2} >
                                
                                </Grid>
                                <Grid item xs={5}>
                                  <ThemeProvider theme={theme}>
                                  <Link to='/diagnosticlist'>
                                    <Button variant="contained"  > Revenir liste </Button>
                                  </Link>
                                </ThemeProvider>
                                </Grid>
                                <Grid item xs={5}>
                                <ThemeProvider theme={theme}>
                                    <Link to={'/diagnosticedit/'+this.props.match.params.id}>
                                    <Button variant="contained" color="primary" className={classesForm.submit}>
                                    Modifier/Supprimer
                                    </Button>
                                    </Link>
                                </ThemeProvider>
                                </Grid>
                            </Grid>
                        </div>
                        

                        
                        
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
