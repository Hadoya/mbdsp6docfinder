import React, { Component } from 'react'
import "./Dashboard.css";
import '@devexpress/dx-react-chart-bootstrap4/dist/dx-react-chart-bootstrap4.css';
import Footer from "./../Template/Footer";
//Pie
import Paper from '@material-ui/core/Paper';
import {
  Chart,
  PieSeries,
  Title,
} from '@devexpress/dx-react-chart-material-ui';

import { Animation } from '@devexpress/dx-react-chart';
const data = [
    { country: 'Russia', area: 12 },
    { country: 'Canada', area: 7 },
    { country: 'USA', area: 7 },
    { country: 'China', area: 7 },
    { country: 'Brazil', area: 6 },
    { country: 'Australia', area: 5 },
    { country: 'India', area: 2 },
    { country: 'Others', area: 55 },
  ];
//End pie
//Pie 2
var CanvasJSReact = require('./../../srcassets/canvasjs-2.3.2/canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
//ENd pie 2

export default class Dashboard extends Component {
    //For the pie that represents the statistics of medical team 
    constructor(props) {
        super(props);
    
        this.state = {
          data,
        };
      }
    //end pie

    render() {
        //pie
        const { data: chartData } = this.state;
        //end pie
        //pie 2
        const options = {
			theme: "dark2",
			animationEnabled: true,
			exportFileName: "New Year Resolutions",
			exportEnabled: true,
			title:{
				text: "Top Categories of New Year's Resolution"
			},
			data: [{
				type: "pie",
				showInLegend: true,
				legendText: "{label}",
				toolTipContent: "{label}: <strong>{y}%</strong>",
				indexLabel: "{y}%",
				indexLabelPlacement: "inside",
				dataPoints: [
					{ y: 32, label: "Health" },
					{ y: 22, label: "Finance" },
					{ y: 15, label: "Education" },
					{ y: 19, label: "Career" },
					{ y: 5, label: "Family" },
					{ y: 7, label: "Real Estate" }
				]
			}]
		}
        //end pie 2

        return (
            <div className="page-wrapper">  
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Tableau de bord</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Tableau de bord</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid">
                    {/*-- The content of the page-->*/}
                    {/*-- ============================================================== -->*/}
                    {/*-- Sales Cards  -->*/}
                    {/*-- ============================================================== -->*/}
                    <div className="row">
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-cyan text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">Clients inscrits: <br/>125 348</h6>

                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-success text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">M&eacute;decins inscrits: <br/>12 563</h6>
                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-6 col-lg-4 col-xlg-3">
                            <div className="card card-hover">
                                <div className="box bg-warning text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-chart-areaspline"></i></h1>
                                    <h6 className="text-white">Attente de validation:<br/>1200</h6>
                                </div>
                            </div>
                        </div>
                        {/*-- Column -->*/}
                        <div className="col-md-12 col-lg-12 col-xlg-3">
                            {/*<div className="card card-hover">
                                <div className="box bg-danger text-center">
                                    <h1 className="font-light text-white"><i className="mdi mdi-border-outside"></i></h1>
                                    <h6 className="text-white">Corps m&eacute;dicaux par sp&eacute;cialit&eacute;s</h6>
                                </div>
                            </div>*/}
                            {/* pie */}
                            <Paper>
                                <Chart
                                data={chartData}
                                >
                                <PieSeries
                                    valueField="area"
                                    argumentField="country"
                                    labels={{visible: true, content: data}}
                                    visible="true"
                                />
                                <Title
                                    text="Corps m&eacute;dicaux par sp&eacute;cialit&eacute;s"
                                />
                                <Animation />
                                </Chart>
                            </Paper>
                            {/* end pie */}
                        </div>
                        
                    </div>
                    {/*-- ============================================================== -->*/}
                    {/*-- Sales chart -->*/}
                    {/*-- ============================================================== -->*/}
                    {/* pie 2 */}
                    <div>
                        <CanvasJSChart options = {options}
                             onRef={ref => this.chart = ref} 
                        />
                        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
                    </div>
                    {/* end pie 2 */}

        
                    {/*-- End content of the page-->*/}
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}
            </div>
        )
    }
}
