import React, { Component }  from 'react';
//import { Link, Switch, Route } from 'react-router-dom'

export default class Header extends Component {
    constructor(props){
        super(props);  
        //const userAdmin=JSON.parse(localStorage.getItem("userAdmin"));
        this.state = {  
           lastname:'',
           firstname:'',
         
        }  
        this.logout=this.logout.bind(this);
       
    }
    componentDidMount(){
        if(localStorage.getItem("userAdmin")!=null){
            const userAdmin=JSON.parse(localStorage.getItem("userAdmin"));
            this.setState({lastname:userAdmin.lastname,firstname:userAdmin.firstname});
        }
    }
    logout(){     
        localStorage.clear();
        window.location="/";       
    }
    render(){
        return (

           
            <header className="topbar" data-navbarbg="skin5">
                 {/* ============================================================== */}
                {/* Topbar header - style you can find in pages.scss */}
                {/* ============================================================== */}
                <nav className="navbar top-navbar navbar-expand-md navbar-dark" >
                    <div className="navbar-header" data-logobg="skin5">
                        {/* This is for the sidebar toggle which is visible on mobile only */}
                        <a className="nav-toggler waves-effect waves-light d-block d-md-none" href="#"><i className="ti-menu ti-close"></i></a>
                        {/* ============================================================== */}
                        {/* Logo */}
                        {/* ============================================================== */}
                        <a className="navbar-brand" href="#" style={{backgroundColor: 'rgb(30, 43, 22)' }} >
                            {/* Logo icon */}
                            <b className="logo-icon p-l-10">
                                <img src="assets/images/logos/LogoDocFinder6.png" width="50" alt="homepage"  className="light-logo" />                      
                            </b> 
                            {/*End Logo icon */}
                             {/* Logo text */}
                            <span className="logo-text" style={{flexWrap: 'wrap'}} >
                                 {/* dark Logo text */}
                                 <img src="assets/images/logos/LogoDocFinder5.png" style={{borderRadius: '10px'}} width="160" alt="homepage" className="light-logo" />
                            </span>
                        </a>
                        {/* ============================================================== */}
                        {/* End Logo */}
                        {/* ============================================================== */}
                        {/* ============================================================== */}
                        {/* Toggle which is visible on mobile only */}
                        {/* ============================================================== */}
                        <a className="topbartoggler d-block d-md-none waves-effect waves-light" href="#" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i className="ti-more"></i></a>
                    </div>
                    {/* ============================================================== */}
                    {/* End Logo */}
                    {/* ============================================================== */}
                    <div className="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5" >
                        {/* ============================================================== */}
                        {/* toggle and nav items */}
                        {/* ============================================================== */}
                        <ul className="navbar-nav float-left mr-auto" >
                            <li className="nav-item d-none d-md-block"><a className="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i className="mdi mdi-menu font-24"></i></a></li>
                            
                            {/* ============================================================== */}
                            {/* Search */}
                            {/* ============================================================== */}
                            {/* <li className="nav-item search-box"> <a className="nav-link waves-effect waves-dark" href="#"><i className="ti-search"></i></a> */}
                                {/* <form className="app-search position-absolute"> */}
                                   
                                {/* </form> */}
                            {/* </li> */}
                        </ul>
                        {/* ============================================================== */}
                        {/* Right side toggle and nav items */}
                        {/* ============================================================== */}
                        <ul className="navbar-nav float-right">
                            
                            {/* ============================================================== */}
                            {/* End Comment */}
                            {/* ============================================================== */}
                            {/* ============================================================== */}
                            {/* Messages */}
                            {/* ============================================================== */}
                            
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                    {/* <i className="font-24 mdi mdi-comment-processing"></i> */}
                                   &nbsp; {this.state.lastname+' '+this.state.firstname} &nbsp; <img src="assets/images/logos/logoPerson.png" alt="user" className="rounded-circle" width="31"/>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="2">
                                    <ul className="list-style-none">
                                        <li>
                                             <div className="dropdown-divider"></div>
                                             <div className="dropdown-item" onClick={this.logout}><i className="fa fa-power-off m-r-5 m-l-5"></i> Se d&eacute;connecter</div>
                                   
                                        </li>
                                        
                                    </ul>
                                </div>
                            </li>
                            {/* ============================================================== */}
                            {/* End Messages */}
                            {/* ============================================================== */}
                            
                        </ul>
                    </div>
                </nav>
                 {/* ============================================================== */}
            {/* End Topbar header */}
            {/* ============================================================== */} 
            </header>
           
        );
    }
}


