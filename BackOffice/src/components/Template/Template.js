import React, { Component } from 'react'
import {  Switch, Route,Redirect } from 'react-router-dom'

import Header from "./Header";
import LeftSideBar from "./LeftSideBar";
import Dashboard from "./../Dashboard/Dashboard";
import LeSaviezVous from "./../LeSaviezVous/LeSaviezVous";
import Diagnostic from '../Diagnostic/Diagnostic';
import Clients from '../Clients/Clients';
import MedicalTeam from '../MedicalTeam/MedicalTeam';
import UserAdmin from '../UserAdmin/UserAdmin';
import Symptom from '../Symptom/Symptom';



export default class Template extends Component {
    constructor(props){
        super(props);
        
        const isLoggedIn= localStorage.getItem("loggedIn");

        let loggedIn = true;
        if(isLoggedIn==null){
            loggedIn=false ;
        }
        this.state={
            loggedIn
        }
    }
  
    render() {
        //const {match} = this.props;
        if(this.state.loggedIn === false){
            
            return  <Redirect to="/"/>
        }
        return (
            <div id="main-wrapper">
            {/*-- ============================================================== -->*/}
            {/*-- Topbar header - style you can find in pages.scss -->*/}
            {/*-- ============================================================== -->*/}
            
            <Header/> 

            {/*-- ============================================================== -->*/}
            {/*-- End Topbar header -->*/}
            {/*-- ============================================================== -->*/}
            
            {/*-- ============================================================== -->*/}
            {/*-- Left Sidebar - style you can find in sidebar.scss  -->*/}
            {/*-- ============================================================== -->*/}
            
            <LeftSideBar/>
            
            {/*-- ============================================================== -->*/}
            {/*-- End Left Sidebar - style you can find in sidebar.scss  -->*/}
            {/*-- ============================================================== -->*/}
            {/*-- ============================================================== -->*/}
            {/*-- Page wrapper  -->*/}
            {/*-- ============================================================== -->*/}
            
            <Switch>
                    {/* <Route path='/' component={Dashboard} exact/> */}
                    <Route path='/dashboard' component={Dashboard} exact/>
                    <Route path='/lesaviezvouslist' component={LeSaviezVous} exact/>
                    {/* inside le saviez-vous */}
                    <Route path='/newlesaviezvous' component={LeSaviezVous} exact />
                    <Route path='/lesaviezvousdetails/:id' component={LeSaviezVous}  exact/>
                    <Route path='/lesaviezvousedit/:id' component={LeSaviezVous} exact />
                    {/* end inside le saviez-vous */}
                    {/* <Route path='/diagnostic' component={Diagnostic} /> */}
                    {/* inside diagnostic */}
                    <Route path='/diagnosticlist' component={Diagnostic} exact />
                    <Route path='/diagnosticdetails/:id' component={Diagnostic}  exact/>
                    <Route path='/diagnosticedit/:id' component={Diagnostic} exact />
                    <Route path='/newdiagnostic' component={Diagnostic} exact />
                    {/* end inside diagnostic */}
                    {/* Symptoms */}
                    <Route path='/symptomlist' component={Symptom}  exact/>
                    <Route path='/editsymptom/:id' component={Symptom} exact />
                    <Route path='/newsymptom' component={Symptom} exact />
                    {/* End Symptoms */}
                    <Route path='/clients' component={Clients} />
                    {/* inside clients  */}
                    <Route path='/clientslist' component={Clients} exact />
                    <Route path='/clientdetails/:id' component={Clients} exact />
                    <Route path='/clientsstatistics' component={Clients} exact /> 
                    {/* end inside clients */}
                    {/* <Route path='/clients/clientsstatistics' component={Clients} /> */}
                    {/* <Route path='/medicalteam' component={MedicalTeam} /> */}
                    {/* <Route path='/medicalteamlist' component={MedicalTeamList} /> */}
                    {/* <Route path='/medicalteamstatistics' component={MedicalTeamStatistics} /> */}
                    {/* inside medicalteam */}
                    <Route path='/medicalteamvalidation' component={MedicalTeam} exact />
                    <Route path='/medicalteamlist' component={MedicalTeam} exact />
                    <Route path='/medicalteamdetails/:id' component={MedicalTeam}  exact/>
                    <Route path='/medicalteamstatistics' component={MedicalTeam} exact />
                    {/* end inside medicalteam */}
                    {/* UserAdmin */}
                    <Route path="/useradminlist" component={UserAdmin} exact/>
                    <Route path="/useradminedit/:id" component={UserAdmin} exact/>
                    <Route path="/useradmincreate" component={UserAdmin} exact/> 
                    {/* End UserAdmin */}

            </Switch>
            
            {/*-- ============================================================== -->*/}
            {/*-- End Page wrapper  -->*/}
            {/* -- ============================================================== -->*/} 
            
        </div>
        
        )
    }
}
