//"Footer is inside page wrapper"
import React, { Component } from 'react'


export default class Footer extends Component {
    render() {
        return(
 
            <footer className="footer text-center">
                © Conçu par  Domoina, Hajavola, Yael <br/>
                Projet TPT - Septembre 2020.
            </footer>

        );
    }
}

