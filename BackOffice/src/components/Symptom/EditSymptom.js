import React, { Component } from 'react';
import "./NewSymptom.css";
import axios from 'axios';
import * as Config from './../Config/Config';
import {Link} from 'react-router-dom';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  makeStyles } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';

import Container from '@material-ui/core/Container';
import { MDBInput } from 'mdbreact';

// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  
// end form


export default class NewSymptom extends Component {
    constructor(props){
        super(props);
        this.state={
            name:"",
            description:'',
            beginningURL: Config.beginningURL

        }
        //local: http://localhost:5000/
        // en ligne: 

        this.onChangeName=this.onChangeName.bind(this);
        this.onChangeDescription=this.onChangeDescription.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
    }

    componentDidMount(){
        axios.get(this.state.beginningURL+'symptom/'+this.props.match.params.id)
        .then(response=>{
            this.setState({
                name:response.data.name,
                description:response.data.description,
                
            })
        })
        .catch(function(error){
            console.log(error);
        });

    }

    onChangeName(e){
        this.setState({
            name:e.target.value
        });
    }
    onChangeDescription(e){
        this.setState({
            description:e.target.value
        });
    }

    onSubmit(e){
        e.preventDefault();
        //here
        const symptom ={
            name:this.state.name,
            description:this.state.description
          
        };
        console.log(symptom);

        axios.post(this.state.beginningURL+'symptom/update/'+this.props.match.params.id,symptom)
        .then(res => console.log(res.data));
        window.location="/symptomlist";
    }

    render() {
        //form 
        const classesForm = useStylesForm;
        return (
            <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} style={{textAlign:'left'}}>
                                        <Link to='/symptomlist' >
                                            <Button variant="contained" color="secondary">
                                                Voir liste
                                            </Button>
                                        </Link>
                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5  style={{marginTop:'10px'}}>
                                        Modification sympt&ocirc;me
                                    </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
                 {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    <div className={classesForm.paper} >
                       
                        
                    <form className={classesForm.form} onSubmit={this.onSubmit} noValidate>                       
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="name"
                            label="Symptôme"
                            name="name"
                            autoComplete="name"
                            autoFocus 
                            type="text" value={this.state.name} onChange={this.onChangeName}/>

                  
                        <label style={{marginTop:'10px'}}>Description</label>
                        <MDBInput type="textarea" placeholder="Description" rows="5" value={this.state.description}  onChange={this.onChangeDescription} />
                        
                        <Grid container style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                                <Link to="/symptomlist">
                                    <Button
                                    type="button"
                                    fullWidth
                                    variant="contained" 
                                    style={{marginRight:'8px'}}                                 
                                    > 
                                        Annuler
                                    </Button>
                                </Link>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={{marginLeft:'8px'}} 
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                        </form>
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
            
                
            </div>
        )
    }
}
