import React, { Component } from 'react';
import './UserAdmin.css'
import UserAdminList from './UserAdminList';
import UserAdminEdit from './UserAdminEdit';
import UserAdminCreate from './UserAdminCreate';
import Footer from "./../Template/Footer";
import {  Switch, Route } from 'react-router-dom'


export default class UserAdmin extends Component {
    constructor(props){  
        super(props);  
        this.state = {
         }  

    }  

    render() {
        return (
            <div className="page-wrapper">  
                
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Administrateurs</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Administrateurs</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                
                <Switch>
                    <Route path="/useradminlist" component={UserAdminList} exact/>
                    <Route path="/useradminedit/:id" component={UserAdminEdit} exact/>
                    <Route path="/useradmincreate" component={UserAdminCreate} exact/>                

                </Switch>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}

                {/* <Scripts/>  */}
            </div>
            
        )
    }
}
