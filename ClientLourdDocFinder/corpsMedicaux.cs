﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientLourdDocFinder
{
    public partial class corpsMedicaux : Form
    {
        List<Medecin> ldv;
        List<Medecin> ldnv;
        public corpsMedicaux()
        {
            InitializeComponent();
            ldv = new MedecinDAO().getListDoctorValide();
             ldnv = new MedecinDAO().getListDoctorNonValide();

            datagriddocv.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            datagriddocv.ColumnHeadersDefaultCellStyle.Font = new Font(datagriddocv.Font, FontStyle.Bold);
            datagriddocv.ColumnHeadersDefaultCellStyle.BackColor = Color.BlueViolet;
            datagriddocv.DefaultCellStyle.BackColor = Color.Bisque;
            datagriddocv.Columns.Add("DocteurId", "DocteurId");
            datagriddocv.Columns.Add("Nom", "Nom et prénom(s)");
            datagriddocv.Columns.Add("Email", "Email");
            datagriddocv.Columns["DocteurId"].Visible = false;

           

            datagridviewdocnv.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            datagridviewdocnv.ColumnHeadersDefaultCellStyle.Font = new Font(datagridviewdocnv.Font, FontStyle.Bold);
            datagridviewdocnv.ColumnHeadersDefaultCellStyle.BackColor = Color.BlueViolet;
            datagridviewdocnv.DefaultCellStyle.BackColor = Color.Bisque;
            datagridviewdocnv.Columns.Add("DocteurId", "DocteurId");
            datagridviewdocnv.Columns.Add("Nom", "Nom et prénom(s)");
            datagridviewdocnv.Columns.Add("Email", "Email");
            datagridviewdocnv.Columns["DocteurId"].Visible = false;

          

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void datagridpatient_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string docId = datagriddocv.Rows[e.RowIndex].Cells[0].Value.ToString();
     

            if (docId != null)
            {
             
                Medecin patient = new MedecinDAO().findById(docId);
                nom.Text = patient.firstname;
                prenom.Text = patient.lastname;
                email.Text = patient.email;
                telephone.Text = patient.telephone;
                region.Text = patient.town;
                city.Text = patient.city;
                description.Text = patient.description;
                adresse.Text = patient.address;
                estValide.Text = patient.etatValidation;
                if (patient.etatValidation == "0") 
                {
                    estValide.Text = "En attente de validiation";
                    estValide.ForeColor = Color.Red;
                }
                else{
                    estValide.Text = "Validé";
                    estValide.ForeColor = Color.DarkGreen;
                }
                createdAt.Text = patient.createdAt.ToShortDateString();
                updatedAt.Text = patient.updatedAt.ToShortDateString();

            }
        }
        public void resetDatagridView()
        {
            datagriddocv.Rows.Clear();
            foreach (var value in ldv)
            {
                datagriddocv.Rows.Add(value._id, value.firstname + " " + value.lastname, value.email);
            }
            if (datagriddocv != null)
            {
                datagriddocv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                datagriddocv.Columns[datagriddocv.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }
        public void resetDatagridView1()
        {
            datagridviewdocnv.Rows.Clear();
            foreach (var value in ldnv)
            {
                datagridviewdocnv.Rows.Add(value._id, value.firstname + " " + value.lastname, value.email);
            }
            if (datagridviewdocnv != null)
            {
                datagridviewdocnv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                datagridviewdocnv.Columns[datagridviewdocnv.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }
        private void corpsMedicaux_Load(object sender, EventArgs e)
        {
            resetDatagridView1();
            resetDatagridView();
        }

        private void datagridviewdocnv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string docId = datagridviewdocnv.Rows[e.RowIndex].Cells[0].Value.ToString();


            if (docId != null)
            {

                Medecin patient = new MedecinDAO().findById(docId);
                nom.Text = patient.firstname;
                prenom.Text = patient.lastname;
                email.Text = patient.email;
                telephone.Text = patient.telephone;
                region.Text = patient.town;
                adresse.Text = patient.address;
                city.Text = patient.city;
                description.Text = patient.description;
                estValide.Text = patient.etatValidation;
                if (patient.etatValidation == "0")
                {
                    estValide.Text = "En attente de validiation";
                    estValide.ForeColor = Color.Red;
                }
                else
                {
                    estValide.Text = "Validé";
                    estValide.ForeColor = Color.ForestGreen;
                }
                createdAt.Text = patient.createdAt.ToShortDateString();
                updatedAt.Text = patient.updatedAt.ToShortDateString();

            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            affichageQRCode acc = new affichageQRCode();
            acc.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            accueil acc = new accueil();
            acc.Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            this.Hide();
            accueil acc = new accueil();
            acc.Show();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            login acc = new login();
            acc.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (search.Text != "")
            {
                ldv = new MedecinDAO().searchOk(search.Text);
                ldnv = new MedecinDAO().searchKO(search.Text);
                resetDatagridView();
                resetDatagridView1();
            }
            else
            {
                ldv = new MedecinDAO().getListDoctorValide();
                ldnv = new MedecinDAO().getListDoctorNonValide();
                resetDatagridView1();
                resetDatagridView();
            }
        }
    }
}
