﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientLourdDocFinder
{
    public partial class rendezVous : Form
    {
        string lastNamePatient;
        string firstNamePatient;
        string gender;
        string appointmentDate;

        string idpatient;
        string idDoctor;
        string dateR;
        string heureR;
        string _id;
        List<Appointment> lp;
        public rendezVous()
        {
            InitializeComponent();
            this.validation.Items.AddRange(new object[] {"1",
                        "0",
                        "-1",
                        });
            lastNamePatient = "";
            firstNamePatient = "";
            gender = "";
            appointmentDate = "";

            idpatient = "";
            idDoctor = "";
            dateR = "";
            heureR = "";
            _id = "";


            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font(dataGridView1.Font, FontStyle.Bold);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.BlueViolet;
            dataGridView1.DefaultCellStyle.BackColor = Color.Bisque;
            dataGridView1.Columns.Add("lastNamePatient", "lastNamePatient");
            dataGridView1.Columns.Add("firstNamePatient", "firstNamePatient");
            dataGridView1.Columns.Add("gender", "gender");
            dataGridView1.Columns.Add("datetime", "datetime");
            dataGridView1.Columns.Add("IdPat", "IdPat");
            dataGridView1.Columns.Add("IdDoc", "IdDoc");
            dataGridView1.Columns.Add("Médecin", "Médecin");
            dataGridView1.Columns.Add("Nom", "Nom");
            dataGridView1.Columns.Add("Prénom(s)", "Prénom(s)");
            dataGridView1.Columns.Add("Date de RDV", "Date de RDV");
            dataGridView1.Columns.Add("Heure", "Heure");
            dataGridView1.Columns.Add("Validation", "Validation");
            dataGridView1.Columns.Add("Id", "Id");

            dataGridView1.Columns["Id"].Visible = false;
            dataGridView1.Columns["IdPat"].Visible = false;
            dataGridView1.Columns["lastNamePatient"].Visible = false;
            dataGridView1.Columns["firstNamePatient"].Visible = false;
            dataGridView1.Columns["datetime"].Visible = false;
            dataGridView1.Columns["gender"].Visible = false;
            dataGridView1.Columns["IdDoc"].Visible = false;
            lp = new AppointmentDAO().getListRDV();

        }
        public  void resetDatagridView1()
        {
            dataGridView1.Rows.Clear();

          

                for (int i = 0; i < lp.Count; i++)
                {
                    Medecin lm = new MedecinDAO().findById(lp[i].idDoctor);
                    string validation = "";
                    if (lp[i].validation == "1")
                    {
                        validation = "ok";

                    }
                    if (lp[i].validation == "0")
                    {
                        validation = "En attente de validation";

                    }
                    if (lp[i].validation == "-1")
                    {
                        validation = "Réfusé";

                    }
                  
                        dataGridView1.Rows.Add(lp[i].lastNamePatient, lp[i].firstNamePatient, lp[i].gender, lp[i].appointmentDate, lp[i].idPatient, lp[i].idDoctor, lm.firstname + ' ' + lm.lastname, lp[i].firstNamePatient, lp[i].lastNamePatient, lp[i].appointmentDate.ToLongDateString(), lp[i].appointmentDate.ToString("hh:mm"), validation, lp[i]._id);
                 




                }
        
            if (dataGridView1 != null)
            {
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.Columns[dataGridView1.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            /*if(dataGridView1.Rows[11].Cells["Validation"].Value.ToString() == "ok")
            {
                dataGridView1.DefaultCellStyle.ForeColor = Color.ForestGreen;
            }
           */
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToString(row.Cells["Validation"].Value) == "ok")
                {
                    row.DefaultCellStyle.BackColor = Color.FromArgb(202, 245, 162);
                    //row.Cells[11].ForeColor = Color.Gray;
                }
                else if (Convert.ToString(row.Cells["Validation"].Value) == "En attente de validation")
                {
                    row.DefaultCellStyle.BackColor = Color.FromArgb(247, 204, 148);
                }
                else if (Convert.ToString(row.Cells["Validation"].Value) == "Réfusé")
                {
                    row.DefaultCellStyle.BackColor = Color.FromArgb(250, 151, 142);
                }
                else
                {
                    row.DefaultCellStyle.BackColor = Color.White;
                }
            }

        }

        public async void resetDatagridView()
        {
            dataGridView1.Rows.Clear();
         
            await Task.Run(() =>
            {
              
                for (int i = 0; i < lp.Count; i++)
                {
                    Medecin lm = new MedecinDAO().findById(lp[i].idDoctor);
                    string validation = "";
                    if (lp[i].validation == "1")
                    {
                        validation = "ok";
                        
                    }
                    if (lp[i].validation == "0")
                    {
                        validation = "En attente de validation";
                       
                    }
                    if (lp[i].validation == "-1")
                    {
                        validation = "Réfusé";
                       
                    }
                    dataGridView1.Invoke(new MethodInvoker(delegate
                    {
                        dataGridView1.Rows.Add(lp[i].lastNamePatient, lp[i].firstNamePatient, lp[i].gender, lp[i].appointmentDate, lp[i].idPatient, lp[i].idDoctor, lm.firstname + ' ' + lm.lastname, lp[i].firstNamePatient, lp[i].lastNamePatient, lp[i].appointmentDate.ToLongDateString(), lp[i].appointmentDate.ToString("hh:mm"), validation, lp[i]._id);
                    }));

                


                }
            });
            if (dataGridView1 != null)
                {
                    dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    dataGridView1.Columns[dataGridView1.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                /*if(dataGridView1.Rows[11].Cells["Validation"].Value.ToString() == "ok")
                {
                    dataGridView1.DefaultCellStyle.ForeColor = Color.ForestGreen;
                }
               */
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (Convert.ToString(row.Cells["Validation"].Value) == "ok")
                    {
                        row.DefaultCellStyle.BackColor = Color.FromArgb(202, 245, 162);
                        //row.Cells[11].ForeColor = Color.Gray;
                    }
                    else if (Convert.ToString(row.Cells["Validation"].Value) == "En attente de validation")
                    {
                        row.DefaultCellStyle.BackColor = Color.FromArgb(247, 204, 148);
                    }
                    else if (Convert.ToString(row.Cells["Validation"].Value) == "Réfusé")
                    {
                        row.DefaultCellStyle.BackColor = Color.FromArgb(250, 151, 142);
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                    }
                }
           
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            new accueil().Show();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            new login().Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rendezVous_Load(object sender, EventArgs e)
        {
            resetDatagridView();
            if (search.Text != "")
            {
                lp = new AppointmentDAO().searchAll(search.Text);
                resetDatagridView();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                lastNamePatient = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                firstNamePatient = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                gender = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
                appointmentDate = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                string medecinR = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
                idpatient = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
                idDoctor = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
                string estValideR = dataGridView1.Rows[e.RowIndex].Cells[11].Value.ToString();
                dateR = dataGridView1.Rows[e.RowIndex].Cells[9].Value.ToString();
                heureR = dataGridView1.Rows[e.RowIndex].Cells[10].Value.ToString();
                _id = dataGridView1.Rows[e.RowIndex].Cells[12].Value.ToString();
                string val = "";
                Patient patient = new PatientDAO().findById(idpatient);
                int ageP = DateTime.Now.Year - patient.birthdate.Year;
                nom.Text = patient.firstname + " " + patient.lastname;
                tel.Text = patient.telephone;
                email.Text = patient.email;
                dateT.Text = appointmentDate;
                daty.Text = dateR;
                heure.Text = heureR;
                medecin.Text = medecinR;

                age.Text = ageP + " ans";

                if (estValideR == "En attente de validation")
                {
                    val = "0";
                }
                if (estValideR == "ok")
                {
                    val = "1";
                }
                if (estValideR == "Réfusé")
                {
                    val = "-1";
                }
                validation.Text = val;
            }
            catch (Exception ex)
            {
                MessageBox.Show("DocFinder indique: "+ex);
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(validation.Text=="" || dateT.Text == "")
            {
                MessageBox.Show("Veuillez d'abord cliquer dans le tableau la  ligne que vous voulez modifier avant d'effectuer une modification");
            }
            else
            {
                DateTime date = DateTime.Parse(dateT.Text);
                new AppointmentDAO().update(_id, idDoctor, idpatient, lastNamePatient, firstNamePatient, gender, date, validation.Text);
                resetDatagridView1();
            }
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            new plannificationRDV().Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (search.Text != "")
            {
                lp = new AppointmentDAO().searchAll(search.Text);
                resetDatagridView1();
            }
            else
            {
                lp = new AppointmentDAO().getListRDV();
                resetDatagridView1();
            }
           
        }
    }
}
