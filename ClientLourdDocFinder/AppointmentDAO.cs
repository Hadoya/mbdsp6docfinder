﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ClientLourdDocFinder
{
    class AppointmentDAO
    {
        public List<Appointment> getListRDV()
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/appointment/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "7aef4142-48ec-c24e-c7b9-c397ce5ccdc1");
            request.AddHeader("cache-control", "no-cache");
            var response = client.Execute<List<Appointment>>(request);
            if (response.Data == null)
            {
                MessageBox.Show(response.ErrorMessage);
            }
            return response.Data;
            
        }
        public List<Appointment> searchAll(string wordToSearch)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/appointment/searchAll/");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "c43057be-c6de-ff87-a102-f0bb860ed7ad");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"wordToSearch\": " + "\"" + wordToSearch + "\"" + "}", ParameterType.RequestBody);
            var response = client.Execute<List<Appointment>>(request);
            if (response.Data == null)
            {
                MessageBox.Show(response.ErrorMessage);
            }
            return response.Data;

        }
        public List<Appointment> searchByIdPatient(string idpatient)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/appointment/searchByIdpatient/"+idpatient);
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "bd574ae5-4de0-7780-883a-063b273e0fb7");
            request.AddHeader("cache-control", "no-cache");
            var response = client.Execute<List<Appointment>>(request);
            if (response.Data == null)
            {
                MessageBox.Show(response.ErrorMessage);
            }
            return response.Data;

        }
        public List<Appointment> searchByDate(string date)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/appointment/searchByDate");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "828a87a0-680a-776e-07e8-c46322afbf14");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"appointmentDateString\":  " + "\"" + date+ "\"" + "}", ParameterType.RequestBody);
            var response = client.Execute<List<Appointment>>(request);
            if (response.Data == null)
            {
                MessageBox.Show(response.ErrorMessage);
            }
            return response.Data;

        }
        public void update(String _id,String idDoctor, String idPatient, String lastNamePatient, String firstNamePatient, String gender, DateTime appointmentDate, String validation)
        {
           // string resultat = '';
            var client = new RestClient("https://docfinder-backend.herokuapp.com/appointment/update/"+_id);
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "45885bc6-34de-7ce3-88f2-41d172eb8c81");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"idDoctor\": " + "\"" + idDoctor.ToString() + "\"" + ",\"idPatient\": " + "\"" + idPatient.ToString() + "\"" + ",\"lastNamePatient\": " + "\"" + lastNamePatient.ToString() + "\"" + ",\"firstNamePatient\": " + "\"" + firstNamePatient.ToString() + "\"" + ",\"gender\":" + "\"" + gender.ToString() + "\"" + ",\n\"appointmentDate\": " + "\"" + appointmentDate.ToString("yyyy-MM-dd\"T\"HH:mm:ss.fffZ") + "\"" + ",\"validation\": " + "\"" + validation.ToString() + "\"" + "}", ParameterType.RequestBody);
            var response = client.Execute(request);
            if (response == null)
            {
                 MessageBox.Show("Modification échouée");
            }
            else
            {
                Console.WriteLine("{\"idDoctor\": " + "\"" + idDoctor.ToString() + "\"" + ",\"idPatient\": " + "\"" + idPatient.ToString() + "\"" + ",\"lastNamePatient\": " + "\"" + lastNamePatient.ToString() + "\"" + ",\"firstNamePatient\": " + "\"" + firstNamePatient.ToString() + "\"" + ",\"gender\":" + "\"" + gender.ToString() + "\"" + ",\n\"appointmentDate\": " + "\"" + appointmentDate.ToString("yyyy-MM-dd\"T\"HH:mm:ss.fffZ") + "\"" + ",\"validation\": " + "\"" + validation.ToString() + "\"" + "}");
                MessageBox.Show("Modification terminée ");
            }
            
        }
    }
}
