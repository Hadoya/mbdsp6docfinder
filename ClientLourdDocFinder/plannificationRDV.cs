﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ClientLourdDocFinder
{
    public partial class plannificationRDV : Form
    {
        DateTime date;
        public plannificationRDV()
        {
            InitializeComponent();
            
            date = new DateTime();
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font(dataGridView1.Font, FontStyle.Bold);
       
            dataGridView1.DefaultCellStyle.BackColor = Color.Bisque;
       
            dataGridView1.Columns.Add("Nom", "Nom");
            dataGridView1.Columns.Add("Heure", "Heure");
            dataGridView1.Columns.Add("validation", "validation");
            dataGridView1.Columns.Add("idpatient", "idpatient");
            dataGridView1.Columns.Add("iddoc", "iddoc");
            dataGridView1.Columns["idpatient"].Visible = false;
            dataGridView1.Columns["iddoc"].Visible = false;

            if (monthCalendar1.Created)
            {
                Application.VisualStyleState = VisualStyleState.NoneEnabled;
                this.Invalidate(true);
            }
        
            List<Appointment> lp = new AppointmentDAO().getListRDV();
            for (int i = 0; i < lp.Count; i++)
            {
                DateTime date = lp[i].appointmentDate;
                monthCalendar1.AddBoldedDate(date);
                monthCalendar1.TitleBackColor = Color.FromArgb(32, 132, 245);
                monthCalendar1.TitleForeColor = Color.Wheat;
                this.monthCalendar1.ShowToday = false;
                // monthCalendar1.SelectionStart = date;
                this.monthCalendar1.ShowWeekNumbers = false;


            }


            /* if (VisualStyleInformation.IsSupportedByOS && VisualStyleInformation.IsEnabledByUser)
             {
                new VisualStyleRenderer(
                     VisualStyleElement.Button.PushButton.Disabled);
             }
             // monthCalendar1.AddBoldedDate(myVacation1);
             //monthCalendar1.AddBoldedDate(myVacation1);*/
        }

        private void plannificationRDV_Load(object sender, EventArgs e)
        {
           
           
           // this.monthCalendar1.Style = System.Windows.Forms.VisualStyle.Office2016DarkGray;
           
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            new accueil().Show();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            new login().Show();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            //monthCalendar1.AddAnnuallyBoldedDate(date);
            dataGridView1.Rows.Clear();
            string validation = "";
            string date = monthCalendar1.SelectionRange.Start.ToString("yyyy-MM-dd");
            List<Appointment> lp = new AppointmentDAO().searchByDate(date);
            for (int i = 0; i < lp.Count; i++)
            {
                if (lp[i].validation == "1")
                {
                    validation = "ok";

                }
                if (lp[i].validation == "0")
                {
                    validation = "En attente de validation";

                }
                if (lp[i].validation == "-1")
                {
                    validation = "Réfusé";

                }
                dataGridView1.Rows.Add(lp[i].firstNamePatient + " " + lp[i].lastNamePatient, lp[i].appointmentDate.ToString("HH:ss"), validation, lp[i].idPatient, lp[i].idDoctor);
            }
            if (dataGridView1 != null)
            {
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.Columns[dataGridView1.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            label3.Text = date;
           
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToString(row.Cells["Validation"].Value) == "ok")
                {
                    row.DefaultCellStyle.BackColor = Color.FromArgb(202, 245, 162);
                    //row.Cells[11].ForeColor = Color.Gray;
                }
                else if (Convert.ToString(row.Cells["Validation"].Value) == "En attente de validation")
                {
                    row.DefaultCellStyle.BackColor = Color.FromArgb(247, 204, 148);
                }
                else if (Convert.ToString(row.Cells["Validation"].Value) == "Réfusé")
                {
                    row.DefaultCellStyle.BackColor = Color.FromArgb(250, 151, 142);
                }
                else
                {
                    row.DefaultCellStyle.BackColor = Color.White;
                }
            }

            // MessageBox.Show("date=" + date);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
           
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
            monthCalendar1.TodayDate = new DateTime(2013, 4, DateTime.Now.Day);
             monthCalendar1.Refresh();

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dataGridView1_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
        {
            string idPatient = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            string idDoctor = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            string heureR = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            Patient patient = new PatientDAO().findById(idPatient);
            int ageP = DateTime.Now.Year - patient.birthdate.Year;
            nom.Text = patient.firstname;
            tel.Text = patient.telephone;
            email.Text = patient.email;
            daty.Text = label3.Text;
            heure.Text = heureR;
            prenom.Text = patient.lastname;
            Medecin med = new MedecinDAO().findById(idDoctor);
            medecin.Text = med.firstname + "" + med.lastname;

            age.Text = ageP + " ans";
        }
    }
}
