﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLourdDocFinder
{
    class MedecinDAO
    {
        public Medecin findById(String id)
        {
    
            var client = new RestClient("https://docfinder-backend.herokuapp.com/doctor/"+id);
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "3a03ac69-bda8-24cb-b494-0dc21d192267");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
    
            var response = client.Execute<Medecin>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response.Data;
        }
        public List<Medecin> getListDoctorValide()
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/doctor/okdoctors");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "af37330e-2861-74de-7997-8558d0532547");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            var response = client.Execute<List<Medecin>>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
           
            return response.Data;

        }
        public List<Medecin> searchOk(String wordToSearch)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/doctor/okdoctorssearch");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "5d6e1cec-b820-5e6e-487e-7e0ff6cd6ff4");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"wordToSearch\": " + "\"" + wordToSearch + "\"" + "}", ParameterType.RequestBody);
            var response = client.Execute<List<Medecin>>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }

            return response.Data;

        }
        public List<Medecin> searchKO(String wordToSearch)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/doctor/nokdoctorssearch");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "acc0fa2b-4fef-a3a8-5634-c26df3401a32");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"wordToSearch\": " + "\"" + wordToSearch + "\"" + "}", ParameterType.RequestBody);
            var response = client.Execute<List<Medecin>>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }

            return response.Data;

        }
        public List<Medecin> getListDoctorNonValide()
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/doctor/nokdoctors");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "0457fd0a-b8a5-628f-b840-4d9d678b8c2e");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            var response = client.Execute<List<Medecin>>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }

            return response.Data;

        }

    }
}
