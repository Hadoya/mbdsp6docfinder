﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLourdDocFinder
{
    public class Patient
    {


        public string _id { get; set; }
        public string firstname { get; set; }
            public string lastname { get; set; }
        public string email { get; set; }
        public string estPatient { get; set; }
        public string gender { get; set; }
        public string telephone { get; set; }
        public DateTime birthdate { get; set; }

    }
}
