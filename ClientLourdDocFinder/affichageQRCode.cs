﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using ZXing;

namespace ClientLourdDocFinder
{
    public partial class affichageQRCode : Form
    {
        public affichageQRCode()
        {
            InitializeComponent();
        }
        FilterInfoCollection filterInfoCollection;
        VideoCaptureDevice captureDevice;
        private void textQRCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void affichageQRCode_Load(object sender, EventArgs e)
        {
            filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo filterInfo in filterInfoCollection)
            cboDevice.Items.Add(filterInfo.Name);
            cboDevice.SelectedIndex = 0;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            captureDevice = new VideoCaptureDevice(filterInfoCollection[cboDevice.SelectedIndex].MonikerString);
            captureDevice.NewFrame += CaptureDevice_NewFrame;
            captureDevice.Start();
            timer1.Start();

        }

        private void CaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
        }

        private void affichageQRCode_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (captureDevice.IsRunning)
                captureDevice.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                BarcodeReader Reader = new BarcodeReader();
                Result result = Reader.Decode((Bitmap)pictureBox1.Image);
                if (result != null)
                {
                    textQRCode.Text = result.ToString();
                    textQRCode.ForeColor = Color.ForestGreen;
                    Patient patient = new PatientDAO().findById(result.ToString());
                    if (patient != null)
                    {
                        nom.Text = patient.firstname;
                        prenom.Text = patient.lastname;
                        email.Text = patient.email;
                        telephone.Text = patient.telephone;
                        datenaissance.Text = patient.birthdate.ToShortDateString();
                        if (patient.gender == "m")
                        {
                            sexe.Text ="Homme";
                        }
                        else
                        {
                            sexe.Text = "Femme";
                        }
                        
                    }
                    timer1.Stop();
                    if (captureDevice.IsRunning)
                        captureDevice.Stop();
                }
                else
                {
                    textQRCode.Text = "QRCode non visible. Veuillez essayer encore!";
                    textQRCode.ForeColor = Color.Red;
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            accueil acc = new accueil();
            acc.Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            accueil acc = new accueil();
            acc.Show();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            login acc = new login();
            acc.Show();
        }
    }
}
