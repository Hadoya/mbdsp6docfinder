﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace ClientLourdDocFinder
{
    class UserAdminDAO
    {
        public UserAdmin login(String username,String password)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/userAdmin/login");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "8b768df6-f4cd-b303-6736-a6579dd56a2d");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            //request.AddParameter("application/json", " {\n\t\"username\": \"andrea\",\n\t\"password\": \"andrea\"\n }\n  ", ParameterType.RequestBody);
            request.AddParameter("application/json", "{\"username\": " + "\"" + username.ToString() + "\"" + ",\"password\": " + "\"" + password.ToString() + "\"" + "}", ParameterType.RequestBody);
            var response = client.Execute<UserAdmin>(request);
            if (response.Data == null)
            {
                Console.WriteLine("{\"username\": "+"\""+ username.ToString() + "\"" + ",\"password\": " + "\""+password.ToString()+ "\"" + "}");
                throw new Exception(response.ErrorMessage);
            }
       
            return response.Data;

        }
    }
}
