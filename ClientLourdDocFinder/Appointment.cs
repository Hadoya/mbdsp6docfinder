﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLourdDocFinder
{
    class Appointment
    {
        public string _id { get; set; }
        public string idDoctor { get; set; }
        public string idPatient { get; set; }
        public string lastNamePatient { get; set; }
        public string firstNamePatient { get; set; }
        public string gender { get; set; }
        public DateTime appointmentDate { get; set; }
        public string validation { get; set; }
    }
}
