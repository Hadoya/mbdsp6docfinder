﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ClientLourdDocFinder
{
    public partial class accueil : Form
    {
        public accueil()
        {
            InitializeComponent();
           
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            plannificationRDV pl = new plannificationRDV();
            pl.Show();
            this.Hide();
        }

        private void accueil_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            patients p = new patients();
            p.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
            rendezVous rd = new rendezVous();
            rd.Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           
            new login().Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
            affichageQRCode af = new affichageQRCode();
            af.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
           
            GenerateQRCode af = new GenerateQRCode();
            af.Show();
            this.Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
           
            corpsMedicaux af = new corpsMedicaux();
            af.Show();
            this.Hide();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
