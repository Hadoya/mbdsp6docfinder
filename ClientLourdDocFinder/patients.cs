﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Newtonsoft.Json;
using QRCoder;
using System.Drawing.Imaging;
using Excel = Microsoft.Office.Interop.Excel;

namespace ClientLourdDocFinder
{
    public partial class patients : Form
    {
        List<Patient> lp;
        public patients()
        {
            InitializeComponent();
             lp = new PatientDAO().getListPatient();

            datagridpatient.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            datagridpatient.ColumnHeadersDefaultCellStyle.Font = new Font(datagridpatient.Font, FontStyle.Bold);
            datagridpatient.ColumnHeadersDefaultCellStyle.BackColor = Color.BlueViolet;
            datagridpatient.DefaultCellStyle.BackColor = Color.Bisque;
            datagridpatient.Columns.Add("PatientId", "PatientId");
            datagridpatient.Columns.Add("Nom", "Nom");
            datagridpatient.Columns.Add("Prénom(s)", "Prénom(s)");
            datagridpatient.Columns.Add("Email", "Email");
            datagridpatient.Columns.Add("gnr", "Générer");
           
          

        }
        public void resetDatagridView()
        {
            datagridpatient.Rows.Clear();
            foreach (var value in lp)
            {

                datagridpatient.Rows.Add(value._id, value.firstname, value.lastname, value.email, "QRCode");


            }
            if (datagridpatient != null)
            {
                datagridpatient.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                datagridpatient.Columns[datagridpatient.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           
            new accueil().Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
          
            new login().Show();
            this.Hide();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void patients_Load(object sender, EventArgs e)
        {
            resetDatagridView();
        }

        private void datagridpatient_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string patientId = datagridpatient.Rows[e.RowIndex].Cells[0].Value.ToString();
            string generer = datagridpatient.Rows[e.RowIndex].Cells[4].Value.ToString();
            datagridpatient.CurrentCell.Style.BackColor = Color.Wheat;

            if (generer != null)
            {
                label6.Text = "pour le patient " + patientId;
                QRCodeGenerator qr = new QRCodeGenerator();
                QRCodeData data = qr.CreateQrCode(patientId, QRCodeGenerator.ECCLevel.Q);
                QRCode code = new QRCode(data);
                pictureBox2.Image = code.GetGraphic(5);

                //Remplir le panel détail client
                Patient patient = new PatientDAO().findById(patientId);
                nom.Text = patient.firstname;
                prenom.Text = patient.lastname;
                email.Text = patient.email;
                telephone.Text = patient.telephone;
                datenaissance.Text = patient.birthdate.ToShortDateString();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Images|*.png;*.bmp;*.jpg";
            ImageFormat format = ImageFormat.Png;
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                pictureBox2.Image.Save(sfd.FileName, format);
                MessageBox.Show("Image de QRCode enregistré");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            affichageQRCode acc = new affichageQRCode();
            acc.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
                datagridpatient.Rows.Clear();
            datagridpatient.Columns.Clear();
            List<Patient> lp = new PatientDAO().getListPatient();
            datagridpatient.Columns.Add("Nom", "Nom");
            datagridpatient.Columns.Add("Prénom(s)", "Prénom(s)");
            datagridpatient.Columns.Add("Email", "Email");
            datagridpatient.Columns.Add("Tel", "Tél");
            datagridpatient.Columns.Add("Date de naissance", "Date de naissance");

            foreach (var value in lp)
            {

                datagridpatient.Rows.Add( value.firstname, value.lastname, value.email, value.telephone, value.birthdate);

            }
            // creating Excel Application  
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            // creating new WorkBook within Excel application  
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            // creating new Excelsheet in workbook  
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            // see the excel sheet behind the program  
            app.Visible = true;
            // get the reference of first sheet. By default its name is Sheet1.  
            // store its reference to worksheet  
            
            worksheet = workbook.ActiveSheet;
            // changing the name of active sheet  
            worksheet.Name = "DocFinder_Liste des patients";
            // storing header part in Excel  
            for (int i = 1; i < datagridpatient.Columns.Count + 1; i++)
            {
                worksheet.Cells[1, i] = datagridpatient.Columns[i - 1].HeaderText;
            }
            // storing Each row and column value to excel sheet  
            for (int i = 0; i < datagridpatient.Rows.Count - 1; i++)
            {
                for (int j = 0; j < datagridpatient.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = datagridpatient.Rows[i].Cells[j].Value.ToString();
                }
            }
            // save the application  
            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = ".xlsx Files (*.xlsx)|*.xlsx",
                OverwritePrompt = true
            };

            /* ImageFormat format = ImageFormat.Png;
             if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
             {
                 string ext = System.IO.Path.GetExtension(sfd.FileName);

                 workbook.SaveAs(sfd.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                 // Exit from the application  
                 app.Quit();

                 MessageBox.Show("Fichier exporté");
             }
             */
            datagridpatient.Rows.Clear();
            datagridpatient.Columns.Clear();
            datagridpatient.Columns.Add("PatientId", "PatientId");
            datagridpatient.Columns.Add("Nom", "Nom");
            datagridpatient.Columns.Add("Prénom(s)", "Prénom(s)");
            datagridpatient.Columns.Add("Email", "Email");
            datagridpatient.Columns.Add("gnr", "Générer");
            foreach (var value in lp)
            {

                datagridpatient.Rows.Add(value._id, value.firstname, value.lastname, value.email, "QRCode");

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
           
            affichageQRCode acc = new affichageQRCode();
            acc.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
           
            accueil acc = new accueil();
            acc.Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (search.Text != "")
            {
                lp = new PatientDAO().searchfrombo(search.Text);
                resetDatagridView();
               // datagridpatient.Refresh();
            }
            else
            {
                lp = new PatientDAO().getListPatient();
                 resetDatagridView();
               // datagridpatient.Refresh();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
    
}
