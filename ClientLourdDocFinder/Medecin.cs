﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLourdDocFinder
{
    class Medecin
    {
        public string _id { get; set; }
        public string lastname { get; set; }

          public string firstname { get; set;}
          public string idDoctorSpeciality { get; set; }
         public string telephone { get; set; }
         public string address { get; set; }
         public string town { get; set; }
         public string city { get; set; }
         public string description { get; set; }
         public string tarif { get; set; }
         public string etatValidation{ get; set; }
         public string password { get; set; }
         public string email { get; set; }
         public string idPatient{ get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }

    }
}
