﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Runtime.Serialization.Json;

namespace ClientLourdDocFinder
{
    class PatientDAO
    {
        public String lastname;
        public String firstname;
        public String email;
        public String password;
        public String estPatient;
        public PatientDAO()
        {
            //last = lastName;
            //first = firstName;
        }
        public Patient findById(String id)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/patient/"+id);
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "a95684f2-bbdd-7c22-0a48-4ca258f4ae74");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
           var response = client.Execute<Patient>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
            // Console.WriteLine(response.Data.Count);
            return response.Data;
        }
        public List<Patient> getListPatient()
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/patient");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "7260ae28-cb21-4df5-3ba4-6fd3b06e1daf");
            request.AddHeader("cache-control", "no-cache");
         
            var response = client.Execute<List<Patient>>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
           
            return response.Data;
        }
        public List<Patient> searchfrombo(string wordToSearch)
        {
            var client = new RestClient("https://docfinder-backend.herokuapp.com/patient/searchfrombo");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "af7f3f4b-5d7f-0c8a-3207-f7bbf799ce1b");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"wordToSearch\": " + "\"" + wordToSearch + "\"" + "}", ParameterType.RequestBody);

            var response = client.Execute<List<Patient>>(request);
            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }

            return response.Data;
        }




    }
}
